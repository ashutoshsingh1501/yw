<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = array();
$config['home'] = 'yellowpages.in';
$config['api'] = 'api.your-domain.com';
$config['shop'] = 'cms.yellowpages.in';
/*
	Define the SITE constant.
*/
foreach ($config as $site => $host)
	if ($_SERVER['HTTP_HOST'] === $host)
	{
		define('SITE', $site);
		break;
	}
/* End of file hosts.php */
/* Location: ./application/config/hosts.php */