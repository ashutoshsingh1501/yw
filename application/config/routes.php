<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |   example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |   http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |   $route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |   $route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

function whichSubRoute() {
    $subs = array(
        "cms" => "cms/",
        "api" => "api/"
    );

    $curr = $_SERVER['HTTP_HOST'];
    $curr = explode('.', $curr);
    if (array_key_exists($curr[0], $subs)) {
        return array($curr[0], $subs[$curr[0]]);
    }
    return false;
}

//due to the the way this setup works, some controller references
//can be found multiple times (and in no particular order).
//also note due to this setup, each method has its own default and 404
$choiceRoute = whichSubRoute();
if ($choiceRoute !== false) {
    if ($choiceRoute[0] == "cms") {
        $route['dashboard'] = 'dashboard';
        $route['default_controller'] = "dashboard";
        $route['404_override'] = '';
        //start version 1 (mvp API)
        $route['1.0/user/(:any)'] = $choiceRoute[1] . 'v1_userinfo/index/$1';
        //controllers outside of "/api"
    }
    if ($choiceRoute[0] == "api") {
        $route['default_controller'] = "welcome";
        $route['404_override'] = '';
        //start version 1 (mobile)
        $route['welcome'] = $choiceRoute[1] . 'm_welcome';
        $route['dashboard'] = $choiceRoute[1] . 'm_dashboard';
        $route['user/(:any)'] = $choiceRoute[1] . 'm_userinfo/index/$1';
        $route['reg'] = //controllers outside of "/m"
                $route['login/auth'] = 'login/auth';
        $route['logout/mobile'] = 'logout/mobile';
        //end version 1 (mobile)
    }
} else {
    $route['default_controller'] = "welcome";
    $route['404_override'] = '';
    
    $route['add-business'] = 'add_business';
    $route['add-business/validate-business-details'] = 'add_business/validate_business_details';
    $route['advertise-with-us'] = 'advertise_with_us';
    $route['advertise-inquiry'] = 'advertise_with_us/advertise_with_us_mail';

    $route['login'] = 'login/signin';
    $route['register'] = 'myaccount/register';
    $route['my-favorites'] = 'myaccount';
    $route['my-account'] = 'myaccount';
    $route['my-reviews'] = 'myaccount/myreviews';
    $route['change-password'] = 'myaccount/change_password';
    $route['sign-out'] = 'login/logout';

    $route['about-us'] = 'info/about_us';
    $route['contact-us'] = 'info/contact_us';
    $route['send-inquiry'] = '/info/contact_us_mail';
    $route['feedback'] = 'info/feedback';
    $route['submit-feedback'] = '/info/feedback_mail';
    $route['frequently-asked-questions'] = 'info/faqs';
    $route['careers'] = 'info/careers';
    $route['terms-and-conditions'] = 'info/terms_conditions';
    $route['privacy-policy'] = 'info/privacy_policy';
    $route['end-user-agreement'] = 'info/eua';
    
    $route['categories'] = 'categories';

    $route['search-map'] = 'search_map';
    
    $route['suggest-edit'] = 'business_detail/suggestEdit';
    $route['report-business'] = 'business_detail/report_business';
    $route['mail-coupon'] = 'business_detail/mailCoupon';
    $route['sendsms'] = 'business_detail/sendSms';
    
    //business-detail-page-routing
    //yp.in/b/business-name/business-id
    $route['b/(:any)/(:num)'] = 'business_detail/businessByNameID/$1/$2';
    
    //yp.in/b/city-name/business-name/business-id
//	$route['b/(:any)/(:any)/(:any)'] = 'business_detail/businessByCityNameID/$1/$2/$3';
	
    //yp.in/b/city-name/area-name/business-name/business-id
//	$route['b/(:any)/(:any)/(:any)/(:any)'] = 'business_detail/businessByCityAreaNameID/$1/$2/$3/$4';
	
    //yp.in/b/business-name-city-name-area-name/business-id   // :TODO
//	$route['b/(:any)/(:num)'] = 'business_detail/businessByCityAreaNameID/$1/$2/$3/$4';
    
    
    //search-result page routing
    //yp.in/s?q=k1+k2+k2+.....+kn
    //$route['s/']='search_result';
    
    //category-list-page-routing
    //yp.in/category-name/id
    $route['([a-zA-Z0-9\-]+)/(:num)'] = 'search_category/getCatByNameId/$2/$1';
    $route['([a-zA-Z0-9\-]+)/(:num)/(:num)'] = 'search_category/getCatByNameId/$2/$1';
    
    //yp.in/city-name/category-name/id
    $route['([a-zA-Z0-9\-]+)/([a-zA-Z\-]+)/(:num)'] = 'search_category/getCatByCityNameId/$3/$2/$1';
    $route['([a-zA-Z0-9\-]+)/([a-zA-Z\-]+)/(:num)/(:num)'] = 'search_category/getCatByCityNameId/$3/$2/$1';
    
    //yp.in/city-name/area-name/ategory-name/id
    $route['([a-zA-Z0-9\-]+)/([a-zA-Z\-]+)/([a-zA-Z\-]+)/(:num)'] = 'search_category/getCatByCityAreaNameId/$4/$3/$2/$1';
    $route['([a-zA-Z0-9\-]+)/([a-zA-Z\-]+)/([a-zA-Z\-]+)/(:num)/(:num)'] = 'search_category/getCatByCityAreaNameId/$4/$3/$2/$1';
    
}
/* End of file routes.php */
/* Location: ./application/config/routes.php */

