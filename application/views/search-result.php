<div class="mobileHeader">
    <div class="mobileMenu hasIcons"><?php echo $msg; ?></div>
    <div class="mobileHeadIcons">
        <button class="mobSearchBtn">Search</button>
        <?php /*<button class="mobLocation">Location</button>*/?>
    </div>
</div>

<div class="container resultsMapBg greyBg">
    <div class="wrapper">
        <div class="resultsBlock">
            <?php /*if (!empty($businesses)) { ?>
                <div class="sortBlock">
                    <div class="sortSelectBox select">
                        <span class="mobileSortText">Sort Results</span>
                        <span class="selectText">Sort results</span>
                        <select name="" id="" class="selectBox">
                            <option value="1">Price Low to High</option>
                            <option value="2">Price High to Low</option>
                            <option value="3">Most Relavant</option>
                            <option value="4">Most Popular</option>
                            <option value="5">New Release</option>
                        </select>
                    </div>
                </div>
            <?php } */?>
            <div class="resultsText"><?php echo $msg; ?></div>
        </div>
    </div>
</div>
<div class="container mainContent greyBg">
    <div class="wrapper">
        <section class="leftBigCol">
            <?php if (!empty($businesses)) { ?>
                <?php if (!empty($filters)) { ?>
                    <div class="filtersBlock">
                        <div class="filtersFullBlock">
                            <form>
                                <?php
                                //echo "<pre>";print_r($filters);exit; 	

                                foreach ($filters as $key => $filter) {
                                    ?>
                                    <div class="filtersEachCol">
                                        <div class="filtersEachColHeading"><?php echo $filter->display_name; ?></div>
                                        <div class="filtersEachColListBlock">
                                            <ul class="filtersEachColList" id="<?php echo $filter->id; ?>">
                                                <?php
                                                $attributes = $filter->attributes;
                                                $filter_attribute = explode(",", $attributes);
                                                if (!empty($filter_attribute)) {
                                                    ?>
                                                    <?php
                                                    foreach ($filter_attribute as $attribute_value) {
                                                        $attribute_value = str_replace(' ', '_', $attribute_value);
                                                        ?>
                                                        <li>
                                                            <label for="<?php echo $attribute_value; ?>" class="eachFilter checkboxLabel" value="<?php echo $attribute_value; ?>" onclick="myfilter();">
                                                                <input type="checkbox" id="<?php echo $attribute_value; ?>" name="filters[]" value="<?php echo $attribute_value; ?>" onclick="myfilter();">
                                                                <span><?php echo $attribute_value; ?></span>
                                                            </label>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>

                            </form>

                        </div>

                        <div class="mobileActions">

                            <input type="button" class="applyFilters" value="Apply filters">
                            <input type="button" class="cancelFilters" value="Cancel">
                        </div>

                        <div class="filterClearFilter">
                            <div class="clearAllBtn">Clear All</div>
                            <button class="moreFiltersBtn popup">More filters</<button>
                                    </div>
                                    </div>
                                <?php } ?>

            
                        
            <?php if (count($paid_business) > 0) { ?>
                <div class="sponsorsLinks">
                    <?php /*<h3 class="secondaryHeading">Sponsorsed Link</h3> */?>
                    <ul class="popularThisWeekList sponsoredList">
                        <?php foreach ($paid_business as $key => $lb) { ?>
                            <li>
                                <div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <?php if ($lb['is_logo'] == 0) { ?>
                                                <img width="170" height="170" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $lb['title']; ?>">
                                            <?php } else { ?>
                                                <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$lb['image_note'].'/190x125_'.$lb['image_note'].'-1.jpg';?>" alt="<?php echo $lb['title']; ?>">
                                            <?php  } ?>
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle advtPlus" title="<?php echo $lb['title']; ?>">
                                                        <?php echo $lb['title']; ?>  
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <?php if ($lb['website_url'] != "") { ?>
                                                        <a href="<?php echo 'http://' . $lb['website_url']; ?>" target="_blank" class="openNewWindow"></a>
                                                    <?php } ?>
                                                    <?php if(isset($lb['is_verified']) && $lb['is_verified']==1){?>
                                                        <span class="ypApproved">.</span>
                                                    <?php }?>
                                                    <span class="certified aaP"><?php echo $lb['rating']; ?></span>
                                                </div>
                                            </div>

                                            <div class="eachPopularRateOpen">
                                                <div class="eachPopularRatingBlock">
                                                    <?php if($lb['overall'] != 0.0 && $lb['review'] != 0){?>
                                                    <span class="rating r<?php echo str_replace(".", "-", $lb['overall']); ?>"><?php echo $lb['overall']; ?></span>
                                                    <a href="#" class="ratingCount">
                                                        <?php
                                                            if ($lb['review'] > 1) {
                                                                echo $lb['review'] . " reviews";
                                                            } else {
                                                                echo $lb['review'] . " review";
                                                            }
                                                        ?>
                                                    </a>
                                                    <?php }?>
                                                </div>
                                                <div class="openNow"><?php if ($lb['datetime']) { ?><strong>Open now</strong> - <?php echo $lb['datetime']; ?><?php } else { ?><strong> </strong><?php } ?></div>
                                            </div>

                                            <ul class="eachPopularTagsList">
                                                <?php if($lb['categories'] !=""){
                                                    echo $lb['categories'];
                                                }?>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <a href="mailto:<?php echo $lb['email']; ?>">Email</a>
                                                <?php if ($lb['website_url'] != "") { ?>
                                                    <a href="<?php echo 'http://' . $lb['website_url']; ?>" target="_blank">Website</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="addFav">Add Favorite</div>
                                        <a class="businessContact" <?php
                                                if ($lb['phone']) {
                                                    echo 'href="tel:'.$lb['phone'].'"';
                                                }?>><?php
                                                if ($lb['phone']) {
                                                    echo $lb['phone'];
                                                }?>
                                        </a>
                                        <address class="businessArea">
                                                <?php if ($lb['area']) { ?>
                                                    <strong> <?php echo $lb['area'];
                                                } else {
                                                    echo "";
                                                } ?> </strong>
                                                <?php
                                                if ($lb['pincode']) {
                                                    echo $lb['city'] . " - " . $lb['pincode'];
                                                } else {
                                                    echo $lb['city'];
                                                }
                                                ?>
                                        </address>

                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount"></span>
                                            <?php
                                            if (!empty($lb['latitude']) && !empty($lb['longitude'])) {
                                                echo "<a  href='https://maps.google.com/maps?f=d&amp;daddr=" . $lb['latitude'] . "," . $lb['longitude'] . "&amp;hl=en' target='_blank'>Directions</a>";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            
            <div class="businessListingBlock">
                <header class="hiddenTitle">
                    Business Listing
                </header>
                <div id="favpopup"></div>
                <div id="searchresults">
                    <ul class="popularThisWeekList">
                        <?php
                        foreach ($businesses as $key => $lb) {
                            ?>
                            <li>
                                <div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <?php if ($lb['is_logo'] == 0) { ?>
                                                <img width="170" height="170" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $lb['title']; ?>">
                                            <?php  } else { ?>
                                                <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$lb['image_note'].'/190x125_'.$lb['image_note'].'-1.jpg';?>" alt="<?php echo $lb['title']; ?>">
                                            <?php } ?>
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <?php if(($lb['website_url'] != "") || (isset($lb['is_verified']) && $lb['is_verified']==1)){  ?>
                                                <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle hasOtherInfo" title="<?php echo $lb['title']; ?>">
                                                <?php } else { ?>
                                                <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle" title="<?php echo $lb['title']; ?>">
                                                <?php } echo $lb['title']; ?>  
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <?php if ($lb['website_url'] != "") { ?>
                                                        <a target="_blank" href="<?php echo $lb['website_url']; ?>"><span class="openNewWindow"></span></a>
                                                    <?php } ?>
                                                    <?php if(isset($lb['is_verified']) && $lb['is_verified']==1){?>
                                                        <span class="ypApproved">.</span>
                                                    <?php }?>
                                                    <span class="certified aaP"><?php echo $lb['rating']; ?></span>
                                                </div>
                                            </div>
                                            <div class="eachPopularRateOpen">
                                                <div class="eachPopularRatingBlock">
                                                    <?php if($lb['overall'] != 0.0 && $lb['review'] !=0){ ?>
                                                    <span class="rating r<?php echo str_replace(".", "-", $lb['overall']); ?>"><?php echo $lb['overall']; ?></span>
                                                    <a href="#" class="ratingCount">
                                                        <?php
                                                            if ($lb['review'] > 1) {
                                                                echo $lb['review'] . " reviews";
                                                            } else {
                                                                echo $lb['review'] . " review";
                                                            }
                                                        ?></a>
                                                    <?php }?>
                                                </div>
                                                <div class="openNow"><?php if ($lb['datetime']) { ?><strong>Open now</strong> - <?php echo $lb['datetime']; ?><?php } else { ?><strong> </strong><?php } ?></div>
                                            </div>
                                            <ul class="eachPopularTagsList">
                                                <?php if($lb['categories'] !=""){
                                                    echo $lb['categories'];
                                                }?>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <?php if($lb['email'] != ""){?>
                                                    <a href="mailto:<?php echo $lb['email']; ?>">Email</a>
                                                <?php } if ($lb['website_url'] != "") { ?>
                                                    <a href="<?php echo $lb['website_url']; ?>" target="_blank">Website</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="<?php echo $lb['favclass']; ?>" onclick="addtofavorite(<?php echo $lb['business_id']; ?>)">Add Favorite</div>
                                        <a class="businessContact" <?php
                                            if ($lb['phone']) {
                                                echo 'href="tel:'.$lb['phone'].'"';
                                            }
                                            ?>><?php
                                            if ($lb['phone']) {
                                                echo $lb['phone'];
                                            }
                                            ?></a>
                                        <address class="businessArea">
                                            <?php if ($lb['area']) { ?>
                                                <strong> <?php echo $lb['area'];
                                            } else {
                                                echo "";
                                            } ?> </strong>
                                            <?php
                                            if ($lb['pincode']) {
                                                echo $lb['city'] . " - " . $lb['pincode'];
                                            } else {
                                                echo $lb['city'];
                                            }
                                            ?>
                                        </address>
                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount"></span>
                                        <?php
                                        if (!empty($lb['latitude']) && !empty($lb['longitude'])) {
                                            echo "<a  href='https://maps.google.com/maps?f=d&amp;daddr=" . $lb['latitude'] . "," . $lb['longitude'] . "&amp;hl=en' target='_blank'>Directions</a>";
                                        }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        <?php } ?>

                            <?php /*<div class="mobileLoadMore">
                                <button class="loadMoreBtn">Load More</button>	
                            </div>*/?>
                            <div class="pagination">
                                <div class="paginationBlock">
                                    <div class="pageNumbersNumbering">
                                        <ul class="paginationNew">
                                            <?php echo $links; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </section>

                            <aside class="rightSmallCol">
                                <div class="rightFeatured">
                                    <?php if (!empty($featured_business[0])) { ?>
                                        <div class="rightFeaturedBlock">
                                            <h4 class="rightFeaturedHeading">Featured Business</h4>
                                            <div class="featuredBusinessBlock">
                                                <a href="<?php echo $featured_business[0]['link']; ?>" class="featuredBusinessTitle"><?php echo $featured_business[0]['title']; ?></a>
                                                <div class="featuredBusinessContact">
                                                    <span class="contact"><?php
                                                        if ($featured_business[0]['phone']) {
                                                            echo $featured_business[0]['phone'];
                                                        }
                                                        ?>
                                                    </span>
                                                    <span class="address"><?php
                                                        if (!$featured_business[0]['area']) {
                                                            echo $featured_business[0]['city'];
                                                        } else {
                                                            echo $featured_business[0]['area'] . "," . $featured_business[0]['city'];
                                                        }
                                                        if ($featured_business[0]['pincode']) {
                                                            echo " - " . $featured_business[0]['pincode'];
                                                        }
                                                        ?>
                                                    </span>
                                                <?php if ($featured_business[0]['website_url'] != "") { ?>
                                                        <a href="<?php echo $featured_business[0]['website_url']; ?>" target="_blank" class="featuredBusinessWebsite">Website</a>
                                                <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="haveBusiness">
                                    <div class="haveBusinessAd" style="background-image:url(<?php echo STYLEURL; ?>front/images/cityDrawing.png);">
                                        Want business owners to contact you?<br>
                                        <a href="#" class="addBusinessBtn" id="get-quotes">Get Quotes</a>
                                    </div>
                                </div>
                                <div class="rightColAdvt">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <script>
                                      (adsbygoogle = window.adsbygoogle || []).push({
                                        google_ad_client: "ca-pub-9587875399157339",
                                        enable_page_level_ads: true
                                      });
                                    </script>
                                </div>
                                <div class="rightRecentReviews">
                                    <div class="recentViewsBlock">
                                        <h3 class="thirdHeading">Recent Reviews</h3>
                                        <ul class="recentReviewsList">
                                            <?php foreach ($reviews as $key => $review) { ?>
                                                <li>
                                                    <div class="eachRR">
                                                        <div class="eachRRPhotoRat">
                                                            <div class="eachRRPhoto">
                                                                <img src="<?php echo STYLEURL . "front/images/" . $review['image']; ?>" alt="">
                                                            </div>
                                                            <div class="titleRating">
                                                                <a href="#" class="reviewerName"><?php echo $review['name']; ?></a>
                                                                <div class="reviewRating">
                                                                    <?php if($review['rating'] != 0.0){?>
                                                                    <span class="rating r<?php echo str_replace(".", "-", $review['rating']); ?>"><?php echo $review['rating']; ?></span>
                                                                    <?php } ?>
                                                                    <span class="ratingTime"><?php echo $review['datetime'] . ' ago'; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="<?php echo $review['link']; ?>" class="eachRRTitle"><?php echo $review['business']; ?></a>
                                                        <p class="eachRRTitleInfo"><?php echo substr($review['comments'], 0, 35); ?></p>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                                <?php if (!empty($cat_suggest)) { ?>
                                    <div class="rightSimilarCategories">
                                        <div class="similarCategoriesBlock">
                                            <h3 class="thirdHeading">Similar Categories</h3>
                                            <ul class="categoriesList">
                                                <?php foreach ($cat_suggest as $key => $c) { ?>
                                                    <li><a href="<?php echo $c['link']; ?>"><?php echo $c['name']; ?></a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="rightSuggestBusiness">
                                    <div class="haveBusiness">
                                        <div class="haveBusinessAd" style="background-image:url(<?php echo STYLEURL; ?>front/images/cityDrawing.png);">
                                            Didn't find what you are looking for?<br>
                                            <a href="/add-business" class="addBusinessBtn" id="suggest-business">Suggest a business</a>
                                        </div>
                                    </div>
                                </div>
                            </aside>
    </div>
</div>
