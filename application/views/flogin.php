
<div class="mobileHeader">
    <div class="mobileMenu">Login</div>
</div>

<div class="container login topMargin">
    <div class="wrapper">
        <div class="loginRegisterBlock">
            <div class="loginFormTitle">
                <h1 class="pageTitle">Login Form</h1>
            </div>
            <?php if(!empty($this->session->flashdata('error'))){?>
                    <h2 style="border: 1px solid #ffca28;color: #ef3900;padding: 5px;text-align: center;"><?php echo $this->session->flashdata('error'); ?></h2> 
                    <?php }?>
                      <?php if(!empty($this->session->flashdata('success'))){?>
                    <h2 style="border: 1px solid #000;color: green;padding: 5px;text-align: center;"><?php echo $this->session->flashdata('success'); ?></h2> 
                    <?php }?>
            <?php echo form_open('login',array('id' => 'login')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    <li>
                        <div class="loginFormElement">
                            <label for="username" class="loginFormLabel">Username</label><?php echo form_error('username'); ?>
                            <input type="text" class="loginFormTextbox" name="username" placeholder="Username">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">Password</label><?php echo form_error('pass'); ?>
                            <input type="password" class="loginFormTextbox" name="pass" placeholder="Password">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement rememberMe">
                            <label for="remember" class="loginFormLabel checkbox">
                                <input type="checkbox" class="checkboxInput" value="" id="remember">
                                <span class="checkboxText">Remember Me</span>
                            </label>
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <!--<div class="forgotPassword">Forgot Password</div>-->
                            <input type="submit" class="loginFormBtn" value="Login">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); ?>
            <div class="registerBlock">
                <?php if($this->session->userdata('otp')) { ?>
                <?php echo form_open('myaccount/check_otp',array('id' => 'otp')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    <li>
                        <div class="loginFormElement">
                            <label for="forgot_email" class="loginFormLabel">OTP</label>
                            <input type="text" class="loginFormTextbox" name="otp" placeholder="Enter Otp" id="otp">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <!--<div class="forgotPassword">Forgot Password</div>-->
                            <input type="submit" class="loginFormBtn" value="Submit" id="post_email">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); } ?>
            <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">Forgot Password</a>    New user? / <a href=<?php echo base_url()."register"?>>Register</a>
  <div id="light" class="white_content"> <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">Close</a> <?php echo form_open('myaccount/forgot_pass',array('id' => 'forgot_change_password')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    <li>
                        <div class="loginFormElement">
                            <label for="forgot_email" class="loginFormLabel">Email</label>
                            <input type="text" class="loginFormTextbox" name="forgot_email" placeholder="Email">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <!--<div class="forgotPassword">Forgot Password</div>-->
                            <input type="submit" class="loginFormBtn" value="Submit">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); ?>
  </div>
            </div>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last"> Login </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>