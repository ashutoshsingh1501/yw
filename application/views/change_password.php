
<div class="mobileHeader">
    <div class="mobileMenu">Login</div>
</div>

<div class="container login topMargin">
    <div class="wrapper">
        <div class="loginRegisterBlock">
            <div class="loginFormTitle">
                <h1 class="pageTitle">Change Password</h1>
            </div>
            <?php if($this->session->userdata('change_pass')) { ?>
            <?php echo form_open('login/do_forgot_to_change_password', array('id' => 'forgot_changepassword')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">New Password</label>
                            <input type="password" class="loginFormTextbox" name="password" placeholder="New Password" id="password">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">Confirm Password</label>
                            <input type="password" class="loginFormTextbox" name="password2" placeholder="Confirm Password" id="password2">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <!--<div class="forgotPassword">Forgot Password</div>-->
                            <input type="submit" class="loginFormBtn" value="Reset password">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); } else {
                ?>
       <?php echo form_open('login/do_change_password', array('id' => 'changepassword')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">Old Password</label>
                            <input type="password" class="loginFormTextbox" name="old_password" placeholder="Old Password" id="old_password">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">New Password</label>
                            <input type="password" class="loginFormTextbox" name="password" placeholder="New Password" id="password">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <label for="pass" class="loginFormLabel">Confirm Password</label>
                            <input type="password" class="loginFormTextbox" name="password2" placeholder="Confirm Password" id="password2">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <!--<div class="forgotPassword">Forgot Password</div>-->
                            <input type="submit" class="loginFormBtn" value="Reset password">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); } ?>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <a href="<?php echo base_url(); ?>my-account" rel="v:url" property="v:title">My Account&nbsp;&nbsp;/</a> 
                            <span rel="v:child" typeof="v:Breadcrumb"> 
                                <span class="breadcrumb_last"> Change Password </span> 
                            </span>
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>