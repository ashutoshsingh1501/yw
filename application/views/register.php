<div class="mobileHeader">
    <div class="mobileMenu">Login</div>
</div>

<div class="container login topMargin">
    <div class="wrapper">
        <div class="loginRegisterBlock">
            <div class="loginFormTitle">
                <h1 class="pageTitle">Register</h1>
            </div>
            <?php if (!empty($this->session->flashdata('error'))) { ?>
                <h2 style="border: 1px solid #ffca28;color: #ef3900;padding: 5px;text-align: center;"><?php echo $this->session->flashdata('error'); ?></h2> 
            <?php } ?>
            <?php echo form_open('login/signup', array('id' => 'register')); ?>
            <div class="loginFormBlock">
                <ul class="loginFormList">
                    <li>
                        <div class="loginFormElement">
                            <label for="user" class="loginFormLabel">Full name*</label><?php echo form_error('fullname'); ?>
                            <input type="text" class="loginFormTextbox" placeholder="Please your name" name="fullname" value="<?php echo set_value('fullname'); ?>">
                        </div>
                    </li>

                    <li>
                        <div class="loginFormElement">
                            <label for="user" class="loginFormLabel">Email* (username)</label><?php echo form_error('email'); ?>
                            <input type="text" class="loginFormTextbox" placeholder="Enter your email" name="email" value="<?php echo set_value('email'); ?>">
                        </div>
                    </li>

                    <li>
                        <div class="loginFormElement">
                            <label for="user" class="loginFormLabel">Password*</label><?php echo form_error('pass1'); ?>
                            <input type="Password" class="loginFormTextbox" placeholder="Password" name="pass1" id="pass1">
                        </div>
                    </li>
                    <li>
                        <div class="loginFormElement">
                            <label for="user" class="loginFormLabel">Confirm Password*</label><?php echo form_error('pass2'); ?>
                            <input type="Password" class="loginFormTextbox" placeholder="confirm password" name="pass2">
                        </div>
                    </li>

                    <li>
                        <div class="loginFormElement phone">
                            <span class="number">+91</span>
                            <label for="user" class="loginFormLabel">Phone*</label><?php echo form_error('phone'); ?>
                            <input type="text" class="loginFormTextbox" placeholder="Enter your phone number" name="phone"  value="<?php echo set_value('phone'); ?>">
                        </div>
                    </li>

                    <li>
                        <div class="loginFormElement">
                            <input type="submit" class="loginFormBtn" value="Register">
                        </div>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); ?>
            <div class="registerBlock">
                Already a user? / <a href=<?php echo base_url() . "my-account" ?>>Login</a>
            </div>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <!--<span rel="v:child" typeof="v:Breadcrumb"> 
                                <a href="/news" rel="v:url" property="v:title">Restaurants&nbsp;&nbsp;/</a> 
                                <span rel="v:child" typeof="v:Breadcrumb"> 
                                        <span class="breadcrumb_last"> Hotel 1 </span> 
                                </span>
                </span>
                        -->
                        <span rel="v:child" typeof="v:Breadcrumb">
                            <span rel="v:child" typeof="v:Breadcrumb"> 
                                <span class="breadcrumb_last"> Register </span> 
                            </span>
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.formElementTextbox, .formElementTextarea').on('focus blur', function (e) {
            $(this).parent('.formElement').toggleClass('formElementLableFocused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    });
</script>
