<section>	
	<div class="section-body">		
		<div class="card">			
			<form class="form" id="permission_index" action="<?php echo base_url(); ?>permission" method="POST">				
				<div class="card-body">					
					<div class="row" style="margin-bottom:30px;">						
						<div class="col-sm-1">							
							<!--<div class="btn-group" style="width:100%">								
								<button type="button" class="btn ink-reaction btn-default-dark"  data-toggle="dropdown" aria-expanded="false">									
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk									
								</button>								
								<ul class="dropdown-menu animation-expand" permission="menu">									
									<li><a href="#">Active</a></li>									
									<li><a href="#">Deactive</a></li>									
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>									
								</ul>								
							</div>-->							
						</div>						
						<div class="col-sm-2">	
					<input type='hidden' name='all_modules' id='all_modules'	value='<?php echo $all_modules; ?>'	>	
					<input type='hidden' name='final_feature_list' id='final_feature_list'	value='<?php echo $final_feature_list; ?>'	>					
							<select id="role_id" name="role_id" class="form-control selectpicker" data-live-search="true" onchange="" >								
								<!--<option value="0" >All Roles</option>-->								
								<?php foreach($role as $key => $sl ){ ?>									
									<option <?php if (isset($_GET['role_id'])) {   if($_GET['role_id'] == $key){ echo 'selected'; } }?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>									
								<?php } ?>								
							</select>							
						</div>		
					<div class="col-sm-1 pull-left" >							
							<input type='submit' name='submit' class="btn ink-reaction btn-default-dark"  value='Save Permissions'>					
						</div>							
						<!--<div class="col-sm-1 pull-right" >							
							<a class="btn ink-reaction btn-default-dark" href="javascript:void(0);" data-toggle="modal" data-target="#addpermission"><i class="fa fa-plus text-default-light"></i> Save</a>							
						</div>-->					
						<!--<div class="col-sm-1 pull-right"><h4 class="rowcount"> <?php //echo count($results); ?> Permissions </h4></div>-->						
					</div>					<?php echo $pagermessage; ?>
				</div>				
				<div class="table-responsive">	
				<table class="table table-index table-striped table-bordered table-hover table-condensed" >						
						<thead>							
							<tr>								
																
								<th>
								<a href="#" >&nbsp;</a></th>								
								<th><a href="#">Module Name</a></th>								
								<th><a href="#" >Feature Name</a></th>								
								<th><a href="#" >Create</a></th>	
								<th><a href="#" >Edit</a></th>	
								<th><a href="#" >View</a></th>	
								<th><a href="#" >Delete</a></th>									 
								<th><a href="#">Freeze</a></th>								
														
							</tr>							
						</thead>						
						<tbody>	
					
							<?php 
							//echo "<pre>";print_r($full_status);
							//echo "<pre>";print_r($results);exit;
							//$allModules=explode(",",$all_modules);
							$countA=0;$match=0;$subcount=1;$module_list='';
							$x1='';	$x2='';	$x3='';$base_module_name='';$business_checked='';$category_checked='';
							$temp1='';$temp2='';$temp3='';$matchcount=0;$j=0;$temp3_old='';$features_list='';$statusMatch=0;
							$fullstatusCount=0;
							for($i=0;$i<count($results);$i++)
							{								
								$module_name= $results[$i]->module_name;
								$feature_name=$results[$i]->feature_name;
								$create=$results[$i]->create;
								$edit=$results[$i]->edit;
								$delete=$results[$i]->delete;
								$view=$results[$i]->view;
								$freeze=$results[$i]->freeze;
								$countA=$countA+1;
								$current_module='';	$current_full_status='';	$c_module='';	$c_full_status='';					
								
							?>	
							<?php if( $i==0 || $base_module_name!=$module_name) {
								
								$base_module_name=$module_name;
								if($module_list=='')
								{
								$module_list=$base_module_name;	
								}else{
									$module_list.=",".$base_module_name;	
								}
							
								
								?>							
								<tr>									
									
									<td><div class="checkbox checkbox-styled">	
											
											<label><input type="checkbox"  name="addcheck_<?php echo $base_module_name; ?>" <?php if(isset($full_status[$fullstatusCount])&& $full_status[$fullstatusCount]==1){echo 'checked';} ?>   id="addcheck_<?php echo $base_module_name; ?>" class="addcheck <?php echo $base_module_name; ?>" onclick="checkAllInModule('<?php echo $base_module_name; ?>')"><span>&nbsp;</span></label>											
										</div>	</td>	
															
								<td><b><?php echo $base_module_name; ?></b></td>									
									<td>&nbsp;</td>	
									<td>
									&nbsp;
									</td>										
									<td>
									&nbsp;
									</td>
									<td>
									&nbsp;
									</td>									
										<td>
									&nbsp;
									</td>								
									<td></td>									
									<td style="text-align:right;">&nbsp;					
						            </td>	
								</tr>	
							<?php 
							$fullstatusCount++;
							} ?>
							<?php 
							
							if($features_list=='')
								{
								$features_list=$module_name.".".$feature_name;	
								}else{
									$features_list.=",".$module_name.".".$feature_name;
								}
						
								
								?>
								<tr>
									
									<td></td>									
									<td>&nbsp;</td>									
									<td><?php echo $feature_name; ?></td>	
									<td>
									<div class="checkbox checkbox-styled">
											<?php if($create==1){$checked='checked';}else{$checked='';}?>						
											<label><input type="checkbox" name="create_<?php echo $module_name; ?>_<?php echo $feature_name; ?>" <?php echo $checked; ?> class="addcheck_<?php echo $module_name; ?>" onclick="featureCheck('<?php echo $module_name; ?>')"><span>&nbsp;</span></label>											
										</div>
									</td>										
									<td>
									<div class="checkbox checkbox-styled">	
									<?php if($edit==1){$checked='checked';}else{$checked='';}?>									
											<label><input type="checkbox"  name="edit_<?php echo $module_name; ?>_<?php echo $feature_name; ?>" <?php echo $checked; ?> class="addcheck_<?php echo $module_name; ?>" onclick="featureCheck('<?php echo $module_name; ?>')"><span>&nbsp;</span></label>											
										</div>
									</td>
									<td>
									<div class="checkbox checkbox-styled">
								<?php if($view==1){$checked='checked';}else{$checked='';}?>										
											<label><input type="checkbox"  name="view_<?php echo $module_name; ?>_<?php echo $feature_name; ?>" <?php echo $checked; ?> class="addcheck_<?php echo $module_name; ?>" onclick="featureCheck('<?php echo $module_name; ?>')"><span>&nbsp;</span></label>											
										</div>
									</td>									
										<td>
									<div class="checkbox checkbox-styled">
										<?php if($delete==1){$checked='checked';}else{$checked='';}?>											
											<label><input type="checkbox" name="delete_<?php echo $module_name; ?>_<?php echo $feature_name; ?>" <?php echo $checked; ?> class="addcheck_<?php echo $module_name; ?>" onclick="featureCheck('<?php echo $module_name; ?>')" ><span>&nbsp;</span></label>											
										</div>
									</td>								
									<td>										
									<div class="checkbox checkbox-styled">
								<?php if($freeze==1){$checked='checked';}else{$checked='';}?>										
											<label><input type="checkbox"  name="freeze_<?php echo $module_name; ?>_<?php echo $feature_name; ?>" <?php echo $checked; ?> class="addcheck_<?php echo $module_name; ?>" onclick="featureCheck('<?php echo $module_name; ?>')" ><span>&nbsp;</span></label>											
										</div>							
									</td>									
																		
								</tr>	
								
							<?php
								
								
														} ?>							
						</tbody>						
					</table>					
				</div>	
<input type='hidden' name='module_list' value='<?php echo $module_list; ?>'>
<input type='hidden' name='features_list' value='<?php echo $features_list; ?>'>					
			</form>			
			<?php				
				echo $links;				
			?>			
		</div>		
	</div>	
		
	<!--Modal For Create Area-->	
	<div id="addpermission" class="modal fade" permission="dialog">		
		<div class="modal-dialog">			
			<div class="card card-underline">				
				<form class="form" id="createpermission" action='<?php echo base_url('permission/create'); ?>' method="post">					
					<div class="card-head">						
						<div class="tools">							
							<div class="btn-group">								
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>								
							</div>							
						</div>						
						<header>Add Permission</header>						
					</div>					
					<div class="card-body ">						
						<div class="checkbox checkbox-styled pull-right">							
							<label>								
								<input type="checkbox" value="1" name="permissionactive" id="permissionactive"><span>Active</span>								
							</label>							
						</div>						
						<div class="row">							
							<div class="col-sm-12">								
								<div class="form-group">									
									<input type="text" class="form-control" id="newpermission" name="newpermission">									
									<label for="newpermission">Enter Permission Name</label>									
								</div>								
							</div>							
						</div>						
                        <div class="row">							
							<div class="col-sm-12">								
								<div class="form-group">									
																		
									<select id="report_id" name="report_id" class="form-control">										
										<option value="">Reports To</option>										
										<?php foreach($permission as $key => $sl ){ ?>											
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>											
										<?php } ?>										
										<option value='0'  >Others</option>										
									</select>									
								</div>								
							</div>							
						</div>						
					</div>					
					<div class="modal-footer">						
						<button type="submit"   class="btn btn-default-dark">Add Permission</button>						
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>						
					</div>					
				</form>				
			</div>			
		</div>		
	</div>	
		
	<!--Modal For Edit Permission-->	
	<div id="editpermission" class="modal fade" permission="dialog">		
		<div class="modal-dialog">			
			<div class="card card-underline">				
				<form class="form" id="edit_permission"  method="post">					
					<div class="card-head">						
						<div class="tools">							
							<div class="btn-group">								
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>								
							</div>							
						</div>						
						<header>Edit Permission</header>						
					</div>					
					<div class="card-body edit-body">						
						<input type="hidden"  name="permissionId" id="cityId" value='permissionId' />						
						<input type="hidden"  name="permissionId" id="permissionId" value='' />						
						<div class="checkbox checkbox-styled pull-right">							
							<label>								
								<input type="checkbox"  name="permissionactive" id="permissionactive"><span>Active</span>								
							</label>							
						</div>						
						<div class="row">							
							<div class="col-sm-12">								
								<div class="form-group">									
									<input type="text" class="form-control" id="newpermission" name="newpermission" value="">									
									<label for="newpermission">Enter Permission Name</label>									
								</div>								
							</div>							
						</div>						
                        <div class="row">							
							<div class="col-sm-12">								
								<div class="form-group">									
									<select id="report_id" name="report_id" class="form-control">										
										<option value="">Reports To</option>										
										<?php foreach($permission as $key => $sl ){ ?>											
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>											
										<?php } ?>										
										<option value='0'  >Others</option>										
									</select>									
								</div>								
							</div>							
						</div>						
					</div>		
					
					<div class="modal-footer" >						
						<button type="submit"   class="btn btn-default-dark">Edit Permission</button>						
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>						
					</div>	
								
				</form>				
			</div>			
		</div>		
	</div>	
</section>	
<script>	
function sort_permission(sort_val)
{
	//alert(sort_val);
$('#sort').val(sort_val);
document.getElementById('permission_index').submit();	
}
function checkAllInModule(module)
{
	var x=$('#addcheck_'+module).val();
	//alert($('#addcheck_'+module).is(":checked"));
	var checked_status=$('#addcheck_'+module).is(":checked");
	if(checked_status==true)
	{
	$('.addcheck_'+module).attr("checked","checked");	
	}else
	{
		//$("#captureImage").removeAttr('checked');
		$('.addcheck_'+module).removeAttr("checked");	
	}
	
}
function featureCheck(module)
{
	//alert(module);
	var root_checked_status=$('#addcheck_'+module).is(":checked");
	if(root_checked_status==false)
	{
	$('#addcheck_'+module).attr("checked","checked");	
	}
	var feature_checked_status=$('.addcheck_'+module).is(":checked");
	if(feature_checked_status==false)
	{
	$('#addcheck_'+module).removeAttr("checked");		
	}
}

	

</script>																									