<section>
	<div class="section-header ">
		<ol class="breadcrumb">
			<li class="active">Roles</li>
		</ol>
	</div>
	<div class="section-body">
		
		<div class="row">
			<div class="card">
				<div class="card-head style-primary">
                    <header>Roles & Permission </header>
					
				</div>
				<div class="card-body">
					
                   	<form class="form floating-label" style="margin:0 auto"   action='<?php echo base_url('roles/create'); ?>' method="post" >
						<div class="row">
							<div class="col-sm-12">						
								<div class="checkbox checkbox-styled">
									<label>
										<input type="checkbox" value="1" name="roleactive" id="roleactive"><span>Active</span>
									</label>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control" id="newrole" name="newrole">
									<label for="newrole">Enter Role Name</label>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									
									<select id="state" name="state" class="form-control">
										<option value="">Select Role</option>
										<?php foreach($role_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0'  >Others</option>
									</select>
								</div>
							</div>
						</div>
						
                        <div class="row">
							<div class="col-sm-12">
								
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="submit"  class="btn btn-primary">Proceed</button>
						</div>
						<p>Clicking on 'Proceed' button will take to you permissions page where you need to set permissions for the role you are adding.</p>
					</form>
					
				</div>
				
			</div>
			<!--end .card -->
		</div>
	</div>
	<div id="addrole" class="modal fade" role="dialog">
		<div class="modal-dialog">
			
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Role</h4>
				</div>
				<div class="modal-body">
					
				</div>
				
			</div>
			
		</div>
	</div>
</div>
</div>
</section>