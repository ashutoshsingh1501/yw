<section>
	<div class="section-body settings">
		<div class="card card-underline">
			<div class="card-head">
				<header>Settings</header>
			</div>
			<form class="form form-validate" style="margin:0 auto"  enctype="multipart/form-data" action='<?php echo base_url('setting/index/'.$user->id); ?>' method="post" >
				<div class="card-body">
					<div class="form-group col-md-12">
						<div class="col-md-6">
							<div class="row">
								<div class="form-group">
									<input type="text" class="form-control" id="username" name="username" value="<?php echo $user->first_name; ?>" >
									<label for="username">Employee Full Name</label>
								</div>
								
								<div class="form-group">
									<input type="email" class="form-control" id="useremail" name="useremail" value="<?php echo $user->email; ?>" readonly>
									<label for="useremail">Email</label>
								</div>
								
								<div class="form-group">
									<input type="number" class="form-control" id="userphone" name="userphone" maxlength='10' minlength='9'  value="<?php echo $user->phone; ?>"   >
									<label for="userphone">Phone</label>
								</div>
								
								<div class="form-group">
									<?php //print_r($user->role_id); print_r($roles);  ?>
									<input type="text" class="form-control" id="userrole" name="userrole" value="<?php if(isset($roles[$user->role_id])){ echo $roles[$user->role_id]; }?>" readonly>
									<label for="userrole">Role</label>
								</div>
								
								<div class="form-group">
									<input type="text" class="form-control" id="usermanager" name="usermanager" value="<?php if(isset($roles[$role->report_id])){ echo $roles[$role->report_id]; }?>" readonly>
									<label for="usermanager">Manager</label>
								</div>
								
								<div class="checkbox checkbox-styled">
									<label>
										<?php if($user->receive_notification=='1'){$checked='checked';}else{$checked='';}?>
										<input type="checkbox" <?php echo $checked; ?> value='1' name="receive_notification" id="receive_notification"> <span>Receive Promotional Emails</span>
									</label>
								</div>
								<div class="form-group">										 
									<input type="password" class="form-control" id="password" name="password" value="" >
									<label for="usermanager">Change Password</label>
								</div>
								<div class="form-group">										 
									<input type="password" class="form-control" id="confirm_password" name="confirm_password" value="" >
									<label for="usermanager">Confirm Password</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row pull-right">
								<div class="col-md-12">
									<input type='hidden' id='old_image_url' name='old_image_url' value='<?php echo $user->image_url; ?>'>
									<img id="dispImage"  accept='.jpeg,.jpg,.png' style="height:200px;width:200px;border:1px solid black;padding:2px;" src="<?php echo $user->image_url; ?>" alt="Image comes over here">
									<input type="hidden"  id="image_url"  name="image_url" value="<?php echo $user->image_url; ?>; ?>">
								</div>
								<div class="form-group col-md-12">	
									<input type="button" onclick="deleteImage()"  id="delImage"  class="btn btn-primary" value="Delete Image">
									<span id="newupload" style="display:none;">
									</span>
									
								</div>
							</div>
						</div>	
					</div>
					<div class="modal-footer">
						<button type="submit" name="submit" class="btn btn-primary">Update</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>			
