<section>
	<div class="section-body">
		<div class="card">
			<form class="form" id="area_index" action="<?php echo base_url(); ?>area" method="get">
				<div class="card-body">
					<div class="row" style="margin-bottom:30px;vertical-align:middle">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn btn-block ink-reaction btn-default-dark"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
									<li><a href="#">Active</a></li>
									<li><a href="#">Deactive</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select id="state_id" name="state_id" class="form-control selectpicker" data-live-search="true" onchange="find_area();" >
								<option value='0' selected >All States</option>
								<?php foreach($states as $key => $sl ){ ?>
									<option <?php if (isset($_GET['state_id'])) {   if($_GET['state_id'] == $key){ echo 'selected'; } }?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						
						<div class="col-sm-1 pull-right" >
							<a class="btn btn-block ink-reaction btn-default-dark" href="javascript:void(0);" data-toggle="modal" data-target="#addarea"><i class="fa fa-plus text-default-light"></i> Add</a>
							
						</div>
												<div class="col-sm-1 pull-right"><h4 class="rowcount"> <?php echo count($results); ?> Areas </h4></div>
						
					</div>
					<?php echo $pagermessage; ?>
				</div>
				<div class="table-responsive">
										<table class="table table-index table-striped table-bordered table-hover table-condensed" >
						<thead>
							<tr>
								<th class="col-sm-1">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th class="col-sm-1"><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='id'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=id-<?php echo $sort; ?>">ID</a></th>
								<th ><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='name'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=name-<?php echo $sort; ?>">Area Name</a> <br><input type="text" value="<?php if (isset($_GET['area_name'])) {   echo $_GET['area_name']; }?>" class="form-control" onchange="find_area();" name="area_name"  placeholder="Search Area"></th>
								<th><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='zip'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=zip-<?php echo $sort; ?>">Pincode</a><br><input type="text" value="<?php if (isset($_GET['zip'])) {   echo $_GET['zip']; }?>" class="form-control" onchange="find_area();" name="zip"  placeholder="Search Pincode"></th>
								<th><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='city_id'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=city_id-<?php echo $sort; ?>">City <br><input type="text" value="<?php if (isset($_GET['city_name'])) {   echo $_GET['city_name']; }?>" class="form-control" onchange="find_area();" name="city_name" placeholder="Search City"></th>
								<th><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='state_id'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=state_id-<?php echo $sort; ?>">State</th>
								<th><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='businesses'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=businesses-<?php echo $sort; ?>">Businesses</th>
								<th><?php 					$sort='';
								if(isset($_REQUEST['sort'])&&$_REQUEST['sort']){$sort=$_REQUEST['sort'];
$sortTemp=explode("-",$sort); $sort_column=$sortTemp[0];$sort_order=$sortTemp[1];
								if($sort_order=='desc' && $sort_column=='status'){$sort='asc'; echo "&#8595; &nbsp;";}else{ $sort='desc'; echo "&#8593; &nbsp;";}
								}else{$sort='desc'; echo "&#8593; &nbsp;";}
								 ?><a href="area?sort=status-<?php echo $sort; ?>">Status</th>
								<th style="text-align:right;">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $a){ ?>
								<tr>
									<td class="col-sm-1">
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="1" name="addcheck[]" class="addcheck"><span>&nbsp;</span></label>
										</div>
									</td>
									<td class="col-sm-1"><?php echo $a->id; ?></td>
									<td><?php echo $a->name; ?></td>
									<td><?php echo $a->zip; ?></td>
									<td><?php if(isset($cities[$a->city_id])){ echo $cities[$a->city_id]; }else{ echo '-'; } ?></td>
									<td><?php if(isset($states[$a->state_id])){ echo $states[$a->state_id]; }else{ echo '-'; } ?></td>
									<td><?php echo $a->total; ?> Business</td>
									<td>
										<select data-id="<?php echo $a->id; ?>"  class="a_status form-control selectpicker" onchange="change_astaus(this)" >
											<option value="1" <?php if($a->status){ echo 'selected'; } ?> >Active</option>
											<option value="0" <?php if(!$a->status){ echo 'selected'; } ?> >Deactive</option>
										</select>
										
									</td>
									<td style="text-align:right;">
										<a data-toggle="modal" data-id="<?php echo $a->id; ?>" title="Edit this item" class="areaDialog btn btn-icon-toggle" href="#editarea"><i class="fa fa-pencil fa-fw"></i></a>
										
										<a href="javascript:void(0);" class="btn  btn-icon-toggle  area-delete" data-id="<?php echo $a->id; ?>"  ><i class="fa fa-trash-o fa-fw"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
						<?php echo $links; ?>
		</div>
	</div>
	
	<div id="addarea" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<div class="card-head">
					<div class="tools">
						<div class="btn-group">
							<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
						</div>
					</div>
					<header>Add Area</header>
				</div>
				<form class="form form-validation" id="createarea">
					<div class="card-body action-body">
						<div class="checkbox checkbox-styled pull-right">
							<label> Active
								<input type="checkbox" checked value="1" name="status" id="status"><span>&nbsp;</span>
							</label>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select id="state_id" name="state_id" class="form-control selectpicker" data-live-search="true" required="" >
										<option value="" data-hidden="true">Select State</option>
										<?php foreach($states as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select id="city_id" name="city_id" class="form-control selectpicker" data-live-search="true" required="" >
										<option value="" data-hidden="true">Select City</option>
										<?php foreach($cities as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group floating-label">
									<input type="text" class="form-control"  name="name" required>
									<label for="name">Enter Area Name</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group floating-label">
									<input type="number" class="form-control" name="zip" minlength='5' maxlength='6' required>
									<label for="zip">Pincode</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"  class="btn ink-reaction btn-default-dark">Add Area</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<div id="editarea" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<div class="card-head">
					<div class="tools">
						<div class="btn-group">
							<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
						</div>
					</div>
					<header>Edit Area</header>
				</div>
				<form class="form form-validation" id="areaedit">
					<div class="card-body action-body">
						<input type="text" hidden name="areaId" class="areaId"  />
						<div class="checkbox checkbox-styled pull-right">
							<label> Active
								<input type="checkbox" checked value="1" name="status" class="status"><span>&nbsp;</span>
							</label>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select id="state_id" name="state_id" class="form-control selectpicker state_id" data-live-search="true" required="" onchange="change_city(#editForm);" >
										<option value="" data-hidden="true">Select State</option>
										<?php foreach($states as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select  name="city_id" id="city_id" class="form-control selectpicker" data-live-search="true" required="" >
										<option value="" data-hidden="true">Select City</option>
										<?php foreach($cities as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control name" name="name" required>
									<label for="name">Enter Area Name</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="number" class="form-control zip" name="zip" minlength='5' maxlength='6' required>
									<label for="zip">Pincode</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"  class="btn ink-reaction btn-default-dark">Edit Area</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>																										