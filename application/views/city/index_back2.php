<section>
	<div class="section-body">
		<div class="card">
			<form class="form" id="city_index" action="<?php echo base_url(); ?>city" method="get">
				<div class="card-body">
					<div class="row" style="margin-bottom:30px;vertical-align:middle">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn ink-reaction btn-default-dark"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select id="state_id" name="state_id" class="form-control selectpicker" data-live-search="true" onchange="find_city();" >
								<option value="0" >All States</option>
								<?php foreach($states as $key => $sl ){ ?>
									<option <?php if (isset($_GET['state_id'])) {   if($_GET['state_id'] == $key){ echo 'selected'; } }?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-1 pull-right" >
							<a class="btn ink-reaction btn-default-dark" href="javascript:void(0);" data-toggle="modal" data-target="#addcity"><i class="fa fa-plus text-default-light"></i> Add</a>
						</div>
						<div class="col-sm-1 pull-right"><h4 class="rowcount"> <?php echo count($results); ?> Cities </h4></div>
					</div>
					<?php echo $pagermessage; ?>
				</div>
				<div class="table-responsive">
					<table class="table table-index table-striped table-bordered table-hover table-condensed" >
						<thead>
							<tr>
								<th>
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th>ID</th>
								<th>City Name <br><input type="text" value="<?php if (isset($_GET['city_name'])) {   echo $_GET['city_name']; }?>" class="form-control" onchange="find_city();" name="city_name" placeholder="Search City Name"></th>
								<th>State</th>
								<th>Areas</th>
								<th style="text-align:right;">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $a){ ?>
								<tr>
									<td style="width:10%">
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="<?php echo $a->id; ?>" name="addcheck[]" class="addcheck" ><span>&nbsp;</span></label>
										</div>
									</td>
									<td><?php echo $a->id; ?></td>
									<td><?php echo $a->name; ?></td>
									<td><?php if(isset($states[$a->state_id])){ echo $states[$a->state_id]; }else{ echo '-'; } ?></td>
									<td><?php echo $a->total; ?> Areas</td>
									<td style="text-align:right;">
										<a data-toggle="modal" data-id="<?php echo $a->id; ?>" title="Edit this item" class="cityDialog btn btn-icon-toggle" href="#editcity"><i class="fa fa-pencil fa-fw"></i></a>
										
										<a href="javascript:void(0);" class="btn btn-icon-toggle city-delete" data-id="<?php echo $a->id; ?>"  ><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
					<?php echo $links; ?>
		</div>
	</div>
	
	<!--Modal For Create Area-->
	<div id="addcity" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<form class="form" id="createcity" method="post">
					<div class="card-head">
						<div class="tools">
							<div class="btn-group">
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
							</div>
						</div>
						<header>Add City</header>
					</div>
					<div class="card-body ">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select  name="state_id" class="form-control selectpicker" data-live-search="true" required >
										<option value="" data-hidden="true">Select State</option>
										<?php foreach($states as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group floating-label">
									<input type="text" class="form-control" id="name" name="name" required>
									<label for="name">Enter City Name</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"   class="btn btn-default-dark">Add City</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!--Modal For Create Area-->
	<div id="editcity" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<form class="form" id="editForm" method="post">
					<div class="card-head">
						<div class="tools">
							<div class="btn-group">
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
							</div></div>
							<header>Edit City</header>
					</div>
					<div class="card-body">
						<input type="text" hidden name="cityId" class="cityId"  />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select  name="state_id" class="form-control selectpicker state_id" data-live-search="true" required >
										<option value=""  data-hidden="true" >Select State</option>
										<?php foreach($states as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control name"  name="name" required>
									<label for="name">Enter City Name</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"   class="btn btn-default-dark">	Edit City</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>																										