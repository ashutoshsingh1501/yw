<section>
	<div class="section-body">
		<div class="card">
			<form class="form" id="role_index" action="<?php echo base_url(); ?>role" method="get">
				<div class="card-body">
					<div class="row" style="margin-bottom:30px;">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn ink-reaction btn-default-dark"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
									<li><a href="#">Active</a></li>
									<li><a href="#">Deactive</a></li>
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select id="role_id" name="role_id" class="form-control selectpicker" data-live-search="true" onchange="find_role();" >
								<option value="0" >All Roles</option>
								<?php foreach($role as $key => $sl ){ ?>
									<option <?php if (isset($_GET['role_id'])) {   if($_GET['role_id'] == $key){ echo 'selected'; } }?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-1 pull-right" >
							<a class="btn ink-reaction btn-default-dark" href="javascript:void(0);" data-toggle="modal" data-target="#addrole"><i class="fa fa-plus text-default-light"></i> Add</a>
						</div>
						<div class="col-sm-1 pull-right"><h4 class="rowcount"> <?php echo count($results); ?> Roles </h4></div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-index table-striped table-bordered table-hover table-condensed" >
						<thead>
							<tr>
								<th>
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th>ID</th>
								<th>Role <br><input type="text" value="<?php if (isset($_GET['role_name'])) {   echo $_GET['role_name']; }?>" class="form-control" onchange="find_role();" name="role_name" placeholder="Search Role"></th>
								<th>Reports To</th>
								<th>Users</th>
								<th>Status</th>
								<th style="text-align:right;">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $a){ ?>
								<tr>
									<td style="width:10%">
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="<?php echo $a->id; ?>" name="addcheck[]" class="addcheck" ><span>&nbsp;</span></label>
										</div>
									</td>
									<td><?php echo $a->id; ?></td>
									<td><?php echo $a->name; ?></td>
									<td><?php if(isset($role[$a->report_id])){ echo $role[$a->report_id]; }else{ echo '-'; } ?></td>
									<td><?php echo $a->total; ?></td>
									<td>
										<select data-id="<?php echo $a->id; ?>"  class="b_status form-control selectpicker"  onchange="change_role_status(<?php echo $a->id; ?>,this.value);" >
											<?php 
												$status=array("Deactive","Active");
												foreach($status as $key => $value ){ ?>
												<option value="<?php echo $key; ?>" <?php if($a->status == $key){ echo 'selected'; } ?> ><?php echo $value; ?></option>
											<?php } ?>
										</select>
									</td>
									<td style="text-align:right;">
										<a data-toggle="modal" data-id="<?php echo $a->id; ?>" title="Edit this item" class="open-EditRoleDialog btn btn-icon-toggle" href="#editrole"><i class="fa fa-pencil fa-fw"></i></a>
										
										<a href="#" class="btn btn-icon-toggle role-delete" data-id="<?php echo $a->id; ?>"  ><i class="fa fa-trash-o fa-fw"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</div>
	
	<!--Modal For Create Area-->
	<div id="addrole" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<form class="form" id="createrole" action='<?php echo base_url('role/create'); ?>' method="post">
					<div class="card-head">
						<div class="tools">
							<div class="btn-group">
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
							</div>
						</div>
						<header>Add Role</header>
					</div>
					<div class="card-body ">
						<div class="checkbox checkbox-styled pull-right">
							<label>
								<input type="checkbox" value="1" name="roleactive" id="roleactive"><span>Active</span>
							</label>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control" id="newrole" name="newrole">
									<label for="newrole">Enter Role Name</label>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									
									<select id="report_id" name="report_id" class="form-control">
										<option value="">Reports To</option>
										<?php foreach($role as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0'  >Others</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"   class="btn btn-default-dark">Add Role</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!--Modal For Edit Role-->
	<div id="editrole" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="card card-underline">
				<form class="form" id="edit_role"  method="post">
					<div class="card-head">
						<div class="tools">
							<div class="btn-group">
								<a class="btn btn-icon-toggle"  data-dismiss="modal" ><i class="md md-close"></i></a>
							</div>
						</div>
						<header>Edit Role</header>
					</div>
					<div class="card-body edit-body">
						<input type="hidden"  name="roleId" id="cityId" value='roleId' />
						<input type="hidden"  name="roleId" id="roleId" value='' />
						<div class="checkbox checkbox-styled pull-right">
							<label>
								<input type="checkbox"  name="roleactive" id="roleactive"><span>Active</span>
							</label>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control" id="newrole" name="newrole" value="">
									<label for="newrole">Enter Role Name</label>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<select id="report_id" name="report_id" class="form-control">
										<option value="">Reports To</option>
										<?php foreach($role as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0'  >Others</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"   class="btn btn-default-dark">Edit Role</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	</section>																										