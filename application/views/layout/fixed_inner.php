<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Yellow Pages </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="shortcut icon" href="<?php echo STYLEURL; ?>front/images/favicon.ico" type="image/x-icon" />
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>
        <?php if ($this->uri->segment(1) == 's' ) { ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-search.css">
        <?php }else if ($this->uri->segment(1) == 'search-map') { ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-searchMap.css">
        <?php }else{ ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-search.css">
        <?php } ?>
    </head>
    <body>
        <div class="main" id="main"> 
            <div class="innerHeader container">
                <div class="wrapper">
                    <a href="/" class="branding">Yellow Pages</a>
                    <div class="innerSearchBlock">
                    <form class="homeSearchBlock" id="qt-search" autocomplete="off">
                        <div class="searchFullBlock">
                                    <div class="searchBusiness">
                                        <input type="text" value="" id="search" class="homeSearchTextbox businessInputField" placeholder="Search local business in India" autocomplete="off">
                                    </div>
                                    <div class="searchLocation">
                                            <input type="text" class="homeSearchTextbox locationInputField" value="Hyderabad" id="location">
                                    </div>
                                    <div class="searchButtonBlock">
                                        <div class="searchButton">Search <i class="searchIcon"></i></div>
                                    </div>
                            </div>
                    </form>
                    </div>
                    <ul class="innerTopNavList">
                        <li><a href="/add-business">Add business</a></li>
                        <li><a href="/categories">Categories</a></li>
                        <?php if (isset($this->session->userdata["active"])) { ?>
                            <li>
                            <span class="eachTopLink haveSubMenu">My Account</span>
                                <ul class="subMenuInner">
                                        <li><a href="/my-favorites">My Favorites</a></li>
                                        <li><a href="/my-reviews">My Reviews</a></li>
                            <li><a href="/sign-out">Sign Out</a></li>
                                </ul>
                            </li>
                        <?php } else{ ?>
                        <li><a href="/my-account">My Account</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <?= $content; ?>

            <?php
            if (isset($this->session->userdata["id"])) {
                $js = "<script src='" . ASSETSURL . "front/js/popupjs.js'></script><script>
				$(document).ready(function() {
				$('.addFav').click(function() {
				if($(this).hasClass('favActive')) {
				$(this).removeClass('favActive');
				} else {
				$(this).addClass('favActive');
				}
				});
				
				$('.opFavClick').click(function() {
				if($(this).hasClass('activeFavIcon')) {
				$(this).removeClass('activeFavIcon');
				} else {
				$(this).addClass('activeFavIcon');
				}
				});
				});	
				</script>";
            }
            ?>
        </div>  

        <div class="container footer">
            <div class="wrapper">
                <div class="footerNavBlocks mobHide">
                    <div class="mobileNavBlocks">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Quick links</h5>
                            <ul class="footerNavList">
                                <li class="active"><a href="/" class="mobHome">Home</a></li>
                                <li><a href="/categories" class="mobCategories">Browse category</a></li>
                                <li><a class="mobSearch">Search business</a></li>
                                <?php if (isset($this->session->userdata["active"])) { ?>
                                    <li><a href="/my-favorites" class="mobMyFavorites">My Favorites</a></li>
                                    <li><a href="/my-reviews" class="mobMyReviews">My Reviews</a></li>
                                    <li><a href="/change-password" class="mobChangePassword">Change Password</a></li>
                                    <li><a href="/sign-out" class="mobSignout">Sign Out</a></li>
                                <?php }else{?>
                                    <li><a href="/my-account" class="mobLogin">Login to your account</a></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="footerEachNavCol businessMenu">
                            <h5 class="footerHeading">Business</h5>
                            <ul class="footerNavList">
                                <li><a class="mobAddBusiness" href="/add-business">Add your Business</a></li>
                                <li><a class="mobAdvertise" href="/advertise-with-us">Advertise with us</a></li>
                            </ul>
                        </div>

                        <div class="footerEachNavCol">
                            <h5 class="footerHeading ypFooterHeading">Yellowpages</h5>
                            <ul class="footerNavList">
                                <li><a href="/about-us" class="mobAboutUs">About us</a></li>
                                <li><a href="/contact-us" class="mobContactUs">Contact us</a></li>
                                <li><a href="/feedback" class="mobFeedback">Feedback</a></li>
                                <li><a href="/frequently-asked-questions" class="mobFaqs">FAQs</a></li>
                                <li><a href="/careers" class="mobCareers">Careers</a></li>
                                <li><a href="/terms-and-conditions" class="mobTermsConditions">Terms &amp; Conditions</a></li>
                                <li><a href="/privacy-policy" class="mobPrivacyPolicy">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="footerSocialMobile">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Mobile &amp; Social</h5>
                            <div class="footerNavList">
                                <div class="footerSocialLinks">
                                    <span class="footerSocialMobTitle">Connect with us</span>
                                    <a href="https://www.facebook.com/Yellowpagesin-718593851493856/" class="footFacebook" target="_blank">Facebook</a>
                                    <a href="https://twitter.com/yellowpagesIn" class="footTwitter" target="_blank" >Twitter</a>
                                    <a href="https://plus.google.com/101163049029744212701" class="footGooglePlus" target="_blank" rel=”nofollow,publisher” >Google Plus</a>
                                    <a href="https://www.youtube.com/channel/UCJgrR6mz-HpliQ7QmrNvBlA" class="footYouTube" target="_blank" rel=”nofollow,publisher” >You Tube</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container copyRights">
            <div class="wrapper">
                Copyright &copy; 2017, Yellowpages.in. All rights reserved.
            </div>
        </div>


        <script src="<?php echo ASSETSURL; ?>front/js/jquery.min.js"></script>
        <?php /* <script src='<?php echo ASSETSURL; ?>front/js/common.js' ></script> */?>
        <script>
            function f1() {
                document.getElementById('light').style.display = 'none';
                document.getElementById('fade').style.display = 'none'

            }
            function addtofavorite(str) {
                if (str == "") {
                    document.getElementById("favpopup").innerHTML = "";
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            //document.getElementById("favpopup").innerHTML = this.responseText;
                                var txt = this.responseText;
                            
                            $('.notification').remove();
                            $(txt).appendTo('body');
                            
                           <?php   if (isset($this->session->userdata["id"])) { ?>
                            autoRemove();
                            <?php }  ?>
                        }
                    };
                    xmlhttp.open("GET", "<?php echo base_url(); ?>welcome/getfavorite_more?q=" + str, true);
                    xmlhttp.send();
                }
            }
            
            function autoRemove() {

 		setTimeout(function() {
 			$('.notification').removeClass('showNotification');
 		}, 5000);

 		setTimeout(function(){
		  $('.notification').remove();
		}, 5500);
            }
          $(document).on('click', 'div.closeNotification', function() {
                    //$('.notification').removeClass('showNotification');
                    //$('.notification').remove();
                    setTimeout(function() {
                            $('.notification').removeClass('showNotification');
                    }, 100);

                    setTimeout(function(){
                      $('.notification').remove();
                    }, 800);
            });
            
            function myfilter() {
                var url = location.href;
//                var url_passed_parameter = url.substring(url.indexOf("?") + 1);
//                var checkboxes = document.getElementsByName('filters[]');

                var categorySet = $('.filterTabInputsList');
                var checkboxSet = $('.filterTabInputsList.filters_list');
                var qryStr='';
                var filter='';
                var values='';
                var flag;
                
                if($('#category')[0].children.length !=0){
                    flag=false;
                    values=' category=';
                    for(var k=0; k< $('#category')[0].children.length; k++){
                     if($('#category')[0].children[k].children[0].children[0].checked){
                         flag=true;
                         values += $('#category')[0].children[k].children[0].children[0].value+",";
                     }
                    }
                     if(flag){
                         qryStr += values.substring(0,values.length-1);
                     }
                 }
                for(j=0;j<checkboxSet.length;j++){
                    flag=false;
                    filter = checkboxSet[j].id;
                    values='';
                    for(i=0;i<checkboxSet[j].children.length;i++){
                        if(checkboxSet[j].children[i].children[0].children[0].checked){
                            flag=true;
                            values += checkboxSet[j].children[i].children[0].children[0].value+",";
                        }
                    }
                    if(flag){
                        qryStr += "&"+filter+"="+values.substring(0,values.length-1); 
                    }
                }
//                qryStr = url_passed_parameter+qryStr;
                
                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("searchresults").innerHTML = this.responseText;
                    }
                };
                qryStr = qryStr.substr(1);
                if(qryStr.length){
                    var uri = encodeURI(location.origin+location.pathname+"?"+qryStr);
                }else{
                    uri = encodeURI(location.origin+location.pathname);
                }
                
                console.log(uri);
                window.location.href = uri;
                xmlhttp.open("POST", uri, true);
                xmlhttp.send();
            }
            
            function search(){
                var searchKey  = $('#search').val();
                var searchVal = $('#search')[0].attributes.data;
                var locationKey = $('#location').val();
                var locationValue = $('#location')[0].attributes.data;
                //alert(searchKey+' : '+searchVal+' : '+locationKey+' : '+locationValue);
                var url = "";
                if(typeof searchVal=="undefined"){
                    url = "/s?q="+searchKey;
                }else{
                    if(searchVal.split('_')[1] === "bus"){
                        url = "/b/"+searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase()+'/'+searchVal.split('_')[2];
                    }else if(searchVal.split('_')[1] === "cat"){
                        var cat_name = searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                        var id = searchVal.split('_')[2];
                        if(locationKey !== ''){
                            console.log(locationKey);
                            if(locationKey.split(',').length > 1){
                                var cityName = locationKey.split(',')[1].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                var areaName = locationKey.split(',')[0].trim();
                                areaName = areaName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+areaName+'/'+cat_name+'/'+id;
                            }else{
                                cityName = locationKey.split(',')[0].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+cat_name+'/'+id;
                            }
                        }else{
                            url = "/"+cat_name+'/'+id;
                        }
                    }
                }

                console.log(url);
                window.location.href = url;
                return false;
            }
            
            $(document).on('click', 'li.sresult', function(){
                var txt = $(this).text();
                $('#search').val(txt);
                $('#search')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
                search();
            });
            
            $(document).on('click', 'li.lresult', function(){
                var txt = $(this).text();
                $('#location').val(txt);
                $('#location')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
            });
            
            $(document).on('click','.searchButton',function(){
                search();
            });

            
             $(document).ready(function () {
                 
                $('.favoriteBtn').click(function() {
			$(this).toggleClass('active');
		});
		$('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
		    $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
		$('.mobileMenu').click(function () {
			if ($('.footerNavBlocks').hasClass('mobHide')) {
				$('.footerNavBlocks').removeClass('mobHide');
				//$('.main').addClass('showMobMenu');
				$('body').append("<div class='overlayMobNav'></div>");
			} else {
				$('.footerNavBlocks').addClass('mobHide');
				//$('.main').removeClass('showMobMenu');
				$('.overlayMobNav').remove();
			}
		});
		$('.mobSearch').click(function () {
			var body = $("html, body");
			body.stop().animate({scrollTop: 0}, '2000', 'swing', function () {
				$('.searchTextbox.typeahead').focus();
			});
		});
		$(document).on('click', 'div.overlayMobNav', function() {
			$('.footerNavBlocks').addClass('mobHide');
			$('.overlayMobNav').remove();
		});
                $('.mobSearchBtn').click(function() {
                    $('.innerSearchBlock').toggleClass('showInnerSearchBlock');
                });
                 
                 
                 
                 
                var added = false;
                $('#search').on('keyup', function () {
                    var search = $('#search').val();
                    if(search.length){
                        if(added===false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='bres'></div>");
                            added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_searchdata?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#bres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        added = false;
                    }
                });

                var l_added = false;
                $('#location').on('keyup', function () {
                    var search = $('#location').val();
                    if(search.length){
                        if(l_added === false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='lres'></div>");
                            l_added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_location?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#lres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        l_added = false;
                    }
                });

                $('.searchButton').on('click',function(){
                    search();
                });
                
                $('.homeSearchTextbox').on('focus', function() {
                    $(this).next('.searchResult').addClass('activeSearchTextbox');
                }).on('blur', function() {
                   if($(this).val() == '') {
                      $(this).next('.searchResult').removeClass('activeSearchTextbox');
                   }
                });
                
                
                if(location.href.indexOf("?") !== -1){
                    var qryStr = location.href.split("?")[1];
                    var qryPrms = qryStr.split("&");
                    for(var i=0;i<qryPrms.length;i++){
                        if(qryPrms[i].trim()){
                            var values = qryPrms[i].split("=")[1].split(",");
                            var search = qryPrms[i].split("=")[0];
                            //if(search=='category'){
                            for(var j=0;j<values.length;j++){
                                document.getElementById('cat_'+values[j]).checked = true;
                            }
                        //}
                        }
                    }
                }
                
                 
             });
        </script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72946436-1', 'auto');
        ga('send', 'pageview');

      </script>        
        <?php
        if (isset($this->session->userdata["id"])) {
            echo $js;
        }
        ?>
      <?php if ($this->uri->segment(1) == 's') { ?>
    <div class="popupBlock">
        <div class="popupOverlay"></div>
        <div class="popupMainBlock">
            <div class="popupMainContent">
                <div class="popupTitleCloseBlock">
                    <div class="popupTitle">More Filters</div>
                    <span class="closePopup"></span>
                </div>
                <div class="popupContent">
                    <div class="filtersBlock">
                        <div class="filtersFullBlock">
                            <div class="filtersEachCol">
                                <div class="filtersEachColHeading">
                                    Filter Type 1
                                </div>
                                <div class="filtersEachColListBlock">
                                    <ul class="filtersEachColList">
                                        <li>
                                            <label for="1" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="1">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="2" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="2">
                                                <span class="checkboxText">Filter Filter Filter Filter Filter Filter Filter Filter Filter Filter Filter 2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="3" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="3">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="4" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="4">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="5" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="5">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="6" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="6">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="7" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="7">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <div class="hideFiltersList">more...</div>
                                </div>
                            </div>

                            <div class="filtersEachCol">
                                <div class="filtersEachColHeading">
                                    Filter Type 2
                                </div>
                                <div class="filtersEachColListBlock">
                                    <ul class="filtersEachColList">
                                        <li>
                                            <label for="8" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="8">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="9" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="9">
                                                <span class="checkboxText">Filter 2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="10" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="10">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="11" class="eachFilter checkboxLabel">
                                                <input type="checkbox" class="checkbox" value="" id="11">
                                                <span class="checkboxText">Filter 1</span>
                                            </label>
                                        </li>

                                    </ul>
                                    <div class="hideFiltersList">more...</div>
                                </div>
                            </div>

                            <div class="filtersEachCol">
                                <div class="filtersEachColHeading">
                                    Filter Type 3
                                </div>
                                <div class="filtersEachColListBlock">
                                    <ul class="filtersEachColList">
                                        <li>
                                            <div class="filterSelectBox select">
                                                <span class="selectText">Range 1</span>
                                                <select name="" id="" class="selectBox">
                                                    <option value="1">Range 1</option>
                                                    <option value="2">Range 2</option>
                                                    <option value="3">Range 3</option>
                                                    <option value="4">Range 4</option>
                                                    <option value="5">Range 5</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="filterSelectBox select">
                                                <span class="selectText">Range 2</span>
                                                <select name="" id="" class="selectBox">
                                                    <option value="1">Range 1</option>
                                                    <option value="2">Range 2</option>
                                                    <option value="3">Range 3</option>
                                                    <option value="4">Range 4</option>
                                                    <option value="5">Range 5</option>
                                                </select>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="filtersEachCol">
                                <div class="filtersEachColHeading">
                                    Filter Type 4
                                </div>
                                <div class="filtersEachColListBlock">
                                    <ul class="filtersEachColList">
                                        <li>
                                            <label for="21" class="eachFilter radioLabel">
                                                <input type="radio" class="radio" name="radio" value="" id="21">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="22" class="eachFilter radioLabel">
                                                <input type="radio" class="radio" name="radio" value="" id="22">
                                                <span class="radioText">Filter 2</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="23" class="eachFilter checkboxLabel">
                                                <input type="radio" class="radio" name="radio" value="" id="23">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="24" class="eachFilter checkboxLabel">
                                                <input type="radio" class="radio" name="radio" id="24">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="25" class="eachFilter checkboxLabel">
                                                <input type="radio" class="radio" name="radio" id="25">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="26" class="eachFilter checkboxLabel">
                                                <input type="radio" class="radio" name="radio" id="26">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label for="27" class="eachFilter checkboxLabel">
                                                <input type="radio" class="radio" name="radio" id="27">
                                                <span class="radioText">Filter 1</span>
                                            </label>
                                        </li>

                                    </ul>
                                    <div class="hideFiltersList">more...</div>
                                </div>
                            </div>
                        </div>

                        <div class="mobileActions">
                            <input type="button" class="applyFilters" value="Apply filters">
                            <input type="button" class="cancelFilters" value="Cancel">
                        </div>

                        <div class="filterClearFilter">
                            <div class="clearAllBtn">Clear All</div>
                            <button class="moreFiltersBtn">More filters</<button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                <?php } ?>

                                <script>
                                    $(document).ready(function () {
                                        $('.popup').click(function () {
                                            $('.popupBlock').addClass('showPopup');
                                            $('body').addClass('noScroll');
                                        });
                                        $('.closePopup').click(function () {
                                            $('.popupBlock').removeClass('showPopup');
                                            $('body').removeClass('noScroll');
                                        });

                                        $('.filtersEachColHeading').click(function () {
                                            if ($('.filtersEachColListBlock').hasClass('showFilters')) {
                                                $('.filtersEachColListBlock').removeClass('showFilters');
                                                $('.filterClearFilter').removeClass('showClearFilters');
                                            } else {
                                                $('.filtersEachColListBlock').addClass('showFilters');
                                                $('.filterClearFilter').addClass('showClearFilters');
                                            }
                                        });
                                        
                                        $('.filterTabTitle').click(function() {
                                        var c = $(this).parent().find('.filterTabContent');
                                        if($(this).hasClass('activeTab')) {
                                                $(this).removeClass('activeTab');
                                                $(this).next('.filterTabContent').removeClass('showFilters');
                                                $('.filtersActions').removeClass('showActions');
                                        } else {
                                                $('.filterTabTitle').removeClass('activeTab');
                                                $('.filterTabContent').removeClass('showFilters');

                                                $(this).addClass('activeTab');
                                                $(this).next('.filterTabContent').addClass('showFilters');
                                                $('.filtersActions').addClass('showActions');
                                        }
                                        });

                                        $('.mobFilterBtn').click(function() {
                                                if($('.filtersBlock').hasClass('showFiltersMobile')) {
                                                        $('.filtersBlock').removeClass('showFiltersMobile');
                                                } else {
                                                        $('.filtersBlock').addClass('showFiltersMobile');
                                                }
                                        });

                                        $('#mobileFilterClose').click(function() {
                                                $('.filtersBlock').removeClass('showFiltersMobile');
                                        });

                                        $('#desktopFilterClose').click(function() {
                                                $('.filterTabTitle').removeClass('activeTab');
                                                $('.filterTabContent').removeClass('showFilters');
                                                $('.filtersActions').removeClass('showActions');
                                        });

                                        // popup js
                                        $('#get-quotes').click(function () {
                                            $('.popupNew').addClass('showPopup');
                                        });

                                        $('.popupClose').click(function () {
                                            $('.popupNew').removeClass('showPopup');
                                        });

                                        //Text box on focus label change

                                        $('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
                                            $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
                                        }).trigger('blur');
                                    });
                                </script>

                                <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/popup.css">

                                <div class="popupNew">
                                    <div class="popupOverlayNew"></div>

                                    <div class="popupMainBlock">
                                        <div class="popupMainContent">
                                            <div class="popupHeader">
                                                <div class="popupTitle">
                                                    Get Quotes
                                                </div>
                                                <div class="popupClose">Close</div>
                                            </div>

                                            <div class="popupContent">
                                                <div class="popupFormBlock">
                                                    <duv class="popupFormListBlock">
                                                        <ul class="popupFormList">
                                                            <li>
                                                                <div class="eachFormElement">
                                                                    <p class="smallInfo">Please select number of business owners you wants to contact you for required services.</p>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="eachFormElement">
                                                                    <input type="text" class="popupFormTextBox" value="">
                                                                    <label for="" class="popupFormLabel">Please enter your name*</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="eachFormElement phone">
                                                                    <span class="number">+91</span>
                                                                    <input type="text" class="popupFormTextBox" value="">
                                                                    <label for="" class="popupFormLabel">Please enter your mobile number*</label>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <div class="eachFormElement">
                                                                    <input type="email" class="popupFormTextBox" value="">
                                                                    <label for="" class="popupFormLabel">Email*</label>
                                                                </div>
                                                            </li>




                                                            <li>
                                                                <p class="smallInfo">I want</p>
                                                                <div class="eachFormElement eachFormElementInline">
                                                                    <label for="phone" class="radio">
                                                                        <input type="radio" class="radioInput" id="phone" name="radio">
                                                                        <span class="radioText">2 Leads</span>
                                                                    </label>

                                                                    <label for="phone1" class="radio">
                                                                        <input type="radio" class="radioInput" id="phone1" name="radio">
                                                                        <span class="radioText">3 Leads</span>
                                                                    </label>

                                                                    <label for="phone2" class="radio">
                                                                        <input type="radio" class="radioInput" id="phone2" name="radio">
                                                                        <span class="radioText">5 Leads</span>
                                                                    </label>

                                                                    <label for="phone3" class="radio">
                                                                        <input type="radio" class="radioInput" id="phone3" name="radio">
                                                                        <span class="radioText">10 Leads</span>
                                                                    </label>

                                                                    <label for="phone4" class="radio">
                                                                        <input type="radio" class="radioInput" id="phone4" name="radio">
                                                                        <span class="radioText">15 Leads</span>
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="eachFormElement phone">
                                                                    <input type="submit" class="popupButton" value="Submit">
                                                                    <input type="button" class="cancelButton popupClose" value="Cancel">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
    </body>
</html>
<script>
          $('.haveSubMenu').click(function() {
            $('.subMenuInner').toggleClass('showSubMenu');
        });
</script>
