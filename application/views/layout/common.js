$(document).ready(function() {
		$('.favoriteBtn').click(function() {
			$(this).toggleClass('active');
		});

		$('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
		    $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
		
		$('.mobileMenu').click(function () {
			if ($('.footerNavBlocks').hasClass('mobHide')) {
				$('.footerNavBlocks').removeClass('mobHide');
				//$('.main').addClass('showMobMenu');
				$('body').append("<div class='overlayMobNav'></div>");
			} else {
				$('.footerNavBlocks').addClass('mobHide');
				//$('.main').removeClass('showMobMenu');
				$('.overlayMobNav').remove();
			}
		});

		$('.mobSearch').click(function () {
			var body = $("html, body");
			body.stop().animate({scrollTop: 0}, '2000', 'swing', function () {
				$('.searchTextbox.typeahead').focus();
			});
		});
		$(document).on('click', 'div.overlayMobNav', function() {
			$('.footerNavBlocks').addClass('mobHide');
			$('.overlayMobNav').remove();
		});
  });