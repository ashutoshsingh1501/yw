<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <?php  if ($this->uri->segment(1) == 'b'){ ?>
                <title><?php echo $bbusiness[0]['title']; ?> - Yellow Pages </title>
        <?php }else{ ?>
            <title>Yellow Pages </title>
        <?php }?>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="shortcut icon" href="<?php echo STYLEURL; ?>front/images/favicon.ico" type="image/x-icon" />
        <?php
        $static_pages_arr = array();
        array_push($static_pages_arr, 'info');
        array_push($static_pages_arr, 'advertise-with-us');
        array_push($static_pages_arr, 'advertise-inquiry');
        array_push($static_pages_arr, 'about-us');
        array_push($static_pages_arr, 'contact-us');
        array_push($static_pages_arr, 'send-inquiry');
        array_push($static_pages_arr, 'feedback');
        array_push($static_pages_arr, 'submit-feedback');
        array_push($static_pages_arr, 'frequently-asked-questions');
        array_push($static_pages_arr, 'careers');
        array_push($static_pages_arr, 'terms-and-conditions');
        array_push($static_pages_arr, 'privacy-policy');
        array_push($static_pages_arr, 'end-user-agreement');
        ?>
        <?php if ($this->uri->segment(1) == 'my-account') { ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-loginRegister.css">
        <?php
        }if ($this->uri->segment(1) == 'categories') {
            ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-categories.css">
            <?php
        }if ($this->uri->segment(1) == 'add-business') {
            ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-addBusiness.css">
            <?php
        }if ($this->uri->segment(1) == 'b') {
            ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-bDetail.css">
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/popup.css">
            <?php
        }if (in_array($this->uri->segment(1), $static_pages_arr)) {
            ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-staticPages.css">
            <?php
        }
        $account_arr = array();
        array_push($account_arr, 'my-account');
        array_push($account_arr, 'register');
        array_push($account_arr, 'my-favorites');
        array_push($account_arr, 'my-reviews');
        array_push($account_arr, 'change-password');

        if (in_array($this->uri->segment(1), $account_arr)) {
            ?>
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-loginRegister.css">
            <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-myAccount.css">
            <?php
        }
        ?>
         <?php if ($this->uri->segment(1) == 'my-account' || $this->uri->segment(1) == 'my-favorites' || $this->uri->segment(1) == 'my-reviews') { ?>
        
        <?php if (isset($this->session->userdata["id"])) { ?>
            <style type="text/css">
        #loading{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container .pagination ul li.inactive,#container .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container .pagination{width:800px;height:25px}#container .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_reviews{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_reviews .pagination ul li.inactive,#container_reviews .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_reviews .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_reviews .pagination{width:800px;height:25px}#container_reviews .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_reviews .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_business_reviews{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_business_reviews .pagination ul li.inactive,#container_business_reviews .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_business_reviews .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_business_reviews .pagination{width:800px;height:25px}#container_business_reviews .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_business_reviews .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_business_offers{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_business_offers .pagination ul li.inactive,#container_business_offers .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_business_offers .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_business_offers .pagination{width:800px;height:25px}#container_business_offers .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_business_offers .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}
        </style>    
        <?php } 
        } else if($this->uri->segment(1) == 'b') { ?>
           <style type="text/css">
        #loading{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container .pagination ul li.inactive,#container .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container .pagination{width:800px;height:25px}#container .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_reviews{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_reviews .pagination ul li.inactive,#container_reviews .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_reviews .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_reviews .pagination{width:800px;height:25px}#container_reviews .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_reviews .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_business_reviews{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_business_reviews .pagination ul li.inactive,#container_business_reviews .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_business_reviews .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_business_reviews .pagination{width:800px;height:25px}#container_business_reviews .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_business_reviews .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}#loading_business_offers{width:100%;position:absolute;top:100px;left:100px;margin-top:200px}#container_business_offers .pagination ul li.inactive,#container_business_offers .pagination ul li.inactive:hover{background-color:#ededed;color:#bababa;border:1px solid #bababa;cursor:default}#container_business_offers .data ul li{list-style:none;font-family:verdana;margin:5px 0;color:#000;font-size:13px}#container_business_offers .pagination{width:800px;height:25px}#container_business_offers .pagination ul li{list-style:none;float:left;border:1px solid #069;padding:2px 6px;margin:0 3px;font-family:arial;font-size:14px;color:#069;font-weight:700;background-color:#f2f2f2}#container_business_offers .pagination ul li:hover{color:#fff;background-color:#069;cursor:pointer}.go_button{background-color:#f2f2f2;border:1px solid #069;color:#c00;padding:2px 6px;cursor:pointer;position:absolute;margin-top:-1px}.total{float:right;font-family:arial;color:#999}.white_content{display:none;position:absolute;top:200px;left:25%;width:50%;height:250px;padding:16px;border:16px solid orange;background-color:#fff;z-index:1002;overflow:auto}
        </style> 
        <?php  } ?>
        <script src="<?php echo ASSETSURL; ?>front/js/jquery.min.js"></script>
    </head>
    <body>
        <div class="main" id="main">
            <div class="innerHeader container desktopHeader">
                <div class="wrapper">
                    <a href="/" class="branding">Yellow Pages</a>
                    <div class="innerSearchBlock">
                        <form class="homeSearchBlock" id="qt-search" autocomplete="off">
                            <div class="searchFullBlock">
                                <div class="searchBusiness">
                                    <input type="text" value="" id="search" class="homeSearchTextbox businessInputField" placeholder="Search local business in India" autocomplete="off">
                                </div>
                                <div class="searchLocation">
                                        <input type="text" class="homeSearchTextbox locationInputField" value="Hyderabad" id="location">
                                </div>
                                <div class="searchButtonBlock">
                                        <div class="searchButton">Search <i class="searchIcon"></i></div>
                                </div>
                            </div>
                    </form>
                    </div>
                    <ul class="innerTopNavList">
                        <li><a href="/add-business">Add business</a></li>
                        <li><a href="/categories">Categories</a></li>
                        <?php if (isset($this->session->userdata["active"])) { ?>
                            <li>
                            <span class="eachTopLink haveSubMenu">My Account</span>
                                <ul class="subMenuInner">
                                        <li><a href="/my-favorites">My Favorites</a></li>
                                        <li><a href="/my-reviews">My Reviews</a></li>
                            <li><a href="/sign-out">Sign Out</a></li>
                                </ul>
                            </li>
                        <?php } else{ ?>
                        <li><a href="/my-account">My Account</a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?= $content; ?>
        </div> 
        <div class="container footer">
            <div class="wrapper">
                <div class="footerNavBlocks mobHide">
                    <div class="mobileNavBlocks">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Quick links</h5>
                            <ul class="footerNavList">
                                <li class="active"><a href="/" class="mobHome">Home</a></li>
                                <li><a href="/categories" class="mobCategories">Browse category</a></li>
                                <li><a class="mobSearch">Search business</a></li>
                                <?php if (isset($this->session->userdata["active"])) { ?>
                                    <li><a href="/my-favorites" class="mobMyFavorites">My Favorites</a></li>
                                    <li><a href="/my-reviews" class="mobMyReviews">My Reviews</a></li>
                                    <li><a href="/change-password" class="mobChangePassword">Change Password</a></li>
                                    <li><a href="/sign-out" class="mobSignout">Sign Out</a></li>
                                <?php }else{?>
                                    <li><a href="/my-account" class="mobLogin">Login to your account</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="footerEachNavCol businessMenu">
                            <h5 class="footerHeading">Business</h5>
                            <ul class="footerNavList">
                                <li><a class="mobAddBusiness" href="/add-business">Add your Business</a></li>
                                <li><a class="mobAdvertise" href="/advertise-with-us">Advertise with us</a></li>
                            </ul>
                        </div>
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading ypFooterHeading">Yellowpages</h5>
                            <ul class="footerNavList">
                                <li><a href="/about-us" class="mobAboutUs">About us</a></li>
                                <li><a href="/contact-us" class="mobContactUs">Contact us</a></li>
                                <li><a href="/feedback" class="mobFeedback">Feedback</a></li>
                                <li><a href="/frequently-asked-questions" class="mobFaqs">FAQs</a></li>
                                <li><a href="/careers" class="mobCareers">Careers</a></li>
                                <li><a href="/terms-and-conditions" class="mobTermsConditions">Terms &amp; Conditions</a></li>
                                <li><a href="/privacy-policy" class="mobPrivacyPolicy">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footerSocialMobile">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Mobile &amp; Social</h5>
                            <div class="footerNavList">
                                <?php /* <a href="#" class="checkCallers">Check unknown callers</a> */ ?>
                                <div class="footerSocialLinks">
                                    <span class="footerSocialMobTitle">Connect with us</span>
                                    <a href="https://www.facebook.com/Yellowpagesin-718593851493856/" class="footFacebook" target="_blank">Facebook</a>
                                    <a href="https://twitter.com/yellowpagesIn" class="footTwitter" target="_blank">Twitter</a>
                                    <a href="https://plus.google.com/101163049029744212701" class="footGooglePlus" target="_blank" rel=”publisher,nofollow" >Google Plus</a>
                                    <a href="https://www.youtube.com/channel/UCJgrR6mz-HpliQ7QmrNvBlA" class="footYouTube" target="_blank" rel=”publisher,nofollow" >You Tube</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container copyRights">
            <div class="wrapper">
                Copyright &copy; 2017, Yellowpages.in. All rights reserved.
            </div>
        </div>
                 
        <?php /*  Business Send SMS  */
        if ($this->uri->segment(1) == 'b') { ?>

        <div class="popupNew" id="sendSMS">
            <div class="popupOverlayNew"></div>

            <div class="popupMainBlock">
                <div class="popupMainContent">
                    <div class="popupHeader">
                        <div class="popupTitle">
                            Send Business Details
                        </div>
                        <div class="popupClose">Close</div>
                    </div>
                    <?php echo form_open("sendsms", array('id' => 'sendsmsmes')); ?>
                    <div class="popupContent">
                        <div class="popupFormBlock">
                            <duv class="popupFormListBlock">
                                <ul class="popupFormList">
                                    <li>
                                        <div class="eachFormElement">
                                            <input type="tel" class="popupFormTextBox" value="" id="user_mobile" name="user_mobile">
                                            <label for="" class="popupFormLabel">Your Mobile Number*</label>
                                            <input type='hidden' id='business_id' name='business_id'>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="submit" class="popupButton" value="Send SMS">
                                            <input type="button" class="cancelButton popupClose" value="Cancel">
                                        </div>
                                    </li>
                                </ul>
                            </duv>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>




        <?php /*  Get to my INBOX  */ ?>

        <div class="popupNew" id="sendMyInbox">
            <div class="popupOverlayNew"></div>

            <div class="popupMainBlock">
                <div class="popupMainContent">
                    <div class="popupHeader">
                        <div class="popupTitle">
                            Send to my Inbox
                        </div>
                        <div class="popupClose">Close</div>
                    </div>

                    <?php echo form_open("mail-coupon"); ?>
                    <div class="popupContent">
                        <div class="popupFormBlock">
                            <div class="popupFormListBlock">
                                <ul class="popupFormList">
                                    <li>
                                        <div class="eachFormElement">
                                            <input type="email" class="popupFormTextBox" value="" id="user_email" name="user_email">
                                            <label for="" class="popupFormLabel">Your Email ID*</label>
                                            <div class="hid_id"><div id="hid_id"></div></div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="submit" class="popupButton" value="Send Now">
                                            <input type="button" class="cancelButton popupClose" value="Cancel">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>


        <?php /*  Business Suggest Edit */ ?>


        <div class="popupNew" id="suggestEdit">
            <div class="popupOverlayNew"></div>

            <div class="popupMainBlock">
                <div class="popupMainContent">
                    <div class="popupHeader">
                        <div class="popupTitle">
                            Suggest Edit
                        </div>
                        <div class="popupClose">Close</div>
                    </div>
                    <?php echo form_open("suggest-edit"); ?>
                    <div class="popupContent">
                        <div class="popupFormBlock">
                            <div class="popupFormListBlock">
                                <ul class="popupFormList">
                                    <li>
                                        <div class="eachFormElement">
                                            <input type="text" class="popupFormTextBox" name="name" id="name" value="<?php echo set_value('name'); ?>">
                                            <label for="" class="popupFormLabel">Your Name*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="tel" class="popupFormTextBox" name="mobile" id="mobile" value="<?php echo set_value('mobile'); ?>" >
                                            <label for="" class="popupFormLabel">Your Mobile*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="email" class="popupFormTextBox" name="email" id="email" value="<?php echo set_value('email'); ?>">
                                            <label for="" class="popupFormLabel">Your Email ID*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <textarea name="message" id="message" class="popupFormTextarea" rows="5"><?php echo set_value('messagge'); ?></textarea>
                                            <label for="" class="popupFormLabel">Message*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="submit" class="popupButton" value="Suggest Now">
                                            <input type="button" class="cancelButton popupClose" value="Cancel">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>

                </div>
            </div>

        </div>

        <?php /*  Report this business */ ?>
        <div class="popupNew red" id="reportBusiness">
            <div class="popupOverlayNew"></div>
            <div class="popupMainBlock">
                <div class="popupMainContent">
                    <div class="popupHeader">
                        <div class="popupTitle">
                            Report this business
                        </div>
                        <div class="popupClose">Close</div>
                    </div>
                    <?php echo form_open("report-business"); ?>	
                    <div class="popupContent">
                        <div class="popupFormBlock">
                            <div class="popupFormListBlock">
                                <ul class="popupFormList">
                                    <li>
                                        <div class="eachFormElement twoColumn">
                                            <label for="phoneIncorrect" class="checkbox">
                                                <input type="checkbox" class="checkboxInput" id="phoneIncorrect" name="phoneIncorrect" value="Phone Number is Incorrect">
                                                <span class="checkboxText">Phone is Incorrect</span>
                                            </label>
                                        </div>

                                        <div class="eachFormElement twoColumn">
                                            <label for="emailIncorrect" class="checkbox">
                                                <input type="checkbox" class="checkboxInput" id="emailIncorrect" name="emailIncorrect" value="Email is Incorrect">
                                                <span class="checkboxText">Email is Incorrect</span>
                                            </label>
                                        </div>

                                        <div class="eachFormElement twoColumn">
                                            <label for="addressIncorrect" class="checkbox">
                                                <input type="checkbox" class="checkboxInput" id="addressIncorrect" name="addressIncorrect" value="Address is Incorrect">
                                                <span class="checkboxText">Address is Incorrect</span>
                                            </label>
                                        </div>

                                        <div class="eachFormElement twoColumn">
                                            <label for="businessClosed" class="checkbox">
                                                <input type="checkbox" class="checkboxInput" id="businessClosed" name="businessClosed" value="Business Closed">
                                                <span class="checkboxText">Business is closed</span>
                                            </label>
                                        </div>

                                        <div class="eachFormElement twoColumn">
                                            <label for="others" class="checkbox">
                                                <input type="checkbox" class="checkboxInput" id="others" name="others" value="Others">
                                                <span class="checkboxText">Others</span>
                                            </label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="text" class="popupFormTextBox" id="user_name" name="user_name">
                                            <label for="" class="popupFormLabel">Your name*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="tel" class="popupFormTextBox" id="user_contact" name="user_contact">
                                            <label for="" class="popupFormLabel">Your Mobile*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="email" class="popupFormTextBox" id="user_email" name="user_email">
                                            <label for="" class="popupFormLabel">Your Email ID*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <textarea name="user_message" id="user_message" class="popupFormTextarea" rows="5"></textarea>
                                            <label for="" class="popupFormLabel">Message*</label>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="eachFormElement">
                                            <input type="submit" class="popupButton" value="Report Now">
                                            <input type="button" class="cancelButton popupClose" value="Cancel">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <?php /*  Image popup */ ?>
        <?php if($bbusiness[0]['carousel_count']>1){?>
            <div class="popupNew imagesPopup" id="imagesPopup">
                <div class="popupOverlayNew"></div>
                <div class="popupMainBlock">
                    <div class="popupMainContent">
                        <div class="popupHeader">
                            <div class="popupTitle">
                                <?php echo "More images for ".$bbusiness[0]['title'];?>
                            </div>
                            <div class="popupClose">Close</div>
                        </div>
                        <div class="popupContent">
                            <div class="bDetailImagesCrouselBlock">
                                <div class="bDetailImagesCrousel" id="bDetailImagesCrousel">
                                    <?php for($i=$bbusiness[0]['carousel_count'];$i>1;$i--){ ?>
                                        <div class="eachbDetalImageBlock">
                                            <img src="<?php echo STYLEURL . 'business-images/' . $bbusiness[0]['image_note'] . '/1170x800_' . $bbusiness[0]['image_note'] . '-'.$i.'.jpg'; ?>" alt="" style="max-height: 800px">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }?>
        <?php } ?>
        <?php if($this->uri->segment(1) == 'my-account') { ?>
			<script src="<?php echo ASSETSURL; ?>front/js/jquery.validate.min.js"></script>
          <?php if (!isset($this->session->userdata["id"])) { ?>
            <style type="text/css">
                .white_content{display:none;position:absolute;top:200px;left:25%;width:50%;height:250px;padding:16px;border:16px solid orange;background-color:#fff;z-index:1002;overflow:auto}
            </style>
        
        <?php } } else if($this->uri->segment(1) == 'register' || $this->uri->segment(1) == 'b' || $this->uri->segment(1) == 'change-password' || $this->uri->segment(1) == 'add-business') {  ?>
        <script src="<?php echo ASSETSURL; ?>front/js/jquery.validate.min.js"></script>
        <?php } ?>
         <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>
       <script type="text/javascript">
           
            function search(){
                var searchKey  = $('#search').val();
                var searchVal = $('#search')[0].attributes.data;
                var locationKey = $('#location').val();
                var locationValue = $('#location')[0].attributes.data;
                //alert(searchKey+' : '+searchVal+' : '+locationKey+' : '+locationValue);
                var url = "";
                if(typeof searchVal=="undefined"){
                    url = "/s?q="+searchKey;
                }else{
                    if(searchVal.split('_')[1] === "bus"){
                        url = "/b/"+searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase()+'/'+searchVal.split('_')[2];
                    }else if(searchVal.split('_')[1] === "cat"){
                        var cat_name = searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                        var id = searchVal.split('_')[2];
                        if(locationKey !== ''){
                            console.log(locationKey);
                            if(locationKey.split(',').length > 1){
                                var cityName = locationKey.split(',')[1].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                var areaName = locationKey.split(',')[0].trim();
                                areaName = areaName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+areaName+'/'+cat_name+'/'+id;
                            }else{
                                cityName = locationKey.split(',')[0].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+cat_name+'/'+id;
                            }
                        }else{
                            url = "/"+cat_name+'/'+id;
                        }
                    }
                }

                console.log(url);
                window.location.href = url;
                return false;
            }
            
            $(document).on('click', 'li.sresult', function(){
                var txt = $(this).text();
                $('#search').val(txt);
                $('#search')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
                search();
            });
            
            $(document).on('click', 'li.lresult', function(){
                var txt = $(this).text();
                $('#location').val(txt);
                $('#location')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
            });
            
            $(document).on('click','.searchButton',function(){
                search();
            });
                      
            $(document).on('click', 'li.cresult', function(){
                var txt = $(this).text();
                $('#category').val(txt);
                $('#category')[0].attributes.data=this.id;
                $('.categoriesList li').remove();
            });
                      
           <?php if ($this->uri->segment(1) == 'my-account' || $this->uri->segment(1) == 'my-favorites') { ?>
           <?php
           if(isset($search_result)){
            foreach ($search_result as $key => $r) { ?>
                function getmy_favorite<?php echo $r->id; ?>(str) {
                    if (str == "") {
                        document.getElementById("myfav_bus<?php echo $r->id; ?>").innerHTML = "";
                        return;
                    } else {
                        if (window.XMLHttpRequest) {
                            // code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        } else {
                            // code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (this.readyState == 4 && this.status == 200) {
                                //document.getElementById("myfav_bus<?php echo $r->id; ?>").innerHTML = this.responseText;
                                var txt = this.responseText;
                                $('.notification').remove();
                                $(txt).appendTo('body');
                        <?php if (isset($this->session->userdata["id"])) { ?>
                                    autoRemove();
                        <?php } ?>
                            }
                        };
                        xmlhttp.open("GET", "<?php echo base_url(); ?>welcome/getmyaccount_favorite?q=" + str, true);
                        xmlhttp.send();
                    }
                }
                  
            <?php } 
                } 
            } ?> 
        
            function ratings(rate) {
                window.location.assign("<?php echo base_url(); ?>myaccount/rating_order/" + rate);
            }
          
            function ratings_favorite(rate) {
                window.location.assign("<?php echo base_url(); ?>myaccount/rate_favorite_order/" + rate);
            }
            function favorite_date() {
                window.location.assign("<?php echo base_url(); ?>myaccount/favorite_date/");
            }
            function close_favorite() {
                window.location.assign("<?php echo base_url(); ?>my-favorites/");
            }
            <?php if ($this->uri->segment(1) == 'business_detail') { ?>
            function business_ratings(rate, bid) {
                window.location.assign("<?php echo base_url(); ?>myaccount/business_rating_order/" + rate + "/" + bid);
            }
            function myvote(review_id, vote) {
                if (review_id == "") {
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {

                            var txt = this.responseText;

                            $('.notification').remove();
                            $(txt).appendTo('body');
                            <?php if (isset($this->session->userdata["id"])) { ?>
                                autoRemove();
                            <?php } ?>
                        }
                    };
                    xmlhttp.open("GET", "<?php echo base_url(); ?>welcome/getreviewvote?review_id=" + review_id + "&vote=" + vote, true);
                    xmlhttp.send();
                }
            }
            // When the browser is ready...
            function favorite_business(str) {
                if (str == "") {
                    document.getElementById("response").innerHTML = "";
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            //document.getElementById("txtHint").innerHTML = this.responseText;
                            var txt = this.responseText;
                            $('.notification').remove();
                            $(txt).appendTo('body');
                            <?php if (isset($this->session->userdata["id"])) { ?>
                                autoRemove();
                            <?php } ?>
                        }
                    };
                    xmlhttp.open("GET", "<?php echo base_url(); ?>welcome/getfavorite_more?q=" + str, true);
                    xmlhttp.send();
                }
            }
        <?php } if ($this->uri->segment(1) == 'business_detail' || $this->uri->segment(1) == 'my-account') { ?>
        <?php if (isset($this->session->userdata["id"])) { ?>
             function autoRemove() {
                setTimeout(function () {
                    $('.notification').removeClass('showNotification');
                }, 5000);

                setTimeout(function () {
                    $('.notification').remove();

                }, 5500);
            }
        <?php } } ?>
            
            $(document).ready(function () {
                
                var added = false;
                $('#search').on('keyup', function () {
                    var search = $('#search').val();
                    if(search.length){
                        if(added===false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='bres'></div>");
                            added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_searchdata?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#bres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        added = false;
                    }
                });

                var l_added = false;
                $('#location').on('keyup', function () {
                    var search = $('#location').val();
                    if(search.length){
                        if(l_added === false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='lres'></div>");
                            l_added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_location?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#lres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        l_added = false;
                    }
                });
                
                $('.searchButton').on('click',function(){
                    search();
                });
                
                $('.homeSearchTextbox').on('focus', function() {
                    $(this).next('.searchResult').addClass('activeSearchTextbox');
                }).on('blur', function() {
                   if($(this).val() == '') {
                      $(this).next('.searchResult').removeClass('activeSearchTextbox');
                   }
                });
                
               
            <?php
            if ($this->uri->segment(1) == 'business_detail') { 
                $i = 1; 
                if(isset($breview)){
                    foreach ($breview as $key => $br) {
                ?>
                    $("#upclick<?php echo $i; ?>").click(function () {
                        var vote = $("#upvote_id<?php echo $i; ?>").val();
                        var id = $("#id<?php echo $i; ?>").val();
                        var review_id = $("#review_id<?php echo $i; ?>").val();

                        var dataString = 'vote=' + vote + '&id=' + id + '&review_id=' + review_id;

                        if (review_id == '')
                        {
                            alert("Please describe about this business");
                        } else
                        {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>welcome/getvote",
                                data: dataString,
                                cache: false,
                                success: function (result) {
                                    //$('#review_responce<?php echo $i; ?>').html(result);
                                    $('.notification').remove();
                                    $(result).appendTo('body');
                                    <?php if(isset($this->session->userdata["id"])) { ?>
                                        autoRemove();
                                    <?php } ?>
                                }
                            });
                        }
                        return false;
                    });
                <?php
                    $i++;
                    } 
                }
                ?>
                <?php
                    $i = 1;
                    if(isset($breview)){
                        foreach ($breview as $key => $br) {
                    ?>
                    $("#downclick<?php echo $i; ?>").click(function () {

                        var vote = $("#downvote_id<?php echo $i; ?>").val();
                        var id = $("#id<?php echo $i; ?>").val();
                        var review_id = $("#review_id<?php echo $i; ?>").val();

                        var dataString = 'vote=' + vote + '&id=' + id + '&review_id=' + review_id;
                        if (review_id == '')
                        {
                            alert("Please describe about this business");
                        } else
                        {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>welcome/getvote",
                                data: dataString,
                                cache: false,
                                success: function (result) {
                                    // $('#review_responce<?php echo $i; ?>').html(result);
                                    $('.notification').remove();
                                    $(result).appendTo('body');
                                    <?php if (isset($this->session->userdata["id"])) { ?>
                                        autoRemove();
                                    <?php } ?>
                                }
                            });
                        }
                        return false;
                    });
                <?php
                        $i++;
                        }
                    }
                }?>
                <?php if (isset($this->session->userdata["id"])) { ?>
                <?php if ($this->uri->segment(1) == 'my-account' || $this->uri->segment(1) == 'my-favorites' ) { ?>
 
                function loading_myfavorite_show() {
                    $('#loading').html("<img src='<?php echo STYLEURL . "front/images/loading.gif" ?>'/>").fadeIn('fast');
                }
                function loading_myfavorite_hide() {
                    $('#loading').fadeOut('fast');
                }
             
                function load_myfavorite_Data(page) {
                    loading_myfavorite_show();
                    $.ajax
                            ({
                                type: "POST",
                                url: "<?php echo base_url(); ?>myaccount/getdata_list",
                                data: "page=" + page,
                                success: function (msg)
                                {
                                    $("#container").ajaxComplete(function (event, request, settings)
                                    {
                                        loading_myfavorite_hide();
                                        $("#container").html(msg);
                                    });
                                }
                            });
                }
             
                load_myfavorite_Data(1);  // For first time page load default results
      
                $('#container .pagination li.active').live('click', function () {
                    var page = $(this).attr('p');
                    load_myfavorite_Data(page);
                });
                
                $('#go_btn').live('click', function () {
                    var page = parseInt($('.goto').val());
                    var no_of_pages = parseInt($('.total').attr('a'));
                    if (page != 0 && page <= no_of_pages) {
                        load_myfavorite_Data(page);
                    } else {
                        alert('Enter a PAGE between 1 and ' + no_of_pages);
                        $('.goto').val("").focus();
                        return false;
                    }

                });
                <?php } } ?>
                <?php if ($this->uri->segment(1) == 'business_detail') { ?>
                function loading_businessreviews_show() {
                    $('#loading_business_reviews').html("<img src='<?php echo STYLEURL . "front/images/loading.gif" ?>'/>").fadeIn('fast');
                }
                function loading_businessreviews_hide() {
                    $('#loading_business_reviews').fadeOut('fast');
                }
                function load_businessreviews_Data(page) {
                    loading_businessreviews_show();
                    $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Business_detail/getreviews_list/<?php echo $bbid; ?>",
                            data: "page=" + page,
                            success: function (msg)
                            {
                                $("#container_business_reviews").ajaxComplete(function (event, request, settings)
                                {
                                    loading_businessreviews_hide();
                                    $("#container_business_reviews").html(msg);
                                });
                            }
                    });
                }
                load_businessreviews_Data(1);  // For first time page load default results
             
                $('#container_business_reviews .pagination li.active').live('click', function () {
                    var page = $(this).attr('p');
                    load_businessreviews_Data(page);

                });
                $('#go_btn').live('click', function () {
                    var page = parseInt($('.goto').val());
                    var no_of_pages = parseInt($('.total').attr('a'));
                    if (page != 0 && page <= no_of_pages) {
                        load_businessreviews_Data(page);
                    } else {
                        alert('Enter a PAGE between 1 and ' + no_of_pages);
                        $('.goto').val("").focus();
                        return false;
                    }

                });
                $("#post_review").click(function () {
                var rating = $(".radioRating:checked").val();

                var rating_desc = $("#rating_desc").val();
                var bbbid = $("#bbbid").val();

                var dataString = 'rating1=' + rating + '&bid=' + bbbid + '&rating_desc1=' + rating_desc;
                if (rating == '')
                {
                    alert("Please rate this business");
                } else
                if (rating_desc == '')
                {
                    alert("Please describe about this business");
                } else
                {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>welcome/getrating",
                        data: dataString,
                        cache: false,
                        success: function (result) {
                            //$('#review_responce').html(result);
                            $('.notification').remove();
                            $(result).appendTo('body');
                <?php if (isset($this->session->userdata["id"])) { ?>
                                    setTimeout(function () {
                                        $('.notification').removeClass('showNotification');
                                    }, 1000);

                                    setTimeout(function () {
                                        $('.notification').remove();
                                        window.location.reload();
                                    }, 1300);

                <?php } ?>
                            }
                        });
                    }
                    return false;
                });
                 $('.snedSMSBtn').click(function () {
                if ($('.shareBtn').hasClass('activeTooltip')) {
                    $('.shareBtn').removeClass('activeTooltip');
                }
                $('#sendSMS').addClass('showPopup');
                var id = $('#sms').val();
                document.getElementById("business_id").value = id;
                $('body').addClass('noScroll');
            });
                                       <?php } ?>
                <?php if (isset($this->session->userdata["id"])) { ?>
                                        <?php if ($this->uri->segment(1) == 'my-reviews') { ?>
                                         function loading_myreviews_show() {
                    $('#loading_reviews').html("<img src='<?php echo STYLEURL . "front/images/loading.gif" ?>'/>").fadeIn('fast');
                }
                function loading_myreviews_hide() {
                    $('#loading_reviews').fadeOut('fast');
                }
                function load_myreviews_Data(page) {
                    loading_myreviews_show();
                    $.ajax
                            ({
                                type: "POST",
                                url: "<?php echo base_url(); ?>myaccount/getreviews_list",
                                data: "page=" + page,
                                success: function (msg)
                                {
                                    $("#container_reviews").ajaxComplete(function (event, request, settings)
                                    {
                                        loading_myreviews_hide();
                                        $("#container_reviews").html(msg);
                                    });
                                }
                            });
                }
             
                load_myreviews_Data(1);  // For first time page load default results
            
                $('#container_reviews .pagination li.active').live('click', function () {
                    var page = $(this).attr('p');
                    load_myreviews_Data(page);

                });
                $('#go_btn').live('click', function () {
                    var page = parseInt($('.goto').val());
                    var no_of_pages = parseInt($('.total').attr('a'));
                    if (page != 0 && page <= no_of_pages) {
                        load_myreviews_Data(page);
                    } else {
                        alert('Enter a PAGE between 1 and ' + no_of_pages);
                        $('.goto').val("").focus();
                        return false;
                    }

                });
                
                                        <?php } } ?>
               
             <?php    if ($this->uri->segment(1) == 'business_detail' || $this->uri->segment(1) == 'my-account') { ?>
          
            $(document).on('click', 'div.closeNotification', function () {
                //$('.notification').removeClass('showNotification');
                //$('.notification').remove();
                setTimeout(function () {
                    $('.notification').removeClass('showNotification');
                }, 100);

                setTimeout(function () {
                    $('.notification').remove();
                }, 800);
            }); 
             <?php }
        if (isset($this->session->userdata["id"])) {
            ?>    
                    $('.favoriteBtn').click(function () {
                        $(this).toggleClass('active');
                    });

                    $('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
                        $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
                    }).trigger('blur');
            
        <?php } ?>
		$('.mobileMenu').click(function () {
                    if ($('.footerNavBlocks').hasClass('mobHide')) {
                        $('.footerNavBlocks').removeClass('mobHide');
                        //$('.main').addClass('showMobMenu');
                        $('body').append("<div class='overlayMobNav'></div>");
                    } else {
                        $('.footerNavBlocks').addClass('mobHide');
                        //$('.main').removeClass('showMobMenu');
                        $('.overlayMobNav').remove();
                    }
		});
		$('.mobSearch').click(function () {
			var body = $("html, body");
			body.stop().animate({scrollTop: 0}, '2000', 'swing', function () {
				$('.searchTextbox.typeahead').focus();
			});
		});
		$(document).on('click', 'div.overlayMobNav', function() {
			$('.footerNavBlocks').addClass('mobHide');
			$('.overlayMobNav').remove();
		});

            <?php /* ALL VALIDATIONS */ ?>
               <?php if($this->uri->segment(1) == 'business_detail') { ?>
                $("#sendsmsmes").validate({
                    // Specify the validation rules
                    rules: {
                        user_mobile: {
                            required: true,
                            number: true
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        user_mobile: {
                            required: "Please provide mobile number",
                            number: "Please enter mobile numbers only"
                        }
                    },

                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                // Setup form validation on the #register-form element
               <?php } ?>

<?php if($this->uri->segment(1) == 'change-password') { ?>
                $("#changepassword").validate({
                    // Specify the validation rules
                    rules: {
                        old_password: {
                            required: true,
                            remote: "<?php echo base_url(); ?>myaccount/oldpasswordcheck"
                        },
                        password: {
                            required: true,
                            minlength: 8
                        },
                        password2: {
                            required: true,
                            equalTo: "#password"
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        old_password: {
                            required: "Please provide existing password",
                            remote: "Invalid Password"
                        },
                        password: {
                            required: "Please provide a password",
                            minlength: "Password must be atleast 8 characters"
                        },
                        password2: {
                            required: "Confirm password field cannot be empty",
                            equalTo: "Your re-enter password must be same as password"
                        }
                    },

                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                   
                $("#forgot_changepassword").validate({

                    // Specify the validation rules
                    rules: {
                        password: {
                            required: true,
                            minlength: 8
                        },
                        password2: {
                            required: true,
                            equalTo: "#password"
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        password: {
                            required: "Please provide a password",
                            minlength: "Password must be atleast 8 characters"
                        },
                        password2: {
                            required: "Confirm password field cannot be empty",
                            equalTo: "Your re-enter password must be same as password"
                        }
                    },

                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                
                <?php } ?>
          <?php if($this->uri->segment(1) == 'register') { ?>
                $("#register").validate({
                    errorClass: "errorMessage",
                    errorElement: "p",
                    // Specify the validation rules
                    rules: {
                        fullname: {
                            required: true
                        },
                        email: {
                            required: true,
                            email: true,
                            remote: "<?php echo base_url(); ?>myaccount/registeremailcheck"
                        },
                        pass1: {
                            required: true,
                            minlength: 8
                        },
                        pass2: {
                            required: true,
                            equalTo: "#pass1"
                        },
                        phone: {
                            required: true,
                            number: true,
                            minlength: 10,
                            maxlength: 11
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        fullname: {
                            required: "Please provide your full name"
                        },
                        email: {
                            required: "Please provide your email",
                            email: "Please enter in email format",
                            remote: "Email exists choose some other email"
                        },
                        pass1: {
                            required: "Please provide a password",
                            minlength: "Password must be atleast 8 characters"
                        },
                        pass2: {
                            required: "Confirm password field cannot be empty",
                            equalTo: "Your re-enter password must be same as password"
                        },
                        phone: {
                            required: "Please provide your phone number",
                            number: "Please enter only numbers",
                            minlength: "Please enter Minimum 10 digits mobile number",
                            maxlength: "Please enter Maximum 11 digits mobile number"
                        }
                    },

                    submitHandler: function (form) {

                        form.submit();
                        
                    }
                });
          <?php } if($this->uri->segment(1) == 'add-business') { ?>
                
                $("#addbusiness").validate({
                    errorClass: "errorMessage",
                    errorElement: "p",
                    // Specify the validation rules
                    rules: {
                        title: {
                            required: true
                        },
                        category: {
                            required: true
                        },
                        /*expectation: {
                            required: true
                        },*/
                        city: {
                            required: true
                        },
                        area: {
                            required: true
                        },
                        address: {
                            required: true
                        },
                        /*landmark: {
                            required: true
                        },
                        pincode: {
                            required: true
                        },*/
                        own: {
                            //required: true,
                            //minlength: 1
                            onecheck: true
                        },
                        i_agree: {
                            required: true
                        },
                        phone: {
                            required: true,
                            number: true,
                            minlength: 10,
                            //remote: "<?php echo base_url(); ?>myaccount/phonecheck",
                            maxlength: 11
                        },
                        email: {
                            required: true,
                            email: true
                            //remote: "<?php echo base_url(); ?>myaccount/businessemailcheck"
                        },
                        website_url: {
                            url: true
                        },
                        facebook_url: {
                            url: true
                        },
                        twitter_url: {
                            url: true
                        },
                        google_url: {
                            url: true
                        },
                        youtube_url: {
                            url: true
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        title: {
                            required: "Please Provide Your Business Name"
                        },
                        category: {
                            required: "Please Provide Your category"
                        },
                        expectation: {
                            required: "Please Provide Your Expectation"
                        },
                        city: {
                            required: "Please Provide Your City Name"
                        },
                        area: {
                            required: "Please Provide Your Area Name"
                        },
                        address: {
                            required: "Please Provide Your Address"
                        },
                        landmark: {
                            required: "Please Provide Your Landmark"
                        },
                        pincode: {
                            required: "Please Provide Your Pincode"
                        },
                        own: {
                            required: "Please Provide Your Landmark"
                        },
                        i_agree: {
                            required: "Please Provide Your Pincode"
                        },
                        website_url: {
                            url: "Please Provide Your Website Url"
                        },
                        facebook_url: {
                            url: "Please Provide Your Facebook Url"
                        },
                        google_url: {
                            url: "Please Provide Your Google Url"
                        },
                        youtube_url: {
                            url: "Please Provide Your youtube Url"
                        },
                        twitter_url: {
                            url: "Please Provide Your twitter Url"
                        },
                        email: {
                            required: "Please provide your email",
                            email: "Please enter in email format",
                            remote: "Email exists choose some other email"
                        },
                        phone: {
                            required: "Please provide your phone number",
                            number: "Please enter only numbers",
                            minlength: "Please enter Minimum 10 digits mobile number",
                            maxlength: "Please enter Maximum 11 digits mobile number",
                            remote: "Phone Number Already Exists"
                        }
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


          <?php } if($this->uri->segment(1) == 'my-account') { ?>
          <?php if (!isset($this->session->userdata["id"])) { ?>

                $("#login").validate({

                    // Specify the validation rules
                    rules: {
                        username: {
                            required: true,
                            remote: "<?php echo base_url(); ?>myaccount/emailcheck"
                        },
                        pass: {
                            required: true,
                            remote: "<?php echo base_url(); ?>myaccount/logincheck"
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        username: {
                            required: "Please Enter Your Email",
                            remote: "Invalid Email"
                        },
                        pass: {
                            required: "Please Enter Your Password",
                            remote: "Incorrect Password"
                        }
                    },

                    submitHandler: function (form) {
                        form.submit();
                    }
                });

                $("#forgot_change_password").validate({

                    // Specify the validation rules
                    rules: {
                        forgot_email: {
                            required: true,
                            remote: "<?php echo base_url(); ?>myaccount/forgotemailcheck"
                        }
                    },

                    // Specify the validation error messages
                    messages: {
                        forgot_email: {
                            required: "Please Enter Your Email",
                            remote: "Invalid Email"
                        }
                    },

                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                  $("#otp").validate({
                    // Specify the validation rules
                    rules: {
                        otp: {
                            required: true,
                            remote: "<?php echo base_url(); ?>myaccount/check_for_otp"
                        }
                    },
                    // Specify the validation error messages
                    messages: {
                        otp: {
                            required: "Please Provide OTP",
                            remote: "Invalid otp"
                        }
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                
    <?php }} ?>
                });
        </script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72946436-1', 'auto');
        ga('send', 'pageview');

      </script>
      <script>
          $('.haveSubMenu').click(function() {
            $('.subMenuInner').toggleClass('showSubMenu');
        });
        </script>
    </body>
</html>
