<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Yellow Pages</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="shortcut icon" href="<?php echo STYLEURL; ?>front/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<?php echo STYLEURL; ?>front/<?php echo CSS;?>/yp-home.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="main" id="main"> 
            <div class="container homeHeader">
                <div class="wrapper">
                    <header class="headerBlock">
                        <div class="mobileMenu">Yellow Pages</div>
                        <a href="/" class="branding">
                            <img width="56" height="62" src="<?php echo STYLEURL; ?>front/images/yellow-pages-logo.png" alt="">
                        </a>
                        <div class="topMenu">
                            <a href="/add-business" class="topMenuLink">Add business</a>
                            <a href="/categories" class="topMenuLink">Categories</a>
                            <a href="/my-account" class="topMenuLink">My Account</a>
                            <?php if (isset($this->session->userdata["active"])) { ?>
                                <a href="/sign-out" class="topMenuLink">Sign Out</a>
                            <?php } ?>
                        </div>
                    </header>
                </div>
            </div>
            <?php echo $content; ?>
        </div>  
        <div class="container footer">
            <div class="wrapper">
                <div class="footerNavBlocks mobHide">
                    <div class="mobileNavBlocks">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Quick links</h5>
                            <ul class="footerNavList">
                                <li class="active"><a href="/" class="mobHome">Home</a></li>
                                <li><a href="/categories" class="mobCategories">Browse category</a></li>
                                <li><a class="mobSearch">Search business</a></li>
                                <?php if (isset($this->session->userdata["active"])) { ?>
                                    <li><a href="/my-favorites" class="mobMyFavorites">My Favorites</a></li>
                                    <li><a href="/my-reviews" class="mobMyReviews">My Reviews</a></li>
                                    <li><a href="/change-password" class="mobChangePassword">Change Password</a></li>
                                    <li><a href="/sign-out" class="mobSignout">Sign Out</a></li>
                                <?php }else{?>
                                    <li><a href="/my-account" class="mobLogin">Login to your account</a></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="footerEachNavCol businessMenu">
                            <h5 class="footerHeading">Business</h5>
                            <ul class="footerNavList">
                                <li><a class="mobAddBusiness" href="/add-business">Add your Business</a></li>
                                <li><a class="mobAdvertise" href="/advertise-with-us">Advertise with us</a></li>
                            </ul>
                        </div>

                        <div class="footerEachNavCol">
                            <h5 class="footerHeading ypFooterHeading">Yellowpages</h5>
                            <ul class="footerNavList">
                                <li><a href="/about-us" class="mobAboutUs">About us</a></li>
                                <li><a href="/contact-us" class="mobContactUs">Contact us</a></li>
                                <li><a href="/feedback" class="mobFeedback">Feedback</a></li>
                                <li><a href="/frequently-asked-questions" class="mobFaqs">FAQs</a></li>
                                <li><a href="/careers" class="mobCareers">Careers</a></li>
                                <li><a href="/terms-and-conditions" class="mobTermsConditions">Terms &amp; Conditions</a></li>
                                <li><a href="/privacy-policy" class="mobPrivacyPolicy">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="footerSocialMobile">
                        <div class="footerEachNavCol">
                            <h5 class="footerHeading">Mobile &amp; Social</h5>
                            <div class="footerNavList">
                                <div class="footerSocialLinks">
                                    <span class="footerSocialMobTitle">Connect with us</span>
                                    <a href="https://www.facebook.com/Yellowpagesin-718593851493856/" class="footFacebook" target="_blank" rel="nofollow">Facebook</a>
                                    <a href="https://twitter.com/yellowpagesIn" class="footTwitter" target="_blank" rel="nofollow">Twitter</a>
                                    <a href="https://plus.google.com/101163049029744212701" class="footGooglePlus" target="_blank" rel="publisher,nofollow" >Google Plus</a>
                                    <a href="https://www.youtube.com/channel/UCJgrR6mz-HpliQ7QmrNvBlA" class="footYouTube" target="_blank" rel="publisher,nofollow" >You Tube</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container copyRights">
            <div class="wrapper">
                Copyright &copy; 2017, Yellowpages.in. All rights reserved.
            </div>
        </div>
        <script src="<?php echo ASSETSURL; ?>front/js/jquery.min.js"></script>
        <?php /*<script src='<?php echo STYLEURL; ?>front/js/common.js' ></script> */?>
        <script src="<?php echo ASSETSURL; ?>front/js/slick.min.js"></script>
        <script>
            function showUser(str) {
                if (str == "") {
                    document.getElementById("txtHint").innerHTML = "";
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            //document.getElementById("txtHint").innerHTML = this.responseText;
                            var txt = this.responseText;
                            $('.notification').remove();
                            $(txt).appendTo('body');
                            <?php if (isset($this->session->userdata["id"])) { ?>
                            autoRemove();
                            <?php } ?>
                        }
                    };
                    xmlhttp.open("GET", "<?php echo base_url(); ?>welcome/getfavorite?q=" + str, true);
                    xmlhttp.send();
                }
            }
            
            function autoRemove() {
 		setTimeout(function() {$('.notification').removeClass('showNotification');}, 3000);
 		setTimeout(function(){$('.notification').remove();}, 3500);
            }
            
            function search(){
                var searchKey  = $('#search').val();
                var searchVal = $('#search')[0].attributes.data;
                var locationKey = $('#location').val();
                var locationValue = $('#location')[0].attributes.data;
                //alert(searchKey+' : '+searchVal+' : '+locationKey+' : '+locationValue);
                var url = "";
                if(typeof searchVal=="undefined"){
                    url = "/s?q="+searchKey.replace(/ {2,}/g,' ').replace(/ /g,'+').toLowerCase();
                }else{
                    if(searchVal.split('_')[1] === "bus"){
                        url = "/b/"+searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase()+'/'+searchVal.split('_')[2];
                    }else if(searchVal.split('_')[1] === "cat"){
                        var cat_name = searchKey.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                        var id = searchVal.split('_')[2];
                        if(locationKey !== ''){
                            console.log(locationKey);
                            if(locationKey.split(',').length > 1){
                                var cityName = locationKey.split(',')[1].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                var areaName = locationKey.split(',')[0].trim();
                                areaName = areaName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+areaName+'/'+cat_name+'/'+id;
                            }else{
                                cityName = locationKey.split(',')[0].trim();
                                cityName = cityName.replace(/ /g,'-').replace(/[^A-Za-z0-9\-]/g,'').replace(/-{2,}/g,'-').toLowerCase();
                                url = '/'+cityName+'/'+cat_name+'/'+id;
                            }
                        }else{
                            url = "/"+cat_name+'/'+id;
                        }
                    }
                }

                console.log(url);
                window.location.href = url;
                return false;
            }
            
            $(document).on('click', 'li.sresult', function(){
                var txt = $(this).text();
                $('#search').val(txt);
                $('#search')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
                search();
            });
            
            $(document).on('click', 'li.lresult', function(){
                var txt = $(this).text();
                $('#location').val(txt);
                $('#location')[0].attributes.data=this.id;
                $(this).closest('.searchResult').removeClass('activeSearchTextbox');
            });
            
            $(document).on('click','.searchButton',function(){
                search();
            });
            
            $(document).on('click', 'div.overlayMobNav', function() {
			$('.footerNavBlocks').addClass('mobHide');
			$('.overlayMobNav').remove();
		});
            
            $(document).ready(function () {
                
                $('.favoriteBtn').click(function() {
			$(this).toggleClass('active');
		});
		$('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
		    $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
		$('.mobileMenu').click(function () {
			if ($('.footerNavBlocks').hasClass('mobHide')) {
				$('.footerNavBlocks').removeClass('mobHide');
				//$('.main').addClass('showMobMenu');
				$('body').append("<div class='overlayMobNav' style='cursor:pointer;'></div>");
			} else {
				$('.footerNavBlocks').addClass('mobHide');
				//$('.main').removeClass('showMobMenu');
				$('.overlayMobNav').remove();
			}
		});

		$('.mobSearch').click(function () {
			var body = $("html, body");
			body.stop().animate({scrollTop: 0}, '2000', 'swing', function () {
				$('.searchTextbox.typeahead').focus();
			});
		});
		
                
            
                var added = false;
                $('#search').on('keyup', function () {
                    var search = $('#search').val();
                    if(search.length){
                        if(added===false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='bres'></div>");
                            added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_searchdata?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#bres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        added = false;
                    }
                });

                var l_added = false;
                $('#location').on('keyup', function () {
                    var search = $('#location').val();
                    if(search.length){
                        if(l_added === false){
                            $(this).parent().append("<div class='searchResult activeSearchTextbox' id='lres'></div>");
                            l_added = true;
                        }
                        var uri = "<?php echo base_url(); ?>ajax/get_location?q="+search.replace(' ','+');
                        $.ajax({
                            async: true,
                            global: false,
                            type: 'POST',
                            url: uri,
                            success: function (res) {
                                $('#lres').html(res);
                            }
                        });
                    }else{
                        $('.searchResult').remove();
                        l_added = false;
                    }
                });
                
                $('.homeSearchTextbox').on('focus', function() {
                    $(this).next('.searchResult').addClass('activeSearchTextbox');
                }).on('blur', function() {
                   if($(this).val() == '') {
                      $(this).next('.searchResult').removeClass('activeSearchTextbox');
                   }
                });
                
                $(document).on('click', 'div.closeNotification', function() {
                        setTimeout(function() {$('.notification').removeClass('showNotification');}, 100);
	 		setTimeout(function(){$('.notification').remove();}, 800);
 		});
            <?php if (isset($this->session->userdata["id"])) { ?>
                  $('.addFav').click(function() {
                        if($(this).hasClass('favActive')) {
                            $(this).removeClass('favActive');
                        } else {
                            $(this).addClass('favActive');
                        }
                    });

                    $('.opFavClick').click(function() {
                        if($(this).hasClass('activeFavIcon')) {
                            $(this).removeClass('activeFavIcon');
                        } else {
                            $(this).addClass('activeFavIcon');
                        }
                    });

                    var inp = $(this).find('.opFavInput');
                    $('.opFavLabel').click(function() {
                        if($(inp).is(':checked')) {
                            $(this).find('.opFavText').text('Favorited');
                        } else {
                        $(this).find('.opFavText').text('Add to Favorite');
                        }
                    });
            <?php } ?>
                $('.homeFBListBlock').css({overflow : 'hidden'});
                $('.homeFBList').slick({
                    slidesToShow: 6,
                    slidesToScroll: 2,
                    infinite: false,
                    responsive: [{
                        breakpoint: 1920,
                        settings: {
                            slidesToShow: 5,
                            slidesToScroll: 4,
                            dots: false,
                            arrows: true
                        }
                    }, {
                        breakpoint: 1120,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 4,
                            slidesToScroll: 3
                        }
                    }, {
                        breakpoint: 920,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 500,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 499,
                        settings: {
                            arrows: true,
                            dots: false,
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }]
                });
            });
        </script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72946436-1', 'auto');
        ga('send', 'pageview');

      </script>        
    </body>
</html>