<!DOCTYPE html>
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Yellowpages CMS | <?php if(($this->uri->segment(1) != null) && !empty($this->uri->segment(1))){ echo ucfirst($this->uri->segment(1)); }else{ echo 'Dashboard'; } ?></title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/frontend/images/favicon.gif" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/backend/images/favicon.gif" type="image/x-icon"/>
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/materialadmin.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/font-awesome.min.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/material-design-iconic-font.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/rickshaw.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/morris.core.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/toastr.css?1422823374" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-tagsinput.css?1422823365" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/typeahead.css?1422823375" />
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/select2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/multi-select.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/datepicker3.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/jquery-ui-theme.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-colorpicker.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-tagsinput.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/dropzone-theme.css">
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-select.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/css/bootstrap-timepicker.css">
		<!-- Latest compiled and minified CSS -->
		<!--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">-->
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/html5shiv.js?1422823601"></script>
			<script type="text/javascript" src="http://www.codecovers.eu/assets/js/modules/materialadmin/libs/utils/respond.min.js?1422823601"></script>
		<![endif]-->
		<style>
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
			padding: 5px 24px;
			line-height: 1.846153846;
			vertical-align: top;
			border-top: 1px solid rgba(189, 193, 193, 0.2);
			}
			
			.pagination>li>a, .pagination>li>span {
			
			color: #20252b;
			
			}
			
			.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
			
			background-color: #20252b;
			border-color: #20252b;
			color: #ffffff;
			
			}
			
			.pcolour>li>a:hover, .pcolour>li>span:hover, .pcolour>li>a:focus, .pcolour>li>span:focus {
			color: #20252b !important;
			}
			
			
			
			.card-actionbar-row {
			padding: 6px 16px;
			text-align: Left; 
			}
			.header-fixed #header {
			
			background-color: #f9cb34;
			}
			.text-primary , .headerbar {
			color: #969c9c;
			}
			.menubar-inverse .gui-controls a.expanded .gui-icon, .menubar-inverse .gui-controls li.active .gui-icon, .menubar-inverse .gui-controls li.active .gui-icon:hover {
			
			color: #f9cb34;
			}
			
			section .style-primary, .offcanvas-pane .style-primary, .card .style-primary, section.style-primary, .offcanvas-pane.style-primary, .card.style-primary {
			background-color: #afb2b5;
			border-color: #ff9800;
			color: #ffffff;
			display:none;
			}
			.headerbar{
			height: 90%;
			background-color: white;
			}
			.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
			border: none;
			border-bottom: 2px solid #f2f3f3;
			color: #f2f3f3;
			background-color: transparent;
			cursor: default;
			opacity: 1;
			font-weight: 800;
			}
			.btn-primary {
			color: #0aa89e;
			color: #ffffff;
			background-color: #f9cb34;
			border-color: #facb34;
			}
			.btn-default-view {
			color: #ffffff;
			background-color: #facb34;
			border-color: #facb34;
			}
			
			.btn-primary:hover, .btn-primary:focus, .open .dropdown-toggle.btn-primary {
			color: #ffffff;
			background-color: #facb34;
			border-color: #facb34;
			}
			
			.btn-default-view:hover, .btn-primary:focus, .open .dropdown-toggle.btn-primary {
			color: #ffffff;
			background-color: #facb34;
			border-color: #facb34;
			}
			
			.modal-footer {
			padding: 15px;
			text-align: left;
			border-top: 0px solid #e5e5e5; 
			}
			.bootstrap-select.btn-group .dropdown-menu {
			z-index:10000;
			}
			
			.header-nav-profile .dropdown img {
			width: 40px;
			height: 40px;
			border-radius: 0px; 
			}
			.headerbar-left .header-nav {
			margin-left: 10px;
			}
			
			.table-responsive>.fixed-column {
			position: absolute;
			display: inline-block;
			width: auto;
			border-right: 1px solid #ddd;
			}
			.table-index{
			width:100%;
			}
			.employee .checkboxtd{
			padding: 0 5px;
			}
			@media(min-width:768px) {
			.table-responsive>.fixed-column {
			display: none;
			}
			}
			
			.card-body {
			padding: 15px;
			position: relative;
			}
			.btn-primary.btn-flat, .btn-primary.btn-icon-toggle {
			color: #ffffff;
			}
			.table thead > tr > th {
			color: inherit;
			font-weight: 900;
			}
			.rowcount{
			font-weight:800;}
			
		</style>
		
	</head>
	
	<body class="menubar-hoverable header-fixed ">
		<header id="header">
			<div class="headerbar">
				<div class="headerbar-left">
					<ul class="header-nav header-nav-options">
						<li class="header-nav-brand">
							<div class="brand-holder">
								<a href="<?php echo base_url(); ?>">
									<span class="text-lg text-bold text-primary">	<img src="<?php echo base_url(); ?>assets/frontend/images/yellow-pages-logo.png" alt=""> </span>
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				
				<div class="headerbar-right">
					<ul class="header-nav header-nav-options">
						<li>
							<a href="<?php echo base_url(); ?>import" class="btn btn-icon-toggle btn-default" >
								<i class="fa fa-file"></i>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>setting" class="btn btn-icon-toggle btn-default" >
								<i class="fa fa-gears"></i>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url(); ?>dashboard/logout" class="btn btn-icon-toggle btn-default" >
								<i class="fa fa-fw fa-power-off text-danger"></i> 
							</a>
						</li>
					</ul>
				</div>
			</div>
		</header>
		
		<div id="base">
			<div class="offcanvas">	</div>
			<div id="content">				
				<?= $content; ?>
			</div>
			<div id="menubar" class="menubar-inverse  animate">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="<?php echo base_url(); ?>">
							<span class="text-lg text-bold text-primary ">YellowPages&nbsp;ADMIN</span>
						</a>
					</div>
				</div>
				<div class="nano has-scrollbar" style="height: 559px;">
					<div class="nano-content" tabindex="0" style="right: -17px;">
						<div class="menubar-scroll-panel" style="padding-bottom: 33px;">
							<ul id="main-menu" class="gui-controls">
								<li <?php if($this->uri->segment(1) == False || $this->uri->segment(1) == "dashboard" ){ echo 'class="active"'; } ?> >
									<a href="<?php echo base_url(); ?>dashboard" <?php if($this->uri->segment(1) == False || $this->uri->segment(1) == "dashboard" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="md md-home"></i></div>
										<span class="title">Dashboard</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "lead" ){ echo 'class="active"'; } ?> >
									<a href="<?php echo base_url(); ?>lead" <?php if($this->uri->segment(1) == "lead" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="md md-email"></i></div>
										<span class="title">Leads</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "business" ){ echo 'class="active"'; } ?> >
									<a href="<?php echo base_url(); ?>business" <?php if($this->uri->segment(1) == "business" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="md md-business"></i></div>
										<span class="title">Business</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "category" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>category" <?php if($this->uri->segment(1) == "category" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="fa fa-puzzle-piece fa-fw"></i></div>
										<span class="title">Categories</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "city" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>city" <?php if($this->uri->segment(1) == "city" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="md md-my-location"></i></div>
										<span class="title">Cities</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "area" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>area" <?php if($this->uri->segment(1) == "area" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><span class="md md-location-on"></span></div>
										<span class="title">Areas</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "billing" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>billing" <?php if($this->uri->segment(1) == "billing" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="md md-computer"></i></div>
										<span class="title">Billing</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "employee" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>employee" <?php if($this->uri->segment(1) == "employee" ){ echo 'class="active"'; } ?> >
										<div class="gui-icon"><i class="fa fa-user"></i></div>
										<span class="title">Employees</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "role" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>role" <?php if($this->uri->segment(1) == "role" ){ echo 'class="active"'; } ?>>
										<div class="gui-icon"><i class="fa fa-tags"></i></div>
										<span class="title">Roles and Permissions</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "permission" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>permission" <?php if($this->uri->segment(1) == "permission" ){ echo 'class="active"'; } ?>>
										<div class="gui-icon"><i class="fa fa-tags"></i></div>
										<span class="title">Permissions</span>
									</a>
								</li>
								<li <?php if($this->uri->segment(1) == "report" ){ echo 'class="active"'; } ?>>
									<a href="<?php echo base_url(); ?>report" <?php if($this->uri->segment(1) == "reports" ){ echo 'class="active"'; } ?>>
										<div class="gui-icon"> <i class="md md-assessment"></i> </div> <span class="title">Reports</span> 
									</a>
								</li>
							</ul>
							<div class="menubar-foot-panel">
								<small class="no-linebreak hidden-folded">
									<span class="opacity-75">Copyright © 2016</span> <strong>YellowPages</strong>
								</small>
							</div>
						</div>
					</div>
					<div class="nano-pane" style="display: none;">
						<div class="nano-slider" style="height: 543px; transform: translate(0px, 0px);"></div>
					</div>
				</div>
			</div>
			
		</div>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery-1.11.2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery-migrate-1.2.1.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery-ui.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/spin.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.autosize.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.knob.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.nanoscroller.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/toastr.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/select2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.multi-select.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-colorpicker.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-select.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/dropzone.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-tagsinput.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/typeahead.bundle.min.js"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.validate.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/additional-methods.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-timepicker.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/App.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppNavigation.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppOffcanvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppCard.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppForm.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppNavSearch.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/AppVendor.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/jquery.inputmask.bundle.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-rating-input.min.js"></script>
		
		<script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>
		
		<script type="text/javascript">
			
			$('#cparent_id').on('change' , function(e){
				e.preventDefault();
				console.log('Next');
				var query = 'parent_id='+$('#cparent_id').val();
				$('#error_div').html('');
				console.log(query);
				$.post('<?php echo base_url(); ?>/category/check_category' , query , function(data){
					console.log(data);
					$('#error_div').html(data);
				});
			});
			
			$('.role-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Are you sure?") == true) {
					window.location="<?php echo base_url(); ?>role/delete/"+id;
				}
			});
			
			$('.employee-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Are you sure?") == true) {
					window.location="<?php echo base_url(); ?>employee/delete/"+id;
				} 
			});
			
			$('#edit_role').on('submit' , function(e){
				e.preventDefault();
				$.post("<?php echo base_url(); ?>role/edit" , $('#edit_role').serialize() , function(data){
					$('#editrole').modal('hide');
					alert('Role has been edited successfully...');
					setTimeout(function(){location.reload();},1000);
				});
			}); 
			
			$(document).on("click", ".open-EditRoleDialog", function () {
				var selectedRoleId = $(this).data('id');
				$.get("<?php echo base_url(); ?>ajax/get_role/" + selectedRoleId , function(data){
					var result = JSON.parse(data);
					$("#roleId").val( result.id );
					if(result.status==1){
						$('.edit-body #roleactive').prop('checked', true);
					}
					$(".edit-body #newrole").val( result.name );
					$(".edit-body #report_id").val( result.report_id );
					$('.edit-body #report_id').selectpicker('refresh')
					
				});
			});
			
			function  change_role_status(id,val){
				$.ajax({
					url: "role/change_status",
					data:{id:id,status:val},
					success: function(msg){
						alert(msg);
					}
				});
			}
			
			function find_role(){
				document.getElementById('role_index').submit();
			}
			
			function find_employee(){
				document.getElementById('employee_index').submit();
			}
			
			
			function readURL(input) {
				var imageurlSrc='';
				//alert("test image file...");
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					
					reader.onload = function (e) {
						$('#dispImage')
						.attr('src', e.target.result)
						.width(150)
						.height(200);
					};
					imageurlSrc=input.files[0];
					reader.readAsDataURL(input.files[0]);
				}
				
				var imageurl="/uploads/"+imageurlSrc;
				$('#image_url').val(imageurl);
				$('#delImage').show();
			}
			
			function deleteImage(){
				if(confirm("Do you want to delete existing Image...!!!"))
				{
					$('#dispImage').attr('src', '');
					//$('#blah').attr('src', '');
					$('#newupload').html('<input type="file" name="fileToUpload" id="fileToUpload" onchange="readURL(this);" required>');
					$('#newupload').show();
					$('#delImage').hide();
					}else{
					return false;
				}
			}
			
			var password = document.getElementById("password")
			, confirm_password = document.getElementById("confirm_password");
			
			function validatePassword(){
				if(password.value != confirm_password.value  && password.value !='' & confirm_password.value=='') {
					confirm_password.setCustomValidity("Please  confirm the password entered");
				}
				else if(password.value != confirm_password.value  && password.value !='' & confirm_password.value!='') {
					confirm_password.setCustomValidity("Passwords Don't Match");
					} else {
					confirm_password.setCustomValidity('');
				}
			}
			
			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;
		</script>
		
		<script type="text/javascript">
			
			function initialize() {
				$latitude = $('#latitude').val();
				$longitude = $('#longitude').val();
				if($latitude == '' && $longitude == ''){
					var coords = new google.maps.LatLng(51.508742, -0.120850);
					$('#latitude').val(51.508742);
					$('#longitude').val(-0.120850);
					var options = {
						zoom: 15,
						center: coords,
						mapTypeControl: false,
						navigationControlOptions: {
							style: google.maps.NavigationControlStyle.SMALL
						},
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					var map = new google.maps.Map(document.getElementById("googleMap"), options);
					
					var marker = new google.maps.Marker({
						position: coords,
						map: map,
						title:"You are here!"
					});
					
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(success);
					}
					
					}else{
					var coords = new google.maps.LatLng($latitude, $longitude);
					var options = {
						zoom: 15,
						center: coords,
						mapTypeControl: false,
						navigationControlOptions: {
							style: google.maps.NavigationControlStyle.SMALL
						},
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};
					var map = new google.maps.Map(document.getElementById("googleMap"), options);
					
					var marker = new google.maps.Marker({
						position: coords,
						map: map,
						title:"You are here!"
					});
					
				}
				
			}
			
			function changecoords(){
				$latitude = $('#latitude').val();
				$longitude = $('#longitude').val();
				var coords = new google.maps.LatLng($latitude, $longitude);
				var options = {
					zoom: 15,
					center: coords,
					mapTypeControl: false,
					navigationControlOptions: {
						style: google.maps.NavigationControlStyle.SMALL
					},
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("googleMap"), options);
				
				var marker = new google.maps.Marker({
					position: coords,
					map: map,
					title:"You are here!"
				});
			}
			
			ROWCOUNT=1;
			function addbimage(){
				if(ROWCOUNT < 10){
					$('#row_'+ROWCOUNT).css('display','block');
					ROWCOUNT = ROWCOUNT + 1;
				}
				else{
					$('#error-msg').html('MAXIMUM 10 IMAGES ONLY UPLOADED');
				}
			}
			
			
			function success(position) {
				$('#latitude').val(position.coords.latitude);
				$('#longitude').val(position.coords.longitude);
				
				var coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				var options = {
					zoom: 15,
					center: coords,
					mapTypeControl: false,
					navigationControlOptions: {
						style: google.maps.NavigationControlStyle.SMALL
					},
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("googleMap"), options);
				
				var marker = new google.maps.Marker({
					position: coords,
					map: map,
					title:"You are here!"
				});
			}
			
			
			google.maps.event.addDomListener(window, 'load', initialize);
			
			
		</script>
		<script type="text/javascript">
			
			function change_subcategory(){
				$('#sub_categories').html('');
				$.post("<?php echo base_url(); ?>/ajax/get_subcategories", 'category_id='+$('#bcategory').val(), function(data){
					result = JSON.parse(data);
					var html = '';
					$.each(result, function(i,item){
						html = html + '<div class="col-sm-2"><div class="checkbox checkbox-styled"><label><input type="checkbox" value="'+ i +'" name="subcategory[]" onclick="add_attributes('+ parseInt(i) +');" class="subcategory"><span>&nbsp;</span>'+ item  +'</label></div></div>';
					});
					$('#sub_categories').html(html);
				});
			}
			
			
			function add_attributes($id){
				$.post("<?php echo base_url(); ?>/ajax/add_attributes", 'subcategory_id='+$id, function(data){
					$('#attributes').append(data);
				});
			}
			
			if(document.getElementById("sub_categories")){
				change_subcategory();
			}
			
			$('#offer-date').datepicker({ 
				startDate:new Date(), //Today's Date
				endDate:0
			});
			
			$('#demo-date').datepicker({ 
				startDate:0, //Today's Date
				endDate:new Date()
			});
			
			$('.timepicker').timepicker();
			
			function change_bstatus(el){
				var query = 'business_id='+ $(el).data('id') + '&status_id='+ $(el).val();
				$.post("<?php echo base_url(); ?>ajax/change_bstatus", query, function(){
					alert("Business Status Change Successfully.");
				});
				
				return false;
			}
			
			function change_estatus(el){
				var query = 'employee_id='+ $(el).data('id') + '&status_id='+ $(el).val();
				$.post("<?php echo base_url(); ?>ajax/change_estatus", query, function(){
					alert("Employee Status Changed Successfully.");
				});
				
				return false;
			}
			
			function change_astaus(el){
				var query = 'area_id='+ $(el).data('id') + '&status_id='+ $(el).val();
				$.post("<?php echo base_url(); ?>ajax/change_astatus", query, function(){
					alert("Area Status Change Successfully.");
				});
				
				return false;
			}
			
			//Area 			
			toastr.options = {
				"closeButton": true,
				"progressBar": false ,
				"debug": false,
				"positionClass": 'toast-top-full-width',
				"showDuration": 330,
				"hideDuration": 330,
				"timeOut":  5000,
				"extendedTimeOut": 1000,
				"showEasing": 'swing',
				"hideEasing": 'swing',
				"showMethod": 'slideDown',
				"hideMethod": 'slideUp',
				"onclick": null
			}
			
			$(document).ready(function() {
				$('#selectall').click(function(event) {  //on click 
					if(this.checked) { // check select status
						//alert('presee');
						$('.addcheck').each(function() { //loop through each checkbox
							this.checked = true;  //select all checkboxes with class "checkbox1"               
						});
					}
					else{
						$('.addcheck').each(function() { //loop through each checkbox
							this.checked = false; //deselect all checkboxes with class "checkbox1"                       
						});         
					}
				});
				
				KEY = 1;
				$('#addoffer').click(function(){
					if(KEY < 10){
						$('#offerblock').after('<div class="col-sm-12" style="border: 1px solid;background-color: beige;margin-bottom:40px;"><div class="form-group col-sm-4 floating-label"><div class="radio radio-styled"><label><input type="radio" name="deal_type_'+ KEY +'" value="Deal"><span>Deal</span></label></div></div><div class="form-group col-sm-4 floating-label"><div class="radio radio-styled"><label><input type="radio" name="deal_type_'+ KEY +'" value="Coupon"  checked><span>Coupon</span></label></div></div><div class="form-group col-sm-8 floating-label"><input  type="text"  class="form-control"  name="offer_title[]"><label class="col-sm-12 "  for="offer_title">Offer Title</label></div><div class="form-group col-sm-4 "><input type="file" class="form-control"   name="offer_image[]" accept=".jpeg,.jpg,.png"><p for="offer_image" class="help-block">Offer Image</p></div><div class="form-group col-sm-8 floating-label"><input  type="text"  class="form-control"  name="offer_url[]"><label class="col-sm-12 " for="offer_url">Coupon code/Deal URL</label></div><div class="form-group col-sm-4 "><input type="date" class="form-control"  name="offer_expire[]"><p for="offer_expire" class="help-block">Expiring on</p></div><div class="form-group col-sm-12 floating-label"><textarea type="text" name="offer_description[]"  maxlength="80" class="form-control" rows="3" placeholder="" ></textarea><div class="form-control-line"></div><label for="offer_description" class="col-sm-12 control-label">Offer Description (max 80 characters)</label></div></div>');
						
						KEY = KEY  + 1 ;
					}
					else{
						$('#error-msg').html('MAXIMUM 10 OFFERS ONLY ADDED');
					}
				});
				
				
				$('#copyall').click(function(){
					$morning_start_time = $('.morning_start_time').first().val();
					$('.morning_start_time').each(function(){
						$(this).val($morning_start_time);
					});
					
					$morning_end_time = $('.morning_end_time').first().val();
					$('.morning_end_time').each(function(){
						$(this).val($morning_end_time);
					});
					
					$evening_start_time = $('.evening_start_time').first().val();
					$('.evening_start_time').each(function(){
						$(this).val($evening_start_time);
					});
					
					$evening_end_time = $('.evening_end_time').first().val();
					$('.evening_end_time').each(function(){
						$(this).val($evening_end_time);
					});
					
					$evening_status = document.getElementById('evening_status').checked;
					$('.evening_status').each(function(){
						this.checked = $evening_status;
					});
					
					$morning_status = document.getElementById('morning_status').checked;
					$('.morning_status').each(function(){
						this.checked = $morning_status;
					});
					
				});
				
				$('#addeve').click(function(){
					if(this.checked){
						$('#evetime').css('display','block');
					}
					else{
						$('#evetime').css('display','none');
					}
					
				});
				
				$('.addcheck').click(function(event) {  //on click 
					document.getElementById("selectall").checked = false;
				});
			});
			
			$('#area_state_id').on('change' , function(e){
				e.preventDefault();
				update_citylist();
			});
			
			var elementExists = document.getElementById("area_state_id");
			
			if(elementExists){
				update_citylist();
			}
			
			var elementExists = document.getElementById("bussiness_state_id");
			
			if(elementExists){
				update_business_citylist();
			}
			
			function update_business_citylist(){
				var query = 'area_state_id='+$('#bussiness_state_id').val();
				$.post("<?php echo base_url(); ?>area/get_citylist" , query , function(data){
					result = JSON.parse(data);
					var options = '';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					options = options + '<option value="0">Others</option>'
					$("#business_city_id").html(options).selectpicker('refresh');
					update_business_arealist();
				});
			}
			
			function update_business_arealist(){
				var query = 'city_id='+$('#business_city_id').val();
				$.post("<?php echo base_url(); ?>area/get_arealist" , query , function(data){
					result = JSON.parse(data);
					var options = '';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					options = options + '<option value="0">Others</option>'
					$("#business_area_id").html(options).selectpicker('refresh');
				});
			}
			
			$('#business_city_id').on('change' , function(){
				update_business_arealist();
			});
			
			$('#bussiness_state_id').on('change' , function(){
				update_business_citylist();
			});
			
			function update_citylist(){
				var query = 'area_state_id='+$('#area_state_id').val();
				$.post("<?php echo base_url(); ?>area/get_citylist" , query , function(data){
					result = JSON.parse(data);
					var options = '';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					options = options + '<option value="0">Others</option>'
					$("#area_city_id").html(options).selectpicker('refresh');
				});
			} 
			
			$('#state-search').on('change' , function(){
				var query = 'area_state_id='+$('#state-search').val();
				$.post("<?php echo base_url(); ?>area/get_citylist" , query , function(data){
					result = JSON.parse(data);
					var options = '';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					options = options + '<option value="0">Others</option>'
					$("#city-search").html(options).selectpicker('refresh');
				});
			});
			
			
			<!----------------------------------------------------------------------------------------------->
			$('#createcity').on('submit' , function(e){
				e.preventDefault();
				$.post("<?php echo base_url(); ?>city/create" , $('#createcity').serialize() , function(data){
					//alert(data);
					$('#addcity').modal('hide');
					if (confirm("City Added Successfully.") == true) {
						
						location.reload();
					}
				});
			}); 
			
			
			$('#createarea').on('submit' , function(e){
				e.preventDefault();
				$.post("<?php echo base_url(); ?>area/create" , $('#createarea').serialize() , function(data){
					$('#addarea').modal('hide');
					if (confirm("Area Added Successfully.") == true) {
						location.reload();
					}
				});
			}); 
			
			
			$(document).on("click", ".cityDialog", function () {
				var cityId = $(this).data('id');
				$.get("<?php echo base_url(); ?>ajax/get_city/" + cityId , function(data){
					var result = JSON.parse(data);
					$("#editForm .cityId").val( result.id );
					$("#editForm .status").val( result.status );
					$("#editForm .state_id").val( result.state_id );
					$('#editForm .state_id').selectpicker('refresh')
					$("#editForm .name").val( result.name );
				});
			});
			
			$(document).on("click", ".areaDialog", function () {
				document.getElementById("areaedit").reset();
				var areaId = $(this).data('id');
				$.get("<?php echo base_url(); ?>ajax/get_area/" + areaId , function(data){
					var result = JSON.parse(data);
					$("#areaedit .areaId").val( result.id );
					$("#areaedit .status").val( result.status );
					$("#areaedit #state_id").val( result.state_id );
					$('#areaedit #state_id').selectpicker('refresh')
					
					$("#areaedit .name").val( result.name );
					$("#areaedit .zip").val( result.zip );
					
					var query = 'state_id='+$('#areaedit #state_id').val();
					$.post("<?php echo base_url(); ?>ajax/get_cities" , query , function(data){
						results = JSON.parse(data);
						var options = '<option value="" data-hidden="true">Select City</option>';
						$.each(results,function(i,item){
							options = options + '<option value="'+i+'" >'+item+'</option>';
						});
						
						$("#areaedit #city_id").html(options).selectpicker('refresh');
						
						$("#areaedit #city_id").val( result.city_id );
						$('#areaedit #city_id').selectpicker('refresh');
						
					});
					
				});
			});
			
			$('#editForm').on('submit' , function(e){
				e.preventDefault();
				$.post("<?php echo base_url(); ?>city/edit" , $('#editForm').serialize() , function(data){
					$('#editcity').modal('hide');
					location.reload();
				});
			});
			
			
			$('#areaedit').on('submit' , function(e){
				e.preventDefault();
				$.post("<?php echo base_url(); ?>area/edit" , $('#areaedit').serialize() , function(data){
					$('#editarea').modal('hide');
					location.reload();
				});
			}); 
			
			
			$('#createarea #state_id').on('change' , function(e){
				var query = 'state_id='+$('#createarea #state_id').val();
				$.post("<?php echo base_url(); ?>ajax/get_cities" , query , function(data){
					result = JSON.parse(data);
					var options = '<option value="" data-hidden=true >All Cities</option>';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					$("#createarea #city_id").html(options).selectpicker('refresh');
				});
			});
			
			
			$('#editarea #state_id').on('change' , function(e){
				var query = 'state_id='+$('#editarea #state_id').val();
				$.post("<?php echo base_url(); ?>ajax/get_cities" , query , function(data){
					result = JSON.parse(data);
					var options = '<option value="" data-hidden=true >All Cities</option>';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					$("#editarea #city_id").html(options).selectpicker('refresh');
				});
			});
			
			function find_city(){
				document.getElementById('city_index').submit();
			}
			
			function find_business(){
				document.getElementById('business_index').submit();
			}
			
			$('#state_ID').on('change' , function(e){
				var query = 'state_id='+$('#state_ID').val();
				$.post("<?php echo base_url(); ?>ajax/get_cities" , query , function(data){
					result = JSON.parse(data);
					var options = '<option value="0" selected >All Cities</option>';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					$("#city_ID").html(options).selectpicker('refresh');
				});
				
				document.getElementById('business_index').submit();
				
			});
			
			$('#city_ID').on('change' , function(e){
				var query = 'city_id='+$('#city_ID').val();
				$.post("<?php echo base_url(); ?>ajax/get_areas" , query , function(data){
					result = JSON.parse(data);
					var options = '<option value="0" selected >All Area</option>';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					$("#area_ID").html(options).selectpicker('refresh');
				});
			});
			
			
			function find_category(){
				document.getElementById('category_index').submit();
			}
			
			function find_area(){
				document.getElementById('area_index').submit();
			}
			
			$('.area-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Deleting this Area might reflect to some Business") == true) {
					$.post("<?php echo base_url() ?>area/delete", 'id='+id , function(data){
						location.reload();
					});
				} 
			});
			
			$('.business-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Do you want to Delete this Business ?") == true) {
					$.post("<?php echo base_url() ?>business/delete", 'id='+id , function(data){
						location.reload();
					});
				} 
			});
			
			$('.city-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Deleting this City might reflect to some Business") == true) {
					$.post("<?php echo base_url() ?>city/delete", 'id='+id , function(data){
						location.reload();
					});
				} 
			});
			
			$('.category-delete').on('click',function(){
				var id = $(this).data("id");
				if (confirm("Deleting this Category might reflect to some Business") == true) {
					$.post("<?php echo base_url() ?>category/delete", 'id='+id , function(data){
						location.reload();
					});
				} 
			});
			
			<!----------------------------------------------------------------------------------------------->
			
			$('#parent_id').on('change' , function(e){
				e.preventDefault();
				update_subcategories();
			});
			
			var elementExists = document.getElementById("parent_id");
			
			if(elementExists){
				update_subcategories();
			}
			
			function update_subcategories(){
				var query = 'category_parent_id='+$('#parent_id').val();
				$.post("<?php echo base_url(); ?>ajax/update_subcategories" , query , function(data){
					result = JSON.parse(data);
					var options = '';
					$.each(result,function(i,item){
						options = options + '<option value="'+i+'" >'+item+'</option>';
					});
					options = options + '<option value="0">None</option>'
					$("#subparent_id").html(options);
				});
			}
			
		</script>
		<script type="text/javascript">
			
			function load_city_monthly(){	
				$('#city_weekly').removeClass('btn-default-dark');
				$('#city_monthly').addClass('btn-default-dark');
				$.post("<?php echo base_url(); ?>dashboard/mtopcities", function(data){
					//alert(data);
					result = JSON.parse(data);
					$('#container-dashboard-city').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						exporting: { enabled: false },
						credits: { enabled: false },
						title: {
							text: 'Top 5 Cities'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: false
								},
								showInLegend: true
							}
						},
						series: [{
							name: 'City',
							colorByPoint: true,
							data: result
						}]
					});
					
				});
				
			}
			
			function load_category_monthly(){	
				$('#category_weekly').removeClass('btn-default-dark');
				$('#category_monthly').addClass('btn-default-dark');
				$.post("<?php echo base_url(); ?>dashboard/mtopcategories", function(data){
					//alert(data);
					result = JSON.parse(data);
					$('#container-dashboard-category').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						exporting: { enabled: false },
						credits: { enabled: false },
						title: {
							text: 'Top 5 Categories'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: false
								},
								showInLegend: true
							}
						},
						series: [{
							name: 'Category',
							colorByPoint: true,
							data: result
						}]
					});
				});
			}
			
			function load_city_weekly(){
				$('#city_monthly').removeClass('btn-default-dark');
				$('#city_weekly').addClass('btn-default-dark');
				// Build the chart
				$.post("<?php echo base_url(); ?>dashboard/wtopcities", function(data){
					//alert(data);
					result = JSON.parse(data);
					$('#container-dashboard-city').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						exporting: { enabled: false },
						credits: { enabled: false },
						title: {
							text: 'Top 5 Cities'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: false
								},
								showInLegend: true
							}
						},
						series: [{
							name: 'City',
							colorByPoint: true,
							data: result
						}]
					});
				});
				
			}
			
			function load_category_weekly(id){
				$('#category_monthly').removeClass('btn-default-dark');
				$('#category_weekly').addClass('btn-default-dark');
				$.post("<?php echo base_url(); ?>dashboard/wtopcategories", function(data){
					//alert(data);
					result = JSON.parse(data);
					$('#container-dashboard-category').highcharts({
						chart: {
							plotBackgroundColor: null,
							plotBorderWidth: null,
							plotShadow: false,
							type: 'pie'
						},
						exporting: { enabled: false },
						credits: { enabled: false },
						title: {
							text: 'Top 5 Categories'
						},
						tooltip: {
							pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions: {
							pie: {
								allowPointSelect: true,
								cursor: 'pointer',
								dataLabels: {
									enabled: false
								},
								showInLegend: true
							}
						},
						series: [{
							name: 'Category',
							colorByPoint: true,
							data: result
						}]
					});
				});
			}
			
			$(function () {
				
				$(document).ready(function () {
					load_category_weekly();
					load_city_weekly();
					
				});
				
				$("#create_employee").on('submit',function(e){
					if($('#userpwd').val()!=$('#cnfpwd').val()){
						$('#cnfpwd').closest('.form-group').addClass('has-error');
						$('#cnfpwd').after('<span id="cnf-error" class="help-block">confirm password did not match</span>').focus();
						return false;
					}
				});
				$("#create_employee #email").on('blur',function(e){
					var query = 'email='+$('#email').val();
					$.post("<?php echo base_url(); ?>employee/check_user" , query , function(data){
						result = JSON.parse(data);
						if(result['email']){
							$('#email').closest('.form-group').addClass('has-error');
							$('#email').attr("aria-invalid","true");
							$("#email").attr("aria-describedby","email-error");
							$('#email').after('<span id="email-error" class="help-block">Email already exists</span>').focus();
							$('#email').val('');
						}
					});
				});
				$("#create_employee #userlogin").on('blur',function(e){
					var query = 'username='+$('#userlogin').val();
					$.post("<?php echo base_url(); ?>employee/check_user" , query , function(data){
						result = JSON.parse(data);
						if(result['username']){
							$('#userlogin').closest('.form-group').addClass('has-error');
							$('#userlogin').attr("aria-invalid","true");
							$("#userlogin").attr("aria-describedby","email-error");
							$('#userlogin').after('<span id="email-error" class="help-block">Username already exists</span>').focus();
							$('#userlogin').val('');
						}
					});
				});
				
			});
			
			
			
		</script>
		
		<div id="device-breakpoints">
			<div class="device-xs visible-xs" data-breakpoint="xs"></div>
			<div class="device-sm visible-sm" data-breakpoint="sm"></div>
			<div class="device-md visible-md" data-breakpoint="md"></div>
			<div class="device-lg visible-lg" data-breakpoint="lg"></div>
		</div>
	</body>
</html>														