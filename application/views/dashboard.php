<section>
	<div class="section-body">
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="card">
					<div class="card-head style-default-dark">
						<header>Sales</header>
										</div>
					<div class="card-body no-padding height-4">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td>Total</td>
									<td>23</td>
								</tr>
								<tr>
									<td>Daily</td>
									<td>23</td>
								</tr>
								<tr>
									<td>Weekly</td>
									<td>23</td>
								</tr>
								<tr>
									<td>Yearly</td>
									<td>23</td>
								</tr>
															</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sn" data-dismiss="modal">Track Sales</button>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6">
				<div class="card  ">
					<div class="card-head style-default-dark">
						<header>Invoice</header>
											</div>
					<div class="card-body no-padding height-4">
						<table class="table table-striped ">
							<tbody>
								<tr>
									<td>Total</td>
									<td>23</td>
								</tr>
								<tr>
									<td>Outstanding</td>
									<td>23</td>
								</tr>
								<tr>
									<td>Closed</td>
									<td>23</td>
								</tr>
								</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-sn" data-dismiss="modal">View Invoice</button>
					</div>
				</div>
			</div>
			
			<!-- BEGIN ALERT - BOUNCE RATES -->
			<div class="col-md-4 col-sm-6">
				<div class="card">
					<div class="card-head  style-default-dark">
						<header>Listing</header>
											</div><!--end .card -->
					<div class="card-body no-padding height-4">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td>Total</td>
									<td><?php echo $total; ?></td>
								</tr>
								<tr>
									<td>Paid</td>
									<td><?php echo $paid; ?></td>
								</tr>
								<tr>
									<td>Unpaid</td>
									<td><?php echo $unpaid; ?></td>
								</tr>
								<tr>
									<td>Review/Moderation</td>
									<td><?php echo $review;?></td>
								</tr>
															</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<a href="<?php echo base_url(); ?>business" class="btn btn-default btn-sn" >View Listing</a>
					</div>
				</div>
			</div>
		</div>
				
		<div class="row">
			<div class="col-md-6">
				<div class="card card-underline ">
					<div class="row">
						<div class="col-md-12">
							<div class="card-head">
								<header>Listing overview</header>
							</div>
							<div class="card-body height-12">
								<div style="text-align: right;">
									<button type="button" class="btn btn-default-dark btn-sn" id="city_weekly" onclick="load_city_weekly();">Weekly</button>
									<button type="button" class="btn btn-default btn-sn" id="city_monthly" onclick="load_city_monthly();">Monthly</button>
								</div>
								<div id="container-dashboard-city" style=" margin: 0 auto"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="card card-underline ">
					<div class="row">
						<div class="col-md-12">
							<div class="card-head">
								<header>Listing overview</header>
							</div>
							<div class="card-body height-12">
								<div style="  
								text-align: right;">
									<button type="button" class="btn btn-default-dark btn-sn" id="category_weekly"  onclick="load_category_weekly();">Weekly</button>
									<button type="button" class="btn btn-default btn-sn" id="category_monthly"  onclick="load_category_monthly();">Monthly</button>
								</div>
								<div  id="container-dashboard-category" style=" margin: 0 auto"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
