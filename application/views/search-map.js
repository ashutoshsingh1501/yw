$(document).ready(function () {
    var mapHeight = $(window).height();
    var mapWidth = $(window).width();
    if (mapWidth < 1169) {

        $('.businessList').slick({
            arrows: false,
            dots: false,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        $('.businessList').on('afterChange', function (event, slick, currentSlide, nextSlide) {
            //alert('adf');

        });

        $('.map').css({height: mapHeight - 300});
        $('.businessListBlock').css({height: ''});

    } else {
        $('.map').css({height: mapHeight - 132});
        $('.businessListBlock').css({height: mapHeight - 132});
    }

    /*if($(window).width() > 1024) {
     $('.businessList').unSlick();
     alert('done');
     }*/
});

$(window).resize(function () {
    var w = $(window).width();
    var mapHeight = $(window).height();
    if (w > 1168) {
        $('.businessList').slick('unslick');
        $('.map').css({height: mapHeight});
        $('.businessListBlock').css({height: mapHeight - 132});

    } else {
        $('.businessList').slick({
            arrows: false,
            dots: false
        });
        $('.map').css({height: mapHeight - 300});
        $('.businessListBlock').css({height: ''});
    };

});

var lt = '', lg = '';

function initMap() {

    var mapDiv = document.getElementById('mapid');

    //alert('init');

    var myLong = new google.maps.LatLng(17.4479, 78.3772);

    map = new google.maps.Map(mapDiv, {
        zoom: 6,
        center: myLong,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var i = 1;

    var tile, myMarker;

    $('.eachPopular').each(function () {

        var myLong = new google.maps.LatLng($(this).data('latitude'), $(this).data('longitude'));

        tile = new google.maps.InfoWindow({
            content: "<div id='info'>" + $(this).data('info') + "</div>"
        });

        myMarker = new google.maps.Marker({
            position: myLong,
            map: map,
            animation: google.maps.Animation.DROP,
            title: $(this).data('info')
        });
        if (lt == $(this).data('latitude') && lg == $(this).data('longitude')) {
            tile.open(map, myMarker);
        }
        ;
    });

    $('.eachPopular').on('mouseover', function () {


    });

}

$('.eachPopular').on('mouseover', function () {

    lt=$(this).data('latitude');
    lg=$(this).data('longitude');
    if(lt!="" &&lg!="")
     {
     initMap();
     }
    /*var  myLong = new google.maps.LatLng($(this).data('latitude'), $(this).data('longitude'));
     
     map = new google.maps.Map(mapDiv, {
     zoom: 6,
     center: myLong,
     mapTypeId: google.maps.MapTypeId.ROADMAP
     });
     
     myMarker = new google.maps.Marker({
     position: myLong,
     map: map,
     animation: google.maps.Animation.DROP,
     title: "hello World!!!"
     });
     
     
     i = $(this).data('seq');
     tile[i].open(map, myMarker);*/
});

$('#hyd').on('mouseleave', function () {
    tile.close(map, myMarker);
});








