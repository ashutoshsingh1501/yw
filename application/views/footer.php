</div>  <?php /* First div of the document  */ ?>

<div class="container footer">
	<div class="wrapper">
		<div class="footerNavBlocks mobHide">
			<div class="mobileNavBlocks">
				<div class="footerEachNavCol">
					<h5 class="footerHeading">Quick links</h5>
					<ul class="footerNavList">
						<li class="active"><a href="index.php" class="mobHome">Home</a></li>
						<li><a href="browse-category.php" class="mobCategories">Browse category</a></li>
						<li><a href="search-business.php" class="mobSearch">Search business</a></li>
						<li><a href="login.php" class="mobLogin">Login your account</a></li>
					</ul>
				</div>

				<div class="footerEachNavCol businessMenu">
					<h5 class="footerHeading">Business</h5>
					<ul class="footerNavList">
						<li><a href="add-business.php">Add your Business</a></li>
						<li><a href="market-business.php">Market your Business</a></li>
						<li><a href="claim-business.php">Claim your Business</a></li>
						<li><a href="advertise-with-us.php">Advertise with us</a></li>
					</ul>
				</div>

				<div class="footerEachNavCol">
					<h5 class="footerHeading ypFooterHeading">Yellowpages</h5>
					<ul class="footerNavList">
						<li><a href="about-us.php" class="mobAboutUs">About us</a></li>
						<li><a href="contact-us.php" class="mobContactUs">Contact us</a></li>
						<li><a href="feedback.php" class="mobFeedback">Feedback</a></li>
						<li><a href="faqs.php" class="mobFaqs">FAQs</a></li>
						<li><a href="careers.php" class="mobCareers">Careers</a></li>
						<li><a href="terms-conditions.php" class="mobTermsConditions">Terms &amp; Conditions</a></li>
						<li><a href="privacy-policy.php" class="mobPrivacyPolicy">Privacy Policy</a></li>
						<li><a href="end-user-agreement.php" class="mobAcceptancePolicy">End User Agreement</a></li>
					</ul>
				</div>
			</div>

			<div class="footerSocialMobile">
				<div class="footerEachNavCol">
					<h5 class="footerHeading">Mobile &amp; Social</h5>
					<div class="footerNavList">
						<a href="#" class="checkCallers">Check unknown callers</a>
						<div class="footerSocialLinks">
							<span class="footerSocialMobTitle">Connect with us</span>
							<a href="#" class="footFacebook">Facebook</a>
							<a href="#" class="footTwitter">Twitter</a>
							<a href="#" class="footGooglePlus">Google Plus</a>
							<a href="#" class="footYouTube">You Tube</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container copyRights">
	<div class="wrapper">
		Copyright &copy; 2016, Yellowpages.in. All rights reserved.
	</div>
</div>






<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('.mobileMenu').click(function() {
			if($('.footerNavBlocks').hasClass('mobHide')) {
				$('.footerNavBlocks').removeClass('mobHide');
				//$('.main').addClass('showMobMenu');
				$('.main').append("<div class='overlayMobNav'></div>");
			} else {
				$('.footerNavBlocks').addClass('mobHide');
				//$('.main').removeClass('showMobMenu');
				$('.main').remove(".overlayMobNav");
			}
		});
	});
</script>





<?php
	if($js!= '') {
		echo $js;
	}
?>

</body>
</html>