<?php // echo "<pre>"; print_r($bbusiness);exit;?>


<div class="mobileHeader transparent">
    <div class="mobileMenu white"></div>
</div>

<div id="txtHint"></div>
<div class="container businessBanner">
    <div class="wrapper">
        <div class="businessBannerImg" style="background-image:url(<?php if($bbusiness[0]['is_featured']){
            echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/1170x480_'.$bbusiness[0]['image_note'].'-1.jpg';
        }else{
            echo STYLEURL.'business-images/1170x480_default.png';
        }?>);">
        <?php if($bbusiness[0]['carousel_count']>1){ ?>
            <button class="mobileImagesCount"><?php echo $bbusiness[0]['carousel_count']-1;?></button>
        <?php } ?>
        </div>
        <div class="businessTitleOptions">
            <div class="businessTitleInfo">
                <h1 class="businessTitle"><?php echo $bbusiness[0]['title']; ?> 
                    <span class="<?php
                    if ($bbusiness[0]['is_verified']) {
                        echo "ypApproved";
                    }
                    ?>  tooltip">
                        <span class="tooltipContent">Verified by Yellowpages</span>
                    </span>
                    <span class="certified aaP tooltip">
                        <span class="tooltipContent">Text runs here like this</span><?php echo $bbusiness[0]['rating_name']; ?></span></h1>
                <?php if (!empty($bbusiness[0]['roverall']) && !empty($bbusiness[0]['treview'])) { ?>
                    <p class="ratingBlock">
                        <span class="rating r<?php echo str_replace(".", "-", $bbusiness[0]['roverall']); ?>"><?php echo $bbusiness[0]['roverall']; ?></span>
                        <a href="#reviews" class="ratingCount"><?php
                            if ($bbusiness[0]['treview']) {
                                if ($bbusiness[0]['treview'] > 1) {
                                    echo $bbusiness[0]['treview'] . " reviews";
                                } else {
                                    echo $bbusiness[0]['treview'] . " review";
                                }
                            }
                            ?></a>
                    </p>
                <?php } ?>
                <div class="businessContact">
                    <address class="businessAddress">
                        <?php echo $bbusiness[0]['area_name']; ?><br>
                        <?php
                        echo $bbusiness[0]['city_name'];
                        if ($bbusiness[0]['pincode']) {
                            echo " - " . $bbusiness[0]['pincode'];
                        }
                        ?>
                    </address>
                    <?php
                    $phone = ($bbusiness[0]['phone'] == "") ? $bbusiness[0]['landline'] : "+91 " . $bbusiness[0]['phone'];
                    if ($phone != "") {
                        ?>
                        <a <?php
                        if ($mobile) {
                            echo "href='tel:" . $phone . "' ";
                        }
                        ?> class="businessContactNumber"><?php echo $phone; ?></a>
                        <?php } ?>
                </div>
            </div>

            <div class="businessTopActions">
                <a href="#reviews" class="writeReviewBtn">Write a review</a>

                <button class="shareBtn tooltip">Share 
                    <span class="tooltipContent">
                        <a rel="nofollow" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() . "?id=" . $bbusiness[0]['id']; ?>" class="share-icon share-facebook" onclick="window.open(this.href, 'facebook-share', 'width=580,height=296');return false;" ></a>
                        <a rel="nofollow" href="http://twitter.com/share?text=&url=<?php echo current_url() . "?id=" . $bbusiness[0]['id']; ?>" class="share-icon share-twitter" onclick="window.open(this.href, 'twitter-share', 'width=550,height=235');return false;"></a>
                        <a rel="nofollow" href="https://plus.google.com/share?url=<?php echo current_url() . "?id=" . $bbusiness[0]['id']; ?>" class="share-icon share-googlePlus" onclick="window.open(this.href, 'google-plus-share', 'width=490,height=530');return false;"></a>
                    </span>
                </button>
                <input type="hidden" value="<?php echo $bbusiness[0]['id']; ?>" id="sms">
                <button class="mobileBtn snedSMSBtn tooltip">Send SMS<span class="tooltipContent">SMS Business Details</span></button>
                <button class="<?php echo $favclass; ?>" onclick="showUser(<?php echo $bbusiness[0]['id']; ?>)">Favorite<span class="tooltipContent right">Favourite This Business</span></button>


            </div>

            <div class="businessBottomActions">
                <?php if ($bbusiness[0]['website_url'] != "") { ?>
                    <a rel="nofollow" href="<?php echo $bbusiness[0]['website_url']; ?>" class="websiteBtn" target="_blank">Website </a>
                <?php } ?>
                <?php if ($bbusiness[0]['email'] != "") { ?>
                    <a href="mailto:<?php echo $bbusiness[0]['email']; ?>" target="_blank" class="emailBtn">Email </a>
                <?php } ?>
                <?php
                if (!empty($bbusiness[0]['latitude']) && !empty($bbusiness[0]['longitude'])) {
                    echo "<a href='//maps.google.com/maps?f=d&amp;daddr=" . $bbusiness[0]['latitude'] . "," . $bbusiness[0]['longitude'] . "&amp;hl=en' target='_blank' class='directionsBtn'>Directions</a>";
                }
                ?>
            </div>
        </div>
    </div>

    <?php if($bbusiness[0]['carousel_count']>=2){?>
        <div class="wrapper">
            <div class="businessDeskAllImages">
                <ul class="bueinssDeskAllImagesList">
                    <?php if($bbusiness[0]['carousel_count']==2){?>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-2.jpg';?>);">
                            </div>
                        </li>
                    <?php }elseif($bbusiness[0]['carousel_count']==3){?>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-3.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-2.jpg';?>);">
                            </div>
                        </li>
                    <?php }elseif($bbusiness[0]['carousel_count']==4){ ?>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-4.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-3.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-2.jpg';?>);">
                            </div>
                        </li>
                    <?php }elseif($bbusiness[0]['carousel_count']==5){ ?>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-5.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-4.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-3.jpg';?>);">
                            </div>
                        </li>
                        <li>
                            <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-2.jpg';?>);">
                            </div>
                        </li>
                    <?php }elseif($bbusiness[0]['carousel_count']>5){
                        for($i=$bbusiness[0]['carousel_count'];$i>$bbusiness[0]['carousel_count']-4;$i--){ ?>
                            <li>
                                <div class="eachBusinessDeskImage" style="background-image: url(<?php echo STYLEURL.'business-images/'.$bbusiness[0]['image_note'].'/190x125_'.$bbusiness[0]['image_note'].'-'.$i.'.jpg';?>);">
                                </div>
                            </li>
                        <?php } ?>
                            <li>
                                <div class="eachBusinessDeskImage viewMore">
                                    View more <br> <?php echo $bbusiness[0]['carousel_count']-5; ?>+
                                </div>
                            </li>
                    <?php }?>
                </ul>
            </div> 
        </div>    
    <?php }?>
    <div class="wrapper">
        <div class="businessShortDesc">
            <?php if ($bbusiness[0]['description'] != "") { ?>
                <div class="offerInfo <?php
                if (strlen($bbusiness[0]['description']) > 250) {
                    echo " showLess";
                }
                ?>">
                    <p><?php echo $bbusiness[0]['description']; ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="business overview">
    <div class="wrapper">
        <ul class="businessMainTabs">
            <li class="overviewTab active"><a href="#overview" class="businessMainEachTab">Overview</a></li>
            <li class="reviewsTab"><a href="#reviews" class="businessMainEachTab">Reviews</a></li>
            <li class="offersTab"><a href="#offers" class="businessMainEachTab">Offers</a></li>
        </ul>

        <div class="bDetailTabContent bDetailOverview showTabContent" id="overview">
            <div class="mobileTitleDisplay">
                Overview
            </div>
            <div class="businessLeft leftSmallCol">
                <div class="businessMainContent overviewBlock">

                    <?php if (isset($bbusiness[0]['est_year']) && $bbusiness[0]['est_year'] > 0) { ?>
                        <div class="yearsBand">
                            <span class="years"><?php echo date('Y') - $bbusiness[0]['est_year']; ?></span>
                            <span class="yearsTxt">Years</span>
                        </div>
                    <?php } ?>

                    <div class="brandLogo">
                        <span class="brandLogoImage">
                            <?php if($bbusiness[0]['is_logo']==0){ ?>
                            <img src="<?php echo $bbusiness[0]['image_path']; ?>" alt="">
                            <?php } else { ?>
                            <img src="<?php echo STYLEURL.'logo/'.$bbusiness[0]['image_note'].'-0.jpg';?>" alt="">
                            <?php } ?>
                        </span>
                        <span class="brandNearByLoc"><?php echo $bbusiness[0]['landmark']; ?></span>
                    </div>

                    <?php
                    if ($bbusiness[0]['address']) {
                        $comlplete_address = $bbusiness[0]['address'] . ", " . ucfirst($bbusiness[0]['area_name']) . ", " . ucfirst($bbusiness[0]['city_name']);
                        if ($bbusiness[0]['pincode']) {
                            $comlplete_address .= " - " . $bbusiness[0]['pincode'];
                        }
                        ?>
                        <div class="businessConctNumb overviewEachRow">
                            <h4 class="businessSubHeading">Address</h4>
                            <div class="contactNumbers">
                                <?php echo wordwrap($comlplete_address, 18, "<br />"); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($bbusiness[0]['alternative_number']) { ?>
                        <div class="businessConctNumb overviewEachRow">
                            <h4 class="businessSubHeading">Alternative Numbers</h4>
                            <div class="contactNumbers">
                                <?php echo str_replace(",", "<br>+91 ", '+91 ' . $bbusiness[0]['alternative_number']); ?><br>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if ($bbusiness[0]['email']) { ?>
                        <div class="businessTimining overviewEachRow">
                            <h4 class="businessSubHeading">Email</h4>
                            <a href="mailto:<?php echo $bbusiness[0]['email']; ?>" target="_blank" class="businessMore"><?php echo $bbusiness[0]['email']; ?></a>
                        </div>
                    <?php } ?>

                    <?php if ($bbusiness[0]['facebook_url'] || $bbusiness[0]['twitter_url'] || $bbusiness[0]['video_url'] || $bbusiness[0]['google_url'] || $bbusiness[0]['intragram_url'] || $bbusiness[0]['pinterest_url']) { ?>

                        <div class="socialLinks overviewEachRow socialLinks">
                            <h4 class="businessSubHeading">Social Links</h4>
                            <?php if ($bbusiness[0]['facebook_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['facebook_url']; ?>">Facebook</a>
                            <?php } ?>
                            <?php if ($bbusiness[0]['twitter_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['twitter_url']; ?>">Twitter</a>
                            <?php } ?>
                            <?php if ($bbusiness[0]['video_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['video_url']; ?>">Youtube</a>
                            <?php } ?>
                            <?php if ($bbusiness[0]['google_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['google_url']; ?>">Google+</a>
                            <?php } ?>
                            <?php if ($bbusiness[0]['intragram_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['intragram_url']; ?>">Instragram</a>
                            <?php } ?>
                            <?php if ($bbusiness[0]['pinterest_url']) { ?>
                                <a href="<?php echo $bbusiness[0]['pinterest_url']; ?>">Pinterest</a>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if (!empty($btime)) { ?>
                        <div class="businessTimining overviewEachRow">
                            <h4 class="businessSubHeading">Business timings</h4>
                            <ul class="leftColList showLessList bTimings">
                                <?php for ($i = 0; $i < count($btime); $i++) { ?>
                                    <li>
                                        <div class="timingsList">
                                            <span class="dayDisplay"><?php echo $btime[$i]['day'] ?></span>
                                            <span class="timeDisplay"><?php echo $btime[$i]['time'] ?></span>
                                            <span class="openStatus <?php echo $btime[$i]['status'] ?>"><?php echo ucfirst($btime[$i]['status']); ?></span>  <!-- closed -->
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="businessMore">More</div>
                        </div>
                    <?php } ?>

                    <?php if ($bservice != "") { ?>
                        <div class="overviewEachRow ourServices">
                            <h4 class="businessSubHeading">Our services</h4>
                            <ul class="ourServicesList showLessList">
                                <?php
                                $bservice = explode(',', trim($bservice));
                                foreach ($bservice as $key => $bs) {
                                    if ($bs != "") {
                                        ?>
                                        <li><?php echo $bs; ?></li>
                                        <?php
                                    }
                                }
                                ?>	
                            </ul>
                            <?php if (count($bservice) > 4) { ?><div class="businessMore">More</div><?php } ?>		
                        </div>
                    <?php } ?>

                    <?php if ($bbrand != "") { ?>
                        <div class="overviewEachRow ourServices brands">
                            <h4 class="businessSubHeading">Our Brands</h4>
                            <ul class="ourServicesList showLessList">
                                <?php
                                $bbrand = explode(",", trim($bbrand));
                                foreach ($bbrand as $key => $bs) {
                                    if ($bs != "") {
                                        ?>
                                        <li><?php echo $bs; ?></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <?php if (count($bbrand) > 4) { ?><div class="businessMore">More</div><?php } ?>	
                        </div>
                    <?php } ?>

                    <?php if ($bpayment != "") { ?>
                        <div class="overviewEachRow paymentMethods">
                            <h4 class="businessSubHeading">Payment methods</h4>
                            <ul class="ourServicesList">
                                <?php
                                $bpayment = explode(",", trim($bpayment));
                                foreach ($bpayment as $key => $bs) {
                                    if ($bs != "") {
                                        ?>
                                        <li><?php echo $bs; ?></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    <?php } ?>

                    <?php if ($bamenties != "") { ?>
                        <div class="overviewEachRow ourServices amenities">
                            <h4 class="businessSubHeading">Amenities</h4>
                            <ul class="ourServicesList showLessList">
                                <?php
                                $bamenties = explode(",", trim($bamenties));
                                foreach ($bamenties as $key => $bs) {
                                    if ($bs != "") {
                                        ?>
                                        <li><?php echo $bs; ?></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                            <?php if (count($bamenties) > 4) { ?><div class="businessMore">More</div><?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="businessRight rightBigCol">

                <div class="businessMapBanner">
                    <div class="businessMap">
                        <?php
                            if (!empty($bbusiness[0]['latitude']) && !empty($bbusiness[0]['longitude'])) {
                                echo "<a href='//maps.google.com/maps?f=d&amp;daddr=" . $bbusiness[0]['latitude'] . "," . $bbusiness[0]['longitude'] . "&amp;hl=en' target='_blank' class='expandButton'>Expand</a>";
                                echo '<iframe src="https://maps.google.com/maps?q='.$bbusiness[0]['latitude'].','.$bbusiness[0]['longitude'].'&z=15&output=embed" width="380" height="250" frameborder="0" style="border:0;" allowfullscreen></iframe>';
                            }else {
                                    $area = $bbusiness[0]['area_name'];
                                    $address = $bbusiness[0]['landmark'];
                                    $prepAddr = str_replace(' ', '+', $area);
                                    $addr = str_replace(' ', '+', $address);
                        ?>
                                    <a href="https://www.google.com/maps/dir//<?php echo $addr; ?>+Hyderabad" class="expandButton" target="_blank">Expand</a>                        
                                    <iframe src="https://maps.google.com/maps?q=<?php echo $addr; ?>+<?php echo $prepAddr; ?>+ON&loc:43.25911+-79.879494&z=15&output=embed" width="380" height="250" frameborder="0" style="border:0;" allowfullscreen></iframe>
                            <?php   }?>            
                    </div>

                    <div class="businessBannerBlock">
                        <div class="bannerImageBlock">
                            <img src="<?php echo STYLEURL; ?>front/images/businessBanner.jpg" alt="" class="bannerImage">
                            <div class="claimBusiness">
                                <span class="claimBusinessText">Is this your business?</span>
                                <a href="javascript:void(0);" class="claimBusinessBtn">Claim your business</a>
                            </div>
                        </div>

                        <div class="businessEditReport">
                            <span class="foundErrorsTxt">Found a problem with this listing?</span>
                            <span class="suggestEdit">Suggest edit</span>
                            <span class="reportBusiness">Report this business</span>
                        </div>

                        <div class="mobileMainTabs">
                            <a href="#reviews" class="mobileMainTabLinks reviewsMobLink">Reviews</a>
                            <a href="#offers" class="mobileMainTabLinks offersMobLink">Offers</a>
                        </div>
                    </div>
                </div>

                <?php if (empty($breview)) { ?>
                    <div class="rightBigCol ratingInputsBlock businessOveralRatingBlock">
                        <div class="overalRatingInputs">

                        </div>
                    </div>
                <?php } else { ?>
                    <div class="businessOveralRatingBlock">
                        <div class="overalRatingblock">
                            <div class="overallRatingInfo">
                                <h4 class="overalRatingText">Overall Rating 
                                    <?php if($bbusiness[0]['roverall'] && $bbusiness[0]['treview']){?>
                                        <span class="rating r<?php echo str_replace(".", "-", $bbusiness[0]['roverall']); ?>"><?php echo $bbusiness[0]['roverall']; ?></span>
                                        <span class="ratingCount"><?php echo $bbusiness[0]['treview']; ?> rating</span> 
                                    <?php }?>
                                </h4>
                            </div>


                            <div class="overalUserRatingInfo">
                                <div class="overalEachRating">
                                    <span class="fullRatingRey">
                                        <span class="acutalRating rating5" style="width: <?php
                                        if ($treview) {
                                            echo ($rating5 / $treview) * 100;
                                        } else {
                                            echo '0';
                                        }
                                        ?>%"></span>
                                    </span>
                                    <span class="usersNumber"><?php echo $rating5; ?> users</span>
                                </div>

                                <div class="overalEachRating">
                                    <span class="fullRatingRey">
                                        <span class="acutalRating rating4" style="width: <?php
                                        if ($treview) {
                                            echo ($rating4 / $treview) * 100;
                                        } else {
                                            echo '0';
                                        }
                                        ?>%"></span>
                                    </span>
                                    <span class="usersNumber"><?php echo $rating4; ?> users</span>
                                </div>

                                <div class="overalEachRating">
                                    <span class="fullRatingRey">
                                        <span class="acutalRating rating3" style="width: <?php
                                        if ($treview) {
                                            echo ($rating3 / $treview) * 100;
                                        } else {
                                            echo '0';
                                        }
                                        ?>%"></span>
                                    </span>
                                    <span class="usersNumber"><?php echo $rating3; ?> users</span>
                                </div>

                                <div class="overalEachRating">
                                    <span class="fullRatingRey">
                                        <span class="acutalRating rating2" style="width: <?php
                                        if ($treview) {
                                            echo ($rating2 / $treview) * 100;
                                        } else {
                                            echo '0';
                                        }
                                        ?>%;"></span>
                                    </span>
                                    <span class="usersNumber"><?php echo $rating2; ?> users</span>
                                </div>

                                <div class="overalEachRating">
                                    <span class="fullRatingRey">
                                        <span class="acutalRating rating1" style="width: <?php
                                        if ($treview) {
                                            echo ($rating1 / $treview) * 100;
                                        } else {
                                            echo '0';
                                        }
                                        ?>%"></span>
                                    </span>
                                    <span class="usersNumber"><?php echo $rating1; ?> users</span>
                                </div>
                            </div>
                    </div>
                    <?php if ($total_upvotes != "0") { ?>
                        <div class="overalVotes">
                            <div class="overalVotesCircle upVote"> <!-- downVote -->
                                    <strong><?php echo round($avarage_count); ?>%</strong>
                                Upvotes
                            </div>
                            <div class="overalBasedCount">
                                    Based on <?php echo $total_upvotes; ?> <?php echo $upvotetitle; ?>
                            </div>
                        </div>
                    <?php } ?>
                    </div>

                    <div class="topAllReviewsBlock">
                        <div id="review_responce33"></div>
                        <span class="topTab">Top</span>
                        <a href="#" class="allReveiwsTab">All reviews (<?php echo count($wreview); ?>)</a>
                    </div>

                    <div class="reviewsListBlock">
                        <ul class="reviewsList">
                            <?php $i = 1;
                            foreach ($breview as $key => $br) { ?>
                                <li>
                                    <div class="eachBusinessReview">
                                        <div class="photoVotesBlock">
                                            <div class="reviewerPhoto">
                                                <img src="<?php if (isset($iusers[$br->user_id])) {
                                                    echo STYLEURL . "front/images/" . $iusers[$br->user_id];
                                                } else {
                                                    echo STYLEURL . 'front/images/nophoto.jpg';
                                                } ?>" alt="">
                                                <input type="hidden" id="review_id<?php echo $i; ?>" value="<?php echo $br->id; ?>">   
                                                <input type="hidden" id="upvote_id<?php echo $i; ?>" value="1">   
                                                <input type="hidden" id="downvote_id<?php echo $i; ?>" value="-1"> 
                                                <input type="hidden" id="id<?php echo $i; ?>" value="<?php echo $i; ?>">                             
                                            </div>
                                            <div class="reviewVoting" id="review_responce<?php echo $i; ?>">
                                                <span class="upVoting" id="upclick<?php echo $i; ?>"></span>
                                                <span class="reviewVotingNumb">
                                                    <div><?php echo $br->upvote; ?></div></span>
                                                <span class="downVoting" id="downclick<?php echo $i; ?>"></span>
                                                <span class="reviewVotingNumb" id="down">
                                                    <div> </div></span>

                                            </div>
                                        </div>

                                        <div class="reviewTextBlock">
                                            <a href="#" class="reviewTitle"><?php echo $nusers[$br->user_id]; ?></a>
                                            <div class="ratingBlock">
                                                <span class="rating r<?php echo str_replace(".", "-", $br->rating); ?>"><?php echo $br->rating; ?></span>
                                                <span class="ratingTimeDate"><?php echo date('Y-m-d H:i', strtotime($br->added_on)); ?></span>
                                            </div>
                                            <p><?php echo $br->comments; ?></p>
                                        </div>
                                    </div>
                                </li>
                                <?php $i++;
                            } ?>
                        </ul>
                        <?php if ($treview > 3) { ?>
                            <a href="#reviews" class="readAllReviews ypButton">Read all reviews</a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>

        </div>   <?php /* Business Overview Ends */ ?>

        <div class="bDetailTabContent bDetailReview" id="reviews">  <?php /* Business Reviews Starts */ ?>
            <div class="mobileTitleDisplay">
                Reviews
            </div>
            <div class="reviewRatingInputs">
                <div class="leftSmallCol">
                    <div class="overalRatingblock">
                        <div class="overallRatingInfo">
                            <h4 class="overalRatingText">Overall Rating 
                                <?php if($bbusiness[0]['roverall'] && $bbusiness[0]['treview']){?>
                                    <span class="rating r<?php echo str_replace(".", "-", $bbusiness[0]['roverall']); ?>"><?php echo $bbusiness[0]['roverall']; ?></span>
                                    <span class="ratingCount"><?php echo $bbusiness[0]['roverall']; ?> rating</span>
                                <?php }?>
                            </h4>
                        </div>


                        <div class="overalUserRatingInfo">
                            <div class="overalEachRating">
                                <span class="fullRatingRey">
                                    <span class="acutalRating rating5" style="width: <?php
                                    if ($treview) {
                                        echo ($rating5 / $treview) * 100;
                                    } else {
                                        echo '0';
                                    }
                                    ?>%"></span>
                                </span>
                                <span class="usersNumber"><?php echo $rating5; ?> users</span>
                            </div>

                            <div class="overalEachRating">
                                <span class="fullRatingRey">
                                    <span class="acutalRating rating4" style="width: <?php
                                    if ($treview) {
                                        echo ($rating4 / $treview) * 100;
                                    } else {
                                        echo '0';
                                    }
                                    ?>%"></span>
                                </span>
                                <span class="usersNumber"><?php echo $rating4; ?> users</span>
                            </div>

                            <div class="overalEachRating">
                                <span class="fullRatingRey">
                                    <span class="acutalRating rating3" style="width: <?php
                                    if ($treview) {
                                        echo ($rating3 / $treview) * 100;
                                    } else {
                                        echo '0';
                                    }
                                    ?>%"></span>
                                </span>
                                <span class="usersNumber"><?php echo $rating3; ?> users</span>
                            </div>

                            <div class="overalEachRating">
                                <span class="fullRatingRey">
                                    <span class="acutalRating rating2" style="width: <?php
                                    if ($treview) {
                                        echo ($rating2 / $treview) * 100;
                                    } else {
                                        echo '0';
                                    }
                                    ?>%;"></span>
                                </span>
                                <span class="usersNumber"><?php echo $rating2; ?> users</span>
                            </div>

                            <div class="overalEachRating">
                                <span class="fullRatingRey">
                                    <span class="acutalRating rating1" style="width: <?php
                                    if ($treview) {
                                        echo ($rating1 / $treview) * 100;
                                    } else {
                                        echo '0';
                                    }
                                    ?>%"></span>
                                </span>
                                <span class="usersNumber"><?php echo $rating1; ?> users</span>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="review_responce"></div>
                <div class="rightBigCol ratingInputsBlock">
                    <div class="overalRatingInputs">
                        <div class="reviewUserInfo">
                            <div class="userPhoto">
                                <img src="<?php echo STYLEURL; ?>front/images/nophoto.jpg" alt="">
                            </div>
                        </div>
                        <div class="reviewUserInputBlock">

                            <div class="reviewUserNameRating">
                                <div class="reviewUserName">Write a review</div>
                                <div class="ratingFormBlock">
                                    <form action="" class="ratingFormList">
                                        <input type="radio" class="radioRating" id="5" name="rating" value="5">
                                        <label for="5" class="radioRatingLabel">5</label>
                                        <input type="radio" class="radioRating" id="4" name="rating" value="4">
                                        <label for="4" class="radioRatingLabel">4</label>
                                        <input type="radio" class="radioRating" id="3" name="rating" value="3">
                                        <label for="3" class="radioRatingLabel">3</label>
                                        <input type="radio" class="radioRating" id="2" name="rating" value="2">
                                        <label for="2" class="radioRatingLabel">2</label>
                                        <input type="radio" class="radioRating" id="1" name="rating" value="1">
                                        <label for="1" class="radioRatingLabel">1</label>
                                        <input type="hidden" value="<?php echo $bbusiness[0]['id']; ?>" id="bbbid">
                                    </form>

                                </div>
                            </div>
                            <textarea name="rating_desc" id="rating_desc" cols="30" rows="10" class="userReviewTextbox"></textarea>
                            <?php if($front_user_logged=="yes")
                            { ?>
                                <input type="submit" class="reviewSubmit ypButton" value="Post Review" id="post_review">
                            <?php } else { ?>
                                <a href="<?php echo base_url(); ?>my-account"><input type="button" class="reviewSubmit ypButton" value="Post Review"></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="advtReviewsList">
                <div class="reviewLeft leftSmallCol">
                    <div class="businessMainContent overviewBlock">
                        <div class="bannerImageBlock">
                            <img src="<?php echo STYLEURL; ?>front/images/businessBanner.jpg" alt="" class="bannerImage">
                            <div class="claimBusiness">
                                <span class="claimBusinessText">Is this your business?</span>
                                <a href="#" class="claimBusinessBtn">Claim your business</a>
                            </div>
                        </div>
                        <div class="bottomLeftAd overviewEachRow">
                            <a href="#">
                                <img src="<?php echo STYLEURL; ?>front/images/mcDonaldAd.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="businessRight rightBigCol">
                    <?php if (empty($breview)) { ?>
                    <?php } else { ?>
                        <div class="reviewsHeadFilters">
                            <div class="reviewHeadTitle">All Reviews</div>

                            <div class="filerSortBlock">
                                <div class="reviewFilters">
                                    <div class="reviewFilterTitle">Filter</div>
                                    <div class="reviewFilterOptions">
                                        <a href="#" class="reviewEachFilter active">All</a>
                                        <a href="#" class="reviewEachFilter">Top</a>
                                        <a href="#" class="reviewEachFilter">Most Recent</a>
                                    </div>
                                </div>

                                <div class="reviewFilterSort">
                                    <div class="reviewFilterSortBox select">
                                        <span class="selectText">Sort results</span>
                                         <select name="rate" id="" class="selectBox" onchange="business_ratings(this.value,<?php echo $_GET['id']; ?>)">
                                            <option value="1.0">Rating &#9733;</option>
                                            <option value="2.0">Rating &#9733;&#9733;</option>
                                            <option value="3.0">Rating &#9733;&#9733;&#9733;</option>
                                            <option value="4.0">Rating &#9733;&#9733;&#9733;&#9733;</option>
                                            <option value="5.0">Rating &#9733;&#9733;&#9733;&#9733;&#9733;</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="reviewsListBlock">
                        <div id="loading_business_reviews"></div>
                        <div id="container_business_reviews">
                            <div class="data"></div>
                            <div class="pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobileMainTabs">
                <a href="#overview" class="mobileMainTabLinks overviewMobLink">Overview</a>
                <a href="#offers" class="mobileMainTabLinks offersMobLink">Offers</a>
            </div>
        </div>   

        <?php
        if (count($beoffer)) {
            ?>
            <div class="bDetailTabContent bDetailOffers" id="offers">   <?php /* Business Offers Starts */ ?>
                <div class="mobileTitleDisplay">
                    Offers
                </div>
                <div class="businessOffersBlock">
                    <div class="leftSmallCol">
                        <div class="leftSmallColEach">
                            <ul class="smallImageBlocks">
                                <?php for ($i = 0; $i < 4; $i++) { ?>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo STYLEURL; ?>front/images/advt.png" alt="">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="rightBigCol">
                        
                        
                        <div class="offersBlock" id="offersresponce">
                            <ul class="offersList">
                            <?php $i=1; foreach($boffer as $key => $bo){ ?>
                                    <li>
                                        <div class="eachOffer">
                                            <div class="offerImage">
                                                <img src="<?php
                                                if ($bo->image_path) {
                                                    echo $bo->image_path;
                                                } else {
                                                    echo STYLEURL . 'front/images/offersImage.jpg';
                                                }
                                                ?>" alt="">
                                                <span class="offerType <?php echo lcfirst($bo->deal_type); ?>"><?php echo $bo->deal_type; ?></span>
                                            </div>
                                            <div class="offerInfoBlock">
                                                <div class="offerTitle"><?php echo $bo->title; ?></div>
                                                <div class="offerCodeBlock">
                                                    <?php if (lcfirst($bo->deal_type) == 'coupon') { ?>
                                                        <div class="offerCode"><?php
                                                            if ($bo->offer_url) {
                                                                echo $bo->offer_url;
                                                            } else {
                                                                echo 'YELLOWXXXX';
                                                            }
                                                            ?></div>
                                                        <div class="offerActions">
                                                <span class="offerCopyCode offerEachAction" onclick="return copyCode<?php echo $i; ?>();">Copy Code</span>
						<input type="hidden" value="<?php echo $bo->id;?>" id="ofr<?php echo $i; ?>">
                                                <span class="offerInbox offerEachAction inbox<?php echo $i; ?>">Get in my inbox</span>
                                                        </div>
                                                        <div class="offerExpiryDate">Expires: <span class="red"><?php
                                                                if ($bo->expire_on != '0000-00-00 00:00:00') {
                                                                    echo date('Y-m-d', strtotime($bo->expire_on));
                                                                } else {
                                                                    echo 'Lifetime';
                                                                }
                                                                ?></span></div>
                                                    <?php } else { ?>
                                                        <div class="offerActions">
                                                            <span class="offerCopyCode offerEachAction">Avail Deal</span>
                                                            <span class="offerInbox offerEachAction">Get in my inbox</span>
                                                        </div>
                                                        <div class="offerExpiryDate">Expires: <span class="red"><?php
                                                                if ($bo->expire_on != '0000-00-00 00:00:00') {
                                                                    echo date('Y-m-d', strtotime($bo->expire_on));
                                                                } else {
                                                                    echo 'Lifetime';
                                                                }
                                                                ?></span></div>
                                                    <?php } ?>
                                                </div>
                                                <div class="offerInfo  <?php
                                                if (strlen($bo->description) > 100) {
                                                    echo " showLess";
                                                }
                                                ?>">
                                                    <p><?php echo $bo->description; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                            <?php if (!empty($beoffer)) { ?>
                                <div class="expeiredCouponsTitle">Expired Coupons</div>
                                <ul class="offersList">
                                    <?php foreach ($beoffer as $key => $bo) { ?>
                                        <li>
                                            <div class="eachOffer">
                                                <div class="offerImage">
                                                    <img src="<?php
                                                    if ($bo->image_path) {
                                                        echo $bo->image_path;
                                                    } else {
                                                        echo STYLEURL . 'front/images/offersImage.jpg';
                                                    }
                                                    ?>" alt="">
                                                    <span class="offerType <?php echo lcfirst($bo->deal_type); ?>"><?php echo $bo->deal_type; ?></span>
                                                </div>
                                                <div class="offerInfoBlock">
                                                    <div class="offerTitle"><?php echo $bo->title; ?></div>
                                                    <div class="offerCodeBlock">
                                                        <?php if (lcfirst($bo->deal_type) == 'coupon') { ?>
                                                            <div class="offerCode"><?php
                                                                if ($bo->offer_url) {
                                                                    echo $bo->offer_url;
                                                                } else {
                                                                    echo 'YELLOWXXXX';
                                                                }
                                                                ?></div>
                                                            <div class="offerActions">
                                                <?php /*<span class="offerCopyCode offerEachAction">Copy Code</span>
                                                <span class="offerInbox offerEachAction">Get in my inbox</span> */?>
                                                            </div>
                                                            <div class="offerExpiryDate">Expires: <span class="red"><?php
                                                                    if ($bo->expire_on != '0000-00-00 00:00:00') {
                                                                        echo date('Y-m-d', strtotime($bo->expire_on));
                                                                    } else {
                                                                        echo 'Lifetime';
                                                                    }
                                                                    ?></span></div>
                                                        <?php } else { ?>
                                                            <div class="offerActions">
                                                                <span class="offerCopyCode offerEachAction">Avail Deal</span>
                                                                <span class="offerInbox offerEachAction">Get in my inbox</span>
                                                            </div>
                                                            <div class="offerExpiryDate">Expires: <span class="red"><?php
                                                                    if ($bo->expire_on != '0000-00-00 00:00:00') {
                                                                        echo date('Y-m-d', strtotime($bo->expire_on));
                                                                    } else {
                                                                        echo 'Lifetime';
                                                                    }
                                                                    ?></span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="offerInfo  <?php
                                                    if (strlen($bo->description) > 100) {
                                                        echo " showLess";
                                                    }
                                                    ?>">
                                                        <p><?php echo $bo->description; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="mobileMainTabs">
                    <a href="#overview" class="mobileMainTabLinks overviewMobLink">Overview</a>							
                    <a href="#reviews" class="mobileMainTabLinks reviewsMobLink">Reviews</a>

                </div>
            </div>
        <?php } ?>
        <?php /* Business Offers Ends  */ ?>
    </div>

    <?php if (!empty($bcategory)) { ?>
        <div class="wrapper">
            <div class="similarBusinessBlock">
                <h3 class="secondaryHeading">Similar businesses</h3>
                <ul class="similarBusinessList">
                    <?php foreach ($bcategory as $key => $fb) { ?> 
                        <li>
                             <a href="<?php echo $fb['link']; ?>" class="eachSimilarBusiness" >
                                <div class="eachSimilarPhoto">
                                    <?php if ($fb['is_verified']) { ?>
                                        <span class="ypApproved">YP</span>
                                    <?php } ?>
                                    <?php if($fb['is_logo']==0){ ?>
                                        <img width="190" height="125" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>"  alt="<?php echo $fb['title'];?>">
                                    <?php } else { ?>
                                        <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$fb['image_note'].'/190x125_'.$fb['image_note'].'-1.jpg';?>" alt="<?php echo $fb['title']; ?>">
                                    <?php } ?>
                                </div>

                                <div class="eachSimilarInfoBlock">
                                    <div  class="eachSimilarTitle"><?php echo $fb['title']; ?></div>
                                    <?php if($fb['area'] !=''){?>
                                    <div class="eachSimilarLocation">
                                        <?php echo ucfirst($fb['area']); ?>
                                    </div>
                                    <?php }?>
                                    <?php if($fb['roverall']!=0.0 && $fb['treview'] !=0){ ?>
                                        <div class="similarRatingBlock">
                                            <span class="rating r<?php echo str_replace(".", "-", $fb['roverall']); ?>"><?php echo $fb['roverall']; ?></span>
                                            <span class="ratingCount">
                                                <?php  if ($fb['treview'] > 1) {
                                                        echo $fb['treview'] . " reviews";
                                                    } else {
                                                        echo $fb['treview'] . " review";
                                                    } ?>
                                            </span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function () {
        // Popup Close
        $('.popupClose').click(function () {
            $('.popupNew').removeClass('showPopup');
            $('body').removeClass('noScroll');
        });

        // Suggest Edit Business
        $('.suggestEdit').click(function () {
            $('#suggestEdit').addClass('showPopup');
            $('body').addClass('noScroll');
        });

        // Report this business
        $('.reportBusiness').click(function () {
            $('#reportBusiness').addClass('showPopup');
            $('body').addClass('noScroll');
        });

       $('.eachBusinessDeskImage, .mobileImagesCount').click(function() {
            $('#imagesPopup').addClass('showPopup');
            var imgMaxHeight = $('#imagesPopup').innerHeight()-55;
            if($(window).width() < 749) {
                $('.eachbDetalImageBlock img').css('max-height', imgMaxHeight);
            } else {
                $('.eachbDetalImageBlock img').css('max-height', imgMaxHeight-164);
            }
            $('body').addClass('noScroll');
            var ind = $(this).parent().index();
            imageCrousel(ind);
        });

        //show more or less
        $('.businessMore').click(function () {
            var showLess = $(this).parent().find('ul');
            if (showLess.hasClass('showLessList')) {
                showLess.removeClass('showLessList');
                $(this).text('Less');
            } else {
                showLess.addClass('showLessList');
                $(this).text('More');
            }
        });

        //Review like or dislike
        $('.upVoting, .downVoting').click(function () {
            $(this).addClass('disabled');
        });

        // Share Active and Inactive
        $('.shareBtn').click(function () {
            $(this).toggleClass('activeTooltip');
        });

        // Overview, Review & Offers Tabs JS
        $('.overviewTab, .overviewMobLink').click(function () {
            if (!$('.bDetailOverview').hasClass('showTabContent')) {
                $('.businessMainTabs').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.bDetailTabContent').removeClass('showTabContent');
                $('.bDetailOverview').addClass('showTabContent');

                $('html, body').animate({
                    scrollTop: $(".bDetailOverview").offset().top - 50
                }, 200);

            }
        });

        $('.reviewsTab, .writeReviewBtn, .ratingCount, .reviewsMobLink, .readAllReviews,.mobileStar').click(reviewTab);

        $('.offersTab, .offersMobLink').click(function () {
            if (!$('.bDetailOffers').hasClass('showTabContent')) {
                $('.businessMainTabs').find('.active').removeClass('active');
                $(this).addClass('active');

                $('.bDetailTabContent').removeClass('showTabContent');
                $('.bDetailOffers').addClass('showTabContent');

                $('html, body').animate({
                    scrollTop: $(".bDetailOffers").offset().top - 50
                }, 200);
            }
        });

        $('.offerInfo').click(function () {
            if ($(this).hasClass('showLess')) {
                $(this).removeClass('showLess').addClass('showMore');
            } else {
                $(this).removeClass('showMore').addClass('showLess');
            }
        });
    });

    var reviewTab = function () {
        if (!$('.bDetailReview').hasClass('showTabContent')) {
            $('.businessMainTabs').find('.active').removeClass('active');
            $('.businessMainTabs').find('.reviewsTab').addClass('active');

            $('.bDetailTabContent').removeClass('showTabContent');
            $('.bDetailReview').addClass('showTabContent');

            $('html, body').animate({
                scrollTop: $(".bDetailReview").offset().top - 50
            }, 200);
        };
    };

    var imageCrousel = function(ind) {
        $('.bDetailImagesCrousel').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true,
            initialSlide : ind
        });
    };
</script>

<script src="<?php echo ASSETSURL; ?>front/js/slick.min.js"></script>
<script>
    $(document).ready(function () {

        $('.similarBusinessList').slick({
            slidesToShow: 5,
            slidesToScroll: 4,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1920,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 4,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 920,
                    settings: {
                        arrows: true,
                        dots: false,
                        slidesToShow: 4,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        arrows: true,
                        dots: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 499,
                    settings: {
                        arrows: true,
                        dots: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
        });
    });
</script>
<script>
<?php $i=1; foreach($boffer as $key => $bo){ ?>
function copyCode<?php echo $i; ?>(){
	var copyDiv<?php echo $i; ?> = document.getElementById("offr<?php echo $i; ?>");
	copyDiv<?php echo $i; ?>.focus();
	document.execCommand('SelectAll');
	document.execCommand("Copy", false, null);		
}
<?php $i++; } ?>
</script>

