<section>
	<div class="section-body">
		<div class="card card-underline">
			<form class="form form-validate" id="myform" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url(); ?>business/profile_info">
				<div class="card-head">
					<header> Add Business </header>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs">
						<li class="active" style="width:25%;"> <button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn btn-default">Profile Info</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn ink-reaction btn-default-dark">Business Info</button></li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn ink-reaction btn-default-dark">Brands and Services</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn ink-reaction btn-default-dark">Payment Info</button> </li>
					</ul>
					<div class="tab-content">
						<div id="profile_info" class="tab-pane fade in active">
							<div class="row">
								<input id="id" value="<?php if(isset($results) && !empty($results)){ echo $results->id; }else{ echo 0; } ?>" type="hidden" class="form-control" name='id' />
								<div class="form-group  col-sm-6 floating-label">
									<input type="text" class="form-control" id="business_name" name="business_name" value="<?php if(isset($results) && !empty($results)){ echo $results->title; } ?>" required=""  minlength="2" >
									<label class="col-sm-12" for="business_name">Business Name</label>
								</div>
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="website_url" name="website_url" value="<?php if(isset($results) && !empty($results)){ echo $results->website_url; } ?>" required="" >
									<p class="help-block">For Example: http://www.example.com</p>
									<label class="col-sm-12" for="website_url">Website</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group  col-sm-6 floating-label">
									<input class="form-control" id="business_phone" name="business_phone" value="<?php if(isset($results) && !empty($results)){ echo $results->phone; } ?>" minlength='9' maxlength=10  type="number" required >
									<label class="col-sm-12" for="business_phone">Phone</label>
								</div>
								<div class="form-group col-sm-3  floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" <?php if(isset($result) && !empty($result)){ if($result->is_phone_display){ echo 'checked'; } } ?> name="is_phone_display" id="is_phone_display"> <span>Display on Site</span> </label>
									</div>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" <?php if(isset($result) && !empty($result)){ if($result->is_phone_notification){ echo 'checked'; } } ?> value="1" name="is_phone_notification" id="is_phone_notification"> <span>Receive Notification</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<input type="number" maxlength="12" minlength="8" value="<?php if(isset($result) && !empty($result)){ echo $result->landline; } ?>" class="form-control" id="primarylandline" name="primarylandline">
									<label class="col-sm-12" for="primarylandline">Landline</label>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" <?php if(isset($result) && !empty($result)){ if($result->is_landline_display){ echo 'checked'; } } ?> value="1" name="is_landline_display" id="is_landline_display"> <span>Display on Site</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<select id="city_ID" name="business_city_id" class="form-control selectpicker" data-live-search="true" required >
										<option value='' data-hidden="true" >All City </option>
										<?php foreach($cities as $key => $sl ){ ?>
											<option  <?php if(isset($result) && !empty($result)){ if($results->city_id == $key){ echo 'selected'; } } ?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>								
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="twitter_url" name="twitter_url" value="<?php if(isset($result) && !empty($result)){ echo $result->twitter_url; } ?>" >
									<label  class="col-sm-12" for="twitter_url">Twitter</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<select id="area_ID" name="business_area_id" class="form-control selectpicker" data-live-search="true" required >
										<option value='' data-hidden="true" >All Area </option>
										<?php foreach($areas as $key => $sl ){ ?>
											<option <?php if(isset($result) && !empty($result)){ if($results->area_id == $key){ echo 'selected'; } } ?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										
									</select>
								</div>
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="facebook_url" name="facebook_url" value="<?php if(isset($result) && !empty($result)){ echo $result->facebook_url; } ?>">
									<label class="col-sm-12"  for="facebook_url">Facebook</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6  floating-label">
									<input type="email" class="form-control" id="business_email" name="business_email" value="<?php if(isset($results) && !empty($results)){ echo $results->email; } ?>" required>
									<label class="col-sm-12" for="business_email">Email</label>
								</div>
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="pinterest_url" name="pinterest_url" value="<?php if(isset($result) && !empty($result)){ echo $result->pinterest_url; } ?>">
									<label class="col-sm-12" for="pinterest_url">Pinterest</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<input class="form-control" id="fax_number"  value="<?php if(isset($result) && !empty($result)){ echo $result->fax_number; } ?>" name="fax_number"  type="number"  step="1" minlength="6" maxlength="12" >
									<label class="col-sm-12" for="fax_number">Fax</label>
								</div>
								<div class="form-group col-sm-6 floating-label">
									<input type="number"step="1" class="form-control" id="tollfree" name="tollfree" minlength="8" maxlength="12" value="<?php if(isset($result) && !empty($result)){ echo $result->tollfree_number; } ?>" >
									<label  class="col-sm-12" for="tollfree">Tollfree Number</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6">
									<p id="error-msg" class="has-error" style="color:red;font-weight:400;"></p>
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage" checked> <span>Make this as featured image</span> </label>
									</div>
								</div>
								<div class="form-group col-sm-1">
									<button class="btn btn-default" onclick="addbimage()" type="button"><i class="fa fa-plus"></i></button>
								</div>
							</div>
							<div class="row" id='row_1' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_2' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
									
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_3' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_4' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_5' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_6' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_7' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_8' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row"  id='row_9' style="display:none;">
								<div class="form-group col-sm-6">
									<input type="file" class="form-control" multiple id="image" name="image[]" accept=".jpeg , .jpg , .png">
									<p class="help-block">Upload Business Images</p>
								</div>
								<div class="form-group col-sm-3 floating-label">
									<div class="radio radio-styled">
										<label>
										<input type="radio" value="1" name="featuredimage[]" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<input type="text" class="form-control" id="contact_person"  value="<?php if(isset($results) && !empty($results)){ echo $results->contact_person; } ?>" name="contact_person" minlength="2" maxlength="25">
									<label class="col-sm-12" for="contact_person">Contact Person Name</label>
								</div>
								<div class="form-group  col-sm-5">
									<div class="input-group date" id="demo-date" data-date-format="yyyy-mm-dd" >
										<div class="input-group-content">
											<input type="text" class="form-control" name="establishedyear" value="<?php if(isset($result) && !empty($result)){ echo $result->establishment_year; } ?>">
											<p class="help-block">Established Year</p>
										</div>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div>
								<div class="form-group col-sm-1 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" <?php if(isset($results) && !empty($results)){ if($results->is_verified){ echo 'checked'; } }  ?> name="is_verfied" id="is_verfied"> <span>Is Verified</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-5">
									<input type="file" class="form-control" id="businesslogo" name="businesslogo" accept=".jpeg, .jpg , .png" >
									<p class="help-block">Upload Business Logo</p>
								</div>
								<div class="form-group col-sm-1">
									<?php if(isset($result) && !empty($result)){ ?>
										<img style="width: 100px; height: 100px;" class="col-sm-2 form-control" src="<?php  echo base_url().$result->logo_image; ?>">
									<?php } ?>
								</div>
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="videourl" name="videourl" value="<?php if(isset($result) && !empty($result)){ echo $result->video_url; } ?>">
									<label class="col-sm-12" for="videourl">Video Url</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-6 floating-label">
									<input type="url" class="form-control" id="businessleadpage" name="businessleadpage" value="<?php if(isset($result) && !empty($result)){ echo $result->lead_page_url; } ?>">
									<p class="help-block">For Example: http://www.example.com</p>
									<label class="col-sm-12" for="businessleadpage">Business Lead Page</label>
								</div>
								<div class="form-group col-sm-3">
									<input type="text" class="form-control" id="latitude" name="latitude" value="<?php if(isset($result) && !empty($result)){ echo $result->latitude; } ?>" onchange="changecoords();">
									<label class="col-sm-12" for="latitude">Latitude</label>
								</div>
								<div class="form-group col-sm-3">
									<input type="text" class="form-control" id="longitude" value="<?php if(isset($result) && !empty($result)){ echo $result->longitude; } ?>" name="longitude" onchange="changecoords();">
									<label class="col-sm-12" for="longitude">Longitude</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group  col-sm-6 floating-label">
									<textarea name="address" id="address" class="form-control" rows="6" placeholder=""><?php if(isset($result) && !empty($result)){ echo $result->address; } ?></textarea>
									<div class="form-control-line"></div>
									<label for="address" class="col-sm-12 control-label">Full Permanent Address</label>
								</div>
								<div class="form-group col-sm-6  floating-label"><div id='googleMap' style="width:100%;height:183px;" ></div>
								</div>
							</div>
							<div class="row">
								<div class="form-group  col-sm-6 floating-label">
									<input name="landmark" id="landmark" class="form-control" rows="3" placeholder="" value="<?php if(isset($result) && !empty($result)){ echo $result->landmark; } ?>" />
									<label class="col-sm-12"  for="landmark">Landmark</label>
								</div>
								<div class="form-group  col-sm-6 floating-label">
									<input type="number" class="form-control" id="pincode" name="pincode" step="1" size='6'
									minlength='5' maxlength='6'value="<?php if(isset($result) && !empty($result)){ echo $result->pincode; } ?>" >
									<label class="col-sm-12" for="pincode">Pincode</label>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit"  class="btn btn-default-dark">Add Profile</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>	
</section>																																					