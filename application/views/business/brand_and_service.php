<section>
	
	<div class="section-body">
		<div class="card card-underline">
			<div class="card-head style-primary">
				<ul class="nav nav-tabs">
					<li> <a data-toggle="tab" href="#profile_info">Profile Info</a> </li>
					<li> <a data-toggle="tab" href="#business_info">Business Info</a> </li>
					<li  class="active" > <a data-toggle="tab" href="#brands_services">Brands and Services</a> </li>
					<li><button type="submit"  class="btn btn-primary">Payment Info</button> </li>
				</ul>
			</div>
			<div class="card-body">
				<div class="tab-content">
					<div id="profile_info" class="tab-pane fade in active">
						<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>business/create_profile">
							<div class="row">
								
								<div class="form-group col-sm-6 floating-label">
									<input type="text" class="form-control" id="business_name" name="business_name" required=""  aria-required="true" >
									<label class="col-sm-12" for="business_name">Business Name</label>
									
								</div>
								
								<div class="form-group  col-sm-6 floating-label">
									<input type="text" class="form-control" id="website_url" name="website_url" required >
									<label class="col-sm-12" for="website_url">Website</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<select id="bussiness_state_id" name="bussiness_state_id" class="form-control" data-live-search="true" required >
										<?php foreach($state_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										
									</select>
									
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<select id="business_city_id" name="business_city_id" class="form-control" data-live-search="true" required >
										<?php foreach($city_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0' selected >None</option>
									</select>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<select id="business_area_id" name="business_area_id" class="form-control" data-live-search="true" required >
										<?php foreach($area_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0' selected >None</option>
									</select>
									
								</div>
								
								<div class="form-group col-sm-3 floating-label">
									<input type="text" class="form-control" id="business_phone" name="business_phone" required>
									<label class="col-sm-12" for="business_phone">Phone</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_phone_display" id="is_phone_display"> <span>Display on Site</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_phone_notification" id="is_phone_notification"> <span>Receive Notification</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-3 floating-label">
									<input type="text" class="form-control" id="primarylandline" name="primarylandline">
									<label class="col-sm-12" for="primarylandline">Landline</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_landline_display" id="is_landline_display"> <span>Display on Site</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="twitter_url" name="twitter_url">
									<label  class="col-sm-12" for="twitter_url">Twitter</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="facebook_url" name="facebook_url">
									<label class="col-sm-12"  for="facebook_url">Facebook</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="pinterest_url" name="pinterest_url">
									<label class="col-sm-12" for="pinterest_url">Pinterest</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="contact_person" name="contact_person">
									<label class="col-sm-12" for="contact_person">Contact person name</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="email" class="form-control" id="business_email" name="business_email">
									<label class="col-sm-12" for="business_email">Email</label>
								</div>
								
								<div class="form-group col-sm-2 ">
									<input type="date" class="form-control" id="establishedyear" name="establishedyear">
									<label for="establishedyear">Established Year</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_year_verfied" id="is_year_verfied"> <span>Is Verified</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-4 ">
									<input type="file" class="form-control" id="businesslogo" name="businesslogo">
									<label for="businesslogo">Upload Business Logo</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="fax_number" name="fax_number">
									<label class="col-sm-12" for="fax_number">Fax</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="tollfree" name="tollfree">
									<label  class="col-sm-12" for="tollfree">Tollfree Number</label>
								</div>
								
								<div class="form-group col-sm-10 ">
									<input type="file" class="form-control" id="image" name="image">
									<label for="image">Image Upload</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="featuredimage" id="featuredimage"> <span>Make this as featured image</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="businessleadpage" name="businessleadpage">
									<label class="col-sm-12" for="businessleadpage">Business Lead Page</label>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" id="videourl" name="videourl">
									<label class="col-sm-12" for="videourl">Video Url</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<input type="text" class="form-control" id="latitude" name="latitude">
									<label class="col-sm-12" for="latitude">latitude</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<input type="text" class="form-control" id="longitude" name="longitude">
									<label class="col-sm-12" for="longitude">longitude</label>
								</div>
								
								<div class="form-group col-sm-6 floating-label">
									<textarea name="address" id="address" class="form-control" rows="3" placeholder=""></textarea>
									<div class="form-control-line"></div>
									<label for="address" class="col-sm-12 control-label">Full Permanent Address</label>
									
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<input type="text" class="form-control" rows="3" id="landmark" name="landmark">
									<label class="col-sm-12"  for="landmark">Landmark</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<input type="text" rows="3" class="form-control" id="pincode" name="pincode">
									<label class="col-sm-12" for="pincode">pincode</label>
								</div>
								
								<!--<div class="form-group col-sm-8 height-5 floating-label">
									<div class="map" id="business_map">
									</div>
								</div>-->
								
								<!--<div class="form-group col-sm-12 floating-label ">
									<div class="checkbox checkbox-styled pull-right">
									<label>
									<input type="checkbox" value="1" name="terms_conditions" id="terms_conditions"> 
									<span> I accept terms & conditions </span> 
									</label>
									</div>
								</div>-->
							</div>
							
							
							<div class="modal-footer">
								<button type="submit"  class="btn btn-primary">Add Profile</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</form>
						<!--end .col -->
					</div>
					
					<div id="business_info" class="tab-pane fade">
						<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>business/create_info">
							<div class="row">
								<div class="col-sm-6">
									<h4>Morning Business Hours</h4>
									<div class="col-sm-12"> 
										
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="monmor" name="monmor">
											<label for="monmor">Mon</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="monmorend" name="monmorend">
											<label for="monmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="monmorstatus" id="monmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="tuemor" name="tuemor">
											<label for="tuemor">Tue</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="tuemorend" name="tuemorend">
											<label for="tuemorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="tuemorstatus" id="tuemorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="wedmor" name="wedmor">
											<label for="wedmor">Wed</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="wedmorend" name="wedmorend">
											<label for="wedmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="wedmorstatus" id="wedmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="thrmor" name="thrmor">
											<label for="thrmor">Thr</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="thrmorend" name="thrmorend">
											<label for="thrmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="thrmorstatus" id="thrmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="frimor" name="frimor">
											<label for="frimor">Fri</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="frimorend" name="frimorend">
											<label for="frimorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="frimorstatus" id="frimorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="satmor" name="satmor">
											<label for="frimor">Sat</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="satmorend" name="satmorend">
											<label for="satmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="satmorstatus" id="satmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="sunmor" name="sunmor">
											<label for="frimor">Sun</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="sunmorend" name="sunmorend">
											<label for="sunmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="sunmorstatus" id="sunmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<h4>Evening Business Hours</h4>
									<div class="col-sm-12"> 
										
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="monmor" name="monmor">
											<label for="monmor">Mon</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="monmorend" name="monmorend">
											<label for="monmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="monmorstatus" id="monmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="tuemor" name="tuemor">
											<label for="tuemor">Tue</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="tuemorend" name="tuemorend">
											<label for="tuemorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="tuemorstatus" id="tuemorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="wedmor" name="wedmor">
											<label for="wedmor">Wed</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="wedmorend" name="wedmorend">
											<label for="wedmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="wedmorstatus" id="wedmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="thrmor" name="thrmor">
											<label for="thrmor">Thr</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="thrmorend" name="thrmorend">
											<label for="thrmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="thrmorstatus" id="thrmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="frimor" name="frimor">
											<label for="frimor">Fri</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="frimorend" name="frimorend">
											<label for="frimorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="frimorstatus" id="frimorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="satmor" name="satmor">
											<label for="frimor">Sat</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="satmorend" name="satmorend">
											<label for="satmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="satmorstatus" id="satmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="sunmor" name="sunmor">
											<label for="frimor">Sun</label>
										</div>
										<div class="form-group col-sm-5">
											<input type="text" class="form-control" id="sunmorend" name="sunmorend">
											<label for="sunmorend">&nbsp;</label>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" name="sunmorstatus" id="sunmorstatus"> <span>Closed</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" id="yprating" name="yprating" placeholder="AA+ OR 8 (out of 10)" >
										<label for="yprating">YP rating (alphanumeric)</label>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" id="relevancyscore" name="relevancyscore" placeholder="e.g. 20" >
										<label for="yprating">Relevancy score (out of 100)</label>
									</div>
								</div>
							</div>
							<div class="row">
								<h4>Is this featured business?</h4>
								<div class="form-group">
									<div class="col-sm-4">
										<div class="radio radio-styled">
											<label>
												<input type="radio" name="featuredbusiness" value="yes">
												<span>Yes. This is featured business</span>
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="radio radio-styled">
											<label>
												<input type="radio" name="featuredbusiness" value="no"  checked>
												<span>No. This is not featured business</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" id="note" name="note" placeholder="Note e.g. beside bhavani palace" >
										<label for="note">Note</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control" id="category" name="category" placeholder="Search category (enter comma separated values)" >
										<label for="category">Category</label>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="col-sm-6">
										<a href="#">Suggest a category</a>
									</div>
									<div class="col-sm-6">
										<a href="">Expand/collapse sub category</a>
									</div>
								</div>
							</div>
							
							<!--<div class="row">
								<h4>Select Sub category</h4>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option1</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option2</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option3</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option4</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option5</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option6</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option7</span>
								</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
								<label>
								<input type="checkbox" value="1" name="subcategory[]" ><span>Option8</span>
								</label>
								</div>
							</div>-->
							
							
							
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit"  class="btn btn-primary">Add Profile</button>
							</div>
							
						</form>
						
					</div>
					
					<div id="brands_services" class="tab-pane fade">
						<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>business/create_brands">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										
										<input data-role="tagsinput" type="text"  style="display: none;" id="subcategories" name="subcategories">
										<label for="subcategories">Services (comma separated values, search and select)</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										
										<input data-role="tagsinput" type="text"  style="display: none;" id="subcategories" name="subcategories">
										<label for="subcategories">Brands (comma separated values, search and select)</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										
										<input data-role="tagsinput" type="text"  style="display: none;" id="subcategories" name="subcategories">
										<label for="subcategories">Amenities (comma separated values, search and select)</label>
									</div>
								</div>
								<h3>Add offers</h3>
								<div class="modal-footer">
									<button type="button" class="btn btn-default btn-sn" data-dismiss="modal">Add Offers</button>
								</div>
								<div class="col-sm-12" style="
								border: 1px solid;
								background-color: beige;
								">
									<div class="row">
										<div class="form-group">
											<div class="col-sm-4">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="featuredbusiness" value="yes">
														<span>Deal</span>
													</label>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="featuredbusiness" value="no"  checked>
														<span>Coupon</span>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-sm-8 floating-label">
											
											<input  type="text"  class="form-control" id="subcategories" name="subcategories">
											<label class="col-sm-12 "  for="subcategories">
												
											Offer title</label>
											
										</div>
										
										<div class="form-group col-sm-4 ">
											
											<input type="file" class="form-control"  id="subcategories" name="subcategories">
											<label for="subcategories">
												
											Offer image</label>
											
										</div>
									</div>
									
									<div class="row">
										<div class="form-group col-sm-8 floating-label">
											
											<input  type="text"  class="form-control" id="subcategories" name="subcategories">
											<label class="col-sm-12 " for="subcategories">
												
											Deal URL OR Coupon code</label>
											
										</div>
										
										<div class="form-group col-sm-4 ">
											
											<input type="date" class="form-control" value="<?php echo '01-01-2017'; ?>" id="subcategories" name="subcategories">
											<label for="subcategories">
												
											Expiring on</label>
											
										</div>
									</div>
									
									<div class="form-group col-sm-12 floating-label">
										<textarea name="address" id="address" class="form-control" rows="3" placeholder=""></textarea>
										<div class="form-control-line"></div>
										<label for="address" class="col-sm-12 control-label">Offer description (max 80 characters)</label>
										
									</div>
								</div>
								<div class="form-group col-sm-12 floating-label">
									<textarea name="address" id="address" class="form-control" rows="3" placeholder=""></textarea>
									<div class="form-control-line"></div>
									<label for="address" class="col-sm-12 control-label">What to expect from your business?</label>
									
								</div>
							</div>
								<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit"  class="btn btn-primary">Add Offers</button>
							</div>
						</form>
					</div>
					<div id="payment_info" class="tab-pane fade">
						<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>business/create_payment">
							<div class="row">
								<h4>Payment Mode</h4>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="payment[]" ><span>Online</span>
									</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="payment[]" ><span>Cash</span>
									</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="payment[]" ><span>Creditcard</span>
									</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="payment[]" ><span>Debitcard</span>
									</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="payment[]" ><span>Cheque/DD</span>
									</label>
								</div>
								<div class="checkbox checkbox-styled col-sm-3">
									<label>
										<input type="checkbox" value="1" name="subcategory[]" ><span>Wire transfer</span>
									</label>
								</div>
							</div>
							
								<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
								<button type="submit"  class="btn btn-primary">Add Payment Mode</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>																																				