<section>
	<div class="section-body">
		<div class="card card-underline">
			<form class="form form-validate" id="myform" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url(); ?>business/brands_services/<?php echo $id; ?>">
				<div class="card-head">
					<header> Add Business </header>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs">
						<li  style="width:25%;"> <button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/profile_info/<?php echo $id; ?>';" class="btn btn-default-dark">Profile Info</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/business_info/<?php echo $id; ?>';"  class="btn btn-default-dark">Business Info</button></li>
						<li class="active" style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;"  class="btn btn-default">Brands and Services</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn btn-default-dark">Payment Info</button> </li>
					</ul>
					<div class="tab-content">
						<div id="brands_services" class="tab-pane fade in active">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input data-role="tagsinput" type="text"  style="display: none;" value="<?php if(isset($service) && !empty($service)){ echo implode(',',$service); } ?>" name="business_service">
										<label for="business_service">Services (comma separated values, search and select)</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input data-role="tagsinput" type="text"  style="display: none;" value="<?php if(isset($brand) && !empty($brand)){ echo implode(',',$brand); } ?>" name="business_brand">
										<label for="business_brand">Brands (comma separated values, search and select)</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input data-role="tagsinput" type="text"  style="display: none;" value="<?php if(isset($amentie) && !empty($amentie)){ echo implode(',',$amentie); } ?>" name="business_amenities">
										<label for="business_amenities">Amenities (comma separated values, search and select)</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<button type="button" id="addoffer" class="btn btn-default btn-sn pull-right" data-dismiss="modal">
									<i class="fa fa-plus"></i> Offers</button>
								</div>
								<?php foreach($offer as $key => $o){ ?>
									<div class="col-sm-12"  >
										<div class="col-sm-12" style="border: 1px solid;background-color: beige;margin-bottom:40px;" id="offerblock">
											<div class="form-group col-sm-4 floating-label">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="deal_type[]" value="Deal" <?php if($o->deal_type == 'Deal'){echo 'checked';} ?> >
														<span>Deal</span>
													</label>
												</div>
											</div>
											<div class="form-group col-sm-4 floating-label">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="deal_type[]" value="Coupon"  <?php if($o->deal_type == 'Coupon'){ echo 'checked'; } ?> >
														<span>Coupon</span>
													</label>
												</div>
											</div>
											<div class="form-group col-sm-8 floating-label">
												<input  type="text"  class="form-control" value="<?php echo $o->title; ?>" name="offer_title[]">
												<label class="col-sm-12 "  for="offer_title">Offer Title</label>
											</div>
											<div class="form-group col-sm-4 ">
												<img src="<?php echo base_url().$o->image_path; ?>" style="height:100px;width:100px;" />
												<input type="file" class="form-control col-sm-10"   name="offer_image[]" accept=".jpeg,.jpg,.png">
												<p for="offer_image" class="help-block">Offer Image</p>
											</div>
											<div class="form-group col-sm-8 floating-label">
												<input  type="text"  class="form-control" value="<?php echo $o->offer_url; ?>" name="offer_url[]">
												<label class="col-sm-12 " for="offer_url">Coupon CODE/Deal URL</label>
											</div>
											<div class="form-group col-sm-4 ">
												<div class="input-group date" id="offer-date" data-date-format="yyyy-mm-dd" >
													<div class="input-group-content">
														<input type="text" class="form-control" value="<?php echo $o->expire_on; ?>" name="offer_expire[]">
														<p class="help-block">Expiring On</p>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="form-group col-sm-12 floating-label">
												<textarea type="text" name="offer_description[]"  maxlength='80' class="form-control" rows="3" placeholder="" ><?php echo $o->description; ?></textarea>
												<div class="form-control-line"></div>
												<label for="offer_description" class="col-sm-12 control-label">Offer Description (max 80 characters)</label>
											</div>
										</div>
									</div>
								<?php } ?>
								<?php if(empty($offer)){ ?>
									<div class="col-sm-12"  >
										<div class="col-sm-12" style="border: 1px solid;background-color: beige;margin-bottom:40px;" id="offerblock">
											<div class="form-group col-sm-4 floating-label">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="deal_type[]" value="Deal"  >
														<span>Deal</span>
													</label>
												</div>
											</div>
											<div class="form-group col-sm-4 floating-label">
												<div class="radio radio-styled">
													<label>
														<input type="radio" name="deal_type[]" value="Coupon" checked  >
														<span>Coupon</span>
													</label>
												</div>
											</div>
											<div class="form-group col-sm-8 floating-label">
												<input  type="text"  class="form-control"  name="offer_title[]">
												<label class="col-sm-12 "  for="offer_title">Offer Title</label>
											</div>
											<div class="form-group col-sm-4 ">
												
												<input type="file" class="form-control col-sm-10"   name="offer_image[]" accept=".jpeg,.jpg,.png">
												<p for="offer_image" class="help-block">Offer Image</p>
											</div>
											<div class="form-group col-sm-8 floating-label">
												<input  type="text"  class="form-control" name="offer_url[]" >
												<label class="col-sm-12 " for="offer_url">Coupon CODE/Deal URL</label>
											</div>
											<div class="form-group col-sm-4 ">
												<div class="input-group date" id="offer-date" data-date-format="yyyy-mm-dd" >
													<div class="input-group-content">
														<input type="text" class="form-control"  name="offer_expire[]">
														<p class="help-block">Expiring On</p>
													</div>
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												</div>
											</div>
											<div class="form-group col-sm-12 floating-label">
												<textarea type="text" name="offer_description[]"  maxlength='80' class="form-control" rows="3" placeholder="" ></textarea>
												<div class="form-control-line"></div>
												<label for="offer_description" class="col-sm-12 control-label">Offer Description (max 80 characters)</label>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<div class="row">
								<div class="form-group col-sm-12 floating-label">
									<textarea name="business_expectation" class="form-control" rows="3" placeholder="" maxlength='150' ><?php if(isset($results) && !empty($results)){ echo $results->expectation; }?></textarea>
									<div class="form-control-line"></div>
									<label for="business_expectation" class="col-sm-12 control-label">What to expect from your business?</label>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit"  class="btn btn-default-dark">Add Offers</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>																																				