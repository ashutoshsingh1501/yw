<section>
	<div class="section-body">
		<div class="card card-underline">
			<form class="form form-validate" id="myform" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url(); ?>business/business_info/<?php echo $id; ?>">
				<div class="card-head style-primary">
					<ul class="nav nav-tabs">
						<li> <a  href="javascript:void(0)">Profile Info</a> </li>
						<li  class="active"> <a data-toggle="tab" href="#business_info">Business Info</a> </li>
						<li> <button type="submit"  class="btn btn-primary">Brands and Services</a> </li>
						<li> <button type="submit"  class="btn btn-primary">Payment Info</a> </li>
					</ul>
				</div>
				<div class="card-head">
					<header> Add Business </header>
				</div>
				<div class="card-body">
					
					<ul class="nav nav-tabs">
						<li  style="width:25%;"> <button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/profile_info/<?php echo $id; ?>';"  class="btn btn-primary">Profile Info</button> </li>
						<li class="active" style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;"  class="btn btn-default">Business Info</button></li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn btn-primary">Brands and Services</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn btn-primary">Payment Info</button> </li>
					</ul>
					<?php //print_r($b_info); exit; ?>
					<div class="tab-content">
						<div id="business_info" class="tab-pane fade in active">
							<div class="row">
								<div class="col-sm-6">
									<h4>Morning Business Hours</h4>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input class="form-control morning_start_time" data-provide="timepicker" data-template="modal"  value="<?php if(isset($time[0]->mor_start_time) && !empty($time[0]->mor_start_time)){ echo $time[0]->mor_start_time; } ?>" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>	
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" value="<?php if(isset($time[0]->mor_end_time) && !empty($time[0]->mor_end_time)){ echo $time[0]->mor_end_time; } ?>" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" <?php if(isset($time[0]->mor_status) && !empty($time[0]->mor_status)){ if($time[0]->mor_status){ echo 'checked' ; } } ?> id="morning_status" value="1" name="morning_status[0]" class="morning_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time"  value="<?php if(isset($time[1]->mor_start_time) && !empty($time[1]->mor_start_time)){ echo $time[1]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[1]->mor_end_time) && !empty($time[1]->mor_end_time)){ echo $time[1]->mor_end_time; } ?>" name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" value="1" <?php if(isset($time[1]->mor_status) && !empty($time[1]->mor_status)){ if($time[1]->mor_status){ echo 'checked'; } } ?> name="morning_status[1]" class="morning_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time" value="<?php if(isset($time[2]->mor_start_time) && !empty($time[2]->mor_start_time)){ echo $time[2]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[2]->mor_end_time) && !empty($time[2]->mor_end_time)){ echo $time[2]->mor_end_time; } ?>" name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[2]->mor_status) && !empty($time[2]->mor_status)){ if($time[2]->mor_status){ echo 'checked' ; } } ?> name="morning_status[2]" class="morning_status"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time" value="<?php if(isset($time[3]->mor_start_time) && !empty($time[3]->mor_start_time)){ echo $time[3]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[3]->mor_end_time) && !empty($time[3]->mor_end_time)){ echo $time[3]->mor_end_time; } ?>" name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[3]->mor_status) && !empty($time[3]->mor_status)){ if($time[3]->mor_status){ echo 'checked' ; } } ?> name="morning_status[3]" class="morning_status"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time" value="<?php if(isset($time[4]->mor_start_time) && !empty($time[4]->mor_start_time)){ echo $time[4]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[4]->mor_end_time) && !empty($time[4]->mor_end_time)){ echo $time[4]->mor_end_time; } ?>"  name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[4]->mor_status) && !empty($time[4]->mor_status)){ if($time[4]->mor_status){ echo 'checked' ; } } ?> name="morning_status[4]" class="morning_status"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time" value="<?php if(isset($time[5]->mor_start_time) && !empty($time[5]->mor_start_time)){ echo $time[5]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[5]->mor_end_time) && !empty($time[5]->mor_end_time)){ echo $time[5]->mor_end_time; } ?>"  name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[5]->mor_status) && !empty($time[5]->mor_status)){ if($time[5]->mor_status){ echo 'checked' ; } } ?> name="morning_status[5]" class="morning_status"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_start_time" value="<?php if(isset($time[6]->mor_start_time) && !empty($time[6]->mor_start_time)){ echo $time[6]->mor_start_time; } ?>" name="morning_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control morning_end_time" value="<?php if(isset($time[6]->mor_end_time) && !empty($time[6]->mor_end_time)){ echo $time[6]->mor_end_time; } ?>" name="morning_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[6]->mor_status) && !empty($time[6]->mor_status)){ if($time[6]->mor_status){ echo 'checked' ; } } ?> name="morning_status[6]" class="morning_status" > <span>Closed</span>
											</label>
										</div>
									</div>
								</div>
								<div class="col-sm-6" id="evetime" style="display:<?php if(isset($results) && !empty($results) && $results->evening_on){ echo 'block'; }else{ echo 'none'; } ?>">
									<h4>Evening Business Hours</h4>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[0]->eve_start_time) && !empty($time[0]->eve_start_time)){ echo $time[0]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input class="form-control evening_end_time" value="<?php if(isset($time[0]->eve_end_time) && !empty($time[0]->eve_end_time)){ echo $time[0]->eve_end_time; } ?>"  name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" id="evening_status" <?php if(isset($time[0]->eve_status) && !empty($time[0]->eve_status)){ if($time[0]->eve_status){ echo 'checked' ; } } ?> value="1" name="evening_status[0]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[1]->eve_start_time) && !empty($time[1]->eve_start_time)){ echo $time[1]->eve_start_time; } ?>"  name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input class="form-control evening_end_time" value="<?php if(isset($time[1]->eve_end_time) && !empty($time[1]->eve_end_time)){ echo $time[1]->eve_end_time; } ?>" name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[1]->eve_status) && !empty($time[1]->eve_status)){ if($time[1]->eve_status){ echo 'checked' ; } } ?> name="evening_status[1]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[2]->eve_start_time) && !empty($time[2]->eve_start_time)){ echo $time[2]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input class="form-control evening_end_time" value="<?php if(isset($time[2]->eve_end_time) && !empty($time[2]->eve_end_time)){ echo $time[2]->eve_end_time; } ?>" name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[2]->eve_status) && !empty($time[2]->eve_status)){ if($time[2]->eve_status){ echo 'checked' ; } } ?> name="evening_status[2]" class="evening_status"> <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[3]->eve_start_time) && !empty($time[3]->eve_start_time)){ echo $time[3]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input class="form-control evening_end_time" value="<?php if(isset($time[3]->eve_end_time) && !empty($time[3]->eve_end_time)){ echo $time[3]->eve_end_time; } ?>" name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[3]->eve_status) && !empty($time[3]->eve_status)){ if($time[3]->mor_status){ echo 'checked' ; } } ?> name="evening_status[3]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[4]->eve_start_time) && !empty($time[4]->eve_start_time)){ echo $time[4]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_end_time" value="<?php if(isset($time[4]->eve_end_time) && !empty($time[4]->eve_end_time)){ echo $time[4]->eve_end_time; } ?>" name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[4]->eve_status) && !empty($time[4]->eve_status)){ if($time[4]->eve_status){ echo 'checked' ; } } ?> name="evening_status[4]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[5]->eve_start_time) && !empty($time[5]->eve_start_time)){ echo $time[5]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_end_time" value="<?php if(isset($time[5]->eve_end_time) && !empty($time[5]->eve_end_time)){ echo $time[5]->eve_end_time; } ?>"  name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[5]->eve_status) && !empty($time[5]->eve_status)){ if($time[5]->eve_status){ echo 'checked' ; } } ?> name="evening_status[5]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
									<div class="col-sm-12"> 
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_start_time" value="<?php if(isset($time[6]->eve_start_time) && !empty($time[6]->eve_start_time)){ echo $time[6]->eve_start_time; } ?>" name="evening_start_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										<div class="form-group control-width-normal col-sm-5">
											<div class="input-group bootstrap-timepicker timepicker">
												<input  class="form-control evening_end_time" value="<?php if(isset($time[6]->eve_end_time) && !empty($time[6]->eve_end_time)){ echo $time[6]->eve_end_time; } ?>" name="evening_end_time[]" data-provide="timepicker" data-template="modal" data-minute-step="1" data-modal-backdrop="true" type="text"/>
											</div>
										</div>
										
										<div class="checkbox checkbox-styled col-sm-2">
											<label>
												<input type="checkbox" value="1" <?php if(isset($time[6]->eve_status) && !empty($time[6]->eve_status)){ if($time[6]->eve_status){ echo 'checked' ; } } ?> name="evening_status[6]" class="evening_status" > <span>Closed</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
							<div class="col-sm-12">
							<a href="javascript:void(0);" class="btn btn-default pull-right" id="copyall">Copy For All Days</a>
								<div  class="copyall checkbox checkbox-styled col-sm-2">
											<label>
												<input id="addeve" type="checkbox" <?php if(isset($results) && !empty($results)){ if($results->evening_on){ echo 'checked'; }} ?> value="1" name="evening_on" > <span>Add Evening Timings</span>
											</label>
										</div>
							<!--<a href="javascript:void(0);" class="btn btn-default" >Add Evening Timings</a>-->
							</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<select id="yp-rating" name="yp-rating" class="form-control" data-live-search="true" >
										<?php foreach($rating as $key => $r){ ?>
											<option <?php if(isset($results) && !empty($results)){ if($results->rating_id == $key){ echo 'selected'; }} ?> value="<?php echo $key; ?>"><?php echo $r; ?></option>
										<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group floating-label">
										<input type="number"  class="form-control" value="<?php if(isset($results) && !empty($results)){ echo $results->relevancy_score; } ?>" name="relevancyscore"  maxlength="100" required>
										<label for="yprating">Relevancy score (out of 100)</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-sm-2"><h4>Is This Featured Business :</h4></div>
									<div class="col-sm-4">
										<div class="radio radio-styled">
											<label>
												<input type="radio" name="featuredbusiness" value="1" <?php if(isset($results) && !empty($results)){ if($results->is_featured){ echo 'checked'; }} ?> >
												<span>Yes. This is featured business</span>
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="radio radio-styled">
											<label>
												<input type="radio" name="featuredbusiness" <?php if(isset($results) && !empty($results)){ if(!$results->is_featured){ echo 'checked'; }} ?> value="0" >
												<span>No. This is not featured business</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group floating-label">
										<input class="form-control" id="note" row='3' name="note" placeholder="Note e.g. beside bhavani palace" >
										<label for="note">Note</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<select  id="bcategory" name="category" class="form-control selectpicker"  multiple onchange="change_subcategory();" data-live-search="true" data-max-options="3" required >
											<?php foreach($categories as $key => $sl ){ ?>
												<option <?php if(in_array($key , $category)){echo 'selected'; } ?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
											<?php } ?>
											
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12"><h4>Select SubCategory</h4></div>
								<div class="form-group" id="sub_categories" name="sub_categories"></div>
								
							</div>
							
							<div class="row">
								<div class="col-sm-12"><h4>Attributes</h4></div>
								<div class="row" id="attributes"></div>
			<?php			$attributes='';		foreach($vattribute as $key => $r){
					
					if($r->field_type == 'Radio'){
						$attributes = $attributes . '<div class="col-sm-12" id="'. $r->category_id .'"><div class="form-group"><div class="col-sm-2"><h4>'. $r->label .' </h4></div>';
						foreach(explode(',' , $r->value) as $keys => $v){
							$attributes = $attributes . '<div class="col-sm-4"><div class="radio radio-styled"><label><input type="radio" name="'. $key .'" value="'. $v .'" checked><span>'. $v .'</span></label></div></div>';
						}
						$attributes = $attributes . '</div></div>';
					}
					
					if($r->field_type == 'Checkbox'){
						$attributes = $attributes . '<div class="col-sm-12" id="'. $r->category_id .'"><div class="form-group"><div class="col-sm-2"><h4>'. $r->label .' </h4></div>';
						foreach(explode(',' , $r->value) as $keys => $v){
							$attributes = $attributes . '<div class="col-sm-2"><div class="checkbox checkbox-styled"><label><input type="checkbox" value="'. $v .'" name="'. $key .'[]"><span>&nbsp;</span>'. $v  .'</label></div></div>';
						}
						$attributes = $attributes . '</div></div>';
					}
					
					if($r->field_type == 'Dropdown'){
						$attributes = $attributes . '<div class="col-sm-12" id="'. $r->category_id .'"><div class="form-group"><div class="col-sm-2"><h4>'. $r->label .' </h4></div><div class="col-sm-6"><div class="form-group"><select id="yp-rating" name="'. $r->category_id .'" class="form-control" data-live-search="true" >';
						foreach(explode(',' , $r->value) as $keys => $v){
							$attributes = $attributes . '	<option value="'. $key .'">'. $v .'</option>';
						}
						$attributes = $attributes . '</select></div></div></div></div>';
					}
					
					if($r->field_type == 'Star'){
						$attributes = $attributes . '<div class="col-sm-12" id="'. $r->category_id .'"><div class="form-group"><div class="col-sm-2"><h4>'. $r->label .' </h4></div>';
						//foreach(explode(',' , $r->value) as $keys => $v){
						$attributes = $attributes . '	<div class="star-rating">
						<div class="rating-input">
						<input type="number" name="your_awesome_parameter" id="rating1" class="rating" data-clearable="remove"/>
						</div>
						</div>
						';
						//}
						$attributes = $attributes . '</div></div>';
					}
				} 
				echo $attributes; ?>
			
							</div>
							
							
							<div class="modal-footer">
								<button type="submit"  class="btn btn-primary">Add Profile</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>	
	</div>	
</section>																																								