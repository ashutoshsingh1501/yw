<section>
	<div class="section-body">
		<div class="card">
			<form class="form" id="business_index" action="<?php echo base_url(); ?>business" method="get">
				<div class="card-body">
					<div class="row" style="margin-bottom:30px;vertical-align:middle">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn dropdown-toggle bulk"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
									<li><a href="#">Published</a></li>
									<li><a href="#">Pending</a></li>
									<li><a href="#">Rejected</a></li>
									<li><a href="#">In Process</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select   id="state_ID" name="state_id" class="form-control selectpicker" data-live-search="true" title="All States" >
								<option value='0'>All States</option>
								<?php foreach($states as $key => $sl ){ ?>
									<option <?php if (isset($_GET['state_id'])) {   if($_GET['state_id'] == $key){ echo 'selected'; } }?>  value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-2">
							<select   name="city_id" id="city_ID" onchange="find_business();" class="form-control selectpicker"  >
								<option value='0'  >All Cities</option>
								<?php foreach($cities as $key => $sl ){ ?>
									<option <?php if (isset($_GET['city_id'])) {   if($_GET['city_id'] == $key){ echo 'selected'; } }?>  value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-2">
							<select  name="category_id" onchange="find_business();" class="form-control selectpicker" data-live-search="true" data-live-search-style="begins" title="All Cities" >
								<option value='0' >All Categories</option>
								<?php foreach($categories as $key => $sl ){ ?>
									<option <?php if (isset($_GET['category_id'])) {   if($_GET['category_id'] == $key){ echo 'selected'; } }?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-sm-1">
							<select  name="type_id" onchange="filter_business();" class="form-control selectpicker" data-live-search="true" >
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 0){ echo 'selected'; } }?> value='0' selected >All </option>
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 1){ echo 'selected'; } }?> value="1">Added by owner - Moderation</option>
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 2){ echo 'selected'; } }?>  value="2">Added by user - Moderation</option>
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 3){ echo 'selected'; } }?> value="3">Claimed - Moderation</option>
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 4){ echo 'selected'; } }?> value="4">Flagged for edit</option>
								<option <?php if (isset($_GET['type_id'])) {   if($_GET['type_id'] == 5){ echo 'selected'; } }?> value="5">Reported closed</option>
							</select>
						</div>
						<div class="col-sm-1">
							<select name="class_id" onchange="find_business();" class="form-control selectpicker"  >
								<option <?php if (isset($_GET['class-search'])) {   if($_GET['class_id'] == 0){ echo 'selected'; } }?> value='0' selected >All</option>
								<option <?php if (isset($_GET['class_id'])) {   if($_GET['class_id'] == 1){ echo 'selected'; } }?> value="1">All Free</option>
								<option <?php if (isset($_GET['class_id'])) {   if($_GET['class_id'] == 2){ echo 'selected'; } }?> value="2">Paid - All</option>
								<option <?php if (isset($_GET['class_id'])) {   if($_GET['class_id'] == 3){ echo 'selected'; } }?> value="3">Featured - All</option>
								<option <?php if (isset($_GET['class_id'])) {   if($_GET['class_id'] == 4){ echo 'selected'; } }?> value="4">Featured - Free</option>
							<option <?php if (isset($_GET['class_id'])) {   if($_GET['class_id'] == 5){ echo 'selected'; } }?> value="5">Featured - Paid</option></select>
						</div>
						<div class="col-sm-1">
							<select name="rating_id" class="form-control selectpicker" onchange="find_business();" >
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 0){ echo 'selected'; } }?> value="0">All</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 1){ echo 'selected'; } }?> value="1">Verified Business</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 2){ echo 'selected'; } }?> value="2">Verified Address</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 3){ echo 'selected'; } }?> value="3">AA YP Rating</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 4){ echo 'selected'; } }?> value="4">BB YP Rating</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 5){ echo 'selected'; } }?> value="5">CC YP Rating</option>
								<option <?php if (isset($_GET['rating_id'])) {   if($_GET['rating_id'] == 6){ echo 'selected'; } }?> value="6">No YP Rating</option>
							</select>
						</div>
						<div class="col-sm-1"><h4 class="rowcount"> <?php echo count($results); ?> Business </h4></div>
						<div class="col-sm-1" >
							<a class="btn  bulk" href="<?php echo base_url(); ?>business/profile_info" >
								<i class="fa fa-plus text-default-light"></i> Add
							</a>
							
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-index table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th>ID</th>
								<th>Business Name <br><input type="text" onchange="find_business();" value="<?php if(isset($_GET['business_name'])){ echo $_GET['business_name']; }?>" class="form-control" name="business_name"  placeholder="Search Business"></th>
								<th>Contact Person <br><input type="text" onchange="find_business();" value="<?php if(isset($_GET['contact_person'])){ echo $_GET['contact_person']; }?>" class="form-control" name="contact_person"  placeholder="Search Contact"></th>
								<th>Phone <br><input type="text" onchange="find_business();" value="<?php if(isset($_GET['phone'])){ echo $_GET['phone']; }?>" class="form-control" name="phone"  placeholder="Search Phone"></th>
								<th>Email <br><input type="text" onchange="find_business();" value="<?php if(isset($_GET['email'])){ echo $_GET['email']; }?>" class="form-control" name="email" placeholder="Search Email"></th>
								<th>City</th>
								<th>Area <br><input type="text" onchange="find_business();" value="<?php if(isset($_GET['area_name'])){ echo $_GET['area_name']; }?>" class="form-control" name="area_name" placeholder="Search Area"></th>
								<th>Status</th>
								<th style="text-align:right;">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $l){ ?>
								<tr>
									<td>
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="1" name="addcheck[]" class="addcheck"><span>&nbsp;</span></label>
										</div>
									</td>
									<td><?php echo $l->id; ?></td>
									<td><?php echo $l->title; ?></td>
									<td><?php echo $l->contact_person; ?></td>
									<td><?php echo $l->phone; ?></td>
									<td><?php echo $l->email; ?></td>
									<td><?php if(isset($cities[$l->city_id])){ echo $cities[$l->city_id]; }else{ echo 'Others'; } ?></td>
									<td><?php if(isset($areas[$l->area_id])){ echo $areas[$l->area_id]; }else{ echo 'Others'; } ?></td>
									<td>					
										<select data-id="<?php echo $l->id; ?>"  class="b_status form-control selectpicker"  onchange="change_bstatus(this);" >
											<?php foreach($status as $key => $value ){ ?>
												<option value="<?php echo $key; ?>" <?php if($l->status == $key){ echo 'selected'; } ?> ><?php echo $value; ?></option>
											<?php } ?>
										</select>
									</td>
									<td style="text-align:right;">
										<a href="<?php echo base_url(); ?>business/profile_info/<?php echo $l->id; ?>" class="btn btn-icon-toggle"><i class="fa fa-pencil fa-fw"></i></a>
										<a href="#" class="btn btn-icon-toggle  business-delete" data-id="<?php echo $l->id; ?>"  ><i class="fa fa-trash-o fa-fw"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
		</div>
	</section>																												