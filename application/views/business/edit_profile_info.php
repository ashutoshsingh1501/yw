<section>
	<div class="section-body">
		<div class="card">
			<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>business/update_profile">
				<div class="card-head style-primary">
					<ul class="nav nav-tabs">
						<li class="active"> <a data-toggle="tab" href="#profile_info">Profile Info</a> </li>
						<li><button type="submit"  class="btn btn-primary">Business Info</button></li>
						<li><button type="submit"  class="btn btn-primary">Brands and Services</button> </li>
						<li><button type="submit"  class="btn btn-primary">Payment Info</button> </li>
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div id="profile_info" class="tab-pane fade in active">
							
							<div class="row">
								
								<div class="form-group col-sm-6 floating-label">
									<input type="text" class="form-control" id="business_name" name="business_name" required=""  aria-required="true" value="<?php echo isset($business->business_name) ? $business->business_name : ''; ?>">
									<label class="col-sm-12" for="business_name">Business Name</label>
								</div>
								
								<div class="form-group  col-sm-6 floating-label">
									<input type="url" class="form-control" id="website_url" name="website_url" required value="<?php echo isset($business->website_url)? $business->website_url:''; ?>">
									<label class="col-sm-12" for="website_url">Website</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-4 floating-label">
									<select id="bussiness_state_id" name="bussiness_state_id" class="form-control selectpicker" data-live-search="true" required >
										<?php foreach($state_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>" <?php if($key==$business->bussiness_state_id){ echo 'selected="selected"'; }?> ><?php echo $sl; ?></option>
										<?php } ?>
										
									</select>
									
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<select id="business_city_id" name="business_city_id" class="form-control selectpicker" data-live-search="true" required >
										<?php foreach($city_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>" <?php if($key==$business->business_city_id){ echo 'selected="selected"'; }?> ><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0' selected >None</option>
									</select>
								</div>
								
								<div class="form-group col-sm-4 floating-label">
									<select id="business_area_id" name="business_area_id" class="form-control selectpicker" data-live-search="true" required >
										<?php foreach($area_list as $key => $sl ){ ?>
											<option value="<?php echo $key; ?>" <?php if($key==$business->business_area_id){ echo 'selected="selected"'; }?> ><?php echo $sl; ?></option>
										<?php } ?>
										<option value='0' selected >None</option>
									</select>
									
								</div>
							</div>
							
							<div class="row">
								<div class="form-group col-sm-3 floating-label">
									<input type="text" class="form-control" id="business_phone" name="business_phone" required value="<?php echo $business->business_phone; ?>">
									<label class="col-sm-12" for="business_phone">Phone</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_phone_display" id="is_phone_display" <?php if(isset($business_info->is_phone_display) && 1==$business_info->is_phone_display){ echo 'checked'; } ?> > <span>Display on Site</span> </label>
									</div>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_phone_notification" id="is_phone_notification" <?php if(isset($business_info->is_phone_notification) && 1==$business_info->is_phone_notification){ echo 'checked'; } ?> > <span>Receive Notification</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								
								
								<div class="form-group col-sm-3 floating-label">
									<input type="text" class="form-control" id="primarylandline" name="primarylandline" value="<?php echo isset($business_info->landline) ?  $business_info->landline :''; ?>" >
									<label class="col-sm-12" for="primarylandline">Landline</label>
								</div>
								
								<div class="form-group col-sm-2 floating-label">
									<div class="checkbox checkbox-styled">
										<label>
										<input type="checkbox" value="1" name="is_landline_display" id="is_landline_display" <?php if(isset($business_info->is_landline_display) && 1==$business_info->is_landline_display){ echo 'checked'; } ?>> <span>Display on Site</span> </label>
									</div>
								</div>
							</div>
							<div class="row">
								
							<div class="form-group col-sm-4 floating-label">
								<input type="url" class="form-control" id="twitter_url" name="twitter_url" value="<?php echo isset($business_info->twitter_url)? $business_info->twitter_url : ''; ?>" >
								<label  class="col-sm-12" for="twitter_url">Twitter</label>
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="url" class="form-control" id="facebook_url" name="facebook_url" value="<?php echo isset($business_info->is_landline_display) ? $business_info->facebook_url :'' ; ?>">
								<label class="col-sm-12"  for="facebook_url">Facebook</label>
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="url" class="form-control" id="pinterest_url" name="pinterest_url" value="<?php echo isset($business_info->pinterest_url) ? $business_info->pinterest_url : ''; ?>">
								<label class="col-sm-12" for="pinterest_url">Pinterest</label>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-4 floating-label">
								<input type="text" class="form-control" id="contact_person" name="contact_person" value="<?php echo isset($business->business_contact_person) ? $business->business_contact_person: '' ; ?>">
								<label class="col-sm-12" for="contact_person">Contact Person Name</label>
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="email" class="form-control" id="business_email" name="business_email" value="<?php echo isset($business->business_email) ? $business->business_email: '' ; ?>">
								<label class="col-sm-12" for="business_email">Email</label>
							</div>
							</div>
							<div class="row">
							
							<div class="form-group col-sm-2 ">
								<input type="date" class="form-control" id="establishedyear" name="establishedyear" value="<?php echo isset($business_info->establishment_year) ? date('Y-m-d', strtotime($business_info->establishment_year)) : '' ;?>">
								<!--<label for="establishedyear">Established Year</label>-->
								<p class="help-block">Established Year</p>
							</div>
							
							<div class="form-group col-sm-2 floating-label">
								<div class="checkbox checkbox-styled">
									<label>
									<input type="checkbox" value="1" name="is_year_verfied" id="is_year_verfied" value="<?php echo isset($business_info->is_year_verfied) ? $business_info->is_year_verfied : ''; ?>"> <span>Is Verified</span> </label>
								</div>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-6 ">
								<!--<input type="file" class="form-control" id="businesslogo" name="businesslogo">-->
								<!--<label for="businesslogo">Upload Business Logo</label>-->
								
								<div class="fileinput fileinput-new input-group" data-provides="fileinput">
								<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div><p class="help-block">Upload Business Logo</p>
								<span class="input-group-addon btn  btn-file"><span class="fileinput-new btn btn-default">Browse</span><span class="fileinput-exists btn btn-default">Change</span><input type="file" name="..." accept=".jpg , .jpeg , .png" ></span>
								<a href="#" class="input-group-addon btn fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
								
								
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-4 floating-label">
								<input type="text" class="form-control" id="fax_number" name="fax_number" value="<?php echo isset($business_info->fax_number)? $business_info->fax_number : '' ; ?>" >
								<label class="col-sm-12" for="fax_number">Fax</label>
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="text" class="form-control" id="tollfree" name="tollfree" value="<?php echo isset($business_info->tollfree_number)? $business_info->tollfree_number : '' ; ?>" >
								<label  class="col-sm-12" for="tollfree">Tollfree Number</label>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-10 ">
								<!--<input type="file" class="form-control" id="image" name="image">
								<label for="image">Image Upload</label>-->
								<div class="fileinput fileinput-new input-group" data-provides="fileinput">
								<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div><p class="help-block">Upload Business Image</p>
								<span class="input-group-addon btn  btn-file"><span class="fileinput-new btn btn-default">Browse</span><span class="fileinput-exists btn btn-default">Change</span><input type="file" name="..." accept=".jpg , .jpeg , .png" ></span>
								<a href="#" class="input-group-addon btn fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
							</div>
							
							<div class="form-group col-sm-2 floating-label">
								<div class="checkbox checkbox-styled">
									<label>
									<input type="checkbox" value="1" name="featuredimage" id="featuredimage" value="<?php if(isset($business_info->featuredimage)){ echo "checked"; }?>"> <span>Make this as featured image</span> </label>
								</div>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-4 floating-label">
								<input type="url" class="form-control" id="businessleadpage" name="businessleadpage" value="<?php echo isset($business_info->lead_page_url)? $business_info->lead_page_url : '' ; ?>">
								<label class="col-sm-12" for="businessleadpage">Business Lead Page</label>
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="url" class="form-control" id="videourl" name="videourl" value="<?php echo isset($business_info->video_url)? $business_info->video_url : '' ; ?>" >
								<label class="col-sm-12" for="videourl">Video Url</label>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-2 floating-label">
								<input type="text" class="form-control" id="latitude" name="latitude" value="<?php echo isset($business_info->latitude)? $business_info->latitude : '' ; ?>" >
								<label class="col-sm-12" for="latitude">Latitude</label>
							</div>
							
							<div class="form-group col-sm-2 floating-label">
								<input type="text" class="form-control" id="longitude" name="longitude" value="<?php echo isset($business_info->longitude)? $business_info->longitude : ''; ?>">
								<label class="col-sm-12" for="longitude">Longitude</label>
							</div>
							</div>
							<div class="row">
							<div class="form-group col-sm-6 floating-label">
								<textarea name="address" id="address" class="form-control" rows="3" placeholder=""><?php echo isset($business_info->address)?$business_info->address:''; ?></textarea>
								<div class="form-control-line"></div>
								<label for="address" class="col-sm-12 control-label">Full Permanent Address</label>
								
							</div>
							
							<div class="form-group col-sm-4 floating-label">
								<input type="text" class="form-control" rows="3" id="landmark" name="landmark" value="<?php echo isset($business_info->landmark) ? $business_info->landmark : '' ; ?>">
								<label class="col-sm-12"  for="landmark">Landmark</label>
							</div>
							
							<div class="form-group col-sm-2 floating-label">
								<input type="text" rows="3" class="form-control" id="pincode" name="pincode" value="<?php echo isset($business_info->pincode)?$business_info->pincode:'' ; ?>">
								<label class="col-sm-12" for="pincode">Pincode</label>
							</div>
							</div>
							<!--<div class="form-group col-sm-8 height-5 floating-label">
								<div class="map" id="business_map">
								</div>
							</div>-->
							
							<!--<div class="form-group col-sm-12 floating-label ">
								<div class="checkbox checkbox-styled pull-right">
								<label>
								<input type="checkbox" value="1" name="terms_conditions" id="terms_conditions"> 
								<span> I accept terms & conditions </span> 
								</label>
								</div>
							</div>-->
							</div>
							
							
							<div class="modal-footer">
								<input type="hidden" name="business_id" value="<?php echo $business->id; ?>" />
								<button type="submit"  class="btn btn-primary">Update Profile</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>	
</section>																																				