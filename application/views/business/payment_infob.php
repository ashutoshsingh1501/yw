<section>
	
	<div class="section-body">
		
		<div class="card card-underline">
			
			<form class="form form-validate" id="myform" method="post" enctype="multipart/form-data" role="form" action="<?php echo base_url(); ?>business/payment_info/<?php echo $id; ?>">
				<div class="card-head">
					<header> Add Business </header>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs">
						<li  style="width:25%;"> <button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/profile_info/<?php echo $id; ?>';" class="btn btn-primary">Profile Info</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/business_info/<?php echo $id; ?>';"  class="btn btn-primary">Business Info</button></li>
						<li class="active" style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="button" onclick="location.href = '<?php echo base_url(); ?>business/brands_services/<?php echo $id; ?>';" class="btn btn-primary">Brands and Services</button> </li>
						<li style="width:25%;"><button style="width:100%;border-radius:30px 30px 0px 0px;" type="submit"  class="btn btn-default">Payment Info</button> </li>
					</ul>
					<div class="tab-content">
						
						<div id="payment_info" class="tab-pane fade in active">
							<div class="row">
								<div class="form-group col-sm-12">
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="1" name="payment[]" <?php if(in_array(1,$payment_id)){ echo 'checked'; }?> ><span>Online</span>
										</label>
									</div>
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="2" name="payment[]" <?php if(in_array(2,$payment_id)){ echo 'checked'; }?> ><span>Cash</span>
										</label>
									</div>
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="3" name="payment[]" <?php if(in_array(3,$payment_id)){ echo 'checked'; }?> ><span>Credit Card</span>
										</label>
									</div>
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="4" name="payment[]" <?php if(in_array(4,$payment_id)){ echo 'checked'; }?> ><span>Debit Card</span>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">		
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="5" name="payment[]" <?php if(in_array(5,$payment_id)){ echo 'checked'; }?> ><span>Cheque/DD</span>
										</label>
									</div>
									<div class="checkbox checkbox-styled col-sm-3">
										<label>
											<input type="checkbox" class='{payment: true}' value="6" name="payment[]" <?php if(in_array(6,$payment_id)){ echo 'checked'; }?> ><span>Wire transfer</span>
										</label>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit"  class="btn btn-primary">Add Payment Mode</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>																																					