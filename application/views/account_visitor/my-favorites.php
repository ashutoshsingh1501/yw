<?php
//	$css = 'css/yp-myAccount.css';
$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<div class="mobileHeader">
    <div class="mobileMenu">Favorites (20)</div>
</div>

<div class="container topMargin sortFilters">
    <div class="wrapper">

        <div class="mobileSortFilterBlock">
            <div class="sortBlock mobileShowBy">
                <div class="sortSelectBox select mobileSort">
                    <span class="mobileSortText">Show by</span>
                    <select name="" id="" class="selectBox">
                        <option value="1">Category</option>
                        <option value="2">City</option>
                        <option value="3">Date</option>
                    </select>
                </div>
            </div>

            <div class="pageHeading">
                <h1 class="pageTitle">My Account</h1>
            </div>

            <div class="sortBlock">
                <div class="sortSelectBox select mobileSort">
                    <span class="mobileSortText">Sort</span>
                    <span class="selectText">Sort results</span>
                    <select name="" id="" class="selectBox" onchange="ratings_favorite(this.value)">
                          <option value="0"></option>
                        <option value="1">Ratings High To Low</option>
                        <option value="2">Ratings Low To High</option>
                        <option value="3">Recent To First</option>
                        <option value="4">First To Recent</option>
                    </select>
                </div>
            </div>

            <div class="showByDesktop">
                <span class="showByText">Show by:</span>
                <div class="showByDesktopList">
                    <label for="cat" class="showByRadioLabel">
                        <input type="radio" class="showByRadioBtn" value="" name="radio2" id="cat">
                        <span class="showByRadioText">Category </span>
                    </label>
                    <label for="city" class="showByRadioLabel">
                        <input type="radio" class="showByRadioBtn" value="" name="radio2" id="city">
                        <span class="showByRadioText">City </span>
                    </label>
                    <label for="date" class="showByRadioLabel">
                        <input type="radio" class="showByRadioBtn" value="" name="radio2" id="date">
                        <span class="showByRadioText" onclick="favorite_date()">Date </span>
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container greyBg myAccount myFavorites">
    <div class="wrapper">
        <div class="leftSmallCol">
            <?php
            $fav = "active";
            include 'inc/my-account-menu.php';
            ?>
        </div>
        <div class="rightBigCol">
            <div class="businessListBlock">
                <div class="businessList">
                    <div id="logindialog" style="display:none;">
    
                    </div>
                    <div id="loading"></div>
                    <div id="container">
                        <div class="data"></div>
                        <div class="pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobileLoadMore">
    <button class="loadMoreBtn">Load More</button>	
</div>

<div class="container bredcrumBlock greyBg">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <a href="" rel="v:url" property="v:title">My Account&nbsp;&nbsp;/</a> 
                            <span rel="v:child" typeof="v:Breadcrumb"> 
                                <span class="breadcrumb_last"> My Favorites </span> 
                            </span>
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>



<?php
$js = "<script>
            $(document).ready(function() {
                $('.popup').click(function() {
                    $('.popupBlock').addClass('showPopup');
                    $('body').addClass('noScroll');
                });
                $('.closePopup').click(function() {
                    $('.popupBlock').removeClass('showPopup');
                    $('body').removeClass('noScroll');
                });
                $('.addFav').click(function() {
                    if($(this).hasClass('favActive')) {
                            $(this).removeClass('favActive');
                    } else {
                            $(this).addClass('favActive');
                    }
                });
            });
    </script>";
?>


