<?php 
	$css = 'css/yp-myAccount.css';
	$mobHeadTitle = '';
	include 'header-inner.php';
?>

<div class="mobileHeader">
	<div class="mobileMenu">Settings</div>
</div>

<div class="container topMargin sortFilters">
	<div class="wrapper">

		<div class="mobileSortFilterBlock">

			<div class="pageHeading">
				<h1 class="pageTitle">My Settings</h1>
			</div>
			
		</div>

	</div>
</div>

<div class="container greyBg myAccount myReviews">
	<div class="wrapper">
		<div class="leftSmallCol">
			<div class="myAccountMenu">
				<ul class="myAccountMenuList">
					<li><a href="my-favorites.php">Favorites (20)</a></li>
					<li><a href="my-business.php">Business</a></li>
					<li><a href="my-reviews.php">Reviews</a></li>
					<li><a href="my-profile.php">Profile</a></li>
					<li class="active"><a href="my-settings.php">Settings</a></li>
				</ul>
			</div>
		</div>
		<div class="rightBigCol">
			<div class="settingsBlock">
				<div class="securityPrivacy settingsCol">
					<div class="settingsColTitle">Security and Privacy</div>
					<div class="changePasswordBlock">
						<div class="changePasswordBtn">Change Password</div>
					</div>
					<div class="hideProfileBlock">
						<label for="hide" class="checkbox">
							<input type="checkbox" class="checkboxInput" value="" id="hide">
							<span class="checkboxText">Hide my profile from search results</span>
						</label>
					</div>
				</div>
				<div class="settingsCol preferencesBlock">
					<div class="settingsColTitle">Change your notification preferences</div>
					<div class="preferencesList">
						<ul>
							<li>
								<label for="pf1" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf1">
									<span class="checkboxText">Notify me for changes in my profile</span>
								</label>
							</li>
							<li>
								<label for="pf2" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf2">
									<span class="checkboxText">Notify me for activities on my reviews</span>
								</label>
							</li>
							<li>
								<label for="pf3" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf3">
									<span class="checkboxText">Notify me when my friends join yellowpages.in</span>
								</label>
							</li>
							<li>
								<label for="pf4" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf4">
									<span class="checkboxText">Keep me updated about latest offers</span>
								</label>
							</li>
							<li>
								<label for="pf5" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf5">
									<span class="checkboxText">Receive promotional emails</span>
								</label>
							</li>
							<li>
								<label for="pf6" class="checkbox">
									<input type="checkbox" class="checkboxInput" value="" id="pf6">
									<span class="checkboxText">Send me weekly updates</span>
								</label>
							</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="container bredcrumBlock greyBg">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<a href="/news" rel="v:url" property="v:title">Restaurants&nbsp;&nbsp;/</a> 
							<span rel="v:child" typeof="v:Breadcrumb"> 
								<span class="breadcrumb_last"> Hotel 1 </span> 
							</span>
			    		</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>



<?php 
	$js = "<script>
			$(document).ready(function() {
				$('.popup').click(function() {
					$('.popupBlock').addClass('showPopup');
					$('body').addClass('noScroll');
				});
				$('.closePopup').click(function() {
					$('.popupBlock').removeClass('showPopup');
					$('body').removeClass('noScroll');
				});
				$('.addFav').click(function() {
					if($(this).hasClass('favActive')) {
						$(this).removeClass('favActive');
					} else {
						$(this).addClass('favActive');
					}
				});
			});
		</script>";
	include "footer.php";
?>


