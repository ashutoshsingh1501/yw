<ul class="leftNavList">
	<li class="<?php echo $au; ?>">
		<a href="about-us.php">About us</a>
	</li>
	<li class="<?php echo $cu; ?>">
		<a href="contact-us.php">Contact us</a>
	</li>
	<li class="<?php echo $fe; ?>">
		<a href="feedback.php">Feedback</a>
	</li>
	<li class="<?php echo $fa; ?>">
		<a href="faqs.php">FAQs</a>
	</li>
	<li class="<?php echo $ca; ?>">
		<a href="careers.php">Careers</a>
	</li>
	<li class="<?php echo $tc; ?>">
		<a href="terms-conditions.php">Terms and Conditions</a>
	</li>
	<li class="<?php echo $ea; ?>">
		<a href="end-user-agreement.php">End user agreement</a>
	</li>
	<li class="<?php echo $pp; ?>">
		<a href="privacy-policy.php">Privacy Policy</a>
	</li>
</ul>