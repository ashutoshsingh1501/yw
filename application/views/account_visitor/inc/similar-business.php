<h3 class="secondaryHeading">Similar business</h3>
<ul class="similarBusinessList">
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property1.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property2.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property3.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property1.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property2.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="eachSimilarBusiness">
			<div class="eachSimilarPhoto">
				<img src="images/property3.jpg" alt="">
				<span class="ypApproved">Approved</span>
			</div>

			<div class="eachSimilarInfoBlock">
				<a href="#" class="eachSimilarTitle">Business name runs like this and runs like this</a>
				<div class="similarRatingBlock">
					<span class="rating r2-5">2.5</span>
					<a href="business-detail-review.php" class="ratingCount">1234 reviews</a>
				</div>
				<div class="eachSimilarLocation">
					Madhapur
				</div>
			</div>
		</div>
	</li>
</ul>