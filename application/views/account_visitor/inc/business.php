<div class="eachPopular">
	<div class="eachPopularLeft">
		<div class="eachPopularImage">
			<img width="170" height="170" src="images/property2.jpg" alt="">
		</div>
		<div class="eachPopularTitleBlock">
			<div class="popularTitleTextBlock">
				<a href="business-detail.php" class="eachPopularTitle"> <!-- advtPlus -->
					Business name runs here like this and goes on 
				</a>
				<div class="eachPopularOtherActions">
					<a href="#" target="_blank" class="openNewWindow"></a>
					<span class="ypApproved"></span>
					<span class="certified aaP">AA++</span>
				</div>
			</div>

			<div class="eachPopularRateOpen">
				<div class="eachPopularRatingBlock">
					<span class="rating r3-5">3.5</span>
					<a href="#" class="ratingCount">100 <span>reviews</span></a>
				</div>

				<div class="openNow"><strong>Open now</strong> - until 10pm</div>
			</div>

			<ul class="eachPopularTagsList">
				<li><a href="#">Helath</a></li>
				<li><a href="#">Food</a></li>
				<li><a href="#">Computer</a></li>
			</ul>
			<div class="eachPopularLink">
				<a href="#">Email</a>
				<a href="#">Website</a>
			</div>
		</div>
	</div>
	<div class="eachPopularRight">
		<div class="addFav">Add Favorite</div>
		<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
		<address class="businessArea">
			<strong>Madhapur</strong>
			Thiruvananthapuram - 500081
		</address>

		<div class="directionsLocationsBlock">
			<a href="#" class="locationsCount">101 Locations</a>
			<a href="#">Directions</a>
		</div>
	</div>
</div>