<?php 
	$css = 'css/yp-myAccount.css';
	$mobHeadTitle = '';
	include 'header-inner.php';
?>

<div class="mobileHeader">
	<div class="mobileMenu">Profile</div>
</div>

<div class="container topMargin sortFilters">
	<div class="wrapper">

		<div class="mobileSortFilterBlock">

			<div class="pageHeading">
				<h1 class="pageTitle">My Profile</h1>
			</div>
			
		</div>

	</div>
</div>

<div class="container greyBg myAccount myReviews">
	<div class="wrapper">
		<div class="leftSmallCol">
			<?php 
				$prof = "active";
				include 'inc/my-account-menu.php';
			?>
		</div>
		<div class="rightBigCol">
			<div class="settingsBlock">
				<div class="profileImage settingsCol">
					<div class="profileImageBlock">
						<div class="profleImagedisplay" style="background-image: url(images/property2.jpg);">
							<span class="addChangePicture">add/change picture</span>
							<span class="deletePicture">delete picture</span>
						</div>
					</div>

					<div class="profileAboutMeBlock">
						<div class="profileAboutMeText">
							<div class="titleAboutMe">About Me</div>
							<p class="aboutMeText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos saepe illum voluptates magni asperiores cumque maxime autem est officia reiciendis, earum neque, assumenda nam accusamus distinctio, itaque mollitia fugit.</p>
							<div class="profileAboutEditBtn">Edit</div>

						</div>

						<div class="profileAboutEditBlock">
							<textarea name="" id="" cols="30" rows="10" class="aboutmeTextArea">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, ex perspiciatis omnis debitis dolor quis id distinctio numquam provident nulla sapiente error aperiam quam est nihil eligendi iure voluptates inventore!</textarea>
							<div class="profileAboutSaveBtn">
								<button class="saveAboutText">Update</button>
							</div>
						</div>

					</div>
				</div>
				<div class="settingsCol preferencesBlock">
					<div class="preferencesList">
						<ul class="profileEditList">
							<li>
								<div class="eachProfileInfo">
									<span class="profileName">John Doe</span>
									<span class="profileEditBtn"></span>
								</div>

								<label for="" class="profileEditInfo">
									<input type="text" class="profileTextbox" value="John Doe">
									<button class="profileSaveBtn" value="Save"></button
								</label>

							</li>

							<li>
								<div class="eachProfileInfo">
									<span class="profileName">yugandhar@ibeesolutions.com</span>
									<span class="profileEditBtn"></span>
								</div>

								<label for="" class="profileEditInfo">
									<input type="email" class="profileTextbox" value="yugandhar@ibeesolutions.com">
									<button class="profileSaveBtn" value="Save"></button
								</label>

							</li>

							<li>
								<div class="eachProfileInfo phone">
									<span class="profileName">+91 00000 00000</span>
									<span class="profileEditBtn"></span>
								</div>

								<label for="" class="profileEditInfo phone">
									<span class="numb">+91</span>
									<input type="text" class="profileTextbox" value="0000000000">
									<button class="profileSaveBtn" value="Save"></button>
								</label>

							</li>

							<li>
								<div class="eachProfileInfo">
									<span class="profileName">Hyderabad</span>
									<span class="profileEditBtn"></span>
								</div>

								<label for="" class="profileEditInfo">
									<input type="text" class="profileTextbox" value="Hyderabad">
									<button class="profileSaveBtn" value="Save"></button
								</label>

							</li>

							<li>
								<div class="eachProfileInfo">
									<span class="profileName">https://www.yellowpages.in/yourname </span>
									<span class="profileEditBtn"></span>
								</div>

								<label for="" class="profileEditInfo">
									<input type="text" class="profileTextbox" value="https://www.yellowpages.in/yourname ">
									<button class="profileSaveBtn" value="Save"></button
								</label>

							</li>


							<li>
								<div class="eachProfileInfo">
									<button class="socialConnect facebook"></button>
								</div>
							</li>


							<li>
								<div class="eachProfileInfo">
									<button class="socialConnect twitter"></button>
								</div>
							</li>
							
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="container bredcrumBlock greyBg">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<a href="/news" rel="v:url" property="v:title">Restaurants&nbsp;&nbsp;/</a> 
							<span rel="v:child" typeof="v:Breadcrumb"> 
								<span class="breadcrumb_last"> Hotel 1 </span> 
							</span>
			    		</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>



<?php 
	$js = "<script>
			$(document).ready(function() {
				$('.popup').click(function() {
					$('.popupBlock').addClass('showPopup');
					$('body').addClass('noScroll');
				});
				$('.closePopup').click(function() {
					$('.popupBlock').removeClass('showPopup');
					$('body').removeClass('noScroll');
				});
				$('.addFav').click(function() {
					if($(this).hasClass('favActive')) {
						$(this).removeClass('favActive');
					} else {
						$(this).addClass('favActive');
					}
				});
			});
		</script>";
	include "footer.php";
?>


