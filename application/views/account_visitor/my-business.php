<?php 
	$css = 'css/yp-myAccount.css';
	$mobHeadTitle = '';
	include 'header-inner.php';
?>

<div class="mobileHeader">
	<div class="mobileMenu">Businesses</div>
</div>

<div class="container topMargin sortFilters">
	<div class="wrapper">

		<div class="mobileSortFilterBlock">
			<div class="sortBlock mobileShowBy">
				<div class="sortSelectBox select mobileSort">
					<span class="mobileSortText">Show by</span>
					<select name="" id="" class="selectBox">
						<option value="1">Category</option>
						<option value="2">City</option>
						<option value="3">Date</option>
					</select>
				</div>
			</div>

			<div class="pageHeading">
				<h1 class="pageTitle">My Businesses</h1>
			</div>
			
			<div class="sortBlock">
				<div class="sortSelectBox select mobileSort">
					<span class="mobileSortText">Sort</span>
					<span class="selectText">Sort results</span>
					<select name="" id="" class="selectBox">
						<option value="1">&#9733;</option>
						<option value="2">&#9733;&#9733;</option>
						<option value="3">&#9733;&#9733;&#9733;</option>
						<option value="2">&#9733;&#9733;&#9733;&#9733;</option>
						<option value="3">&#9733;&#9733;&#9733;&#9733;&#9733;</option>
					</select>
				</div>
			</div>

			<div class="showByDesktop">
				<span class="showByText">Show by:</span>
				<div class="showByDesktopList">
					<label for="cat" class="showByRadioLabel">
						<input type="radio" class="showByRadioBtn" value="" name="radio2" id="cat">
						<span class="showByRadioText">Category </span>
					</label>
					<label for="city" class="showByRadioLabel">
						<input type="radio" class="showByRadioBtn" value="" name="radio2" id="city">
						<span class="showByRadioText">City </span>
					</label>
					<label for="date" class="showByRadioLabel">
						<input type="radio" class="showByRadioBtn" value="" name="radio2" id="date">
						<span class="showByRadioText">Date </span>
					</label>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="container greyBg myAccount myBusiness">
	<div class="wrapper">
		<div class="leftSmallCol">
			<?php 
				$bus = "active";
				include 'inc/my-account-menu.php';
			?>
		</div>
		<div class="rightBigCol">
			<div class="businessListBlock">
				<div class="businessList">
					<div class="categoryName">City 1</div>
					<ul class="popularThisWeekList">
						<li>
							<div class="eachPopular">
								<div class="eachPopularLeft">
									<div class="eachPopularImage">
										<img width="170" height="170" src="images/property3.jpg" alt="">
									</div>
									<div class="eachPopularTitleBlock">
										<div class="popularTitleTextBlock">
											<a href="business-detail.php" class="eachPopularTitle advtPlus">
												Business name runs here like this and goes on 
											</a>
											<div class="eachPopularOtherActions">
												<span class="openNewWindow"></span>
												<span class="ypApproved"></span>
												<span class="certified aaP">AA++</span>
											</div>
										</div>

										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>

										<!-- <div class="claimBlock">
											20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
										</div> -->

										<div class="eachPopBusinsReview">
											<span class="eachPopBusinsReviewPhoto"><img src="images/profileImage.jpg" alt=""></span>
											<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
										</div>

										<div class="eachPopularLink">
											<a href="#" target="_blank">Email</a>
											<a href="#" target="_blank">Website</a>
										</div>
									</div>
								</div>
								<div class="eachPopularRight">
									<div class="editBusiness editActive">Add Favorite</div>
									<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
									<address class="businessArea">
										<strong>Madhapur</strong>
										Thiruvananthapuram - 500081
									</address> 
									<a href="search-map.php" class="businessMapDirections">Directions</a>
								</div>
							</div>
						</li>

						<li>
							<div class="eachPopular">
								<div class="eachPopularLeft">
									<div class="eachPopularImage">
										<img width="170" height="170" src="images/property3.jpg" alt="">
									</div>
									<div class="eachPopularTitleBlock">
										<div class="popularTitleTextBlock">
											<a href="business-detail.php" class="eachPopularTitle">
												Business name runs here like this and goes on 
											</a>
											<div class="eachPopularOtherActions">
												<span class="openNewWindow"></span>
												<span class="ypApproved"></span>
												<span class="certified aaP">AA++</span>
											</div>
										</div>

										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>

										<div class="claimBlock">
											20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="business-detail.php">Claim now</a>
										</div>

										<div class="eachPopularLink">
											<a href="#" target="_blank">Email</a>
											<a href="#" target="_blank">Website</a>
										</div>
									</div>
								</div>
								<div class="eachPopularRight">
									<div class="editBusiness editActive">Add Favorite</div>
									<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
									<address class="businessArea">
										<strong>Madhapur</strong>
										Thiruvananthapuram - 500081
									</address> 
									<a href="#" class="businessMapDirections">Directions</a>
								</div>
							</div>
						</li>
					</ul>

					<div class="categoryName">City 2</div>
					<ul class="popularThisWeekList">
						<li>
							<div class="eachPopular">
								<div class="eachPopularLeft">
									<div class="eachPopularImage">
										<img width="170" height="170" src="images/property3.jpg" alt="">
									</div>
									<div class="eachPopularTitleBlock">
										<div class="popularTitleTextBlock">
											<a href="business-detail.php" class="eachPopularTitle advtPlus">
												Business name runs here like this and goes on 
											</a>
											<div class="eachPopularOtherActions">
												<span class="openNewWindow"></span>
												<span class="ypApproved"></span>
												<span class="certified aaP">AA++</span>
											</div>
										</div>

										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>

										<!-- <div class="claimBlock">
											20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
										</div> -->

										<div class="eachPopBusinsReview">
											<span class="eachPopBusinsReviewPhoto"><img src="images/profileImage.jpg" alt=""></span>
											<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
										</div>

										<div class="eachPopularLink">
											<a href="#" target="_blank">Email</a>
											<a href="#" target="_blank">Website</a>
										</div>
									</div>
								</div>
								<div class="eachPopularRight">
									<div class="editBusiness editActive">Add Favorite</div>
									<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
									<address class="businessArea">
										<strong>Madhapur</strong>
										Thiruvananthapuram - 500081
									</address> 
									<a href="search-map.php" class="businessMapDirections">Directions</a>
								</div>
							</div>
						</li>

						<li>
							<div class="eachPopular">
								<div class="eachPopularLeft">
									<div class="eachPopularImage">
										<img width="170" height="170" src="images/property3.jpg" alt="">
									</div>
									<div class="eachPopularTitleBlock">
										<div class="popularTitleTextBlock">
											<a href="business-detail.php" class="eachPopularTitle">
												Business name runs here like this and goes on 
											</a>
											<div class="eachPopularOtherActions">
												<span class="openNewWindow"></span>
												<span class="ypApproved"></span>
												<span class="certified aaP">AA++</span>
											</div>
										</div>

										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>

										<div class="claimBlock">
											20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="business-detail.php">Claim now</a>
										</div>

										<div class="eachPopularLink">
											<a href="#" target="_blank">Email</a>
											<a href="#" target="_blank">Website</a>
										</div>
									</div>
								</div>
								<div class="eachPopularRight">
									<div class="editBusiness editActive">Add Favorite</div>
									<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
									<address class="businessArea">
										<strong>Madhapur</strong>
										Thiruvananthapuram - 500081
									</address> 
									<a href="#" class="businessMapDirections">Directions</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
				

			<div class="pagination">
				<div class="paginationBlock">
					<div class="pageNumber">
						<span class="pageNumberTxt">Page </span>
						<input type="text" class="pageNumberInput" maxlength="3" value="999">
					</div>
					<div class="pageNumbersNumbering">
						<a href="#" class="pageNumberingText">1</a>
						<a href="#" class="pageNumberingText">2</a>
						<a href="#" class="pageNumberingText active">3</a>
						<a href="#" class="pageNumberingText">4</a>
						<a href="#" class="pageNumberingText">5</a>
						<span class="pageNumberingDots">....</span>
						<a href="#" class="pageNumberingText">21</a>
						<a href="#" class="pageNumberingText">22</a>
						<a href="#" class="pageNumberingText">23</a>
						<a href="#" class="pageNumberingText">24</a>
						<a href="#" class="pageNumberNextBtn">Next</a>
					</div>

				</div>
			</div>


		</div>
	</div>
</div>

<div class="mobileLoadMore">
	<button class="loadMoreBtn">Load More</button>	
</div>

<div class="container bredcrumBlock greyBg">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<a href="/news" rel="v:url" property="v:title">Restaurants&nbsp;&nbsp;/</a> 
							<span rel="v:child" typeof="v:Breadcrumb"> 
								<span class="breadcrumb_last"> Hotel 1 </span> 
							</span>
			    		</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>



<?php 
	$js = "<script>
			$(document).ready(function() {
				$('.popup').click(function() {
					$('.popupBlock').addClass('showPopup');
					$('body').addClass('noScroll');
				});
				$('.closePopup').click(function() {
					$('.popupBlock').removeClass('showPopup');
					$('body').removeClass('noScroll');
				});
				$('.editBusiness editActive').click(function() {
					if($(this).hasClass('favActive')) {
						$(this).removeClass('favActive');
					} else {
						$(this).addClass('favActive');
					}
				});
			});
		</script>";
	include "footer.php";
?>


