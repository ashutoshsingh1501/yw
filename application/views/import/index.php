<section>
	<div class="section-body">
		<div class="card">
			<div class="card-head style-primary">
				<header>Import Data</header>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="form-group">
						<div class="col-md-3 col-sm-3">
							<div class="radio radio-styled">
								<label>
									<input checked type="radio" name="importdata" value="yes">
									<span>Cities</span>
								</label>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="radio radio-styled">
								<label>
									<input type="radio" name="importdata" value="no"  >
									<span>Areas</span>
								</label>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="radio radio-styled">
								<label>
									<input type="radio" name="importdata" value="no"  >
									<span>Sub categories</span>
								</label>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="radio radio-styled">
								<label>
									<input type="radio" name="importdata" value="no"  >
									<span>Employees</span>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<div class="fileinput fileinput-new input-group" data-provides="fileinput">
								<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
								<span class="input-group-addon btn  btn-file"><span class="fileinput-new btn btn-default">Browse</span><span class="fileinput-exists btn btn-default">Change</span><input type="file" name="..." accept=".xlsx , .xlsm , .xls" ></span>
								<a href="#" class="input-group-addon btn fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit"  class="btn btn-primary">Import Data</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>                	
	</div>
</section>