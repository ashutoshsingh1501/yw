<?php 
//	$css = 'css/yp-staticPages.css';
	$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<div class="mobileHeader transparent">
	<div class="mobileMenu white">End User Agreement</div>
</div>

<div class="container aboutUsBanner banner">
	<div class="wrapper">
		<div class="aboutUsBannerBlock">
			<img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
			<h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
		</div>
	</div>
</div>

<div class="container mainContent aboutUsContentBlock">
	<div class="wrapper">
		<div class="pageHeading ypHeading">
			Yellowpages
		</div>
		<div class="leftNav">
			<div class="leftNavBlock">
				<?php 
					$ea = "active";
					include 'inc/static-pages-menu.php';
				?>
			</div>
		</div>
		<div class="rightContent staticFullWidthBlock">
			<div class="termsConditionsBlock staticFullWidth">
				<h3 class="headingTC">1. Genral</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi impedit, voluptates ut eligendi, deserunt provident necessitatibus, minus esse itaque hic fugit iure facere est praesentium nesciunt. Aspernatur omnis facilis, nostrum.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae provident magni nam perspiciatis cumque? Eaque quisquam amet doloribus voluptatum ipsam alias voluptatem, nostrum cupiditate quaerat, dolor mollitia nam, libero doloremque! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum similique nam consequatur nemo quo rem voluptatum veniam, natus minima consequuntur! Eaque assumenda, aut nihil pariatur facere, alias tempore accusantium possimus!</p>
				<h3 class="headingTC">2. Scope of this Agreement</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem unde, amet nam eius esse dolores, at nostrum tenetur blanditiis rem quo quam! Ipsam neque eaque, ullam similique corporis, officia voluptate?</p>
				<h4 class="subHeading">2.1 Scope of this Agreement</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut impedit necessitatibus quae, amet explicabo enim quis non, commodi deserunt totam repellendus eligendi, dignissimos obcaecati placeat quia, maxime ipsa fugit est.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, consequuntur eum commodi excepturi dolores, maiores expedita voluptas atque a! Incidunt ab laboriosam, debitis cumque assumenda quasi veritatis molestias explicabo eveniet!</p>
				<h5 class="subSubHeading">2.1.1 Scopeof this Agreement</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora odit illum pariatur praesentium. Exercitationem cumque esse sapiente dolor magnam excepturi eveniet quaerat expedita tempore quam quibusdam id temporibus minus, vitae?</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti aspernatur aliquid veritatis, consequuntur ratione repudiandae deserunt vel quas cupiditate, itaque impedit magni nemo! Temporibus rerum quod quo debitis, quis odio.</p>
				<ul class="listTC">
					<li>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum in aliquid vitae ratione libero incidunt perferendis veritatis, dolore molestias deserunt enim impedit non aperiam, officiis nisi sit error. Maxime, voluptatem.</p>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente enim eos quae excepturi necessitatibus, eum saepe eveniet nesciunt, eaque ut harum, veniam exercitationem, eius quibusdam explicabo praesentium laborum. Soluta, pariatur?</p>
					</li>
					<li>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus enim eius, atque eaque accusamus, blanditiis magnam harum doloremque voluptas quis repellendus. Laborum ratione atque quidem, error repudiandae ipsa cupiditate unde!</p>
						<ul class="listTC">
							<li>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo veritatis vitae debitis magnam corporis nobis, et eius voluptates, nisi officiis. Nihil aliquid odit numquam ut totam doloremque aut tempora, provident.</p>
							</li>
							<li>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam voluptatum saepe fugiat, eius quasi. Ea ab ipsam, a officiis, consequuntur veniam nihil rerum natus nobis blanditiis fugit fugiat velit facilis.</p>

								<ul class="listTC">
									<li>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia delectus ipsum magni nobis suscipit corporis cumque et ea dolorum, assumenda aliquid beatae, similique eaque sit modi sunt eos vitae. Saepe!</p>
									</li>
									<li>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis, velit voluptatum eius recusandae ab quam sapiente perferendis autem a aperiam, officia. Labore quisquam animi, voluptas molestias autem tempora perspiciatis similique.</p>
									</li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
				<h3 class="headingTC">Payment</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate laborum, quam architecto culpa. Totam tempora, vel iusto. Harum reiciendis placeat quam eum beatae voluptas, quisquam quaerat quod dolore, cumque ut.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam odio sunt obcaecati nulla dolorem laborum nesciunt? Numquam nihil eum, animi nesciunt ratione, porro ad accusamus aut repellat officia cupiditate hic.</p>
			</div>
		</div>
	</div>
</div>

<div class="container bredcrumBlock">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<span class="breadcrumb_last"> End User Agreement </span> 
						</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>








<?php 
//	include "footer.php";
?>

<script>
	$(document).ready(function() {

		var body = $("html, body");
		var currentTop = $(this).offset();


		/*body.stop().animate({scrollTop:0}, '500', 'swing', function() { 
		   alert("Finished animating");
		});*/

		var scrollNow = "";
		$('.faqQuestion').click(function() {
			$('.faqsList').find('.open').removeClass('open');
			$(this).addClass('open');
			scrollNow = $(this).offset().top;
			//console.log(scrollNow);
			body.stop().animate({scrollTop: scrollNow}, '500', 'swing');
			$(this).parent().find('.faqAnswer').addClass('open');
		});	
	});
</script>
