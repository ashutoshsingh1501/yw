<ul class="leftNavList">
	<li class="<?php echo (isset($au))? $au:''; ?>">
		<a href="<?php echo base_url(); ?>about-us">About us</a>
	</li>
	<li class="<?php echo (isset($cu))? $cu:''; ?>">
		<a href="<?php echo base_url(); ?>contact-us">Contact us</a>
	</li>
	<li class="<?php echo (isset($fe))? $fe:''; ?>">
		<a href="<?php echo base_url(); ?>feedback">Feedback</a>
	</li>
	<li class="<?php echo (isset($fa))? $fa:''; ?>">
		<a href="<?php echo base_url(); ?>frequently-asked-questions">FAQs</a>
	</li>
	<li class="<?php echo (isset($ca))? $ca:''; ?>">
		<a href="<?php echo base_url(); ?>careers">Careers</a>
	</li>
	<li class="<?php echo (isset($tc))? $tc:''; ?>">
		<a href="<?php echo base_url(); ?>terms-and-conditions">Terms and Conditions</a>
	</li>
	<li class="<?php echo (isset($pp))? $pp:''; ?>">
		<a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a>
	</li>
</ul>