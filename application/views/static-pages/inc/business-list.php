<li>
	<?php include 'inc/business.php' ?>
</li>
<li>
	<div class="eachPopular">
		<div class="eachPopularLeft">
			<div class="eachPopularImage">
				<img width="170" height="170" src="images/property3.jpg" alt="">
			</div>
			<div class="eachPopularTitleBlock">
				<div class="popularTitleTextBlock">
					<a href="business-detail.php" class="eachPopularTitle advtPlus">
						Business name runs here like this and goes on 
					</a>
					<div class="eachPopularOtherActions">
						<span class="openNewWindow"></span>
						<span class="ypApproved"></span>
						<span class="certified aaP">AA++</span>
					</div>
				</div>

				<div class="eachPopularRatingBlock">
					<span class="rating r3-5">3.5</span>
					<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
				</div>

				<div class="openNow"><strong>Open now</strong> - until 10pm</div>

				<!-- <div class="claimBlock">
					20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
				</div> -->

				<div class="eachPopBusinsReview">
					<span class="eachPopBusinsReviewPhoto"><img src="images/profileImage.jpg" alt=""></span>
					<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
				</div>

				<div class="eachPopularLink">
					<a href="#" target="_blank">Email</a>
					<a href="#" target="_blank">Website</a>
				</div>
			</div>
		</div>
		<div class="eachPopularRight">
			<div class="addFav">Add Favorite</div>
			<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
			<address class="businessArea">
				<strong>Madhapur</strong>
				Thiruvananthapuram - 500081
			</address> 
			<a href="search-map.php" class="businessMapDirections">Directions</a>
		</div>
	</div>
</li>

<li>
	<div class="eachPopular">
		<div class="eachPopularLeft">
			<div class="eachPopularImage">
				<img width="170" height="170" src="images/property3.jpg" alt="">
			</div>
			<div class="eachPopularTitleBlock">
				<div class="popularTitleTextBlock">
					<a href="business-detail.php" class="eachPopularTitle">
						Business name runs here like this and goes on 
					</a>
					<div class="eachPopularOtherActions">
						<span class="openNewWindow"></span>
						<span class="ypApproved"></span>
						<span class="certified aaP">AA++</span>
					</div>
				</div>

				<div class="eachPopularRatingBlock">
					<span class="rating r3-5">3.5</span>
					<a href="business-detail-review.php" class="ratingCount">100 <span>reviews</span></a>
				</div>

				<div class="openNow"><strong>Open now</strong> - until 10pm</div>

				<div class="claimBlock">
					20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="business-detail.php">Claim now</a>
				</div>

				<div class="eachPopularLink">
					<a href="#" target="_blank">Email</a>
					<a href="#" target="_blank">Website</a>
				</div>
			</div>
		</div>
		<div class="eachPopularRight">
			<div class="addFav">Add Favorite</div>
			<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
			<address class="businessArea">
				<strong>Madhapur</strong>
				Thiruvananthapuram - 500081
			</address> 
			<a href="#" class="businessMapDirections">Directions</a>
		</div>
	</div>
</li>