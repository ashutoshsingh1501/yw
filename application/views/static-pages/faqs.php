<div class="mobileHeader transparent">
    <div class="mobileMenu white">FAQs</div>
</div>

<div class="container aboutUsBanner banner">
    <div class="wrapper">
        <div class="aboutUsBannerBlock">
            <img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
            <h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
        </div>
    </div>
</div>

<div class="container mainContent aboutUsContentBlock">
    <div class="wrapper">
        <div class="pageHeading ypHeading">
            Yellowpages
        </div>
        <div class="leftNav">
            <div class="leftNavBlock">
                <?php
                $fa = "active";
                include 'inc/static-pages-menu.php';
                ?>
            </div>
        </div>
        <div class="rightContent staticFullWidthBlock">
            <div class="faqsBlock staticFullWidth">
                <ul class="faqsList">
                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                What is a YellowPages.in Account and what are the benefits?
                            </div>
                            <div class="faqAnswer">
                                <p>“My Account” lets you connect to the Yellowpages.in by using a single email account and password. With a single confidential and secured subscription, My Yellow Pages Account makes it possible for you to Add your business, give your opinion on your favorite businesses, share your reviews and securely edit your personal information. Watch for more exciting features coming soon!</p>
                            </div>
                        </div>
                    </li>                  

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How do I “register/ login / make changes” for a Yellow Pages Account?
                            </div>
                            <div class="faqAnswer">
                                <p>Click on "<a href="register">Register</a>” on the Log in page. Type in your Name, Email address, Phone number and choose a password. Then click on 'Sign Up'. Follow verification of email address/phone Number to complete the registration process. Click on 'My Account' from the top navigation bar. Then Login with registered email id and password.  After logging to “My Account”, Click on “profile” to make changes to your profile. </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to “add a business” to Yellowpages.in?
                            </div>
                            <div class="faqAnswer">
                                <p>Go to "<a href="add-business">Add Business</a>” form, Enter the business information in the provided fields. Your added business will be available under “My business” in My Account dashboard.</p>
                            </div>
                        </div>
                    </li>


                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to Update Business Page?
                            </div>
                            <div class="faqAnswer">
                                <p>Go to “My account” dashboard and get on to “My business”. All the details can be updated from here about your business. </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to “Get a Deal”?
                            </div>
                            <div class="faqAnswer">
                                <p>This feature enables a business owner to contact you, for the business related to search term. </p>
                            </div>
                        </div>
                    </li>



                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to Update Business Page?
                            </div>
                            <div class="faqAnswer">
                                <p>Go to “My account” dashboard and get on to “My business”. All the details can be updated from here about your business. </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to “Suggest a Business”?
                            </div>
                            <div class="faqAnswer">
                                <p>Didn’t find what you are looking for, this feature enables you to “Suggest a business” by entering the business information in the provided fields. </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to add business into “Featured Business” and how does it work?
                            </div>
                            <div class="faqAnswer">
                                <p>Write an email to business@yellowpages.in to get featured. “Featured Businesses” will be shown in the top position for relevant search results. As this is paid service, suitable charges will apply. </p>
                            </div>
                        </div>
                    </li>


                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to moderate my business reviews?
                            </div>
                            <div class="faqAnswer">
                                <p>After logging into “My Account”, go to “My Business” and moderate comments for your businesses.  </p>
                            </div>
                        </div>
                    </li>


                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to add business to “Favorites”? 
                            </div>
                            <div class="faqAnswer">
                                <p>Click on “Heart” symbol on your favorite business to make into “Favorite” list. This favorites section will be accessible through “My Account” dashboard.  </p>
                            </div>
                        </div>
                    </li>


                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How to “Advertise with Yellowpages.in”?
                            </div>
                            <div class="faqAnswer">
                                <p>Write an email to advertise@yellowpages.in mentioning advertising objective. Our business managers will contact you and explain proceedings. As this is paid services, suitable charges will apply. </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                How do I deactivate my account?
                            </div>
                            <div class="faqAnswer">
                                <p>Please <a href="contact-us">contact us</a> and we'll do it for you! </p>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                               I still need help. What should I do?
                            </div>
                            <div class="faqAnswer">
                                <p>If you can't find an answer to your problem in the FAQ, go to the <a href="contact-us">contact us</a> page and describe your problem in detail. This will help us understand your problem and quickly find a solution. </p>
                            </div>
                        </div>
                    </li>


                </ul>
                <div class="writeToUs">Haven't found answer for your question? <a href="contact-us">Write to us</a></div>
            </div>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last"> FAQs </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.faqQuestion').click(function () {
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).parent().find('.faqAnswer').removeClass('open');
            } else {
                $('.faqsList').find('.open').removeClass('open');
                $(this).addClass('open');
                $("html, body").stop().animate({scrollTop: $(this).offset().top}, '500', 'swing');
                $(this).parent().find('.faqAnswer').addClass('open');
            }
        });
    });
</script>
