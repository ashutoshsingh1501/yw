<?php
//	$css = 'css/yp-staticPages.css';
$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<div class="mobileHeader transparent">
    <div class="mobileMenu white">Feedback</div>
</div>

<div class="container aboutUsBanner banner">
    <div class="wrapper">
        <div class="aboutUsBannerBlock">
            <img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
            <h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
        </div>
    </div>
</div>

<div class="container mainContent aboutUsContentBlock">
    <div class="wrapper">
        <div class="pageHeading ypHeading">
            Yellowpages
        </div>
        <div class="leftNav">
            <div class="leftNavBlock">
                <?php
                $fe = "active";
                include 'inc/static-pages-menu.php';
                ?>
            </div>
        </div>

        <?php echo form_open("submit-feedback"); ?>
        
        <div class="rightContent">
            <div class="rightSidebar">
                <div class="rightSideEachBlock rateExperience">
                    <div class="rateExperienceTitle">Rate your experience with Yellowpages.in </div>
                    <div class="ratingFormBlock">
                        <div class="ratingFormList">
                            <input type="radio" class="radioRating" id="5" value="5" name="rating">
                            <label for="5" class="radioRatingLabel">5</label>
                            <input type="radio" class="radioRating" id="4" value="4" name="rating">
                            <label for="4" class="radioRatingLabel">4</label>
                            <input type="radio" class="radioRating" id="3" value="3" name="rating">
                            <label for="3" class="radioRatingLabel">3</label>
                            <input type="radio" class="radioRating" id="2" value="2" name="rating">
                            <label for="2" class="radioRatingLabel">2</label>
                            <input type="radio" class="radioRating" id="1" value="1" name="rating">
                            <label for="1" class="radioRatingLabel">1</label>
                        </div>
                    </div>
                </div>

                <div class="rightSideEachBlock rateExperience">
                    <div class="rateExperienceTitle">Would you recommend Yellowpages.in to your friends?</div>
                    <div class="suggestFriendsBlock">
                        <div class="suggestFriendsForm">
                            <label for="yes" class="radio">
                                <input type="radio" class="radioInput" id="yes" value="yes" name="suggest">
                                <span class="radioText">Yes</span>
                            </label>
                            <label for="no" class="radio">
                                <input type="radio" class="radioInput" id="no" value="no" name="suggest">
                                <span class="radioText">No</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            
            

            <div class="content">
                <p>Your participation will definitely help us to improve user experience on our website and in turn gives a better experience for you.</p>
                <div class="formListBlock">
                    <ul class="formList">
                        <li class="radioBtnsBlock">
                            <div class="radioBtnsBlockTitle">I am</div>
                            <div class="eachFormElement">
                                <!--<label for="ypEmployee" class="radio">
                                    <input type="radio" class="radioInput" name="iam" id="ypEmployee" value="YP-Employee">
                                    <span class="radioText">YP Employee</span>
                                </label>-->

                                <label for="bOwner" class="radio">
                                    <input type="radio" class="radioInput" name="iam" id="bOwner" value="business-Owner">
                                    <span class="radioText">Business Owner</span>
                                </label>

                                <label for="consume" class="radio">
                                    <input type="radio" class="radioInput" name="iam" id="consume" value="consumer">
                                    <span class="radioText">Consumer</span>
                                </label>
                            </div>
                        </li>

                        <li class="radioBtnsBlock">
                            <div class="radioBtnsBlockTitle">I want to talk about</div>
                            <div class="eachFormElement">
                                <label for="website" class="radio">
                                    <input type="radio" class="radioInput" name="iwant" id="website" value="website">
                                    <span class="radioText">Website</span>
                                </label>
                                <label for="business" class="radio">
                                    <input type="radio" class="radioInput" name="iwant" id="business" value="business">
                                    <span class="radioText">Business</span>
                                </label>
                                <label for="data" class="radio">
                                    <input type="radio" class="radioInput" name="iwant" id="data" value="data">
                                    <span class="radioText">Data</span>
                                </label>
                                <label for="others" class="radio">
                                    <input type="radio" class="radioInput" name="iwant" id="others" value="others">
                                    <span class="radioText">Others</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <input type="text" class="eachFormTextbox" name="user_name" value="<?php echo set_value('user_name'); ?>">
                                <label for="user_name" class="eachFormLabel">Please enter your name*</label><?php echo form_error('user_name'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement phone">
                                <span class="number">+91</span>
                                <input type="text" class="eachFormTextbox" name="user_contact" value="<?php echo set_value('user_contact'); ?>">
                                <label for="user_contact" class="eachFormLabel">Mobile*</label><?php echo form_error('user_contact'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <input type="email" class="eachFormTextbox" name="user_mail" value="<?php echo set_value('user_mail'); ?>">
                                <label for="user_mail" class="eachFormLabel">Email*</label><?php echo form_error('user_mail'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <textarea cols="30" rows="10" class="eachFormTextarea" name="user_message"><?php echo set_value('user_message'); ?></textarea>
                                <label for="user_message" class="eachFormLabel">Write your feedback and suggestions?*</label><?php echo form_error('user_message'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <input type="submit" class="eachFormButton" value="Send Feedback">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
      <?php echo form_close(); ?>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last"> Feedback </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>

<?php
//	include "footer.php";
?>

<script>
    $(document).ready(function () {
        $('.eachFormTextbox, .eachFormTextarea').on('focus blur', function (e) {
            $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    });
</script>
