<?php
//	$css = 'css/yp-staticPages.css';
$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<h2><?php echo $this->session->flashdata('error'); ?></h2> 

<div class="mobileHeader transparent">
    <div class="mobileMenu white">Contact us</div>
</div>

<div class="container aboutUsBanner banner">
    <div class="wrapper">
        <div class="aboutUsBannerBlock">
            <img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
            <h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
        </div>
    </div>
</div>

<div class="container mainContent aboutUsContentBlock">
    <div class="wrapper">
        <div class="pageHeading ypHeading">
            Yellowpages
        </div>
        <div class="leftNav">
            <div class="leftNavBlock">
                <?php
                $cu = "active";
                include 'inc/static-pages-menu.php';
                ?>
            </div>
        </div>
        <div class="rightContent">
            <div class="rightSidebar">
                <div class="rightSideEachBlock yellowBg email">
                    <div class="rightSideHeading">Promote your business with Yellowpages.in!</div>

                    <div class="emailUs">
                        <div class="emailUsText">Email us</div>
                        <div class="emailIDBlock">advertise@yellowpages.in</div>
                    </div>
                </div>

                <div class="rightSideEachBlock ourOffice">
                    <div class="ourOfficeHeading">Our office</div>
                    <address class="addressBlock">
                        Yellowpages.in, <br />
                        Smartworkz Digital Pvt Ltd,<br />
                        6B, Vaishnavi's Cynosure,<br />
                        Gachibowli, Hyderabad-500032, <br />
                        Telangana, India
                    </address>
                </div>

            </div>

            <?php echo form_open("send-inquiry"); ?>
            <div class="content">
                <p>We would Love to Hear from You, fill in the below details to help you better.</p>
                <div class="formListBlock">
                    <ul class="formList">
                        <li>
                            <div class="eachFormElement">
                                <input type="text" class="eachFormTextbox"  name="user_name" value="<?php echo set_value('user_name'); ?>">
                                <label for="" class="eachFormLabel">Name*</label><?php echo form_error('user_name'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement phone">
                                <span class="number">+91</span>
                                <input type="text" class="eachFormTextbox"  name="user_contact" value="<?php echo set_value('user_contact'); ?>">
                                <label for="" class="eachFormLabel">Mobile*</label><?php echo form_error('user_contact'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <input type="email" class="eachFormTextbox"  name="user_mail" value="<?php echo set_value('user_mail'); ?>">
                                <label for="" class="eachFormLabel">Email*</label><?php echo form_error('user_mail'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <textarea name="user_message" id="" cols="30" rows="10" class="eachFormTextarea" ><?php echo set_value('user_message'); ?></textarea>
                                <label for="" class="eachFormLabel">What you want to talk about?*</label><?php echo form_error('user_message'); ?>
                            </div>
                        </li>
                        <li>
                            <div class="eachFormElement">
                                <input type="submit" class="eachFormButton" value="Send inquriy">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last">Contact us </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>








<?php
//	include "footer.php";
?>

<script>
    $(document).ready(function () {
        $('.eachFormTextbox, .eachFormTextarea').on('focus blur', function (e) {
            $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    });
</script>
