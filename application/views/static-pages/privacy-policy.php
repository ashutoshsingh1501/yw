<?php 
//	$css = 'css/yp-staticPages.css';
	$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<div class="mobileHeader transparent">
	<div class="mobileMenu white">Privacy Policy</div>
</div>

<div class="container aboutUsBanner banner">
	<div class="wrapper">
		<div class="aboutUsBannerBlock">
			<img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
			<h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
		</div>
	</div>
</div>

<div class="container mainContent aboutUsContentBlock">
	<div class="wrapper">
		<div class="pageHeading ypHeading">
			Yellowpages
		</div>
		<div class="leftNav">
			<div class="leftNavBlock">
				<?php 
					$pp = "active";
					include 'inc/static-pages-menu.php';
				?>
			</div>
		</div>
		<div class="rightContent staticFullWidthBlock">
			<div class="termsConditionsBlock staticFullWidth">
				<h3 class="headingTC">1. WHAT DO WE DO WITH YOUR INFORMATION?</h3>
				<p>When you register with or purchase something from our website www.Yellowpages.in, as part of the registration or buying and selling process, we collect the personal information you give us such as your name, address, email address and other data such as (but not limited to) location, photographs and social media details.</p>
				<p>When you browse our website or contribute content such as ratings, comments, testimonials, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that helps us learn about your browser and operating system.</p>
				<p>Email marketing: With your permission, we may send you emails about our website, new products and other updates.</p>
				<p>We also communicate with the users through in-app (mobile application) notifications, SMS, email which may lead to data collected by us from the user such as permissions for sending offers/information from our partners, opinion polls etc</p>
				<p>We show online advertisements & use tracking tools including Google Analytics, cookie tracking, location tracking & access to your phone book (after receiving your explicit consent) & other behavioural tracking when you are using our website or mobile application.</p>
				<h3 class="headingTC">2. CONSENT</h3>
				<h4 class="subHeading">2.1 How do you get my consent?</h4>
				<p>When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only.</p>
				<p>If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.</p>
				<h4 class="subHeading">2.2 How do I withdraw my consent?</h4>
				<p>If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at info@yellowpages.in or mailing us at: Yellowpages.in c/o Smartworkz Digital Pvt Ltd, 6B, Vaishnavi’s Cynosure, Gachibowli, Hyderabad-500032.</p>
				<h3 class="headingTC">3. DISCLOSURE</h3>
				<p>We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.</p>
				<h3 class="headingTC">4. PAYMENT</h3>
				<p>We use Razorpay for processing payments. We/Razorpay do not store your card data on their servers. The data is encrypted through the Payment Card Industry Data Security Standard (PCI-DSS) when processing payment. Your purchase transaction data is only used as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is not saved.</p>
				<p>Our payment gateway adheres to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover.</p>
				<p>PCI-DSS requirements help ensure the secure handling of credit card information by our website and its service providers.</p>
				<p>For more insight, you may also want to read terms and conditions of razorpay on their website www.razorpay.com.</p>
				<h3 class="headingTC">5. THIRD-PARTY SERVICES</h3>
				<p>In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us.</p>
				<p>However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.</p>
				<p>For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers.</p>
				<p>In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located.</p>
				<p>Once you leave our website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service. </p>
				<p>Links : When you click on links on our website, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements. </p>
				<h3 class="headingTC">6. SECURITY</h3>
				<p>To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.</p>
				<h3 class="headingTC">7. COOKIES</h3>
				<p>We use cookies to maintain session of your user. It is not used to personally identify you on other websites.</p>
				<h3 class="headingTC">8. AGE OF CONSENT</h3>
				<p>By using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.</p>
				<h3 class="headingTC">9. CHANGES TO THIS PRIVACY POLICY</h3>
				<p>We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it.</p>
				<p>If our website is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.</p>
				<h3 class="headingTC">10. QUESTIONS AND CONTACT INFORMATION</h3>
				<p>If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at at info@yellowpages.in or by mail at: Yellowpages.in c/o Smartworkz Digital Pvt Ltd, 6B, Vaishnavi’s Cynosure, Gachibowli, Hyderabad-500032.</p>
			</div>
		</div>
	</div>
</div>

<div class="container bredcrumBlock">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<span class="breadcrumb_last"> Privacy Policy </span> 
						</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>

