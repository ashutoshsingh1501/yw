<?php 
//	$css = 'css/yp-staticPages.css';
	$mobHeadTitle = '';
//	include 'header-inner.php';
?>
<div class="mobileHeader transparent">
	<div class="mobileMenu white">About us</div>
</div>

<div class="container aboutUsBanner banner">
	<div class="wrapper">
		<div class="aboutUsBannerBlock">
			<img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
			<h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
		</div>
	</div>
</div>

<div class="container mainContent aboutUsContentBlock">
	<div class="wrapper">
		<div class="pageHeading ypHeading">
			Yellowpages
		</div>
		<div class="leftNav">
			<div class="leftNavBlock">
				<?php 
					$au = "active";
					include 'inc/static-pages-menu.php';
				?>
			</div>
		</div>
		<div class="rightContent">
			<!--<div class="rightSidebar yellowContent">
				<div class="yellowGlanceText">
					Yellow pages at a glance
				</div>
				<ul class="glanceInfo">
					<li>
						<div class="yearTitlte">Founded</div>
						<div class="yearsText">
							2016
						</div>
					</li>

					<li>
						<div class="yearTitlte">Founders</div>
						<div class="yearsText">
							John Doe 1<br>
							John Doe 2
						</div>
					</li>

					<li>
						<div class="yearTitlte">Our Office</div>
						<div class="yearsText">
							2-48/5/6, Flat no 6B, Vaishnavi’s Cynosure,<br>
							Beside Gachibowli flyover, Gachibowli,<br>
							Hyderabad - 500034<br>
							Telangana.
						</div>
					</li>
				</ul>
			</div>-->

			<div class="content textView">
				<h2 class="secondHeading">Our mission is to organise Indian business information and leverage technology to eliminate barriers between businesses and their customers.</h2>
				<p>
                                  Yellowpages.in is a business information portal where local businesses can list themselves so that the people of their city can locate the right business for their needs conveniently and quickly.
                                </p><br />
                                <p>
                                    Yellowpages.in goes beyond being a business listing portal by offering richer listings and a user experience that today’s digital natives crave! Millions of businesses have recognised Yellowpages.in for its ability to build their online identity, connect them with customers across India, increase footfalls and improve revenues.
                                </p><br />
                                <p>
                                    As a member of Yellowpages.in, you can unlock the opportunity to be discovered by customers interested in your products and services, and reach them with targeted promotions and content. Not just that, your Yellowpages.in listing is the hub of your online reputation as it consolidates reviews from users and helps customers choose your business over your competitors!
                                </p><br />
                                 <p>
                                     Listing on Yellowpages.in is simple. <a href="add-business" class="external-link" rel="nofollow">Click here</a> to get listed!
                                 </p>

				<!--<div class="yellowPagesTeam">
					<div class="yellowPagesTeamTitle">People behind Yellowpages</div>
					<div class="yellowPagestTeamListBlock">
						<ul class="yellowPagesTeamList">
							
							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>

							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>

							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>

							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>

							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>

							<li>
								<div class="eachYpProfile">
									<div class="eachYpProfileImage">
										<img src="<?php echo STYLEURL; ?>front/images/profileYP1.jpg" alt="">
									</div>
									<div class="eachYpProfileName">
										John Doe John
									</div>
									<div class="eachYpProfileDesignation">
										Designation here
									</div>
								</div>
							</li>
						</ul>
					</div>
						
				</div>-->
			</div>
		</div>
	</div>
</div>

<div class="container bredcrumBlock">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<span class="breadcrumb_last"> About us </span> 
						</span>
			    	</span>
			    </span>
			</p>
		</div>
	</div>
</div>

<?php 
//	include "footer.php";
?>

<script>
	$(document).ready(function() {
		$('.formElementTextbox, .formElementTextarea').on('focus blur', function (e) {
		    $(this).parent('.formElement').toggleClass('formElementLableFocused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
	});
</script>
