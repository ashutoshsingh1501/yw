<?php 
//	$css = 'css/yp-staticPages.css';
	$mobHeadTitle = '';
//	include 'header-inner.php';
?>

<div class="mobileHeader transparent">
	<div class="mobileMenu white">Advertise with us</div>
</div>

<div class="container mainContent aboutUsContentBlock">
	<div class="wrapper">
		<div class="pageHeading ypHeading">
			Advertise with us
		</div>
		<div class="leftNav">
			<div class="leftNavBlock advertiseWithus">
				<div class="advertiseWithUsInfo">
					Please call us or use the form on the right to share your details and we will respond shortly to discuss how you can promote and grow your business with Yellowpages.in. </div>
				<div class="benefits">
					<div class="benefitsTitle">Some of the benefits of advertising us on Yellowpages.in are :</div>
					<ul class="benefitsList">
						<li>Get discovered by customers searching for your products and services</li>
						<li>Deliver targeted promotions and content to your customers</li>
                                                <li>Establish presence in new categories and geographies rapidly</li>
						
					</ul>
				</div>
			</div>
		</div>
		<div class="rightContent">
			<div class="rightSidebar yellowContent">
				<ul class="glanceInfo">
					<li>
						<div class="yearTitlte">You can call us at below number</div>
						<div class="yearsText">
							040-6694 9100
						</div>
					</li>
					<li>
						<div class="yearTitlte">Our Office</div>
						<div class="yearsText">
							Yellowpages.in,<br />
                                                        Smartworkz Digital Pvt Ltd,
                                                        6B, Vaishnavi's Cynosure,
                                                        Gachibowli, <br />
                                                        Hyderabad-500032, 
                                                        Telangana, India.
						</div>
					</li>
				</ul>
			</div>

		<?php echo form_open("advertise-inquiry"); ?>	
                    <div class="content">
				<div class="formListBlock advertiseWithFormBlock">
					<ul class="formList">
						<li>
							<div class="eachFormElement">
								<input type="text" class="eachFormTextbox" name="user_name" value="<?php echo set_value('user_name'); ?>">
								<label for="" class="eachFormLabel">Name*</label><?php echo form_error('user_name'); ?>
							</div>
						</li>
						<li>
							<div class="eachFormElement phone">
								<span class="number">+91</span>
								<input type="text" class="eachFormTextbox" name="user_contact" value="<?php echo set_value('user_contact'); ?>">
								<label for="" class="eachFormLabel">Mobile*</label><?php echo form_error('user_contact'); ?>
							</div>
						</li>
						<li>
							<div class="eachFormElement">
								<input type="email" class="eachFormTextbox"   name="user_mail" value="<?php echo set_value('user_mail'); ?>">
								<label for="" class="eachFormLabel">Email*</label><?php echo form_error('user_mail'); ?>
							</div>
						</li>
						<li>
							<div class="eachFormElement">
								<input type="text" class="eachFormTextbox"   name="user_city">
								<label for="" class="eachFormLabel">City</label>
							</div>
						</li>
						<!--<li>
							<div class="eachFormElement">
								<div class="sortSelectBox select">
									<span class="selectText">Sort results</span>
									<select name="ad_type" id="" class="selectBox">
										<option value="1">Advertisement type 1</option>
										<option value="2">Advertisement type 2</option>
										<option value="3">Advertisement type 3</option>
										<option value="4">Advertisement type 4</option>
										<option value="5">Advertisement type 5</option>
									</select>
								</div>
							</div>
						</li>-->
						<li>
							<div class="eachFormElement" >
								<textarea name="user_message" id="" cols="30" rows="10" class="eachFormTextarea"><?php echo set_value('user_message'); ?></textarea>
								<label for="" class="eachFormLabel">Tell us more about your business, products, services, locations and needs</label><?php echo form_error('user_message'); ?>
							</div>
						</li>
						<li>
							<div class="eachFormElement">
								<input type="submit" class="eachFormButton" value="Send inquriy">
							</div>
						</li>
					</ul>
				</div>
			</div>
		<?php echo form_close(); ?>
                </div>
	</div>
</div>

<div class="container bredcrumBlock">
	<div class="wrapper">
		<div class="bredcrumNav">
			<p id="breadcrumbs">You are here:
				<span xmlns:v="http://rdf.data-vocabulary.org/#"> 
					<span typeof="v:Breadcrumb"> 
						<a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
						<span rel="v:child" typeof="v:Breadcrumb"> 
							<span class="breadcrumb_last"> Advertise with us </span> 
						</span>
			    	</span>
			    </span>
			<!--</p>-->
		</div>
	</div>
</div>

<?php 
//	include "footer.php";
?>

<script>
    $(document).ready(function () {
        $('.eachFormTextbox, .eachFormTextarea').on('focus blur', function (e) {
            $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
        }).trigger('blur');
    });
</script>
