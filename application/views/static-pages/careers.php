<div class="mobileHeader transparent">
    <div class="mobileMenu white">Careers</div>
</div>

<div class="container aboutUsBanner banner">
    <div class="wrapper">
        <div class="aboutUsBannerBlock">
            <img src="<?php echo STYLEURL; ?>front/images/aboutBanner.jpg" alt="">
            <h1 class="aboutUsContent bannerTitle"> The easiest way to list your business online and get noticed </h1>
        </div>
    </div>
</div>

<div class="container mainContent aboutUsContentBlock">
    <div class="wrapper">
        <div class="pageHeading ypHeading">
            Yellowpages
        </div>
        <div class="leftNav">
            <div class="leftNavBlock">
                <?php
                $ca = "active";
                include 'inc/static-pages-menu.php';
                ?>
            </div>
        </div>
        <div class="rightContent staticFullWidthBlock">
            <div class="faqsBlock staticFullWidth">
                
                
          <!--    <div class="careersFilters">
                    <a href="#" class="eachCareerFilter active">All</a>
                    <a href="#" class="eachCareerFilter">Sales</a>
                    <a href="#" class="eachCareerFilter">Programming</a>
                    <a href="#" class="eachCareerFilter">Manager</a>
                </div>
                <ul class="faqsList">
                    <li>
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                                Why do people join us, stay with us and thrive? 
                            </div>
                            <div class="faqAnswer">
                                <p>We make it a point to ask our employees what matters most to them. Here is an opportunity to work with passion, enjoy great salary and benefits and just love the people they work with to learn and grow. Submit your resume to Career@Yellowpages.in .</p>
                               <div class="applyFormButton">
                                    <button class="applyNowButton">Apply Now</button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>-->
             
                
                <div class="faqsList">
                    
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                               
                    Looking for a rockstar Visual Designer
                            
                            </div>
                            <div class="faqAnswer">
                                
                                <p class="jobDesc">
                                    Yellowpages is looking for a Visual Designer to work on our exciting products. A Visual Designer would work closely with our rockstar design team and project managers. As a Visual Designer, your day-to-day tasks will consist of creating visually engaging, innovative, and functional mobile and web products. Typical work would include color, iconography, typography, space and texture together, and incorporating new and emerging designs trends in software industry. Final outcome is always expected as beautiful, eye-catchy, and aesthetically pleasing which can delight, engage, and excite end user. One should be well versed with graphic design principles and laws.
                                </p>
                                
                                <h2 class="qualHead">Preferred qualifications</h2>
                                <ul class="listTC">
                                
                                    <li>
                                        Bachelor’s degree, or equivalent experience in the graphic design field.
                                    </li>
                                    <li>Proficient with industry standard design tools (e.g. Adobe Photoshop, Illustrator, InDesign, and other relevant design tools).</li>
                                    <li>Experience designing outstanding mobile and web based products for a consumer-oriented products.</li>
                                    <li>At least two years of professional experience in designing brand identities, grid systems, and applications for web and mobile with proven ability of delivering high quality designs to customers.</li>
                                    <li>Excellent problem-solving skills and familiarity with technical constraints and limitations as they apply to designing for platforms such as desktop and mobile, Android and iOS.</li>
                                    <li>Should be able to communicate ideas properly.</li>
                                    <li>Excellent interpersonal skills and the ability to build good working relationships.</li>
                                    <li>The ability to refine other people’s ideas and come up with new ones to create a highly designed visual experience.</li>
                                    <li>Must be self-motivated to prioritise and manage workload and meet critical project deadlines.</li>
                                    <li>Well organized, responsible, and dedicated, with the ability to work on multiple projects and deliver refined design in a short time.</li>
                                    <li class="lastItemGap" >Self-learning, able to learn new tools quickly and work in a fast evolving environment.</li>
                                    
                                
                               </ul>                              
                                
                                <h2 class="qualHead">Responsibilities</h2>
                                
                                <ul class="listTC" >
                                
                                    <li>
                                        Design our business and consumer facing projects/products.
                                    </li>
                                    <li>
                                        Present the user interface visually so information is easy to read, easy to understand, and easy to find, and emotionally engaging</li>
                                    <li>
                                        Provide modern, trendy, and aesthetically pleasing interfaces</li>
                                    <li>
                                        Generate clear ideas, concepts and designs of creative assets for our projects/products</li>
                                    <li>
                                        Work collaboratively with other designers to ensure a consistent, integrated brand perception and user-experience</li>
                                    <li>
                                        Collaborate with Interaction Designers, and UX Strategists to create elegant, engaging solutions that reflect the results of user-based research and analytics
                                    </li>
                                    <li>
                                       Collaborate with project/product team that includes front-end developers, programmers, project/product managers, in order to create simple and easy-to-use software</li>
                                    <li>
                                        Staying updated with latest standards, changes, trends in visual design field</li>
                                    <li>
                                        Craft graphic elements, assets, and visuals that adapt and flow with mobile apps and responsive websites</li>
                                    <li>
                                        Show an intuitive ability to transform data into understanding- strong infographic design skills and narrative storytelling abilities</li>
                                    <li>
                                        Create marketing collateral, both online and in print, with special emphasis on innovative presentation experiences
                                    </li>
                                    <li class="lastItemGap">Produce brand standards and style guides for creating interactive, user-centric products</li>
                                    
                                
                               
                               <div id="topMargin">
                                <p class="applyJob">
Candidates are requested to share link to their online portfolios or attach pdf alongwith their resume. To apply for this job, you can send your resume to careers@yellowpages.in or click on 'Apply Now' button.
</p>
</div>
                                
                               <div class="applyFormButton">
                 <a class="mailto noUnderline" href="mailto:careers@Yellowpages.in"><button class="applyNowButton">Apply Now</button></a>
                                </div>
                                
                            </div>
                        </div>
                    
                </div>
                            
                   
                <div class="faqsList" >
                    
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
                               
                    Senior PHP developer required
                            
                            </div>
                            <div class="faqAnswer">
                      
                                <p class="jobDesc">
                                    As a Sr. PHP Developer, you need to be extremely enthusiastic and energetic while aggressively attacking problems as a self-starter. An ability to pivot quickly is a must.
                                </p>
                                
                                
                                <ul class="listTC" >
                                
                                    <li >
                                        Have hands-on programming experience in PHP web applications (eg. web products, portals)
                                    </li>
                                    <li>Contribute enhancements and bug fixes to existing services.</li>
                                    <li>Triage and/or respond to support requests and keep service documentation current</li>
                                    <li>Regular exposure to business stakeholders and executive management, as well as the authority and scope to apply your expertise to many interesting technical problems.</li>
                                    <li>The position requires constant communication with Managers, Architects, BAs, and subordinates.</li>
                                    <li>Experience in planning and delivering software platforms used across multiple products and organizational units.</li>
                                    <li>Deep expertise and hands on experience with Web Applications and programming languages: Core PHP, HTML, CSS, JavaScript, JQuery, AngularJS, and Bootstrap.</li>
                                    <li>Deep functional knowledge or hands on design experience with Web Services is needed to be successful in this position</li>
                                    <li class="lastItemGap">Strong grasp of security principles and how they apply to E-Commerce &  E-Directory applications.</li>
                                    
                                
                               </ul>
                                
                                <h2 class="qualHead">Requirements for the role:</h2>
                                <ul class="listTC">
                                
                                    <li>
                                        Bachelors or Masters in Engineering or Technology courses
                                    </li>
                                    <li>
                                        4+ years of experience in PHP<li>
                                        <li>
                                        3+ years of experience developing on Javascript frameworks like AngularJS, JQuery etc.</li>
                                    <li>
                                        Advanced PHP and MYSQL</li>
                                    <li>
                                        Proficient understanding of client-side scripting and JavaScript frameworks, including jQuery</li>
                                    <li>
                                        HTML/HTML5/CSS Web Programming Skills
                                    </li>
                                    <li>
                                       WordPress</li>
                                    <li>
                                        Object-Oriented Design</li>
                                    <li>
                                        Web Services (REST/SOAP) and previously worked on Worked on APIs</li>
                                    <li>
                                        Proficient understanding of cross-browser compatibility issues and ways to work around them</li>
                                    <li>
                                        Deep expertise and hands on experience with Web Applications and programming languages such as PHP, HTML, CSS, JavaScript, JQuery and APIs.  Also exp. in PHP frameworks like Codeigniter, Cake PHP , Laravel, Zend etc
                                    </li>
                                    <li>Deep functional knowledge or hands on design experience with Web Services (REST, SOAP, etc) is needed to be successful in this position.</li>
                                    <li></li>
                                <li>Web hosting management (cPanel, WHM, VPN, Cloud Hosting)</li>
                                <li>Willing to work on multiple projects simultaneously</li>
                                <li>Excellent communication skills</li>
                                <li>Logical, analytical & problem solving skill </li>
                                <li class="lastItemGap">Team player & quick responder </li>
                               </ul>
                               
                                <div id="topMargin">
                                <p class="applyJob">
Knowledge - HTML & HTML5 - CSS & CSS3 - Javascript & jQuery, Java, .Net - PHP, PERL, ROR (Ruby On Rail - optional) - CMS (Drupal, WordPress) - Ecommerce (Magento, Drupal Commerce, Woo commerce) . To apply for this job, you can send your resume to careers@yellowpages.in or click on 'Apply Now' button.</h5>
                                </p>
                                </div>
                                
                                <div class="applyFormButton" >
                                    <a class="mailto noUnderline" href="mailto:careers@Yellowpages.in" ><button class="applyNowButton">Apply Now</button></a>
                                </div>
                                
                            </div>
                        </div>
                    
                    
                    
                </div>
               
		<div class="faqsList" >
                        <div class="eachFaqItem">
                            <div class="faqQuestion">
				HR Generalist required
                            </div>
                            <div class="faqAnswer">
                                <p class="jobDesc">
                                  <a href="http://yellowpages.in/" target="_blank">Yellowpages</a> is looking for hr generalist with following responsibilities and skills.  
				</p>
                                
                                <h2 class="qualHead">Responsibilities:</h2>
                                <ul class="listTC" >
                                    <li>5+ years Generalist experience in Information Technology HR operations would be preferred.</li>
                                    <li>Partners with business heads to facilitate high productivity achievement of all teams.</li>
                                    <li>Recruitment planning for IT (software development) and operations roles.</li>
                                    <li>Creates job descriptions for recruitment & performs recruitment activities such as resume search, interview scheduling, internal interview management, offer letters & onboarding.</li>
                                    <li>Creates & assists in rolling out workplace policies, processes and systems.</li>
                                    <li>Fulfils HR compliance requirements for a large IT organisation.</li>
                                    <li>Coordinates with third party service providers such as Chartered Accountants & Recruiters, when needed.</li>
                                    <li>Partners with business heads to identify KRAs & implement Performance Management System.</li>
                                    <li class="lastItemGap">Candidate must be a full time MBA/PGDBM in HR.</li>
                               </ul>
                                
                                <h2 class="qualHead">Skills:</h2>
                                <ul class="listTC">
                                    <li>Grievance Handling.</li>
                                    <li>Sound knowledge of creating workplace policies & implementing them.</li>
                                    <li>Ability to identify & support development of talents and skills in employees so they can contribute to diverse projects.</li>
                                    <li>Proficient & up to date with HR compliance requirements for a large IT organisation.</li>
                                    <li>Excellent communication skills and a pleasant personality is a must.</li>
                                    <li>Competent is use of MS Excel & Google apps.</li>
                                    <li>Ability to multi-task and deliver HR solutions under deadlines.</li>
                               </ul>
                                
                                <div id="topMargin">
                                    <p class="applyJob">
                                        To apply for this job, you can send your resume to careers@yellowpages.in or click on 'Apply Now' button.
                                    </p>
                                </div>
                                
                                
                                <div class="applyFormButton" >
                                    <a class="mailto noUnderline" href="mailto:careers@Yellowpages.in" ><button class="applyNowButton">Apply Now</button></a>
                                </div>
                            </div>
                        </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last"> Careers </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.faqQuestion').click(function () {
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
                $(this).parent().find('.faqAnswer').removeClass('open');
            } else {
                $('.faqsList').find('.open').removeClass('open');
                $(this).addClass('open');
                $("html, body").stop().animate({scrollTop: $(this).offset().top}, '500', 'swing');
                $(this).parent().find('.faqAnswer').addClass('open');
            }
        });
    });
</script>
