<div class="mobileHeader">
    <div class="mobileMenu ">Add Business</div>
    <?php /*<div class="mobileHeadIcons">
        <button class="mobSearchBtn">Search</button>
        <button class="mobLocation">Location</button>
    </div>*/?>
</div>

<?php echo form_open_multipart('add-business/validate-business-details', array('id' => 'addbusiness')); ?>

<div class="container addBusiness">
    <div class="wrapper">
        <div class="addBusinessBlock">
            <div class="addBusinessTitleBlock">
                <h1 class="pageTitle">Add business</h1>

            </div>

            <div class="businessOnlineBlock">
                <div class="businessBlock">
                    <h2 class="businessOnlineHeading">Business info</h2>
                    <ul class="formList">
                        <li>
                            <div class="formElement">
                                <input type="text" id="bname" class="formElementTextbox" name="title"  value="" placeholder="">
                                <label for="bname" class="formElementLable">Business name*</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement mobile">
                                <input type="text" class="formElementTextbox" name="phone" id="phone" value="" placeholder="">
                                <span class="mobilePreNumber">+91</span>
                               <label for="phone" class="formElementLable">Mobile*</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="email" class="formElementTextbox" name="email" id="email" value="" placeholder="">
                                <label for="email" class="formElementLable">Email*</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" id="city" class="formElementTextbox" name="city" placeholder="">
                                <label for="city" class="formElementLable">City*</label>
                            </div>
                        </li>
                <?php /*        <li>
                            <div class="formElement">
                                <input type="text" id="area" class="formElementTextbox" name="area" placeholder="">
                                <label for="area" class="formElementLable">Area</label>
                            </div>
                        </li> */ ?>
                        <li>
                            <div class="formElement">
                                <textarea name="address" id="address" cols="30" rows="8" class="formElementTextarea" placeholder=""></textarea>
                                <label for="address" class="formElementLable">Address*</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="landmark" id="landmark" placeholder="">
                                <label for="landmark" class="formElementLable">Landmark</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="pincode" id="pincode" placeholder="">
                                <label for="pincode" class="formElementLable">Pincode</label>
                            </div>
                        </li>
                        <li><?php //echo "<pre>";  print_r($category);exit; ?>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="category" id="category" placeholder="" onkeyup="get_categories()">
                                <label for="category" class="formElementLable">Category*</label>
                                <ul class="categoriesList showList" id="category_list"></ul>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <textarea cols="30" rows="3" class="formElementTextarea" name="expectation" id="expectation" placeholder=""></textarea>
                               <label for="expectation" class="formElementLable">What to expect from this business?</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <label for="own" class="checkbox">
                                    <input type="checkbox" class="checkboxInput" id="own" name="own" value="1" checked="true">
                                    <span class="checkboxText">I own this business</span>
                                </label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <label for="tc" class="checkbox">
                                    <input type="checkbox" name="i_agree" class="checkboxInput" id="tc" checked="true" disabled="">
                                    <span class="checkboxText">I agree to <span class="tcLinks"><a href="<?php echo base_url(); ?>terms-and-conditions">Terms &amp; Conditions</a></span></span>
                                </label>
                            </div>
                        </li>

                        <li>
                            <div class="formElement">
                                <input type="submit" class="addBusinessBtn" value="Add business">
                            </div>
                        </li>
                <?php /*         <li>
                            <div class="formElement imageUpload">
                                <input id="upload" type="file" class="formElementImage fileUpload" name="upload" accept=".jpg, .jpeg, .png, .gif">
                                <label for="upload" class="uploadLabel fileUploadLabel">Upload Images</label>
                            </div>
                        </li> */ ?>
                    </ul>
                </div>
            <?php /*    <div class="onlineBlock">
                    <h2 class="businessOnlineHeading">Online presence</h2>
                    <ul class="formList">
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="website_url" id="website_url" placeholder="">
                               <label for="website_url" class="formElementLable">Website URL</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="facebook_url" id="facebook_url" placeholder="">
                              <label for="facebook_url" class="formElementLable">Facebook URL</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="twitter_url" id="twitter_url" placeholder="">
                               <label for="twitter_url" class="formElementLable">Twitter URL</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="google_url" id="google_url" placeholder="">
                               <label for="google_url" class="formElementLable">Google+ URL</label>
                            </div>
                        </li>
                        <li>
                            <div class="formElement">
                                <input type="text" class="formElementTextbox" name="youtube_url" id="youtube_url" placeholder="">
                                <label for="youtube_url" class="formElementLable">Youtube URL</label>
                            </div>
                        </li>
                    </ul>
                </div> */?>
                </div>
<?php /*
            <div class="businessOnlineBlock addressBlock">
                <div class="businessBlock">
                    <ul class="formList">
                        
                    </ul>
                </div>
                <div class="onlineBlock">
                    <div class="googleMapBlock">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d487295.0253204453!2d78.12785124924685!3d17.412153075682923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb99daeaebd2c7%3A0xae93b78392bafbc2!2sHyderabad%2C+Telangana!5e0!3m2!1sen!2sin!4v1467377766691" width="600" height="276" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
                    </div>
                </div>
            </div> */?>
            <!--
                        <div class="businessOnlineBlock businessHours">
                            <div class="businessBlock">
                                <h2 class="businessOnlineHeading">Business hours</h2>
                                <ul class="formList">
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Monday</span>
                                            <span class="closedTiming">
                                                <label for="mon" class="checkbox">
                                                    <input id="mon" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tuesday</span>
                                            <span class="closedTiming">
                                                <label for="tue" class="checkbox">
                                                    <input id="tue" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                            
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Wedesday</span>
                                            <span class="closedTiming">
                                                <label for="wed" class="checkbox">
                                                    <input id="wed" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Thursday</span>
                                            <span class="closedTiming">
                                                <label for="thu" class="checkbox">
                                                    <input id="thu" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Friday</span>
                                            <span class="closedTiming">
                                                <label for="fri" class="checkbox">
                                                    <input id="fri" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Saturday</span>
                                            <span class="closedTiming">
                                                <label for="sat" class="checkbox">
                                                    <input id="sat" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Sunday</span>
                                            <span class="closedTiming">
                                                <label for="sun" class="checkbox">
                                                    <input id="sun" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElementButtons">
                                            <button class="copyAllBtn otherBtns">Copy for all days</button>
                                            <button class="addEveningTimings otherBtns">Add evening timings</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
            
                            <div class="onlineBlock ">closedTimings
                                <h2 class="businessOnlineHeading">s </h2>
                                <ul class="formList">
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Mon</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="text formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="mon2" class="checkbox">
                                                    <input id="mon2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
            
                                    <li>
                                        <div class="formElement">
                                            <span class="dayName">Tue</span>
                                            <span class="startTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="endTime timeTextbox">
                                                <input type="text" class="formElementTextbox" value="">
                                            </span>
                                            <span class="closedTiming">
                                                <label for="tue2" class="checkbox">
                                                    <input id="tue2" type="checkbox" class="checkboxInput">
                                                    <span class="checkboxText">Closed</span>
                                                </label>
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>-->

     <?php /*       <div class="businessOnlineBlock">
                <div class="businessBlock">
                    <ul class="formList">
                                
                    </ul>
                </div>
            </div> */?>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>

<div class="container bredcrumBlock">
    <div class="wrapper">
        <div class="bredcrumNav">
            <p id="breadcrumbs">You are here:
                <span xmlns:v="http://rdf.data-vocabulary.org/#"> 
                    <span typeof="v:Breadcrumb"> 
                        <a href="<?php echo base_url(); ?>" rel="v:url" property="v:title">Home&nbsp;&nbsp;/</a> 
                        <span rel="v:child" typeof="v:Breadcrumb"> 
                            <span class="breadcrumb_last"> Add Business </span> 
                        </span>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
$('.formElementTextbox, .formElementTextarea').on('focus blur', function (e) {
   $(this).parent('.formElement').toggleClass('formElementLableFocused', (e.type === 'focus' || this.value.length > 0));
}).trigger('blur');

/*$('#select_file').click(function() {
   $('#image_file').show();
   $('.btn').prop('disabled', false);
   $('#image_file').change(function() {
       var filename = $('#image_file').val();
       $('#select_file').html(filename);
   });
});​*/

//File Upload Name
/*$('.fileUpload').click(function() {
               $('.fileUpload').change(function() {
                   var filename = $('.fileUpload').val().split('/').pop().split('\\').pop();
                   $('.fileUploadLabel').text('Selected File Name : '+filename);
               });
           });*/
});
</script>
<script>
    function get_categories(){
        var cat_name=document.getElementById('category').value;
        var uri = "<?php echo base_url(); ?>ajax/get_allcategories?q="+cat_name;
            $.ajax({
                async: true,
                global: false,
                type: 'POST',
                url: uri,
                success: function (res) {
                    $('#category_list').html(res);
                }
            });
    }
</script>




