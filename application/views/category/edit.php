<section>
	<div class="section-body col-sm-6">
		<div class="card card-underline">
			<div class="card-head">
				<header>Edit Category</header>
			</div>
			<form class="form" style="margin:0 auto"  enctype="multipart/form-data" action='<?php echo base_url(); ?>category/edit/<?php echo $id; ?>' method="post" >
				<div class="card-body">
					<div class="row">
						<div class="form-group col-sm-12">
							<select  name="parent_id" id="cparent_id" class="form-control selectpicker" data-live-search="true" required >
								<option value="" data-hidden="true">Select Parent Category</option>
								<option <?php if($results->parent_id == 0){ echo 'selected'; } ?>  value='0'>None</option>
								<?php foreach($categories as $key => $sl ){ ?>
									<option <?php if($results->parent_id == $key){ echo 'selected'; } ?> value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group col-sm-12  floating-label">
							<input  type="text"  class="form-control" name="name" value="<?php echo $results->name; ?>" required >
							<label class='col-sm-12' for="cname">Category Name</label>
						</div>
						<div class="form-group col-sm-12">
							
							<input type="file" class="form-control" name="excelfile[]" accept=".csv" multiple />
							<p for="excelfile" class="help-block col-sm-12"><a href="/uploads/attribute_sample.csv"class='pull-right' style="color:black;font-weight:600;" download>  Click Here For Attribute Sample</a></p>
						</div>
						<div class="form-group col-sm-12">
							
							<input data-role="tagsinput" type="text" value="<?php echo $results->tags; ?>" style="display: none;" id="tags" name="tags" required>
							<p for="help-block">Enter tags/keywords/synonyms (comma separated, search and select)</p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit"  class="btn btn-default-dark">Edit Category</button>
						<a  class="btn btn-default" href="<?php echo base_url('category'); ?>">Cancel</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>	