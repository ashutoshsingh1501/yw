<section>
	<div class="section-body">
		<div class="card">
			<form class="form" id="category_index" action="<?php echo base_url(); ?>category" method="get">
						<div class="card-body">
					<div class="row" style="margin-bottom:30px;vertical-align:middle">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn ink-reaction btn-default-dark"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
													<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select class="form-control selectpicker"  name="parent" onchange="find_category();" >
								<option value='0' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 0){ echo 'selected'; } }?>>All</option>
								<option value='1' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 1){ echo 'selected'; } }?>>Level 1</option>
								<option value='2' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 2){ echo 'selected'; } }?>>Level 2</option>
								<option value='3' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 3){ echo 'selected'; } }?>>Level 3</option>
								<option value='4' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 4){ echo 'selected'; } }?>>Level 1 and 2</option>
								<option value='5' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 5){ echo 'selected'; } }?>>Level 2 and 3</option>
								<option value='6' <?php if (isset($_GET['parent'])) {   if($_GET['parent'] == 6){ echo 'selected'; } }?>>Level 3 and 1</option>
							</select>
						</div>
											<div class="col-sm-1 pull-right" >
							<a class="btn ink-reaction btn-default-dark" href="<?php echo base_url(); ?>category/create" >
								<i class="fa fa-plus text-default-light"></i> Add
							</a>
											</div>
						<div class="col-sm-2 pull-right"><h4 class="pull-right rowcount"> <?php echo count($results); ?> Categories </h4></div>
					</div>
					<?php echo $pagermessage; ?>
				</div>
				<div class="table-responsive">
					<table class="table table-index table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th>ID</th>
								<th>Category Name <br><input type="text" class="form-control" value="<?php if (isset($_GET['category_name'])) {   echo $_GET['category_name']; }?>" onchange="find_category();" name="category_name"  placeholder="Search Category"></th>
								<th>Parent Category <br><input type="text" value="<?php if (isset($_GET['parent_name'])) {   echo $_GET['parent_name']; }?>" onchange="find_category();" class="form-control" name="parent_name" placeholder="Search Parent">
								</th>
								<th>Sub Categories</th>
								<th>Hierarchy Level</th>
								<th>Display Name</th>
								<th style="text-align:right;">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $a){ ?>
								<tr>
									<td>
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="1" name="addcheck[]" class="addcheck"><span>&nbsp;</span></label>
										</div>
									</td>
									<td><?php echo $a->id; ?></td>
									<td><?php echo $a->name; ?></td>
									<td><?php if(isset($categories[$a->parent_id])){ echo $categories[$a->parent_id]; } else{ echo '-'; } ?></td>
									<td><?php echo $a->total; ?> Categories</td>
									<td><?php echo $a->level + 1; ?></td>
									<td><?php echo $a->display_name; ?></td>
									<td style="text-align:right;">
										<a href="<?php echo base_url(); ?>category/edit/<?php echo $a->id; ?>" class="btn btn-icon-toggle"><i class="fa fa-pencil fa-fw"></i></a>
										<a href="#" class="btn btn-icon-toggle category-delete" data-id="<?php echo $a->id; ?>"  ><i class="fa fa-trash-o fa-fw"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</form>
						<?php echo $links; ?>
		</div>
	</div>
</section>