
<div class="mobileHeader">
    <div class="mobileMenu">Categories</div>
</div>

<div class="container categories topMargin">
    <div class="wrapper">
        <div class="categoriesAlphabets">
            <div class="categoriesTitle">Categories</div>
            <div class="catAlphabetsBlock">
                <ul class="catAlphabetsLink ">
                    <li <?php echo ($active=="all")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories">All</a>
                    </li>
                    <li <?php echo ($active=="a")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=a">A</a>
                    </li>
                    <li <?php echo ($active=="b")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=b">B</a>
                    </li>
                    <li <?php echo ($active=="c")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=c">C</a>
                    </li>
                    <li <?php echo ($active=="d")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=d">D</a>
                    </li>
                    <li <?php echo ($active=="e")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=e">E</a>
                    </li>
                    <li <?php echo ($active=="f")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=f">F</a>
                    </li>
                    <li <?php echo ($active=="g")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=g">G</a>
                    </li>
                    <li <?php echo ($active=="h")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=h">H</a>
                    </li>
                    <li <?php echo ($active=="i")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=i">I</a>
                    </li>
                    <li <?php echo ($active=="j")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=j">J</a>
                    </li>
                    <li <?php echo ($active=="k")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=k">K</a>
                    </li>
                    <li <?php echo ($active=="l")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=l">L</a>
                    </li>
                    <li <?php echo ($active=="m")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=m">M</a>
                    </li>
                    <li <?php echo ($active=="n")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=n">N</a>
                    </li>
                    <li <?php echo ($active=="o")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=0">O</a>
                    </li>
                    <li <?php echo ($active=="p")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=p">P</a>
                    </li>
                    <li <?php echo ($active=="q")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=q">Q</a>
                    </li>
                    <li <?php echo ($active=="r")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=r">R</a>
                    </li>
                    <li <?php echo ($active=="s")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=s">S</a>
                    </li>
                    <li <?php echo ($active=="t")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=t">T</a>
                    </li>
                    <li <?php echo ($active=="u")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=u">U</a>
                    </li>
                    <li <?php echo ($active=="v")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=v">V</a>
                    </li>
                    <li <?php echo ($active=="w")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=w">W</a>
                    </li>
                    <li <?php echo ($active=="x")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=x">X</a>
                    </li>
                    <li <?php echo ($active=="y")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=y">Y</a>
                    </li>
                    <li <?php echo ($active=="z")?"class='active'":""; ?>>
                        <a href="<?php echo base_url(); ?>categories?s=z">Z</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="categoriesListBlock">
            <ul class="categoriesList">
                <?php if (!empty($categories)) {
                    foreach ($categories as $key => $lb) {
                        ?>
                        <li>
                            <a href="<?php echo $lb['link']; ?>"><?php echo $lb['name']; ?></a>
                        </li>
                    <?php }
                }
                ?>
            </ul>
        </div>
    </div>
</div>






<?php
//include "footer.php";
?>

