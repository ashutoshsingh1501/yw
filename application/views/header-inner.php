<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Yellow Pages Home Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
	<?php /*

	<link rel="apple-touch-icon" sizes="57x57" href="images/app/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/app/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/app/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/app/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/app/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/app/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/app/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/app/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/app/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/app/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/app/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/app/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/app/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#facb34">
	<meta name="msapplication-TileImage" content="images/app/ms-icon-144x144.png">
	<meta name="theme-color" content="#facb34">

	*/ ?>

	<link rel="stylesheet" href="<?php if ($css != '') { echo $css; } ?>">
</head>

<body>
<div class="main" id="main"> <?php /* First div of the document  */ ?>

	

	<div class="innerHeader container desktopHeader">
		<div class="wrapper">
			<a href="index.php" class="branding">
				Yellow Pages
			</a>

			<div class="innerSearchBlock">
				<div class="innerSearchTextBlock">
					<input type="text" class="innerSearchTextbox" value="" placeholder="Search for business in India">
				</div>
				<div class="innerLocationSearchBtn">
					<div class="innerSearchLocation">Hyderabad</div>
					<button class="innerSearchBtn">Search</button>
				</div>
			</div>

			<ul class="innerTopNavList">
				<li><a href="add-business.php">Add business</a></li>
				<li><a href="categories.php">Categories</a></li>
				<li><a href="my-favorites.php">My Account</a></li>
			</ul>
		</div>
	</div>
	
