<div class="home-banner container">
    
    <div class="wrapper">
        <div class="searchBlock">
            <h1 class="searchTitle">#1 Indian website for listing local business</h1>
            <div class="searchInputBlock">
                <form class="homeSearchBlock" id="qt-search" autocomplete="off" >
                    <div class="searchFullBlock">
                                <div class="searchBusiness">
                                    <input type="text" value="" id="search" class="homeSearchTextbox businessInputField" placeholder="Search local business in India" autocomplete="off">
                                </div>
                                <div class="searchLocation">
                                        <input type="text" class="homeSearchTextbox locationInputField" value="Hyderabad" id="location">
                                </div>
                                <div class="searchButtonBlock">
                                    <div class="searchButton">Search <i class="searchIcon"></i></div>
                                </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="homeCategoriesBlock">
        <div class="wrapper">
            <ul class="homeCategoriesList">
                <?php foreach ($categories as $key => $c) { ?>
                    <li><a href="<?php echo $c['link']; ?>" class="eachHomeCategory">
                            <div class="eachCategoryIcon" style="background-image:url('<?php echo STYLEURL.'front/images/category/'.$c['image_path']; ?>');"></div>
                            <div class="eachCategoryname"><?php echo $c['name']; ?></div>
                        </a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

</div>

<div class="homeFBBlock container">
    <div class="wrapper">
        <h2 class="secondaryHeading">Featured businesses</h2>
        <div class="homeFBListBlock">
            <ul class="homeFBList">
                <?php foreach ($featured as $key => $fb) { ?>
                    <li class="<?php if ($fb['is_verified']) {
                        echo 'ypApprovedItem';
                    } ?>" >
                        <div class="eachFB">
                            <div class="eachFBImage">
                                <?php if($fb['is_logo']==0){ ?>
                                    <img width="190" height="125" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>"  alt="<?php echo $fb['title'];?>">
                                <?php } else { ?>
                                    <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$fb['image_note'].'/190x125_'.$fb['image_note'].'-1.jpg';?>" alt="<?php echo $fb['title']; ?>">
                                <?php } ?>
                                <?php if ($fb['is_verified']) { ?>
                                    <span class="ypApprovedBlock">YP</span>
                                <?php } ?>
                            </div>
                            <a href="<?php echo $fb['link']; ?>" class="eachFBLink" title="<?php echo $fb['title']; ?>"><?php echo $fb['title']; ?></a>
                            <div class="eachFBLocation"><?php echo ucfirst($fb['area']); ?></div>		
                            <?php if($fb['roverall']!=0.0 && $fb['treview'] !=0){?>
                            <div class="eachFBRatingBlock">
                                <span class="rating r<?php echo str_replace(".", "-", $fb['roverall']); ?>"><?php echo $fb['roverall']; ?></span>
                                <a href="#" class="ratingCountLink">
                                    <?php  if ($fb['treview'] > 1) {
                                                echo $fb['treview'] . " reviews";
                                            } else {
                                                echo $fb['treview'] . " review";
                                            } ?>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="viewAllFB"><a href="/s?featured=yes" class="viewAllFBLink" data-mobileText = "View all &raquo;">View more featured businesses</a></div> 
    </div>
</div>


<div class="container homePopular">
    <div class="wrapper">
        <h3 class="secondaryHeading">Popular this week</h3>
        <div id="txtHint"></div>
        <div class="popularLeftBlock">
            <div class="popularThisWeekBlock">
                <ul class="popularThisWeekList">
                    <?php foreach ($recently as $key => $lb) { ?>
                        <li>
                            <div class="eachPopular">
                                <div class="eachPopularLeft">
                                    <div class="eachPopularImage">
                                        <?php if($lb['is_logo']==0){ ?>
                                        <img width="190" height="125" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $lb['title']; ?>">
                                        <?php } else { ?>
                                        <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$lb['image_note'].'/190x125_'.$lb['image_note'].'-1.jpg';?>" alt="<?php echo $lb['title']; ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="eachPopularTitleBlock">
                                        <div class="popularTitleTextBlock">
                                            <?php if(($lb['website_url'] != "") || (isset($lb['is_verified']) && $lb['is_verified']==1)){  ?>
                                                <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle hasOtherInfo" title="<?php echo $lb['title']; ?>">
                                                    <?php echo $lb['title']; ?> 
                                                </a>
                                            <?php }else{?>
                                            <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle" title="<?php echo $lb['title']; ?>">
                                                <?php echo $lb['title']; ?> 
                                            </a>
                                            <?php } ?>
                                            <div class="eachPopularOtherActions">
                                                <?php if(isset($lb['website_url']) && $lb['website_url'] != ''){?>
                                                    <a target="_blank" href="<?php echo $lb['website_url']; ?>"><span class="openNewWindow"></span></a>
                                                <?php }?>
                                                <?php if(isset($lb['is_verified']) && $lb['is_verified']==1){?>
                                                    <span class="ypApproved">.</span>
                                                <?php }?>
                                                <span class="certified aaP"><?php echo $lb['rating']; ?></span>
                                            </div>
                                        </div>
                                        <div class="eachPopularRateOpen">
                                            <div class="eachPopularRatingBlock">
                                            <?php if($lb['overall'] != 0.0 && $lb['review'] !=0) {?>
                                                <span class="rating r<?php echo str_replace(".", "-", $lb['overall']); ?>"><?php echo $lb['overall']; ?></span>
                                                <a href="#" class="ratingCount"><?php if ($lb['review']) {
                                                    if ($lb['review'] > 1) {
                                                        echo $lb['review'] . " reviews";
                                                    } else {
                                                        echo $lb['review'] . " review";
                                                    }
                                                } ?></a>                                            
                                            <?php }?>
                                            </div>
                                            <div class="openNow"><?php if ($lb['datetime']) { ?><strong>Open now</strong> - <?php echo $lb['datetime']; ?><?php } else { ?><strong> </strong><?php } ?></div>
                                        </div>
                                        <ul class="eachPopularTagsList">
                                            <?php if ($lb['categories']) {
                                                echo $lb['categories'];
                                            } ?>
                                        </ul>
                                        <div class="eachPopularLink">
                                            <?php if(isset( $lb['email']) &&  $lb['email'] != ''){?>
                                                <a href="mailto:<?php echo $lb['email']; ?>">Email</a>
                                            <?php }?>
                                            <?php if(isset($lb['website_url']) && $lb['website_url'] != ''){?>
                                                <a target="_blank" href="<?php echo $lb['website_url']; ?>" rel="nofollow">Website</a>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                                <div class="eachPopularRight">
                                    <div class="<?php echo $lb['favclass']; ?>" onclick="showUser(<?php echo $lb['business_id']; ?>)">Add Favorite</div>
                                    <?php if(isset($lb['phone']) && $lb['phone']!=''){?>
                                        <a class="businessContact" href="tel:<?php echo $lb['phone']; ?>">+91 <?php echo $lb['phone']; ?></a>
                                    <?php } elseif(isset($lb['alternative_number']) && $lb['alternative_number']!=''){?>
                                        <a class="businessContact" href="tel:<?php echo $lb['alternative_number']; ?>">+91 <?php echo $lb['alternative_number']; ?></a>
                                    <?php } elseif(isset($lb['landline']) && $lb['landline']!=''){?>
                                        <a class="businessContact" href="tel:<?php echo $lb['landline']; ?>"><?php echo $lb['landline']; ?></a>
                                    <?php } elseif(isset($lb['secondary_land_line']) && $lb['secondary_land_line']!=''){?>?>
                                        <a class="businessContact" href="tel:<?php echo $lb['secondary_land_line']; ?>"><?php echo $lb['secondary_land_line']; ?></a>
                                    <?php }?>
                                    <address class="businessArea">
                                         <?php if($lb['area']){ ?>
                                        <strong> <?php echo trim($lb['area']); } else { echo ""; } ?> </strong>
                                            <?php if ($lb['pincode']) {
                                                echo $lb['city'] . " - " . $lb['pincode'];
                                            } else {
                                                echo $lb['city'];
                                            } ?>
                                    </address>
                                    <div class="directionsLocationsBlock">
                                        <span class="locationsCount"></span>
                                    <?php
                                    if (!empty($lb['latitude']) && !empty($lb['longitude'])) {
                                        echo "<a href='https://maps.google.com/maps?q=" . $lb['latitude'] . "," . $lb['longitude'] . "&output=embed' target='_blank'>Directions</a>";
                                    } else {
                                        echo '<a  href="https://maps.google.com/maps?q=' . $lb['area'] . '+ON&loc:43.25911+-79.879494&z=15&output=embed" target="_blank">Directions</a>';
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                
                <ul class="otherPopular">
                    <?php foreach ($popular as $key => $fb) { ?>
                        <li class="<?php if ($fb['is_verified']) {
                        echo 'ypApprovedItem';
                    } ?>" >
                            <div class="eachOP">
                                <div class="eachOPImage">
                                    <?php if($fb['is_logo']==0){ ?>
                                    <img width="190" height="125" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $fb['title']; ?>">
                                    <?php } else { ?>
                                    <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$fb['image_note'].'/190x125_'.$fb['image_note'].'-1.jpg';?>" alt="<?php echo $fb['title']; ?>">
                                    <?php } ?>
                                     <?php if ($fb['is_verified']) { ?>
                                    <span class="ypApprovedBlock">YP</span>
                                <?php } ?>
                                </div>
                                <div class="eachOPTitleBlock">
                                    <a href="<?php echo $fb['link']; ?>" class="eachOPTitle" title="<?php echo $fb['title']; ?>"><?php echo $fb['title']; ?></a>
                                    <div class="opRating">
                                        <?php if($fb['overall'] != 0.0 && $fb['review'] != 0 ){?>
                                            <span class="rating r<?php echo str_replace(".", "-", $fb['overall']); ?>"><?php echo $fb['overall']; ?></span>
                                            <a href="#" class="ratingCount">
                                                <?php if ($fb['review'] > 1) {
                                                    echo $fb['review'] . " reviews";
                                                } else {
                                                    echo $fb['review'] . " review";
                                                }?></a>
                                        <?php }?>
                                    </div>
                                    <?php if(isset($fb['phone']) && $fb['phone']!=''){?>
                                        <a href="tel:<?php echo "+91-" . $fb['phone']; ?>" class="opContact">+91 <?php echo $fb['phone']; ?></a>
                                    <?php }elseif(isset($fb['alternative_number']) && $fb['alternative_number']!=''){?>
                                        <a href="tel:<?php echo "+91-" . $fb['alternative_number']; ?>" class="opContact">+91 <?php echo $fb['alternative_number']; ?></a>
                                    <?php }elseif(isset($fb['landline']) && $fb['landline']!=''){?>
                                        <a href="tel:<?php echo $fb['landline']; ?>" class="opContact"><?php echo $fb['landline']; ?></a>
                                    <?php }elseif(isset($fb['secondary_land_line']) && $fb['secondary_land_line']!=''){?>
                                        <a href="tel:<?php echo $fb['secondary_land_line']; ?>" class="opContact"><?php echo $fb['secondary_land_line']; ?></a>
                                    <?php }?>
                                    <?php if ($fb['datetime'] !== "") { ?>
                                        <div class="opTimings"><span class="day"><?php echo date('D'); ?></span><?php echo $fb['datetime']; ?></div>
                                    <?php } else { ?>
                                        <div class="opTimings"><span class="day"><?php echo date('D') . " "; ?></span>Not Available</div>
                                    <?php } ?>

                                    <address class="opAddress">
                                        <strong><?php $fb['area']; ?></strong>
                                        <?php if ($fb['pincode']) {
                                            echo $fb['city'] . " - " . $fb['pincode'];
                                        } else {
                                            echo $fb['city'];
                                        } ?>
                                    </address>
                                    <div class="opFavLike">
                                        <div class="opFav"><div class="<?php echo $fb['popular_favclass']; ?>" onclick="showUser(<?php echo $fb['business_id']; ?>)">Add to Favorite</div></div>
                                    </div>
                                    <div class="opViewDetails"><a href="<?php echo $fb['link']; ?>" class="opViewDetailsLink">View more details</a></div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        
        <div class="popularRightBlock">
            <div class="recentViewsBlock">
                <h3 class="thirdHeading">Recent Reviews</h3>
                <ul class="recentReviewsList">
                    <?php foreach ($reviews as $key => $review) { ?>
                        <li>
                            <div class="eachRR">
                                <div class="eachRRPhotoRat">
                                    <div class="eachRRPhoto"><img width="100" height="100" src="<?php echo STYLEURL . "front/images/" . $review['image']; ?>" alt=""></div>
                                    <div class="titleRating">
                                        <a href="#" class="reviewerName"><?php echo $review['name']; ?></a>
                                        <div class="reviewRating">
                                            <?php if($review['rating'] != 0.0){?>
                                            <span class="rating r<?php echo str_replace(".", "-", $review['rating']); ?>"><?php echo $review['rating']; ?></span>
                                            <?php }?>
                                            <span class="ratingTime"><?php echo $review['datetime'] . ' ago'; ?></span>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo $review['link']; ?>" class="eachRRTitle"><?php echo $review['business']; ?></a>
                                <p class="eachRRTitleInfo"><?php echo substr($review['comments'], 0, 35); ?></p>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="haveBusiness">
                <div class="haveBusinessAd">
                    Have a business to list with us?<br>
                    <a href="/add-business" class="addBusinessBtn">Add your business</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mobileAppsDownload">
    <div class="wrapper">
        <div class="appsDownloadPhoneImage"></div>
        <div class="appsDownloadContent">
            <h4 class="appsDownloadHeading"><strong>Yellowpages</strong> on the go</h4>
            <p>Download our mobile application to search for any business on the go</p>
            <div class="appsDownloadBtns">
                <a href="#" class="iosBtn">Available on iOS Apps Atore</a>
                <a href="#" class="androidBtn">Available on Android Google Play</a>
            </div>
        </div>
    </div>
</div>
