<div class="mobileHeader">
    <a href="#" class="mobileBackBtn">Back</a>
    <div class="mobileHeadTitle">100 results or caregory name runs here</div>
    <div class="mobileHeadIcons">
        <button class="mobSearchBtn">Search</button>
        <button class="mobListView">List View</button>
    </div>
</div>

<div class="container resultsMapBg greyBg">
    <div class="wrapper">
        <div class="resultsBlock">
            <div class="sortBlock">
                <div class="sortSelectBox select">
                    <span class="mobileSortText">Sort Results</span>
                    <span class="selectText">Sort results</span>
                    <select name="" id="" class="selectBox">
                        <option value="1">Price Low to High</option>
                        <option value="2">Price High to Low</option>
                        <option value="3">Most Relavant</option>
                        <option value="4">Most Popular</option>
                        <option value="5">New Release</option>
                    </select>
                </div>
            </div>
            <div class="mobFilterOptionBlock">
                <button class="mobFilterBtn popup">Filters</button>
            </div>
            <div class="resultsText"><?php echo $msg; ?></div>
            <div class="desktopMapView"><a href="<?php echo base_url(); ?>search_result?<?php echo $_SERVER['QUERY_STRING'];?>" class="mapViewBtn">List View</a></div>
        </div>
    </div>
</div>

<!--  header height 148, each popular block height = 85,  -->

<div class="container mapListBlock greyBg">
    <div class="wrapper">

        <div class="mapBlock">
            <div class="map" id="mapid">

            </div>
        </div>

        <div class="businessListBlock">
            <div class="businessList">
                <?php for ($i = 0; $i < count($businesses); $i++) { 
                            if(!empty($businesses[$i]['latitude'])&& !empty($businesses[$i]['longitude'])){
                ?>
                <div class="eachPopular" data-latitude="<?php echo $businesses[$i]['latitude'];?>" data-longitude="<?php echo $businesses[$i]['longitude'];?>" data-info="<?php echo $businesses[$i]['title'].",".$businesses[$i]['location']; ?>" data-seq="<?php echo $i+1;?>">
                        <div class="eachPopularLeft">
                            <div class="eachPopularImage">
                                <img width="170" height="170" src="<?php echo $businesses[$i]['image']; ?>" alt="">
                            </div>
                            <div class="eachPopularTitleBlock">
                                <div class="popularTitleTextBlock">
                                    <a href="<?php echo base_url(); ?>business_detail?id=<?php echo $businesses[$i]['business_id']; ?>" class="eachPopularTitle <?php if($businesses[$i]['is_paid']){echo "advtPlus";}?>">
                                        <?php echo $businesses[$i]['title']; ?>
                                    </a>
                                    <div class="eachPopularOtherActions">
                                        <a href="<?php echo $businesses[$i]['page_url']; ?>" target="_blank" class="openNewWindow"></a>
                                        <span class="<?php if ($businesses[$i]['is_verified']) {
                                            echo 'ypApprovedItem';
                                        } ?>"></span>
                                        <span class="certified aaP"><?php echo $businesses[$i]['rating']; ?></span>
                                    </div>
                                </div>

                                <div class="eachPopularRateOpen">
                                    <div class="eachPopularRatingBlock">
                                        <span class="rating r<?php echo str_replace(".", "-", $businesses[$i]['overall']); ?>"><?php echo $businesses[$i]['overall']; ?></span>
                                        <a href="#" class="ratingCount"><?php if($businesses[$i]['review']){ if($businesses[$i]['review']>1){echo $businesses[$i]['review']." reviews";}else{echo $businesses[$i]['review']." review"; }}?></a>
                                    </div>
                                    <div class="openNow"><?php if($businesses[$i]['datetime']){ ?><strong>Open now</strong> - <?php echo $businesses[$i]['datetime']; ?><?php }else{ ?><strong></strong><?php } ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="eachPopularRight">
                            <div class="addFav">Add Favorite</div>
                            <a class="businessContact" href="tel:<?php if($businesses[$i]['phone']){echo $businesses[$i]['phone'];}?>"><?php if($businesses[$i]['phone']){echo "+91 ".$businesses[$i]['phone'];} ?></a>
                            <address class="businessArea">
                                <strong><?php echo $businesses[$i]['area']; ?></strong>
                                <?php echo $businesses[$i]['city']; if($businesses[$i]['pincode']){echo " - ".$businesses[$i]['pincode'];} ?>
                            </address>

                            <div class="eachPopularLink">
                                <a href="#"><?php echo $businesses[$i]['email']; ?></a>
                                <a href="#"><?php echo $businesses[$i]['website_url']; ?></a>
                            </div>
                        </div>
                    </div>
            <?php }//end if
                }//end for
            ?>
            </div>
        </div>




    </div>
</div>

<script src="<?php echo ASSETSURL; ?>front/js/slick.min.js"></script>
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAW2h1ODSpSpioeUTgdN8ZqwPJ7sKXotAY&callback=initMap' async defer></script>
<script src="<?php echo ASSETSURL; ?>front/js/search-map.js"></script>
<script>
    $(document).ready(function () {
        $('.favoriteBtn').click(function () {
            $(this).toggleClass('active');
        });
    });
</script>

</body>
</html>

<!--
<link rel="stylesheet" href="css/leaflet.css" type="text/css">
<script src="js/leaflet.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function() {
        var mymap = L.map('mapid').setView([51.505, -0.09], 13);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={AIzaSyAW2h1ODSpSpioeUTgdN8ZqwPJ7sKXotAY}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'your.mapbox.project.id',
    accessToken: 'AIzaSyAW2h1ODSpSpioeUTgdN8ZqwPJ7sKXotAY'
}).addTo(mymap);
});
</script> -->




<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=initialize"></script> -->



