<?php 
	//$css = 'css/yp-search.css';
	//include 'header-inner.php';
?>

<div class="mobileHeader">
	<div class="mobileMenu hasIcons"><?php echo $msg; ?></div>
	<div class="mobileHeadIcons">
		<button class="mobSearchBtn">Search</button>
		<button class="mobLocation">Location</button>
	</div>
</div>

<div class="container resultsMapBg greyBg">
	<div class="wrapper">
		<div class="resultsBlock">
			<div class="sortBlock">
				<div class="sortSelectBox select">
					<span class="mobileSortText">Sort Results</span>
					<span class="selectText">Sort results</span>
					<select name="" id="" class="selectBox">
						<option value="1">Price Low to High</option>
						<option value="2">Price High to Low</option>
						<option value="3">Most relevant</option>
						<option value="4">Most Popular</option>
						<option value="5">New Release</option>
					</select>
				</div>
			</div>
			<div class="mobFilterOptionBlock">
				<button class="mobFilterBtn">Filters</button>
			</div>
			<div class="resultsText"><?php echo $msg; ?></div>
			<div class="desktopMapView">
				<a href="#" class="mapViewBtn">See Results on Map View</a>
			</div>
		</div>
	</div>
</div>
<?php /*
<script>
	$(document).ready(function() {
		$('.filterTabTitle').click(function() {
			var c = $(this).parent().find('.filterTabContent');
			if($(this).hasClass('activeTab')) {
				$(this).removeClass('activeTab');
				$(this).next('.filterTabContent').removeClass('showFilters');
				$('.filtersActions').removeClass('showActions');
			} else {
				$('.filterTabTitle').removeClass('activeTab');
				$('.filterTabContent').removeClass('showFilters');

				$(this).addClass('activeTab');
				$(this).next('.filterTabContent').addClass('showFilters');
				$('.filtersActions').addClass('showActions');
			}
		});

		$('.mobFilterBtn').click(function() {
			if($('.filtersBlock').hasClass('showFiltersMobile')) {
				$('.filtersBlock').removeClass('showFiltersMobile');
			} else {
				$('.filtersBlock').addClass('showFiltersMobile');
			}
		});

		$('#mobileFilterClose').click(function() {
			$('.filtersBlock').removeClass('showFiltersMobile');
		});

		$('#desktopFilterClose').click(function() {
			$('.filterTabTitle').removeClass('activeTab');
			$('.filterTabContent').removeClass('showFilters');
			$('.filtersActions').removeClass('showActions');
		});
	});
</script>
 */ ?>


<div class="container mainContent greyBg">
	<div class="wrapper">
		<section class="leftBigCol">
			<div class="filtersBlock">   <!-- showFiltersMobile -->
				<div class="filtersBlockContent">
					<div class="filtersHeader">
						<div class="filtersClose" id="mobileFilterClose"></div>
					</div>
					<div class="filtersFullContentBlock fullWidth">
						<div class="filtersMainTabs fullWidth">
                                                    <?php if(count($sub_category_filters)){ ?>
							<div class="filterTab1 eachFilterTab">
                                                            <div class="filterTabTitle">Categories <span class="countInfo"><?php echo count($sub_category_filters);?></span></div>
								<div class="filterTabContent">
                                                                    <ul class="filterTabInputsList" id="category">
                                                                        <?php foreach ($sub_category_filters as $key => $value) {?>
                                                                            <li>
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox" class="checkboxInput" name="cat_filter" id="<?php echo 'cat_'.$value['id']?>" value="<?php echo $value['id']?>">
                                                                                    <span class="checkboxText"><?php echo $value['name']?></span>
                                                                                </label>
                                                                            </li>
                                                                        <?php }?>
                                                                    </ul>
								</div>
							</div>
                                                    <?php } ?>
							<div class="filterTab2 eachFilterTab">
								<div class="filterTabTitle">Localities  <span class="countInfo">9</span></div>
								<div class="filterTabContent">
									<ul class="filterTabInputsList">
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Panjagutta</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Madhapur</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Saidabad</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Dilsukhnagar</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Gachibowli</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">SR Nagar</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Hi-Tech City</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Inorbit Mall</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Mehdipatnam</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Charminar</span>
											</label>
										</li>
									</ul>
								</div>
							</div>
							<div class="filterTab3 eachFilterTab">
								<div class="filterTabTitle">Price Range <span class="countInfo">2</span></div>
								<div class="filterTabContent">
									<ul class="filterTabInputsList">
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Rs.500 - Rs.1000</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Rs.1001 - Rs.5000</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Rs.5001 - Rs.10,000</span>
											</label>
										</li>
									</ul>
								</div>
							</div>
							<div class="filterTab4 eachFilterTab">
								<div class="filterTabTitle">Filter Name</div>
								<div class="filterTabContent">
									<ul class="filterTabInputsList">
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Mobile Phones</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Smart TVs</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Smart Tablets</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">2 in 1 Laptops</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">General Laptops</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Business Laptops</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Gaming Laptops</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Smart TV</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">Full HD TV</span>
											</label>
										</li>
										<li>
											<label class="checkbox">
												<input type="checkbox" class="checkboxInput" name="">
												<span class="checkboxText">4K Smart TV</span>
											</label>
										</li>
									</ul>
								</div>
							</div>
							<div class="filterTab5 eachFilterTab">
								<div class="filterTabTitle">More Filters  <span class="countInfo"><?php echo count($filters); ?></span></div>
								<div class="filterTabContent">
                                                                    <?php foreach ($filters as $key => $filter_value) {?>
									<div class="eachFilterRow">
										<div class="eachFilterRowTitle"><?php echo $filter_value->display_name; ?> </div>
										<div class="eachFilterRowContent">
											<ul class="filterTabInputsList filters_list" id="<?php echo $filter_value->id; ?>">
                                                                                            <?php 
                                                                                            $attributes = explode(',',$filter_value->attributes);
                                                                                            foreach ($attributes as $att) {
                                                                                            $att_val = str_replace(' ', '_', $att);
                                                                                            ?>
												<li>
                                                                                                    <label class="checkbox">
                                                                                                            <input type="checkbox" class="checkboxInput" id="<?php echo 'cat_'.$att_val; ?>" name="filters" value="<?php echo $att_val; ?>">
                                                                                                            <span class="checkboxText"><?php echo $att; ?></span>
                                                                                                    </label>
												</li>
                                                                                                <?php } ?>
											</ul>
										</div>
									</div>
                                                                    <?php } ?>


								</div>
							</div>
						</div>
						
						

					</div>

				</div>

				<div class="filtersActions">
					<div class="filtersActionsButtons">
						<button class="filtersApplyBtn" onclick="myfilter();">Apply</button>
						<button class="filterCancelBtn">Clear All</button>
						<div class="filtersClose" id="desktopFilterClose"></div>
					</div>

				<?php /*	<div class="filtersSelected">
						<div class="eachSelectedFilter">
							<span class="eachSelectedFilterTitle">Rs10,000-Rs.99,999</span>
							<span class="eachSelectedFilterRemove">X</span>
						</div>

						<div class="eachSelectedFilter">
							<span class="eachSelectedFilterTitle">Hotels</span>
							<span class="eachSelectedFilterRemove">X</span>
						</div>
						<div class="eachSelectedFilter">
							<span class="eachSelectedFilterTitle">Gachibowli, Hyderabad</span>
							<span class="eachSelectedFilterRemove">X</span>
						</div>
						<div class="eachSelectedFilter">
							<span class="eachSelectedFilterTitle">Luxary Restaurants</span>
							<span class="eachSelectedFilterRemove">X</span>
						</div>
						<div class="eachSelectedFilter">
							<span class="eachSelectedFilterTitle">Rs10,000-Rs.99,999</span>
							<span class="eachSelectedFilterRemove">X</span>
						</div>
					</div> */ ?>
				</div>

					


			</div>
			<div class="businessListingBlock">
				<header class="hiddenTitle">
					Business Listing
				</header>
				<ul class="popularThisWeekList">

                                <?php
                                    foreach ($businesses as $key => $lb) {
                                        ?>
                                        <li>
                                            <div class="eachPopular">
                                                <div class="eachPopularLeft">
                                                    <div class="eachPopularImage">
                                                        <?php if ($lb['is_logo'] == 0) { ?>
                                                            <img width="170" height="170" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $lb['title']; ?>">
                                                        <?php  } else { ?>
                                                            <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$lb['image_note'].'/190x125_'.$lb['image_note'].'-1.jpg';?>" alt="<?php echo $lb['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="eachPopularTitleBlock">
                                                        <div class="popularTitleTextBlock">
                                                            <?php if(($lb['website_url'] != "") || (isset($lb['is_verified']) && $lb['is_verified']==1)){  ?>
                                                            <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle hasOtherInfo" title="<?php echo $lb['title']; ?>">
                                                            <?php } else { ?>
                                                            <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle" title="<?php echo $lb['title']; ?>">
                                                            <?php } echo $lb['title']; ?>
                                                            </a>
                                                            <div class="eachPopularOtherActions">
                                                                <?php if ($lb['website_url'] != "") { ?>
                                                                    <a target="_blank" href="<?php echo $lb['website_url']; ?>"><span class="openNewWindow"></span></a>
                                                                <?php } ?>
                                                                <?php if(isset($lb['is_verified']) && $lb['is_verified']==1){?>
                                                                    <span class="ypApproved">.</span>
                                                                <?php }?>
                                                                <span class="certified aaP"><?php echo $lb['rating']; ?></span>
                                                            </div>
                                                        </div>
                                                        <div class="eachPopularRateOpen">
                                                            <div class="eachPopularRatingBlock">
                                                                <?php if($lb['overall'] != 0.0 && $lb['review'] !=0){ ?>
                                                                <span class="rating r<?php echo str_replace(".", "-", $lb['overall']); ?>"><?php echo $lb['overall']; ?></span>
                                                                <a href="#" class="ratingCount">
                                                                    <?php
                                                                        if ($lb['review'] > 1) {
                                                                            echo $lb['review'] . " reviews";
                                                                        } else {
                                                                            echo $lb['review'] . " review";
                                                                        }
                                                                    ?></a>
                                                                <?php }?>
                                                            </div>
                                                            <div class="openNow"><?php if ($lb['datetime']) { ?><strong>Open now</strong> - <?php echo $lb['datetime']; ?><?php } else { ?><strong> </strong><?php } ?></div>
                                                        </div>
                                                        <ul class="eachPopularTagsList">
                                                            <?php if($lb['categories'] !=""){
                                                                echo $lb['categories'];
                                                            }?>
                                                        </ul>
                                                        <div class="eachPopularLink">
                                                            <?php if($lb['email'] != ""){?>
                                                                <a href="mailto:<?php echo $lb['email']; ?>">Email</a>
                                                            <?php } if ($lb['website_url'] != "") { ?>
                                                                <a href="<?php echo $lb['website_url']; ?>" target="_blank">Website</a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="eachPopularRight">
                                                    <div class="<?php echo $lb['favclass']; ?>" onclick="addtofavorite(<?php echo $lb['business_id']; ?>)">Add Favorite</div>
                                                    <a class="businessContact" <?php
                                                        if ($lb['phone']) {
                                                            echo 'href="tel:'.$lb['phone'].'"';
                                                        }
                                                        ?>><?php
                                                        if ($lb['phone']) {
                                                            echo $lb['phone'];
                                                        }
                                                        ?></a>
                                                    <address class="businessArea">
                                                        <?php if ($lb['area']) { ?>
                                                            <strong> <?php echo $lb['area'];
                                                        } else {
                                                            echo "";
                                                        } ?> </strong>
                                                        <?php
                                                        if ($lb['pincode']) {
                                                            echo $lb['city'] . " - " . $lb['pincode'];
                                                        } else {
                                                            echo $lb['city'];
                                                        }
                                                        ?>
                                                    </address>
                                                    <div class="directionsLocationsBlock">
                                                        <span class="locationsCount"></span>
                                                    <?php
                                                    if (!empty($lb['latitude']) && !empty($lb['longitude'])) {
                                                        echo "<a  href='https://maps.google.com/maps?f=d&amp;daddr=" . $lb['latitude'] . "," . $lb['longitude'] . "&amp;hl=en' target='_blank'>Directions</a>";
                                                    }
                                                    ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
				</ul>
			</div>

			<?php /*<div class="mobileLoadMore">
				<button class="loadMoreBtn">Load More</button>	
			</div> */?>

			<div class="pagination">
                                <div class="paginationBlock">
                                    <div class="pageNumbersNumbering">
                                        <ul class="paginationNew">
                                            <?php echo $links; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                     <?php if (count($paid_business) > 0) { ?>
			<div class="sponsorsLinks">
				<h3 class="secondaryHeading">Sponsored Link</h3>
				<ul class="popularThisWeekList sponsoredList">
                                    <?php foreach ($paid_business as $key => $lb) { ?>
					<li>
                                            <div class="eachPopular">
                                                <div class="eachPopularLeft">
                                                    <div class="eachPopularImage">
                                                        <?php if ($lb['is_logo'] == 0) { ?>
                                                            <img width="170" height="170" src="<?php echo STYLEURL.'business-images/190x125_default.png'; ?>" alt="<?php echo $lb['title']; ?>">
                                                        <?php } else { ?>
                                                            <img width="190" height="125" src="<?php echo STYLEURL.'business-images/'.$lb['image_note'].'/190x125_'.$lb['image_note'].'-1.jpg';?>" alt="<?php echo $lb['title']; ?>">
                                                        <?php  } ?>
                                                    </div>
                                                    <div class="eachPopularTitleBlock">
                                                        <div class="popularTitleTextBlock">
                                                            <a href="<?php echo $lb['link']; ?>" class="eachPopularTitle advtPlus" title="<?php echo $lb['title']; ?>">
                                                                <?php echo $lb['title']; ?> 
                                                            </a>
                                                            <div class="eachPopularOtherActions">
                                                                <?php if ($lb['website_url'] != "") { ?>
                                                                    <a href="<?php echo 'http://' . $lb['website_url']; ?>" target="_blank" class="openNewWindow"></a>
                                                                <?php } ?>
                                                                <?php if(isset($lb['is_verified']) && $lb['is_verified']==1){?>
                                                                    <span class="ypApproved">.</span>
                                                                <?php }?>
                                                                <span class="certified aaP"><?php echo $lb['rating']; ?></span>
                                                            </div>
                                                        </div>
                                                            <div class="eachPopularRateOpen">
                                                                <div class="eachPopularRatingBlock">
                                                                    <?php if($lb['overall'] != 0.0 && $lb['review'] != 0){?>
                                                                        <span class="rating r<?php echo str_replace(".", "-", $lb['overall']); ?>"><?php echo $lb['overall']; ?></span>
                                                                        <a href="#" class="ratingCount">
                                                                            <?php
                                                                                if ($lb['review'] > 1) {
                                                                                    echo $lb['review'] . " reviews";
                                                                                } else {
                                                                                    echo $lb['review'] . " review";
                                                                                }
                                                                            ?>
                                                                        </a>
                                                                    <?php }?>
                                                                </div>
                                                                <div class="openNow"><?php if ($lb['datetime']) { ?><strong>Open now</strong> - <?php echo $lb['datetime']; ?><?php } else { ?><strong> </strong><?php } ?></div>
                                                            </div>

                                                            <ul class="eachPopularTagsList">
                                                                <?php if($lb['categories'] !=""){
                                                                    echo $lb['categories'];
                                                                }?>
                                                            </ul>
                                                            <div class="eachPopularLink">
                                                                <a href="mailto:<?php echo $lb['email']; ?>">Email</a>
                                                                <?php if ($lb['website_url'] != "") { ?>
                                                                    <a href="<?php echo 'http://' . $lb['website_url']; ?>" target="_blank">Website</a>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="eachPopularRight">
                                                        <div class="addFav">Add Favorite</div>
                                                        <a class="businessContact" <?php
                                                                if ($lb['phone']) {
                                                                    echo 'href="tel:'.$lb['phone'].'"';
                                                                }?>><?php
                                                                if ($lb['phone']) {
                                                                    echo $lb['phone'];
                                                                }?>
                                                        </a>
                                                        <address class="businessArea">
                                                                <?php if ($lb['area']) { ?>
                                                                    <strong> <?php echo $lb['area'];
                                                                } else {
                                                                    echo "";
                                                                } ?> </strong>
                                                                <?php
                                                                if ($lb['pincode']) {
                                                                    echo $lb['city'] . " - " . $lb['pincode'];
                                                                } else {
                                                                    echo $lb['city'];
                                                                }
                                                                ?>
                                                        </address>

                                                        <div class="directionsLocationsBlock">
                                                            <span class="locationsCount"></span>
                                                            <?php
                                                            if (!empty($lb['latitude']) && !empty($lb['longitude'])) {
                                                                echo "<a  href='https://maps.google.com/maps?f=d&amp;daddr=" . $lb['latitude'] . "," . $lb['longitude'] . "&amp;hl=en' target='_blank'>Directions</a>";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
						</div>
					</li>
                                    <?php }?>
                                </ul>
			</div>
                     <?php }?>


		</section>
		<aside class="rightSmallCol">
			<div class="rightMap" style="display: none;">
				<div class="rightMapBlock">
					<a href="#" class="mapViewBtn">View on Map</a>
				</div>
			</div>
			<div class="rightFeatured">
                            <div class="rightFeaturedBlock">
                                <h4 class="rightFeaturedHeading">Featured</h4>
                                <ul class="featuredBusinessList">
                                    <?php foreach ($featured_business as $key => $lb) { ?>
                                    
                                        <li class="ad">
                                            <div class="eachFeaturedBusiness">
                                                <a href="<?php echo $lb['link']; ?>" class="featuredBusinessTitle"><?php echo $lb['title']; ?></a>
                                                <div class="featuredBusinessContact">
                                                    <span class="contact" <?php
                                                        if ($lb['phone']) {
                                                            echo 'href="tel:'.$lb['phone'].'"';
                                                        }
                                                        ?>><?php
                                                        if ($lb['phone']) {
                                                            echo $lb['phone'];
                                                        }
                                                        ?>
                                                    </span>
                                                    <span class="address"><?php if ($lb['area']) { ?>
                                                            <strong> <?php echo $lb['area'];
                                                        } else {
                                                            echo "";
                                                        } ?> </strong>
                                                        <?php
                                                        if ($lb['pincode']) {
                                                            echo $lb['city'] . " - " . $lb['pincode'];
                                                        } else {
                                                            echo $lb['city'];
                                                        }
                                                        ?></span>
                                                    
                                                    <?php if ($lb['website_url'] != "") { ?>
                                                                <a href="<?php echo $lb['website_url']; ?>" target="_blank">Website</a>
                                                            <?php } ?>
                                                </div>
                                            </div>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
			</div>
			<div class="haveBusiness">
                            <div class="haveBusinessAd" style="background-image:url(<?php echo STYLEURL; ?>front/images/cityDrawing.png);">
                                Want business owners to contact you?<br>
                                <a href="#" class="addBusinessBtn">Get a deal</a>
                            </div>
			</div>
                        <div class="rightColAdvt">
                            <script async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://pagead2.googlesyndication.com/pub-config/r20160913/ca-pub-9587875399157339.js"></script><script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <script>(adsbygoogle=window.adsbygoogle||[]).push({google_ad_client:"ca-pub-9587875399157339",enable_page_level_ads:true});</script>
                        </div>
			<div class="rightRecentReviews">
				<div class="recentViewsBlock">
					<h3 class="thirdHeading">Recent Reviews</h3>
					<ul class="recentReviewsList">
                                            <?php foreach ($reviews as $key => $review) { ?>
                                                <li>
                                                    <div class="eachRR">
                                                        <div class="eachRRPhotoRat">
                                                            <div class="eachRRPhoto">
                                                                <img src="<?php echo STYLEURL . "front/images/" . $review['image']; ?>" alt="">
                                                            </div>
                                                            <div class="titleRating">
                                                                <a href="#" class="reviewerName"><?php echo $review['name']; ?></a>
                                                                <div class="reviewRating">
                                                                    <?php if($review['rating'] != 0.0){?>
                                                                    <span class="rating r<?php echo str_replace(".", "-", $review['rating']); ?>"><?php echo $review['rating']; ?></span>
                                                                    <?php } ?>
                                                                    <span class="ratingTime"><?php echo $review['datetime'] . ' ago'; ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a href="<?php echo $review['link']; ?>" class="eachRRTitle"><?php echo $review['business']; ?></a>
                                                        <p class="eachRRTitleInfo"><?php echo substr($review['comments'], 0, 35); ?></p>
                                                    </div>
                                                </li>
                                            <?php } ?>
					</ul>
				</div>
			</div>

                        <?php if (!empty($cat_suggest)) { ?>
                            <div class="rightSimilarCategories">
                                <div class="similarCategoriesBlock">
                                    <h3 class="thirdHeading">Similar Categories</h3>
                                    <ul class="categoriesList">
                                        <?php foreach ($cat_suggest as $key => $c) { ?>
                                            <li><a href="<?php echo $c['link']; ?>"><?php echo $c['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>

			<div class="rightSuggestBusiness">
                            <div class="haveBusiness">
                                <div class="haveBusinessAd" style="background-image:url(<?php echo STYLEURL; ?>front/images/cityDrawing.png);">
                                    Didn't find what you are looking for?<br>
                                    <a href="/add-business" class="addBusinessBtn" id="suggest-business">Suggest a business</a>
                                </div>
                            </div>
                        </div>
		</aside>
	</div>
</div>



<?php
/*	
$js = "<script src='js/popupjs.js'></script><script>
		$(document).ready(function() {
			/*$('.addFav').click(function() {
				if($(this).hasClass('favActive')) {
					$(this).removeClass('favActive');
				} else {
					$(this).addClass('favActive');
				}
			});*/

/*			$('.opFavClick').click(function() {
				if($(this).hasClass('activeFavIcon')) {
					$(this).removeClass('activeFavIcon');
				} else {
					$(this).addClass('activeFavIcon');
				}
			});
		});	
		</script>";

	include 'footer.php';

*/        
?>


<?php /*
<script>
	$(document).ready(function() {
		$('.popup').click(function() {
			$('.popupBlock').addClass('showPopup');
			$('body').addClass('noScroll');
		});
		$('.closePopup').click(function() {
			$('.popupBlock').removeClass('showPopup');
			$('body').removeClass('noScroll');
		});
		
		$('.filtersEachColHeading').click(function() {
			if($('.filtersEachColListBlock').hasClass('showFilters')) {
				$('.filtersEachColListBlock').removeClass('showFilters');
				$('.filterClearFilter').removeClass('showClearFilters');
			}  else {
				$('.filtersEachColListBlock').addClass('showFilters');
				$('.filterClearFilter').addClass('showClearFilters');
			}
		});

		// popup js

		$('.addBusinessBtn').click(function() {
			$('.popupNew').addClass('showPopup');
		});

		$('.popupClose').click(function() {
			$('.popupNew').removeClass('showPopup');
		});

		//Text box on focus label change

		$('.popupFormTextBox, .popupFormTextarea').on('focus blur', function (e) {
		    $(this).parent('.eachFormElement').toggleClass('eachFormElementFocused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');



		// Notification Script
 		var note = '<div class="notification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="#" class="loginBtn notificationBtn">Login</a> or <a href="#" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>';

 		$('.addFav').click(function() {
 			$(note).appendTo('body').delay(3000, function() {
				$('.notification').addClass('showNotification');
			});	
 		});

 		$(document).on('click', 'div.closeNotification', function() {
 			//$('.notification').removeClass('showNotification');
			$('.notification').remove();
 		});
	});
</script>
*/?>


<div class="popupNew">
	<div class="popupOverlayNew"></div>

	<div class="popupMainBlock">
		<div class="popupMainContent">
			<div class="popupHeader">
				<div class="popupTitle">
					Get a deal
				</div>
				<div class="popupClose">Close</div>
			</div>
			
			<div class="popupContent">
				<div class="popupFormBlock">
					<duv class="popupFormListBlock">
						<ul class="popupFormList">
							<li>
								<div class="eachFormElement">
									<p class="smallInfo">Please select number of business owners you wants to contact you for required services.</p>
								</div>
							</li>

							<li>
								<div class="eachFormElement">
									<input type="text" class="popupFormTextBox" value="">
									<label for="" class="popupFormLabel">Please enter your name*</label>
								</div>
							</li>
							
							<li>
								<div class="eachFormElement phone">
									<span class="number">+91</span>
									<input type="text" class="popupFormTextBox" value="">
									<label for="" class="popupFormLabel">Please enter your mobile number*</label>
								</div>
							</li>

							<li>
								<div class="eachFormElement">
									<input type="email" class="popupFormTextBox" value="">
									<label for="" class="popupFormLabel">Email*</label>
								</div>
							</li>

							
							

							<li>
								<p class="smallInfo">I want</p>
								<div class="eachFormElement eachFormElementInline">
									<label for="phone" class="radio">
										<input type="radio" class="radioInput" id="phone" name="radio">
										<span class="radioText">2 Leads</span>
									</label>

									<label for="phone1" class="radio">
										<input type="radio" class="radioInput" id="phone1" name="radio">
										<span class="radioText">3 Leads</span>
									</label>

									<label for="phone2" class="radio">
										<input type="radio" class="radioInput" id="phone2" name="radio">
										<span class="radioText">5 Leads</span>
									</label>

									<label for="phone3" class="radio">
										<input type="radio" class="radioInput" id="phone3" name="radio">
										<span class="radioText">10 Leads</span>
									</label>

									<label for="phone4" class="radio">
										<input type="radio" class="radioInput" id="phone4" name="radio">
										<span class="radioText">15 Leads</span>
									</label>
								</div>
							</li>
							
							
							<li>
								<div class="eachFormElement phone">
									<input type="submit" class="popupButton" value="Submit">
									<input type="button" class="cancelButton popupClose" value="Cancel">
								</div>
							</li>
						</ul>
					</duv>
				</div>
			</div>
			
		</div>
	</div>
</div>