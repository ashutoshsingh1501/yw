<div class="img-backdrop" style="background-image: url('assets/backend/images/login.jpg')"></div>
<div class="spacer"></div>
<div class="card contain-sm style-transparent">
	<div class="card-body">
		<div class="row">
			<div class="col-sm-6">
				<br/>
				<span class="text-lg text-bold text-primary"><img src="<?php echo base_url(); ?>assets/frontend/images/yellow-pages-logo.png" alt="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yellowpages ADMIN</span>
				<br/><br/>
				
				<?php $attributes = array('class' => 'form', 'id' => 'login-form');
					
				echo form_open('dashboard' , $attributes); ?>
				<div class="form-group">
					<input type="text" class="form-control" id="username" name="username">
					<label for="username">Username</label>
				</div>
				<div class="form-group">
					<input type="password" class="form-control" id="password" name="password">
					<label for="password">Password</label>
					<!--<p class="help-block"><a href="#">Forgotten?</a></p>-->
				</div>
				<br/>
				<div class="row">
					<div class="col-xs-6 text-left">
						<div class="checkbox checkbox-inline checkbox-styled">
							<label>
								<input type="checkbox"> <span>Remember me</span>
							</label>
						</div>
					</div>
					<div class="col-xs-6 text-right">
						<button class="btn btn-primary btn-raised" type="submit">Login</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-sm-5 col-sm-offset-1 text-center" style="display:none;">
			<br><br><br><br>
			<h3 class="text-light">
				No account yet?
			</h3>
			
			<a class="btn btn-block btn-raised btn-primary" href="#">Sign up here</a>
		</div>
	</div>
</div>
</div>