<section>
	<div class="section-body settings">
		<div class="card card-underline">
			<div class="card-head">
				<header>Add Employee</header>
			</div>
			<form class="form form-validate" style="margin:0 auto"  enctype="multipart/form-data" action='<?php echo base_url('employee/create/'); ?>' method="post" >
				<div class="card-body">
					<div class="form-group col-md-12">
						<div class="col-md-6">
							<div class="row">
								<div class="form-group">
									<input type="text" class="form-control" id="username" name="username" required>
									<label for="username">Display Name</label>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" id="useremail" name="useremail" required>
									<label for="useremail">Email</label>
								</div>
								<div class="form-group">
									<input type="number" class="form-control" maxlength='10' minlength='9' id="userphone" name="userphone" required >
									<label for="userphone">Phone</label>
								</div>
								<div class="form-group">
									<select class="form-control selectpicker"  name="userrole" data-live-search="true" required>
										<option value="">Select Role</option>
										<?php foreach($roles as $key => $sl ){								
										?>
										<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<select class="form-control selectpicker"id="usermanager" name="usermanager" data-live-search="true" required>
										<option value="">Select Department</option>
										<?php foreach($departments as $key => $sl ){								
										?>
										<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<select class="form-control selectpicker"id="usercity" name="usercity" data-live-search="true" required>
										<option value="">Select City</option>
										<?php foreach($cities as $key => $sl ){								
										?>
										<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="checkbox checkbox-styled">
									<label>
										<input type="checkbox" value='1' name="receive_notification" id="receive_notification" > <span>Receive Promotional Emails</span>
									</label>
								</div>
								<div class="form-group">										 
									<input type="password" class="form-control" id="password" name="password" required>
									<label for="usermanager">New Password</label>
								</div>
								<div class="form-group">										 
									<input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
									<label for="usermanager">Confirm Password</label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row pull-right">
								<div class="col-md-12">
									<input type='hidden' id='old_image_url' name='old_image_url' value=''>
									<img id="dispImage" src="" alt="Image comes over here" style="height:200px;width:200px;border:1px solid black;padding:2px;" >
									<input type="hidden"  id="image_url"  name="image_url" value="">
								</div>
								<div class="form-group col-md-12">	 
									<input type="file" name="fileToUpload" id="fileToUpload" onchange="readURL(this);" required>
									<span id="newupload" style="display:none;">
									</span>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" name="submit" class="btn btn-default-dark">Add Employee</button>
						<a  class="btn btn-default" href="<?php echo base_url('employee'); ?>">Cancel</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>			
<script type="text/javascript">
	
	function readURL(input) {
		var imageurlSrc='';
		if (input.files && input.files[0]) {
            var reader = new FileReader();
			
            reader.onload = function (e) {
                $('#dispImage')
				.attr('src', e.target.result)
				.width(150)
				.height(200);
			};
			imageurlSrc=input.files[0];
            reader.readAsDataURL(input.files[0]);
		}
		
		var imageurl="/uploads/"+imageurlSrc;
		$('#image_url').val(imageurl);
		$('#delImage').show();
	}
	
	function deleteImage()
	{
		if(confirm("Do you want to delete existing Image...!!!"))
		{
			$('#dispImage').attr('src', '');
			$('#newupload').html('<input type="file" name="fileToUpload" id="fileToUpload" onchange="readURL(this);" required>');
			$('#newupload').show();
			$('#delImage').hide();
			}else{
			return false;
		}
	}
	
	var password = document.getElementById("password")
	, confirm_password = document.getElementById("confirm_password");
	
	function validatePassword(){
		if(password.value != confirm_password.value  && password.value !='' & confirm_password.value=='') {
			confirm_password.setCustomValidity("Please  confirm the password entered");
		}
		else if(password.value != confirm_password.value  && password.value !='' & confirm_password.value!='') {
			confirm_password.setCustomValidity("Passwords Don't Match");
			} else {
			confirm_password.setCustomValidity('');
		}
	}
	
	password.onchange = validatePassword;
	confirm_password.onkeyup = validatePassword;
</script>						