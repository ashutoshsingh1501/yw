<section>
	<div class="section-body">
		<div class="row">
			<div class="card card-underline">
				<div class="card-body">
                    <div class="row" style="margin-bottom:30px;vertical-align:middle">
						<div class="col-sm-1">
							<div class="btn-group" style="width:100%">
								<button type="button" class="btn dropdown-toggle bulk"  data-toggle="dropdown" aria-expanded="false">
									<i class="fa fa-caret-down text-default-light"></i>&nbsp;Bulk
								</button>
								<ul class="dropdown-menu animation-expand" role="menu">
									<li><a href="#">Active</a></li>
									<li><a href="#">Deactive</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="fa fa-fw fa-times text-danger"></i> Remove item</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<select class="form-control selectpicker" id="state_id" name="state_id" data-live-search="true" >
								<option value="0">All Role</option>
								<?php foreach($roles as $key => $sl ){ ?>
									<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
								
							</select>
						</div>
						<div class="col-sm-2">
							<select class="form-control selectpicker"  name="status_id" data-live-search="true" >
								<option>Status</option>
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</div>
						<div class="col-sm-2">
							<select class="form-control selectpicker" name="department_id" data-live-search="true" >
								<option>All Departments</option>
								<?php foreach($departments as $key => $sl ){ ?>
									<option value="<?php echo $key; ?>"><?php echo $sl; ?></option>
								<?php } ?>
								
							</select>
						</div>
						<div class="col-sm-1 pull-right" >
							<a class="btn  bulk" href="<?php echo base_url(); ?>employee/create" >
								 <i class="fa fa-plus text-default-light"></i> Add
							</a>
							
						</div>
						<div class="col-sm-2 pull-right"><h4 class="pull-right rowcount"> <?php echo count($results); ?> Employees </h4></div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-index table-striped table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" value="1" name="selectall" id="selectall"><span>&nbsp;</span>
										</label>
									</div>
								</th>
								<th>Employee Name <br><input type="text" value="" class="form-control"  name="emp_name" placeholder="Search Employee"></th>
								<th>Role</th>
								<th>Department</th>
								<th>Phone</th>
								<th>Email</th>
								<!--<th>City</th>-->
								<th>Status</th>
								<th style="text-align:right;" >Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($results as $key => $a){ ?>
								<tr>
									<td>
										<div class="checkbox checkbox-styled">
											<label><input type="checkbox" value="1" name="addcheck[]" class="addcheck"><span>&nbsp;</span></label>
										</div>
									</td>
									<td><?php echo $a->display_name; ?></td>
									<td><?php if(isset($roles[$a->role_id])){ echo $roles[$a->role_id]; }else{ echo '-'; } ?></td>
									<td><?php if(isset($deprtments[$a->department_id])){ echo $deprtments[$a->department_id]; }else{ echo 'Others'; } ?></td>							
									<td><?php echo $a->phone; ?></td>
									<td><?php echo $a->email; ?></td>
									<!--<td><?php //if(isset($city_list[$a->city_id])){ echo $city_list[$a->city_id]; }else{ echo '-'; } ?></td>-->	
									<td>
										<select data-id="<?php echo $a->id; ?>"  class="e_status form-control selectpicker" onchange="change_estatus(this)" >
											<option value="1" <?php if($a->status){ echo 'selected'; } ?> >Active</option>
											<option value="0" <?php if(!$a->status){ echo 'selected'; } ?> >Deactive</option>
										</select>
										
									</td>
									<td style="text-align:right;">
										<a href="<?php echo base_url(); ?>employee/edit/<?php echo $a->id; ?>" class="btn btn-flat ink-reaction"><i class="fa fa-pencil fa-fw"></i></a>
										<a href="#" class="btn btn-flat ink-reaction employee-delete" data-id="<?php echo $a->id; ?>"  ><i class="fa fa-trash-o fa-fw"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>