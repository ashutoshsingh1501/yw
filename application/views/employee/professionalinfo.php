<section>
	<div class="section-header ">
		<ol class="breadcrumb">
			<li class="active">Employees</li>
		</ol>
	</div>
	<div class="section-body employee">
		
		<div class="row col-sm-12 col-md-12 col-xs-12">
			<div class="card">
				<form class="form form-validate" method="post" role="form" action="<?php echo base_url(); ?>employee/create_professional">
					<div class="card-head style-primary">
	                    <ul class="nav nav-tabs">
							<li> <a href="javascript:void(0);">Personal Info</a> </li>
							<li class="active"> <a data-toggle="tab" href="#userprofessional">Professional Info</a> </li>
						</ul>
					</div>
					
					<div class="card-body">
						<div class="tab-content">
							<div id="userprofessional" class="tab-pane fade in active">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" class="form-control" id="qualification" name="qualification" required>
										<label for="qualification">Qualification</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" data-role="tagsinput" style="display: none;" class="form-control" id="languages" name="languages" required>
										<label for="languages">Languages (comma separated values, search and select)</label>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input data-role="tagsinput" type="text" class="form-control" id="keyskills" name="keyskills" required>
										<label for="keyskills">Key Skills (comma separated values, search and select)</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2"><div class="form-group"> Total Exp: </div></div>
								<div class="col-sm-4">
									<div class="form-group">
										<select id="userrole" name="expyears" class="form-control selectpicker" data-live-search="true" >
											<option value="">Select Year</option>
											<?php for($year=0; $year<30; $year++){ ?>
												<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
											<?php } ?>
										</select>
									</div>

								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<select id="userrole" name="expmonths" class="form-control selectpicker" data-live-search="true" >
											<option value="">Select Months</option>
											<?php for($month=1; $month<12; $month++){ ?>
												<option value="<?php echo $month; ?>"><?php echo $month; ?></option>
											<?php } ?>
										</select>
									</div>

								</div>
							</div>
							<div class="table-responsive">
							<p> Previous Company Details</p>
							<table class="table table-index table-striped table-bordered table-hover table-condensed">
							<?php for($i=1; $i<4;$i++) { ?>	
								<tr>

									<td>
										<div class="form-group">
											<input type="text" class="form-control" id="prev_employee_id<?php echo $i; ?>" name="prev_employee_id<?php echo $i; ?>" >
											<label for="prev_employee_id<?php echo $i; ?>">Employee ID</label>
										</div>
									</td>
									<td>
										<div class="form-group">
											<input type="text" class="form-control" id="designation<?php echo $i; ?>" name="designation<?php echo $i; ?>" >
											<label for="designation<?php echo $i; ?>">Designation</label>
										</div>
									</td>

									<td>
										<div class="form-group">
											<input type="text" class="form-control" id="companyname<?php echo $i; ?>" name="companyname<?php echo $i; ?>">
											<label for="companyname<?php echo $i; ?>">Company Name</label>
										</div>
									</td>

									<td>
										<div class="form-group">
											<input type="text" class="form-control" id="location<?php echo $i; ?>" name="location<?php echo $i; ?>" >
											<label for="location<?php echo $i; ?>">Location</label>
										</div>
									</td>


									<td>
										<div class="form-group">
											<input type="date" class="form-control" id="start_date<?php echo $i; ?>" name="start_date<?php echo $i; ?>">
											<label for="start_date<?php echo $i; ?>">Start Date</label>
										</div>
									</td>
										

									<td>
										<div class="form-group">
											<input type="date" class="form-control" id="end_date<?php echo $i; ?>" name="end_date<?php echo $i; ?>">
											<label for="end_date<?php echo $i; ?>">End Date</label>
										</div>
									</td>


									<td>
										<div class="form-group">
											<input type="text" class="form-control" id="salary<?php echo $i; ?>" name="salary<?php echo $i; ?>" >
											<label for="salary<?php echo $i; ?>">Salary</label>
										</div>
									</td>
									<td class="checkboxtd">
										<div class="form-group floating-label">
											<div class="radio radio-styled">
												<label>
													<input type="radio" name="latestcompany" value="<?php echo $i; ?>"  >
													<span>Last Company</span>
												</label>
											</div>
										</div>
									</td>
									</tr>
									<?php } ?>
									</table>
								</div>
								<div class="table-responsive">
									<p> Your Reference Details</p>
									<table class="table table-index table-striped table-bordered table-hover table-condensed">
									<tr>
									<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference1_name" name="reference1_name">
												<label for="reference1_name">Reference1 Name</label>
											</div>
										</td>

										<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference1_designation" name="reference1_designation">
												<label for="reference1_designation">Reference1 Designation</label>
											</div>
										</td>

										<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference1_email" name="reference1_email">
												<label for="reference1_email">Reference1 Email</label>
											</div>
										</td>


										<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference2_name" name="reference2_name">
												<label for="reference2_name">Reference2 Name</label>
											</div>
										</td>
										
										<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference2_designation" name="reference2_designation">
												<label for="reference2_designation">Reference2 Designation</label>
											</div>
										</td>
																	
										<td>
											<div class="form-group">
												<input type="text" class="form-control" id="reference2_email" name="reference2_email">
												<label for="reference2_email">Reference2 Email</label>
											</div>
										</td>
										</tr>
										</table>
									</div>
								<div class="modal-footer">
									<input type="hidden" name="employee_id" id="employee_id" value="<?php echo $result->id;?>" />
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
									<button type="submit"  class="btn btn-primary">Add Professional Info</button>
								</div>
							
							</div>
							
						</div>
					</div>
				</form>
			</div><!--end .card -->
		</div>
		
	</div>
</section>
