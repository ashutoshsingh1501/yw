
<div class="container resultsMapBg greyBg">
	<div class="wrapper">
		<div class="resultsBlock">
			<div class="sortBlock">
				<div class="sortSelectBox select">
					<span class="mobileSortText">Sort Results</span>
					<span class="selectText">Sort results</span>
					<select name="" id="" class="selectBox">
						<option value="1">Price Low to High</option>
						<option value="2">Price High to Low</option>
						<option value="3">Most Relavant</option>
						<option value="4">Most Popular</option>
						<option value="5">New Release</option>
					</select>
				</div>
			</div>
			<div class="mobFilterOptionBlock">
				<button class="mobFilterBtn popup">Filters</button>
			</div>
			<div class="resultsText">
				400 results for best hair saloon and spa near tolichowki
			</div>
			<div class="desktopMapView">
				<a href="#" class="mapViewBtn">Map View</a>
			</div>
		</div>
	</div>
</div>


<div class="container mainContent greyBg">
	<div class="wrapper">
		<section class="leftBigCol">
			<div class="filtersBlock">
				<div class="filtersFullBlock">
					<div class="filtersEachCol">
						<div class="filtersEachColHeading">
							Filter Type 1
						</div>
						<div class="filtersEachColListBlock">
							<ul class="filtersEachColList">
								<li>
									<label for="1" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="1">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="2" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="2">
										<span class="checkboxText">Filter Filter Filter Filter Filter Filter Filter Filter Filter Filter Filter 2</span>
									</label>
								</li>
								<li>
									<label for="3" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="3">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="4" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="4">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="5" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="5">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="6" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="6">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="7" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="7">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
							</ul>
							<div class="hideFiltersList">more...</div>
						</div>
					</div>

					<div class="filtersEachCol">
						<div class="filtersEachColHeading">
							Filter Type 2
						</div>
						<div class="filtersEachColListBlock">
							<ul class="filtersEachColList">
								<li>
									<label for="8" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="8">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="9" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="9">
										<span class="checkboxText">Filter 2</span>
									</label>
								</li>
								<li>
									<label for="10" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="10">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="11" class="eachFilter checkboxLabel">
										<input type="checkbox" class="checkbox" value="" id="11">
										<span class="checkboxText">Filter 1</span>
									</label>
								</li>
								
							</ul>
							<div class="hideFiltersList">more...</div>
						</div>
					</div>

					<div class="filtersEachCol">
						<div class="filtersEachColHeading">
							Filter Type 3
						</div>
						<div class="filtersEachColListBlock">
							<ul class="filtersEachColList">
								<li>
									<div class="filterSelectBox select">
										<span class="selectText">Range 1</span>
										<select name="" id="" class="selectBox">
											<option value="1">Range 1</option>
											<option value="2">Range 2</option>
											<option value="3">Range 3</option>
											<option value="4">Range 4</option>
											<option value="5">Range 5</option>
										</select>
									</div>
								</li>
								<li>
									<div class="filterSelectBox select">
										<span class="selectText">Range 2</span>
										<select name="" id="" class="selectBox">
											<option value="1">Range 1</option>
											<option value="2">Range 2</option>
											<option value="3">Range 3</option>
											<option value="4">Range 4</option>
											<option value="5">Range 5</option>
										</select>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<div class="filtersEachCol">
						<div class="filtersEachColHeading">
							Filter Type 4
						</div>
						<div class="filtersEachColListBlock">
							<ul class="filtersEachColList">
								<li>
									<label for="21" class="eachFilter radioLabel">
										<input type="radio" class="radio" name="radio" value="" id="21">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="22" class="eachFilter radioLabel">
										<input type="radio" class="radio" name="radio" value="" id="22">
										<span class="radioText">Filter 2</span>
									</label>
								</li>
								<li>
									<label for="23" class="eachFilter checkboxLabel">
										<input type="radio" class="radio" name="radio" value="" id="23">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="24" class="eachFilter checkboxLabel">
										<input type="radio" class="radio" name="radio" id="24">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="25" class="eachFilter checkboxLabel">
										<input type="radio" class="radio" name="radio" id="25">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="26" class="eachFilter checkboxLabel">
										<input type="radio" class="radio" name="radio" id="26">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								<li>
									<label for="27" class="eachFilter checkboxLabel">
										<input type="radio" class="radio" name="radio" id="27">
										<span class="radioText">Filter 1</span>
									</label>
								</li>
								
							</ul>
							<div class="hideFiltersList">more...</div>
						</div>
					</div>
				</div>

				<div class="mobileActions">
					<input type="button" class="applyFilters" value="Apply filters">
					<input type="button" class="cancelFilters" value="Cancel">
				</div>

				<div class="filterClearFilter">
					<div class="clearAllBtn">Clear All</div>
					<button class="moreFiltersBtn popup">More filters</<button>
				</div>
			</div>
			<div class="businessListingBlock">
				<header class="hiddenTitle">
					Business Listing
				</header>
				<ul class="popularThisWeekList">
					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property2.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle advtPlus">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<a href="#" target="_blank" class="openNewWindow"></a>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRateOpen">
										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="#" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>
									</div>

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>
									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address>

								<div class="directionsLocationsBlock">
									<span class="locationsCount">101 Locations</span>
									<a href="#">Directions</a>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle advtPlus">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<!-- <div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div> -->

									<div class="eachPopBusinsReview">
										<span class="eachPopBusinsReviewPhoto"><img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt=""></span>
										<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property2.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRateOpen">
										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="#" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>
									</div>

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>
									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address>

								<div class="directionsLocationsBlock">
									<span class="locationsCount">101 Locations</span>
									<a href="#">Directions</a>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<!-- <div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div> -->

									<div class="eachPopBusinsReview">
										<span class="eachPopBusinsReviewPhoto"><img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt=""></span>
										<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property2.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<a href="#" target="_blank" class="openNewWindow"></a>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRateOpen">
										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="#" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>
									</div>

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>
									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address>

								<div class="directionsLocationsBlock">
									<span class="locationsCount">101 Locations</span>
									<a href="#">Directions</a>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<!-- <div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div> -->

									<div class="eachPopBusinsReview">
										<span class="eachPopBusinsReviewPhoto"><img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt=""></span>
										<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property2.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRateOpen">
										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="#" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>
									</div>

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>
									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address>

								<div class="directionsLocationsBlock">
									<span class="locationsCount">101 Locations</span>
									<a href="#">Directions</a>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<!-- <div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div> -->

									<div class="eachPopBusinsReview">
										<span class="eachPopBusinsReviewPhoto"><img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt=""></span>
										<p class="eachPopBusinRevewText">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium quod necessitatibus aperiam, veniam qui, distinctio quaerat blanditiis adipisci. Recusandae, iure. Voluptatem cupiditate cum in ad labore facere iusto magni nemo!</p>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<div class="mobileLoadMore">
				<button class="loadMoreBtn">Load More</button>	
			</div>

			<div class="pagination">
				<div class="paginationBlock">
					<div class="pageNumber">
						<span class="pageNumberTxt">Page </span>
						<input type="text" class="pageNumberInput" maxlength="3" value="999">
					</div>
					<div class="pageNumbersNumbering">
						<a href="#" class="pageNumberingText">1</a>
						<a href="#" class="pageNumberingText">2</a>
						<a href="#" class="pageNumberingText active">3</a>
						<a href="#" class="pageNumberingText">4</a>
						<a href="#" class="pageNumberingText">5</a>
						<span class="pageNumberingDots">....</span>
						<a href="#" class="pageNumberingText">21</a>
						<a href="#" class="pageNumberingText">22</a>
						<a href="#" class="pageNumberingText">23</a>
						<a href="#" class="pageNumberingText">24</a>
						<a href="#" class="pageNumberNextBtn">Next</a>
					</div>

				</div>
			</div>

			<div class="sponsorsLinks">
				<h3 class="secondaryHeading">Sponsorsed Link</h3>
				<ul class="popularThisWeekList sponsoredList">
					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property2.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle advtPlus">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<a href="#" target="_blank" class="openNewWindow"></a>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRateOpen">
										<div class="eachPopularRatingBlock">
											<span class="rating r3-5">3.5</span>
											<a href="#" class="ratingCount">100 <span>reviews</span></a>
										</div>

										<div class="openNow"><strong>Open now</strong> - until 10pm</div>
									</div>

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>
									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address>

								<div class="directionsLocationsBlock">
									<span class="locationsCount">101 Locations</span>
									<a href="#">Directions</a>
								</div>
							</div>
						</div>
					</li>

					<li>
						<div class="eachPopular">
							<div class="eachPopularLeft">
								<div class="eachPopularImage">
									<img width="170" height="170" src="<?php echo base_url();  ?>assets/frontend/images/property3.jpg" alt="">
								</div>
								<div class="eachPopularTitleBlock">
									<div class="popularTitleTextBlock">
										<a href="#" class="eachPopularTitle advtPlus">
											Business name runs here like this and goes on 
										</a>
										<div class="eachPopularOtherActions">
											<span class="openNewWindow"></span>
											<span class="ypApproved"></span>
											<span class="certified aaP">AA++</span>
										</div>
									</div>

									<div class="eachPopularRatingBlock">
										<span class="rating r3-5">3.5</span>
										<a href="#" class="ratingCount">100 <span>reviews</span></a>
									</div>

									<div class="openNow"><strong>Open now</strong> - until 10pm</div>

									<!-- <div class="claimBlock">
										20% off&nbsp;&nbsp;|&nbsp;&nbsp;Is this your business? <a href="#">Claim now</a>
									</div> -->

									<ul class="eachPopularTagsList">
										<li>Helath</li>
										<li>Food</li>
										<li>Computer</li>
									</ul>

									<div class="eachPopularLink">
										<a href="#">Email</a>
										<a href="#">Website</a>
									</div>
								</div>
							</div>
							<div class="eachPopularRight">
								<div class="addFav">Add Favorite</div>
								<a class="businessContact" href="tel:9030130658">+91 00000 00000</a>
								<address class="businessArea">
									<strong>Madhapur</strong>
									Thiruvananthapuram - 500081
								</address> 
								<a href="#" class="businessMapDirections">Directions</a>
							</div>
						</div>
					</li>
				</ul>
			</div>



		</section>
		<aside class="rightSmallCol">
			<div class="rightFeatured">
				<div class="rightFeaturedBlock">
					<h4 class="rightFeaturedHeading">Featured Health Care</h4>
					<div class="featuredBusinessBlock">
						<a href="#" class="featuredBusinessTitle">Business title runs here like this and it extend</a>
						<div class="featuredBusinessContact">
							<span class="contact">+91 9030130658</span>
							<span class="address">Madhapur, Thiruvananthapuram - 500081</span>
							<a href="#" class="featuredBusinessWebsite">Website</a>
						</div>
					</div>
				</div>
			</div>
			<div class="haveBusiness">
				<div class="haveBusinessAd" style="background-image:url(assets/frontend/images/cityDrawing.png);">
					Want business owners to contact you?<br>
					<a href="#" class="addBusinessBtn">Get a deal</a>
				</div>
			</div>
			<div class="rightColAdvt">
				<a href="#">
					<img src="<?php echo base_url();  ?>assets/frontend/images/advt.png" alt="">
				</a>
			</div>
			<div class="rightRecentReviews">
				<div class="recentViewsBlock">
					<h3 class="thirdHeading">Recent Reviews</h3>
					<ul class="recentReviewsList">
						<li>
							<div class="eachRR">
								<div class="eachRRPhotoRat">
									<div class="eachRRPhoto">
										<img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt="">
									</div>
									<div class="titleRating">
										<a href="#" class="reviewerName">S Yugandhar</a>
										<div class="reviewRating">
											<span class="rating r0-5">0.5</span>
											<span class="ratingTime">2 hrs ago</span>
										</div>
									</div>
								</div>
								<a href="#" class="eachRRTitle">ABC Enterprises</a>
								<p class="eachRRTitleInfo">Best review.. Lorem ipsum dolor sit amet, consetetur sadipscing elitr...</p>
							</div>
						</li>
						<li>
							<div class="eachRR">
								<div class="eachRRPhotoRat">
									<div class="eachRRPhoto">
										<img src="<?php echo base_url();  ?>assets/frontend/images/profileImage2.jpg" alt="">
									</div>
									<div class="titleRating">
										<a href="#" class="reviewerName">S Yugandhar</a>
										<div class="reviewRating">
											<span class="rating r0-5">0.5</span>
											<span class="ratingTime">2 hrs ago</span>
										</div>
									</div>
								</div>
								<a href="#" class="eachRRTitle">ABC Enterprises</a>
								<p class="eachRRTitleInfo">Best review.. Lorem ipsum dolor sit amet, consetetur sadipscing elitr...</p>
							</div>
						</li>

						<li>
							<div class="eachRR">
								<div class="eachRRPhotoRat">
									<div class="eachRRPhoto">
										<img src="<?php echo base_url();  ?>assets/frontend/images/profileImage.jpg" alt="">
									</div>
									<div class="titleRating">
										<a href="#" class="reviewerName">S Yugandhar</a>
										<div class="reviewRating">
											<span class="rating r0-5">0.5</span>
											<span class="ratingTime">2 hrs ago</span>
										</div>
									</div>
								</div>
								<a href="#" class="eachRRTitle">ABC Enterprises</a>
								<p class="eachRRTitleInfo">Best review.. Lorem ipsum dolor sit amet, consetetur sadipscing elitr...</p>
							</div>
						</li>
						
						<li>
							<div class="eachRR">
								<div class="eachRRPhotoRat">
									<div class="eachRRPhoto">
										<img src="<?php echo base_url();  ?>assets/frontend/images/profileImage2.jpg" alt="">
									</div>
									<div class="titleRating">
										<a href="#" class="reviewerName">S Yugandhar</a>
										<div class="reviewRating">
											<span class="rating r0-5">0.5</span>
											<span class="ratingTime">2 hrs ago</span>
										</div>
									</div>
								</div>
								<a href="#" class="eachRRTitle">ABC Enterprises</a>
								<p class="eachRRTitleInfo">Best review.. Lorem ipsum dolor sit amet, consetetur sadipscing elitr...</p>
							</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="rightSimilarCategories">
				<div class="similarCategoriesBlock">
					<h3 class="thirdHeading">Similar Categories</h3>
					<ul class="categoriesList">
						<li><a href="#">Category 1</a></li>
						<li><a href="#">Category 2</a></li>
						<li><a href="#">Category 3</a></li>
						<li><a href="#">Category 4</a></li>
						<li><a href="#">Category 5</a></li>
						<li><a href="#">Category 6</a></li>
						<li><a href="#">Category 7</a></li>
						<li><a href="#">Category 8</a></li>
						<li><a href="#">Category 9</a></li>
						<li><a href="#">Category 10</a></li>
						<li><a href="#">Category 11</a></li>
						<li><a href="#">Category 12</a></li>
					</ul>
				</div>
			</div>


			<div class="rightSuggestBusiness">
				<div class="haveBusiness">
					<div class="haveBusinessAd" style="background-image:url(assets/frontend/images/cityDrawing.png);">
						Did'nt find what you are looking for?<br>
						<a href="#" class="addBusinessBtn">Suggest a business</a>
					</div>
				</div>
			</div>
		</aside>
	</div>
</div>

