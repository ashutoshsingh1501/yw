<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Add_business extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Business_model');
    }

    function index() {
        $this->load->helper('form');
        $data['category'] = $this->general_model->find_by_sql('SELECT * FROM category WHERE level = "L1" AND status = 1 AND is_deleted = 0 AND is_primary = 1');
        $data['fbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1');
        $data['cbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 order by count desc Limit 4');
        $data['areas'] = $this->general_model->get_enum('area', 'id', 'name');
        $data['cities'] = $this->general_model->get_enum('city', 'id', 'name');
        $data['ratings'] = $this->general_model->get_enum('business_rating', 'id', 'title');

        $data['lbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 order by id desc Limit 2');

        $data['recentrev'] = $this->general_model->find_by_sql('SELECT * FROM `business_review` LEFT JOIN business on business.id = business_review.business_id WHERE business.is_deleted = 0 and business.status = 1 ORDER BY business_review.id Limit 4');

        $data['nusers'] = $this->general_model->get_enum('users', 'id', 'display_name');
        $data['iusers'] = $this->general_model->get_enum('users', 'id', 'image_url');

        $content = $this->load->view('add-business', $data, true);
        $this->render_user($content);
    }

    function validate_business_details() {
        $flag = true;
        $this->load->helper('inflector');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required',array('required'=>'Please provide a business name'));
        $this->form_validation->set_rules('phone', 'Phone No', 'trim|required|is_numeric|min_length[10]|max_length[10]',
                                array('required'=>'Please provide 10 digit valid phone number, without +91 or 0 prefix',
                                    'min_length[10]'=>'Please provide 10 digit valid phone number, without +91 or 0 prefix',
                                    'max_length[10]'=>'Please provide 10 digit valid phone number, without +91 or 0 prefix'));
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email',
                                array('required'=>'Please provide valid email id',
                                    'valid_email'=>'Please provide valid email id'));

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', 'Please fill valid data to proceed further!');
            $flag = false;
        } else {
            $category_name = $this->input->post('category');
            $category_id=$this->general_model->fetch_row_by_sql('select id from category where name rlike "'.$category_name.'"');
			if($category_id['id']!=''){
                $cat_id = $category_id['id'];
            }else{
                $cat_id = '';
            }
            //$status = 6;
            $ipaddress = $_SERVER['REMOTE_ADDR'];
            $data = array(
                'title' => $this->input->post('title'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
//            'area' => $this->input->post('area'), 
//            'city' => $this->input->post('city'), 
//                'website_url' => $this->input->post('website_url'),
//                'facebook_url' => $this->input->post('facebook_url'),
//                'twitter_url' => $this->input->post('twitter_url'),
//                'google_url' => $this->input->post('google_url'),
//                'video_url' => $this->input->post('youtube_url'),
                'address' => $this->input->post('address'),
                'landmark' => $this->input->post('landmark'),
                'pincode' => $this->input->post('pincode'),
            //'category' => $this->input->post('category'),
                'categories_list' => $cat_id,
                'expectation' => $this->input->post('expectation'),
                'created_by' => (null !== $this->input->post('own')) ? $this->input->post('own') : 0,
                'ipaddress' => $ipaddress,
                'status' => (null !== $this->input->post('own')) ? 6 : 5,
                'note' => $category_name
            );
//echo "<pre>";print_r($data);exit;
            $Q = $this->Business_model->insert_business($data);

            if ($Q) {
                if (!empty($_FILES['upload']['name'])) {

                    $config['upload_path'] = './assets/frontend/images';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['overwrite'] = False;
                    $config['remove_spaces'] = True;
                    $_FILES['upload']['name'] = $Q . "_" . $_FILES['upload']['name'];

                    $this->load->library('upload', $config);

                    if (!$this->upload->do_upload('upload')) {
                        $this->session->set_flashdata('message', $this->upload->display_errors('Error', 'Something went wrong while uploading image.'));
                        $flag = false;
                    } else {
                        if ($this->Business_model->update_business('add_business', 'image_path', $_FILES['upload']['name'], 'id', $Q) == FALSE) {
                            $this->session->set_flashdata('message', 'Images uploaded but db entry is missing !');
                            $flag = false;
                        } else {
                            $this->session->set_flashdata('message', 'Successfully added !');
                        }
                    }
                } /*else {
                    $this->session->set_flashdata('message', 'Image not selected !');
                }*/
            } else {
                $this->session->set_flashdata('Business details was not saved, please try again!');
                $flag = false;
            }
//            echo $this->session->flashdata('message');//DEBUG
        }//form Validation condition

        if ($flag) {
            redirect('welcome');
        } else {
            $this->index();
        }
    }

}

?>