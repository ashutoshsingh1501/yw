<?php

session_start();
require_once 'Classes/PHPExcel/IOFactory.php';
include("dbconnect.php");
$target_file = '';
$import_file = '';

if (isset($_POST) && !empty($_POST)) {
    $dir = "bulk-import-files";
    $handle_dir = opendir($dir);
    $target_dir = "bulk-import-files/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
    $import_file = basename($_FILES["fileToUpload"]["name"]);
    $split = explode(".", $import_file);
    $fran = $split[0];
    $extension = $split[1];
    if ($extension != 'xlsx' && $extension != 'xls') {
        header("Location: import?status=0");
        exit;
    }

    if ($extension == 'xlsx' || $extension == 'xls') {
        $objPHPExcel = PHPExcel_IOFactory::load($target_file);
        $data = PHPExcel_IOFactory::load($target_file);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $j = 0;
        $dataTemp = array();
        for ($row = 1; $row <= $highestRow; $row++) {
            $dataTemp[$j] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $j++;
        }

        $dataArray = array();
        for ($i = 0; $i < count($dataTemp); $i++) {
            $j = 0;
            $j = count($dataTemp[$i][0]);

            for ($k = 0; $k < $j; $k++) {
                if ($dataTemp[$i][0][$k] != '') {
                    $dataArray[$i][$k] = $dataTemp[$i][0][$k];
                }
            }
        }

        //echo "<pre>";print_r($dataArray);exit;
        $query_data = '';
        for ($i = 0; $i < count($dataArray); $i++) {

            if ($i != 0) {

                $categories_list = '';
                if (isset($dataArray[$i][3])) {
                    $categories_list = trim($dataArray[$i][3]);
                }
                
                $title = '';
                if (isset($dataArray[$i][1])) {
                    $title = trim($dataArray[$i][1]);
                }
                
                $state_id = '29';$city_id = '1'; //Only for Hyderababd, Telangana
                $area_id = '';$area_name = '';
                if (isset($dataArray[$i][22])) {
                    $area_name = trim($dataArray[$i][22]);
                    $area_data = mysql_query("select id from area where name='$area_name'");
                    $area_result = mysql_fetch_array($area_data);
                    $area_id = (isset($area_result['id']) && !empty($area_result['id'])) ? $area_result['id'] : '0';
                }

                $status = '';
                if (isset($dataArray[$i][32])) {
                    $status = trim($dataArray[$i][32]);
                }

                $contact_person = '';
                if (isset($dataArray[$i][6])) {
                    $contact_person = trim($dataArray[$i][6]);
                }

                $phone = '';
                if (isset($dataArray[$i][9])) {
                    $phone = trim($dataArray[$i][9]);
                }

                $email = '';
                if (isset($dataArray[$i][7])) {
                    $email = trim($dataArray[$i][7]);
                }

                $website_url = '';
                if (isset($dataArray[$i][12])) {
                    $website_url = trim($dataArray[$i][12]);
                }

                $is_featured = '';
                if (isset($dataArray[$i][31])) {
                    $is_featured = trim($dataArray[$i][31]);
                }

                $relevancy_score = '';
                if (isset($dataArray[$i][34])) {
                    $relevancy_score = trim($dataArray[$i][34]);
                }

                $note = '';
                if (isset($dataArray[$i][40])) {
                    $note = trim($dataArray[$i][40]);
                }

                $rating_id = '';
                if (isset($dataArray[$i][33])) {
                    $rating_id = trim($dataArray[$i][33]);
                }

                $is_verified = '';
                if (isset($dataArray[$i][5])) {
                    $is_verified = trim($dataArray[$i][5]);
                }

                $evening_on = '';
                if (isset($dataArray[$i][37])) {
                    $evening_on = trim($dataArray[$i][37]);
                }

                $landline = '';
                if (isset($dataArray[$i][11])) {
                    $landline = trim($dataArray[$i][11]);
                }

                $fax_number = '';
                if (isset($dataArray[$i][15])) {
                    $fax_number = trim($dataArray[$i][15]);
                }

                $tollfree_number = '';
                if (isset($dataArray[$i][16])) {
                    $tollfree_number = trim($dataArray[$i][16]);
                }

                $est_year = '';
                if (isset($dataArray[$i][4])) {
                    $est_year = trim($dataArray[$i][4]);
                }

                $video_url = '';
                if (isset($dataArray[$i][14])) {
                    $video_url = trim($dataArray[$i][14]);
                }

                $page_url = '';
                if (isset($dataArray[$i][17])) {
                    $page_url = trim($dataArray[$i][17]);
                }

                $address = '';
                if (isset($dataArray[$i][19]) && isset($dataArray[$i][20])) {
                    $address = trim($dataArray[$i][19]) . ", " . trim(isset($dataArray[$i][20]));
                }

                $landmark = '';
                if (isset($dataArray[$i][21])) {
                    $landmark = trim($dataArray[$i][21]);
                }

                $pincode = '';
                if (isset($dataArray[$i][18])) {
                    $pincode = trim($dataArray[$i][18]);
                }

                
                $expectation = '';
                if (isset($dataArray[$i][42])) {
                    $expectation = trim($dataArray[$i][42]);
                }

                $description = '';
                if (isset($dataArray[$i][39])) {
                    $description = trim($dataArray[$i][39]);
                }

                $alternative_number = '';
                if (isset($dataArray[$i][10])) {
                    $alternative_number = trim($dataArray[$i][10]);
                }

                $sub_categories = '';
                $uniqueid = '';
                $created_by = '0';
                $created_at = '';
                $modified_at = '';
                $is_deleted = '0';
                $is_review = '';
                $is_paid = '';
                $image_path = '';
                $treview = '';
                $roverall = '';
                $count = '';
                $is_phone = '';
                $is_notification = '';
                $is_landline = '';
                $logo_image = '';
                $twitter_url = '';
                $facebook_url = '';
                $pinterest_url = '';
                $latitude = '';
                $longitude = '';
                $total_like = '';
                $is_offer = '';
                $google_url = '';
                $intragram_url = '';
                $sku = '';
                $offer_image = '';
                $ad_price = '';
                $multiple_location_flag = '';
                $multiple_location_id = '';
                $category_ids = '';
                $display_on_site_phone = "";
                $display_on_site_landline = "";
                $display_on_site_email = "";

                $secondary_email_id = "";
                if (isset($dataArray[$i][8])) {
                    $secondary_email_id = trim($dataArray[$i][8]);
                }

                $secondary_land_line = "";
                if (isset($dataArray[$i][12])) {
                    $secondary_land_line = trim($dataArray[$i][12]);
                }

                $brands = "";
                if (isset($dataArray[$i][26])) {
                    $brands = trim($dataArray[$i][26]);
                }

                $services = "";
                if (isset($dataArray[$i][27])) {
                    $services = trim($dataArray[$i][27]);
                }

                $amenities = "";
                if (isset($dataArray[$i][28])) {
                    $amenities = trim($dataArray[$i][28]);
                }

                $payment_method = "";
                if (isset($dataArray[$i][41])) {
                    $payment_method = trim($dataArray[$i][41]);
                }

                $created_on = date('Y-m-d H:i:s');
                $query_data.='INSERT INTO `business` (`categories_list`, `sub_categories`,`uniqueid`,`title`,`state_id`
						, `area_id`,`city_id`,`status`,`contact_person`,`phone`,`email`,`created_by`
						, `created_at`,`modified_at`,`is_deleted`,`website_url`
						, `is_review`,`is_paid`,`is_featured`,`relevancy_score`,`note`,`rating_id`,`is_verified`
						, `evening_on`,`image_path`,`treview`,`roverall`
						, `count`,`landline`,`fax_number`,`tollfree_number`,`is_phone`,`is_notification`,`is_landline`
						, `est_year`,`video_url`,`logo_image`,`page_url`
						, `address`,`landmark`,`pincode`,`twitter_url`,`facebook_url`,`pinterest_url`,`latitude`
						,`longitude`,`expectation`,`description`,`total_like`,`is_offer`,`alternative_number`,`google_url`
						,`intragram_url`,`sku`,`offer_image`,`ad_price`,`multiple_location_flag`,`multiple_location_id`,`category_ids`
						,`display_on_site_phone`,`display_on_site_landline`,`display_on_site_email`,`secondary_email_id`,`secondary_land_line`,
						`brands`,`services`,`amenities`,`payment_method`)
						VALUES  ( "' . $categories_list . '", "' . $sub_categories . '", "' . $uniqueid . '", "' . $title . '"
						, "' . $state_id . '", "' . $area_id . '", "' . $city_id . '", "' . $status . '"
						, "' . $contact_person . '", "' . $phone . '", "' . $email . '", "' . $created_by . '", "' . $created_at . '", "' . $modified_at . '"
						, "' . $is_deleted . '", "' . $website_url . '", "' . $is_review . '", "' . $is_paid . '", "' . $is_featured . '", 
						"' . $relevancy_score . '", "' . $note . '"
						, "' . $rating_id . '", "' . $is_verified . '", "' . $evening_on . '", "' . $image_path . '", "' . $treview . '", "' . $roverall . '"
						, "' . $count . '", "' . $landline . '", "' . $fax_number . '", "' . $tollfree_number . '", "' . $is_phone . '", "' . $is_notification . '"
						, "' . $is_landline . '", "' . $est_year . '", "' . $video_url . '", "' . $logo_image . '", "' . $page_url . '", "' . $address . '"
						, "' . $landmark . '", "' . $pincode . '", "' . $twitter_url . '", "' . $facebook_url . '", 
						"' . $pinterest_url . '", "' . $latitude . '", "' . $longitude . '", "' . $expectation . '", "' . $description . '", "' . $total_like . '"
						, "' . $is_offer . '", "' . $alternative_number . '", "' . $google_url . '", "' . $intragram_url . '", "' . $sku . '"
						, "' . $offer_image . '", "' . $ad_price . '", "' . $multiple_location_flag . '", "' . $multiple_location_id . '", "' . $category_ids . '"
						, "' . $display_on_site_phone . '", "' . $display_on_site_landline . '", "' . $display_on_site_email . '"
						, "' . $secondary_email_id . '", "' . $secondary_land_line . '", "' . $brands . '"
						, "' . $services . '", "' . $amenities . '", "' . $payment_method . '");';
            }//End of condition of ignoring ZERO Row [Column Names row]
        }//End of Loop
        //Adding $content
        //	 echo nl2br($query_data);exit;
        $backup_name = "business_listings_" . date('d-m-Y') . ".sql";
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . $backup_name . "\"");
        echo $query_data;
        //echo "excel data has been successfully added";exit;	
        header("Location: /import?status=1");
        exit;
    }//End of if excel sheet condition...
}//End of $_POST
?>