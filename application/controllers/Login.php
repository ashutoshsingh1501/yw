<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_model');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function signin() {
        
        $this->load->helper('inflector');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required',array('required' => 'Please provide a username'));
        $this->form_validation->set_rules('pass', 'Password', 'trim|required',array('required' => 'Password cannot be blank'));
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error','Wrong Username or Password!');
            redirect('my-account');
        }else{
            $user = $this->input->post('username');
            $pass = md5($this->input->post('pass'));
            $type = $this->Login_model->get_login_detail($user, $pass);
            if ($type) {
                foreach ($type as $t) {
                    $status = $t->active;
                    $uname = $t->username;
                    $id = $t->id;
                }
                $data = array(
                    'username' => $uname,
                    'active' => $status,
                    'id' => $id
                );
//                $this->session->sess_destroy();
                $this->session->set_userdata($data);
                redirect('my-favorites');
            } else {
                $this->session->set_flashdata('error','Username does not exists!');
                redirect('register');
            }
        } 
    }

    public function signup() {
        
        $this->load->helper('inflector');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fullname', 'Name', 'required|trim',array('required'=>'Please provide your name'));
   
        if (!$this->Login_model->check_username()) {
            if ($this->form_validation->run()==FALSE) {
                $this->session->set_flashdata('error',validation_errors());
                redirect('register');
            }else{
                $data = array(
                    'username' => $this->input->post('email'),
                    'password' => md5($this->input->post('pass1')),
                    'active' => 1
                );
                $id = $this->Login_model->add_account($data);
                if ($id) {
                    $s_data = array(
                        'username' => $this->input->post('email'),
                        'active' => 1,
                    );
                    $this->session->sess_destroy();
                    $this->session->set_userdata($s_data);

                    $detail_data = array(
                        'id' => $id,
                        'name' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'phone' => $this->input->post('phone')
                    );
                    if ($this->Login_model->add_account_detail($detail_data)) {
                        $this->session->set_flashdata('success','Account details saved successfully!');
                        redirect('my-account');
                    } else {
                        $this->session->set_flashdata('error','Account details cannot be saved, please try again!');
                        redirect('welcome'); //redirect to profile page
                    }
                } else {
                    $this->session->set_flashdata('error','Account cannot be created, please try again!');
                    redirect('my-account/register');
                }
            }  
        } else {
            $this->session->set_flashdata('error','Username already in use!');
            redirect('my-account/register');
        }
    }

    public function do_change_password() {

        $this->load->library('form_validation');
//        $this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[4]|max_length[32]');
//        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|callback_validate_currentpassword');
        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|min_length[4]|max_length[32]|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            redirect('my-account/change_password');
        } else {
            if ($query = $this->Login_model->change_password()) {
                $this->session->set_flashdata('success', 'Password changed Successfully !');
                redirect('my-account');
            } else {
                $this->session->set_flashdata('error', 'Please enter correct data. Try agan.!');
                redirect('my-account');
            }
        }
    }
     public function do_forgot_to_change_password() {

        $this->load->library('form_validation');
//        $this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[4]|max_length[32]');
//        $this->form_validation->set_rules('old_password', 'Old Password', 'trim|required|callback_validate_currentpassword');
       
        $this->form_validation->set_rules('password', 'New Password', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|min_length[4]|max_length[32]|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            redirect('my-account/forgot_to_change_password');
        } else {
            if ($query = $this->Login_model->forgot_to_change_password()) {
                $this->session->set_flashdata('success', 'Password changed Successfully !');
                redirect('my-account');
            } else {
                $this->session->set_flashdata('error', 'Please enter correct data. Try agan.!');
                redirect('my-account');
            }
        }
    }

    public function validate_credentials() {
        $msg = $this->Login_model->can_log_in();
        if ($msg == 'true') {
            return true;
        } else if ($msg == 'blocked') {
            $this->form_validation->set_message('validate_credentials', 'User has been blocked please contact Administrator ');
            return false;
        } else {
            $this->form_validation->set_message('validate_credentials', 'Incorrect Username And Password');
            return false;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('welcome');
    }

//    public function index() {
//        $this->session->sess_destroy();
//        $this->load->view('Login/login.php');
//    }
//    public function login_validation() {
//        $this->load->library('form_validation');
//        $this->form_validation->set_rules('username', 'Username', 'required|trim');
//        $this->form_validation->set_rules('password', 'Password', 'required|trim');
//
//        if ($this->form_validation->run()) {
//            $user = $this->input->post('username');
//            $pass = md5($this->input->post('password'));
//            $type = $this->Login_model->get_login_detail($user, $pass);
//            $module = "";
//
//            foreach ($type as $t) {
//                $u_type = $t->type;
//                $status = $t->active;
//                $uname = $t->username;
//                $name = $t->name;
//            }
//
//            $data = array(
//                'username' => $this->input->post('username'),
//                'type' => $u_type,
//                'active' => $status,
//                'uname' => $uname,
//                'name' => $name
//            );
//
//            $this->session->set_userdata($data);    //creating session
//            if ($u_type == 'A') {
//                redirect('Admin_C/dashboard'); //on successful login
//            } else if ($u_type == 'C') {
////                redirect('Client_C/dashboard');
//                redirect('Admin_C/dashboard');
//            } else if ($u_type == 'U') {
////                redirect('User_C/dashboard');
//                redirect('Admin_C/dashboard');
//            }
//        } else {
//            $this->load->view('Login/login.php');  //load login page for wrong credentials
//        }
//    }
//    public function dashboard() {
//        if ($this->session->userdata('type') == 'A' && $this->session->userdata('active') == '1') {
//            $this->load->view('Admin/index.php');
//        } else {
//            redirect('Login/index');
//        }
//    }
}
