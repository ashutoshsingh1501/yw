<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Advertise_with_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        
        $this->load->helper('form');
        $data = "";
        $content = $this->load->view('static-pages/advertise-with-us', $data, true);
        $this->render_user($content);
    }

    function send_mail($receiver_email, $subject, $message) {
        
        $this->load->library('email');
        $this->email->from(SENDER_MAIL, SENDER_NAME);
        $this->email->to($receiver_email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return TRUE;
        } else {
            //echo $this->email->print_debugger();//DEBUG
            return FALSE;
        }
    }

    function advertise_with_us_mail() {

        $this->load->helper('inflector');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required', array('required' => 'Please provide a business name'));
        $this->form_validation->set_rules('user_contact', 'Phone No', 'trim|required|is_numeric|min_length[10]|max_length[10]', array('required' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'min_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'max_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix'));
        $this->form_validation->set_rules('user_mail', 'Email', 'trim|required|valid_email', array('required' => 'Please provide valid email id',
            'valid_email' => 'Please provide valid email id'));
        $this->form_validation->set_rules('user_message', 'Message', 'trim|required', array('required' => 'Please write your message.'));

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {

            $user_name = $this->input->post('user_name');
            $user_contact = $this->input->post('user_contact');
            $user_city = $this->input->post('user_city');
            $ad_type = $this->input->post('ad_type');
            $user_mail = $this->input->post('user_mail');
            $user_message = $this->input->post('user_message');

            $subject = "$user_name - $user_contact want to advertise with yellowpages.in";
            $message = "<br><br>Hi,<br><br>$user_name, wants to advertise with yellowpages.in. Below is the user's information for further communications,<br><br>Name: $user_name<br>Mobile: $user_contact<br>Email: $user_mail<br>Selected city for advertisement: $user_city<br>Purpose of advertisement: Ad Type - $ad_type<br>What they said: $user_message.";
            $ack_subject = "Your email regarding advertisement with yellowpages.in";
            $ack_message = "<br><br>Hi $user_name,<br><br>We have noted your interest in advertising with us and our team would get back to you shortly. Below is the informaiton you shared,<br><br>Name: $user_name<br>Mobile: $user_contact<br>Email: $user_mail<br>Selected city for advertisement: $user_city<br>Purpose of advertisement: Ad type - $ad_type<br>What they said: $user_message.<br><br>If you sent this by mistake or someone else used your email, please email to admin@yellowpages.in to report this issue.";

            //echo "SUCCESS: Contact Us<br/><br/>" . TEAM_MAIL . "<br/>" . $subject . "<br/>" . $message . "<br/>" . $ack_subject . "<br/>" . $ack_message;exit;

            if ($this->send_mail(TEAM_MAIL, $subject, $message)) {
                if($this->send_mail($user_mail, $ack_subject, $ack_message)){
                    $this->session->set_flashdata("success", "Advertisement request mail sent successfully.");
                    redirect('welcome');
                }else{
                    $this->session->set_flashdata("error", "Error in sending Acknowledgement.");
                    $this->index();
                }
            } else {
                $this->session->set_flashdata("error", "Error in sending advertise request.");
                $this->index();
            }
        }
    }
}

?>