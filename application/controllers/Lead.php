<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Lead extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
				
		function index(){
			$content = 	$this->load->view('lead/index',$data=null,true);
			$this->render($content);
		}
		
	}
?>