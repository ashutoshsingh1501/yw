<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_category extends CI_Controller {

    private $data;
    public $id;
    public $category_name;
    public $city_name;
    public $area_name;

    public function __construct() {
        parent::__construct();
        $this->categories = $this->general_model->get_enum('category', 'id', 'name');
    }

    function index($pagination_uri_segment, $pagination_base_uri) {

        //echo "<pre>"; //DEBUG
        //echo $_SERVER['QUERY_STRING']; //DEBUG
        //$temp = parse_str($_SERVER['QUERY_STRING'], $_GET); 
        //print_r($_SERVER); //DEBUG
        //to detect device type
        $this->load->library('user_agent');
        $this->data["mobile"] = $this->agent->is_mobile();

        $c_id = $this->id;
        //get category tree
        $cat_tree = $this->getCategoryTree($c_id);

        //to show in search result message
        $cname_qry = 'SELECT name FROM category WHERE id = ' . $c_id;
        $cat_name = $this->general_model->fetch_row_by_sql($cname_qry);
        $this->data['term'] = " in " . $cat_name['name'];

        //get sub category tree, children of category
        $sub_cat_tree = $cat_tree;
        unset($sub_cat_tree[0]);
        //print_r($sub_cat_tree); //DEBUG
        if (!empty($sub_cat_tree)) {
            $category_filters_query = "Select id,name from category where id in (" . implode(',', $sub_cat_tree) . ")";
            //echo "<br>".$category_filters_query;//DEBUG
            $sub_category_filters = $this->general_model->find_by_sql($category_filters_query);
            $filter_category = array();
            foreach ($sub_category_filters as $key => $r) {
                array_push($filter_category, array('id' => $r->id, 'name' => $r->name));
            }
            $this->data['sub_category_filters'] = $filter_category;
            //print_r($this->data['sub_category_filters']); //DEBUG
        }
        else{
            $filter_category = array();
            $this->data['sub_category_filters'] = $filter_category;
        }

        if (!empty($_GET)) {
            //print_r($_GET); //DEBUG

            $attr = "";$b_qry="";
            foreach ($_GET as $key => $value) {

                //get businesses id for that category tree when no sub category filter selected
                //$businesses_in = " select distinct(business_id) from business_category_mapping where category_id in (" . implode(',', $cat_tree) . ")";

                //if sub category is selected
                if ($key === "category" && isset($value)) {
                    $businesses_in = " select distinct(business_id) from business_category_mapping where category_id in (" . $value . ") ";
                    //echo "<br>".$businesses_in; exit;//DEBUG

                    $fil_suggest_query = "select * from filters where category_id in ( " . $value . " )";
                    //echo "<br>".$fil_suggest_query;//DEBUG
                    $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
                    $this->data['filters'] = $fil_suggest_result;
                    
                } else {
                    $businesses_in = " select distinct(business_id) from business_category_mapping where category_id in (" . implode(',', $cat_tree) . ")";
                    $attr .= "," . $value;
                    $fil_suggest_query = "select * from filters where category_id in ( " . implode(',', $cat_tree) . " )";
                    $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
                    $this->data['filters'] = $fil_suggest_result;
                }
            }
            
            if (strlen($attr)) {
                $attr = substr($attr, 1);
                $atr_arr = explode(',', $attr);

                //for filter specific query
                for ($j = 0; $j < count($atr_arr); $j++) {
                    $b_qry .= " ( find_in_set('" . trim(str_replace("_", " ", $atr_arr[$j])) . "',merged_value) <> 0 ) ";
                    if (($j != count($atr_arr) - 1)) {
                        $b_qry .= " AND "; //CHECK
                    }
                }
            }



            $filter_business_query = "SELECT z.business_id FROM ( "
                    . "SELECT id, business_id, GROUP_CONCAT( value ) AS merged_value FROM business_filter_mapping "
                    . "WHERE business_id IN ( " . $businesses_in . ") GROUP BY business_id ) z ";
            if(strlen($b_qry)){
                $filter_business_query .= " WHERE $b_qry";
            }





            if (count($cat_tree)) {

                if ($pagination_uri_segment == 4) {

                    //only city
                    $this->data['term'] = " in " . $cat_name['name'] . " in " . $this->city_name;
                    $meta_query = ' SELECT DISTINCT * from business where  id in (' . $filter_business_query . ') '
                            . ' AND ( city_name rlike "' . $this->city_name . '") AND status = 1 '
                            . 'ORDER BY relevancy_score DESC ';
                } else if ($pagination_uri_segment == 5) {

                    //city and area both
                    $this->data['term'] = " in " . $cat_name['name'] . " in " . $this->area_name . ", " . $this->city_name;
                    $meta_query = ' SELECT DISTINCT * from business where '
                            . ' city_name rlike "' . $this->city_name . '" AND area_name rlike "' . $this->area_name . '"  AND  id in (' . $filter_business_query . ')'
                            . ' AND status = 1 ORDER BY relevancy_score DESC ';
                } else {
                    // category search without location
                    $meta_query = ' SELECT DISTINCT * from business where status = 1 AND id in (' . $filter_business_query . ') ORDER BY relevancy_score DESC ';
                }
            }
        } else {
            //get filters for category tree
            $fil_suggest_query = "select * from filters where category_id in ( " . implode(',', $cat_tree) . " )";
            // echo "<br>".$fil_suggest_query;//DEBUG
            $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
            $this->data['filters'] = $fil_suggest_result;
            //print_r($this->data['filters']);//DEBUG
            //exit;
            //get businesses id for that category tree when no filter selected
            $businesses_in = "select distinct(business_id) from business_category_mapping where category_id in (" . implode(',', $cat_tree) . ")";
            //echo "<br>".$businesses; //DEBUG

            if (count($cat_tree)) {

                if ($pagination_uri_segment == 4) {

                    //only city
                    $this->data['term'] = " in " . $cat_name['name'] . " in " . $this->city_name;
                    $meta_query = ' SELECT DISTINCT * from business where  id in (' . $businesses_in . ') '
                            . ' AND ( city_name rlike "' . $this->city_name . '") AND status = 1 '
                            . 'ORDER BY relevancy_score DESC ';
                } else if ($pagination_uri_segment == 5) {

                    //city and area both
                    $this->data['term'] = " in " . $cat_name['name'] . " in " . $this->area_name . ", " . $this->city_name;
                    $meta_query = ' SELECT DISTINCT * from business where '
                            . ' city_name rlike "' . $this->city_name . '" AND area_name rlike "' . $this->area_name . '"  AND  id in (' . $businesses_in . ')'
                            . ' AND status = 1 ORDER BY relevancy_score DESC ';
                } else {
                    // category search without location
                    $meta_query = ' SELECT DISTINCT * from business where status = 1 AND id in (' . $businesses_in . ') ORDER BY relevancy_score DESC ';
                }
            }
        }


//        else {
//            $meta_query = 'SELECT * from business where  find_in_set("' . $c_id . '", categories_list) <> 0';
//        }
//
//        echo "<pre>".$meta_query;exit;
//        $segment_count = count($this->uri->segments);
//        $page_uri_segment = 3;
//        if($segment_count == 2 || $segment_count == 3){
//            $page_uri_segment = 3;
//        }elseif($segment_count == 4 || $segment_count == 5){
//            $page_uri_segment = 5;
//        }elseif($segment_count == 4 || $segment_count == 5){
//            $page_uri_segment = 5;
//        }
//        
//        echo $segment_count;
//        print_r($this->uri);
//        exit;
//        
//        
//start of pagination code
        $currentPage = $this->uri->segment($pagination_uri_segment) ? $this->uri->segment($pagination_uri_segment) : 1;
        //echo $currentPage;exit;
        $limitPage = $currentPage - 1;
        $perpage = 10;
        $limitRecords = $perpage;
        $limitPage = $limitPage * $perpage + 1;
        $this->data['businesses'] = $this->general_model->find_by_sql($meta_query);
        if (count($this->data['businesses'])) {
            $this->data['msg'] = count($this->data['businesses']) . " businesses found " . $this->data['term'];
        } else {
            $this->data['msg'] = "Sorry! no businesses found " . $this->data['term'];
        }
        $config['total_rows'] = count($this->data['businesses']);
        $config['per_page'] = $perpage;
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = $pagination_uri_segment;

//        $config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
//        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '>';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="" class="active">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';


        $meta_query .= " LIMIT " . ($limitPage - 1) . "," . $limitRecords;
        //echo "<pre> Meta query with pagination : $meta_query"; exit;
        $search_result = $this->general_model->find_by_sql($meta_query);
        $count = count($search_result);
        $businesses = array();
        foreach ($search_result as $key => $r) {
            if (isset($this->session->userdata["id"])) {
                $uid = $this->session->userdata["id"];
                $bid = $r->id;
                $table = "visitor_business";
                $cond = array(
                    "visitor_id" => $uid,
                    "business_id" => $bid
                );
                $favourite_records = $this->general_model->record_count_where($table, $cond);
                if ($favourite_records == "0") {
                    $favclass = "addFav";
                } else {
                    $favclass = "addFav favActive";
                }
            } else {
                $favclass = "addFav";
            }
            array_push($businesses, array('image' => $r->image_path,
                'business_id' => $r->id,
                'title' => $r->title,
                'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                'page_url' => $r->page_url,
                'is_verified' => $r->is_verified,
                'rating' => $r->rating_name,
                'overall' => $r->roverall,
                'review' => $r->treview,
                'datetime' => $this->getbussinesstime($r->id),
                'categories' => $this->getbussinesscat($r->categories_list),
                'email' => $r->email,
                'website_url' => $r->website_url,
                'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                'area' => (isset($r->area_name)) ? $r->area_name : "",
                'city' => (isset($r->city_name)) ? $r->city_name : "",
                'pincode' => $r->pincode,
//                'location' => $this->getbussinesloc($r->id),
                'latitude' => $r->latitude,
                'favclass' => $favclass,
                'longitude' => $r->longitude,
                'image_note' => $r->image_note,
                'is_logo' => $this->checkThumbnail($r->image_note)
                    )
            );
        }
        $this->data['businesses'] = $businesses;

//        if (count($_GET) > 0)
//            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
//        $config['base_url'] = base_url() . 'search_result/index';
        $config['base_url'] = $pagination_base_uri;
        $config['first_url'] = $config['base_url'] . '/1';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $this->data['pagermessage'] = '';
        $this->data['total_cur_page'] = $this->pagination->cur_page * $config["per_page"];
        // $data['cur_page']=$this->pagination->cur_page;
        $this->data['per_page'] = $config["per_page"];
        if ($this->data['links'] != '') {
            $this->data['msg'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config["per_page"]) + 1) . ' to ' . ($this->pagination->cur_page * $config["per_page"]) . ' of ' . $config["total_rows"] . ' businesses ' . $this->data['term'];
        }
//End of pagination code
        //Category suggest query to fetch, 
        //1. parent category, 
        //2. Sibling categories (but not root) & 
        //3. Children categories

        $cat_suggest_query = 'SELECT id,name from (
                                    SELECT id,name from category where id = ( select parent_id from category where id =' . $c_id . ' )
                                    UNION
                                    SELECT id,name from category where parent_id = ( select parent_id from category where id = ' . $c_id . ' ) AND parent_id != 0 AND id != ' . $c_id . ' 
                                    UNION
                                    SELECT id,name from category where parent_id = ' . $c_id . '
                                )as a ORDER BY id LIMIT 10 ';

        $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
        $cat_suggest = array();
        foreach ($cat_suggest_result as $key => $r) {
            array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name, 'link' => $this->prepareCategoryLink($r->id, $r->name)));
        }
        $this->data['cat_suggest'] = $cat_suggest;

        //featured business
        $f_query = 'select * from business where is_featured=1 ORDER BY id DESC LIMIT 4';
        $featured_result = $this->general_model->find_by_sql($f_query);
        $featured_business = array();
        foreach ($featured_result as $key => $r) {
            array_push($featured_business, array(
                'business_id' => $r->id,
                'title' => $r->title,
                'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                'website_url' => $r->website_url,
                'phone' => ($r->phone == "") ? $r->landline : "+91" . $r->phone,
                'area' => (isset($r->area_name)) ? $r->area_name : "",
                'city' => (isset($r->city_name)) ? $r->city_name : "",
                'pincode' => $r->pincode
                    )
            );
        }
        $this->data['featured_business'] = $featured_business;

        $reviews_qry = "SELECT visitor_detail.profile_pic, visitor_detail.name, business.title, "
                . "business.id, business.area_name, business.city_name, business_review.rating, business_review.comments, business_review.added_on "
                . "FROM `business_review` "
                . "LEFT JOIN business on business.id = business_review.business_id "
                . "LEFT JOIN visitor_detail on visitor_detail.id = business_review.user_id "
                //. "WHERE business.is_deleted = 0 and business.status = 1 "
                . "ORDER BY business_review.id desc Limit 4";


        $reviews_result = $this->general_model->find_by_sql($reviews_qry);
        $reviews = array();
        foreach ($reviews_result as $key => $review) {
            array_push($reviews, array(
                'image' => $review->profile_pic,
                'name' => $review->name,
                'rating' => $review->rating,
                'datetime' => $this->humanTiming(strtotime($review->added_on)),
                'business_id' => $review->id,
                'business' => $review->title,
                'link' => $this->prepareBusinessLink($review->id, $review->title, $review->area_name, $review->city_name),
                'comments' => $review->comments
                    )
            );
        }
        //print_r($reviews);exit; //DEBUG

        $this->data['reviews'] = $reviews;


        $paid_business_query = 'SELECT * FROM business WHERE status=1 AND is_deleted=0 AND is_paid=1 LIMIT 2';
        $paid_business_result = $this->general_model->find_by_sql($paid_business_query);
        $paid_business = array();
        foreach ($paid_business_result as $key => $r) {
            array_push($paid_business, array('image' => $r->image_path,
                'business_id' => $r->id,
                'title' => $r->title,
                'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                'page_url' => $r->page_url,
                'is_verified' => $r->is_verified,
                'rating' => $r->rating_name,
                'overall' => $r->roverall,
                'review' => $r->treview,
                'datetime' => $this->getbussinesstime($r->id),
                'categories' => $this->getbussinesscat($r->categories_list),
                'email' => $r->email,
                'website_url' => $r->website_url,
                'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                'area' => (isset($r->area_name)) ? $r->area_name : "",
                'city' => (isset($r->city_name)) ? $r->city_name : "",
                'pincode' => $r->pincode,
//                'location' => $this->getbussinesloc($r->id),
                'image_note' => $r->image_note,
                'is_logo' => $this->checkThumbnail($r->image_note)
                    )
            );
        }
        $this->data['paid_business'] = $paid_business;

        $content = $this->load->view('category-result', $this->data, true);
        $this->render_inner($content);
    }

    function getCategoryTree($id) {

        $cat_level = $this->general_model->fetch_row_by_sql("select level from category where id = " . $id);
        $level = $cat_level['level'];
        $qry = "";
        $cat_arr = array();
        if ($level === "L1") {
            $qry = " select id from category where parent_id = " . $id
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ) "
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ))";
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        } elseif ($level === "L2") {
            $qry = " select id from category where parent_id = " . $id
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ) ";
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        } elseif ($level === "L3") {
            $qry = " select id from category where parent_id = " . $id;
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        }
        array_unshift($cat_arr, $id);
        return $cat_arr;
    }

    function array_col($arr, $col) {
        $res = array();
        $k = 0;
        for ($i = 0; $i < count($arr); $i++) {
            $res[$k++] = $arr[$i]->$col;
        }
        return $res;
    }

    function prepareBusinessLink($id, $name, $area, $city) {
        $name = $name . "-" . $area . "-" . $city;
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/b/' . strtolower($name) . '/' . $id;
    }

    function humanTiming($time) {
        $time = time() - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'min',
            1 => 'sec'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
        }
    }

    function getbussinesstime($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            //echo 'Not empty';
            //echo $business_id;
            //print_r($businesstime);
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                    //echo 'Case 1';
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 2';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } elseif (date('H:i:s') > $bt->eve_start_time && date('H:i:s') < $bt->eve_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    //echo 'Case 3';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 4';
                    $datetime = date('G:i:s'); //echo $datetime;echo '             ';echo strtotime($datetime);echo '         ';
                    //echo strtotime($bt->eve_start_time);echo '                         ';
                    //echo strtotime($bt->eve_end_time);
                    if (strtotime($datetime) > strtotime($bt->eve_start_time) && strtotime($datetime) < strtotime($bt->eve_end_time)) {
                        //echo 'in';
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                }
            }
        }
        return $result;
    }

    function getbussinesscat($categories_list) {
        //$categories = $this->general_model->get_enum('category', 'id', 'name');
        $businesscat = explode(',', $categories_list);
//        echo "<pre>";print_r($businesscat);exit;
        $result = '';
        if (!empty($businesscat)) {
            foreach ($businesscat as $key => $val) {
                if ($val != $this->id) {
                    if ($key < count($businesscat) - 1) {
                        $result = $result . '<li>' . $this->categories[trim($val)] . ',</li>';
                    } else {
                        $result = $result . '<li>' . $this->categories[trim($val)] . '</li>';
                    }
                }
            }
        }
        return $result;
    }

    function checkThumbnail($id) {

        $file = '/var/www/yellowpages.in/static/business-images/' . $id . '/190x125_' . $id . '-1.jpg';
        return file_exists($file);
    }

    function getCatByNameId($id, $cat_name) {
        $this->id = $id;
        $this->category_name = $cat_name;
        $pagination_base_uri = '/' . $this->category_name . '/' . $this->id;
        $this->index(3, $pagination_base_uri);
    }

    function getCatByCityNameId($id, $cat_name, $city) {
        $this->id = $id;
        $this->category_name = $cat_name;
        $this->city_name = $city;
        $pagination_base_uri = '/' . $this->city_name . '/' . $this->category_name . '/' . $this->id;
        $this->index(4, $pagination_base_uri);
    }

    function getCatByCityAreaNameId($id, $cat_name, $area, $city) {
        $this->id = $id;
        $this->category_name = $cat_name;
        $this->city_name = $city;
        $this->area_name = $area;
        $pagination_base_uri = '/' . $this->city_name . '/' . $this->area_name . '/' . $this->category_name . '/' . $this->id;
        $this->index(5, $pagination_base_uri);
    }

    function prepareCategoryLink($id, $name) {
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/hyderabad/' . strtolower($name) . '/' . $id;
    }

}
