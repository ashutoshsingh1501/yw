<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class City extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		
		function index(){
			$data['states'] = $this->general_model->get_enum('state' , 'id' , 'name');
			$data['cities'] = $this->general_model->get_enum('city' , 'id' , 'name');
			$data['areas'] = $this->general_model->get_enum('area' , 'id' , 'name');
			
			
			$query = "SELECT city.*, Count(area.id) AS total FROM   city LEFT JOIN area ON city.id = area.city_id	WHERE city.is_deleted = 0
			GROUP  BY city.id
			";
			
			
			if(!empty($_GET) && isset($_GET)){
				
				
				$subquery = '   AND  1';
				if(!empty($_GET['state_id']) && isset($_GET['state_id']) && ($_GET['state_id'] != 0)){
					$subquery .= '   AND  city.state_id = ' . $_GET['state_id'] ;
				}
				
				if(!empty($_GET['city_name']) && isset($_GET['city_name'])){
					$subquery .= '   AND  city.name  LIKE  "%' . $_GET['city_name'] .'%"' ;
				}
				
				$query = "SELECT city.*,
				Count(area.id) AS total
				FROM   city 
				LEFT JOIN area 
				ON city.id = area.city_id
				WHERE city.is_deleted = 0  " . $subquery . "
				GROUP  BY city.id
				ORDER  BY city.id";
				
					$query = "SELECT city.*, Count(area.id) AS total FROM   city LEFT JOIN area ON city.id = area.city_id	WHERE city.is_deleted = 0
			" . $subquery . "  GROUP  BY city.id
			";
		
			}
			
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
			$data['results'] = $this->general_model->find_by_sql($query);
			
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = PERPAGE;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			if(isset($_REQUEST['sort']))
			{
				$sort=$_REQUEST['sort'];
				$sortTemp=explode("-",$sort);
				$sort_column=$sortTemp[0];
				$sort_order=$sortTemp[1];
				if($sort_column!='areas')
				{
				$query.=" ORDER BY city.".$sort_column." ".$sort_order;	
				}else
				{
				$query.=" ORDER BY Count(area.id)"." ".$sort_order;	
				}
			}
			
			$query.=" LIMIT ".$currentPage.",".$config['per_page'];			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['base_url'] = base_url().'city/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();	
			$data['pagermessage'] = '';
			if($data['links']!= '') {
				$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			
			$content = 	$this->load->view('city/index',$data,true);
			$this->render($content);
			
			
			
		}
		
		
		function create(){
			$flag = 0;
			if(isset($_POST) && !empty($_POST)){
				$status = (isset($_POST['status'])) ? $_POST['status'] : 0;
				$data=array(
				"created_by" => 0,/*$this->logged_in['id'],*/
				"name" => ucfirst($_POST['name']),
				"state_id" => $_POST['state_id'],
				"status" => $status,
				"created_on" => date('Y-m-d H:i:s')
				);
				if($this->general_model->save('city' , $data)){
					$flag = 1;
				}
			}
			echo $flag;
			exit;
		}
		
		
		function edit(){
			if($_POST){
				$status = (isset($_POST['status'])) ? $_POST['status'] : 0;
				$data=array(
				"name" => ucfirst($_POST['name']),
				"state_id" => $_POST['state_id'],
				"status" => $status,
				"modified_on" => date('Y-m-d H:i:s')
				);
				$this->general_model->update_row("city", $data, $_POST['cityId']);
				echo 1;
				//redirect('city');
			}
			exit;
		}
		
		function delete(){
			if($_POST['id']){
				$this->general_model->raw_sql('UPDATE city SET is_deleted = !(is_deleted) WHERE id ='. $_POST['id']);
				//redirect('category');
				echo 1;
				exit;
			}
			/*if($id){
				$this->general_model->raw_sql('UPDATE city SET is_deleted = !(is_deleted) WHERE id ='. $id);
				redirect('city');
				}
			return false;*/
		}
		
	}
