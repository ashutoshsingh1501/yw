<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Employee extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		
		function index(){
			$data['roles'] = $this->general_model->get_enum_where('role' , 'id' , 'name',array('is_deleted'=>0));
			$data['departments'] = $this->general_model->get_enum_where('department' , 'id' , 'name',array('is_deleted'=>0));
			
			$query = 'SELECT * from users where is_deleted = 0';
			
			if(isset($_GET) && !empty($_GET) && !empty($_GET['role_id']) && !empty($_GET['status_id']) && !empty($_GET['department_id'])){
				$subquery = '   AND  1';
				if($_GET['role_id'] != 0 ){
					$subquery = $subquery . "   and role_id = " . $_GET['role_id'];
				}
				
				if($_GET['status_id'] != 2){
					$subquery = $subquery . "   and status = " . $_GET['status_id'];
				}
				
				if($_GET['department_id'] != 0){
					$subquery = $subquery . "   and department_id = " . $_GET['department_id'];
				}
				
				if(!empty($_GET['emp_name']) && isset($_GET['emp_name'])){
					$subquery .= '   AND  first_name  LIKE  "%' . $_GET['emp_name'] .'%"' ;
				}
					if(isset($_REQUEST['sort']))
			{
				$sort=$_REQUEST['sort'];
				$sortTemp=explode("-",$sort);
				$sort_column=$sortTemp[0];
				$sort_order=$sortTemp[1];
				
				$query.=" ORDER BY ".$sort_column." ".$sort_order;	
			}
				$query = 'SELECT * from users where is_deleted = 0 '. $subquery;
			}
			
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
			$data['results'] = $this->general_model->find_by_sql($query);
			
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = PERPAGE;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			if(isset($_REQUEST['sort']))
			{
				$sort=$_REQUEST['sort'];
				$sortTemp=explode("-",$sort);
				$sort_column=$sortTemp[0];
				$sort_order=$sortTemp[1];
				if($sort_column!='user')
				{
				$query.=" ORDER BY ".$sort_column." ".$sort_order;	
				}else
				{
				$query.=" ORDER BY Count(users.id)"." ".$sort_order;	
				}
			}
			
			$query.=" LIMIT ".$currentPage.",".$config['per_page'];	
			//echo $query;exit;			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['base_url'] = base_url().'city/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();
			$data['pagermessage'] = '';
			if($data['links']!= '') {
				$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			
			$content = 	$this->load->view('employee/index',$data,true);
			$this->render($content);
			
		}
		
		function create(){
			$data['roles'] = $this->general_model->get_enum_where('role' , 'id' , 'name', array('is_deleted'=>0));
			$data['cities'] = $this->general_model->get_enum_where('city' , 'id' , 'name',array('is_deleted' => 0));
			$data['departments'] = $this->general_model->get_enum_where('department' , 'id' , 'name', array('is_deleted' => 0));
			if(isset($_POST) && !empty($_POST)){
				$user_id ='';
				$receive_notification=0;
				if(isset($_POST['receive_notification'])&& $_POST['receive_notification']!='')	{
					$receive_notification=$_POST['receive_notification'];
				}
				$data = array(										
				"username" => $_POST['username'],
				"name" => $_POST['username'],
				"display_name" => ucfirst($_POST['username']),
				"email" => $_POST['useremail'],
				"city_id" => $_POST['usercity'],
				"password" => md5($_POST['password']),
				"phone" => $_POST['userphone'],
				"role_id" => $_POST['userrole'],
				"status" => 0,
				"department_id" => $_POST['usermanager'],
				"receive_notification" =>$receive_notification				
				);
				
				$user_id=$this->general_model->save('users' , $data);
				
				//echo $user_id;exit;
				if(isset($_FILES)&&!empty($_FILES))
				{
					//echo"123";exit;
					
					$target_dir = "uploads/";
					//$old_profile_image_url=$_REQUEST['old_image_url'];
					//$oldimage_url='http://'.$_SERVER['HTTP_HOST'].$target_dir.$old_profile_image_url;
					//unlink($oldimage_url);
					$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
					//echo $target_file ;exit;
					//unlink($oldimage_url);
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);					
					//$user_id=mysql_insert_id();
					//uploads/test.jpg
					$imagedata = array(				
					"image_url" =>  $target_file );
					$this->general_model->update_row('users' , $imagedata,$user_id);
				}
				redirect('employee');
				
			}
			$content = 	$this->load->view('employee/create',$data,true);
			$this->render($content);
		}
		function edit($id=NULL){
		$data['roles'] = $this->general_model->get_enum_where('role' , 'id' , 'name', array('is_deleted'=>0));
			$data['cities'] = $this->general_model->get_enum_where('city' , 'id' , 'name',array('is_deleted' => 0));
			$data['departments'] = $this->general_model->get_enum_where('department' , 'id' , 'name', array('is_deleted' => 0));
		    $data['user'] = $this->general_model->fetch_row_by_key('users' , 'id' ,$id);
			if(isset($_POST) && !empty($_POST)){
				//echo "<pre>"; print_r($_POST);exit;
				$user_id ='';
				//echo $this->session->userdata('logged_in')['id'];		
				//echo "<pre>";print_r($_POST);exit;
				$receive_notification=0;
				if(isset($_POST['receive_notification'])&& $_POST['receive_notification']!='')
				{
					$receive_notification=$_POST['receive_notification'];
				}
				$data = array(										
				"username" => $_POST['username'],
				"name" => $_POST['username'],
				"display_name" => ucfirst($_POST['username']),
				"email" => $_POST['useremail'],
				"city_id" => $_POST['usercity'],
				"password" => md5($_POST['password']),
				"phone" => $_POST['userphone'],
				"role_id" => $_POST['userrole'],				
				"department_id" => $_POST['usermanager'],
				"receive_notification" =>$receive_notification				
				);
				
				$user_id = $this->general_model->update_row('users' , $data,$id);
				if(isset($_FILES)&&!empty($_FILES))
				{
					//echo"123";exit;
					
					$target_dir = "uploads/";
					$old_profile_image_url=$_REQUEST['old_image_url'];
					$oldimage_url='http://'.$_SERVER['HTTP_HOST'].$target_dir.$old_profile_image_url;
					//unlink($oldimage_url);
					$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
					//echo $target_file ;exit;
					//unlink($oldimage_url);
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);					
					
					//uploads/test.jpg
					$imagedata = array(				
					"image_url" =>  $target_file );
					$this->general_model->update_row('users' , $imagedata,$id);
				}
				if(isset($_REQUEST['password'])&&!empty($_REQUEST['password'])&&isset($_REQUEST['confirm_password'])&&!empty($_REQUEST['confirm_password']))
				{
					$passworddata = array(				
					"password" => md5($_REQUEST['password']));
					$this->general_model->update_row('users' , $passworddata,$id);
				}
				redirect('employee');
				
			}
			$content = 	$this->load->view('employee/edit',$data,true);
			$this->render($content);
		}
		
		public function check_user(){
			$data=[];
			if(isset($_REQUEST['email']) && $_REQUEST['email']!=''){
				$email = $this->general_model->fetch_row_where('employees', 'email' , $_REQUEST['email']);
				if(!empty($email)){
					$data['email']=$email->email;
				}
			}
			else if(isset($_REQUEST['username']) && $_REQUEST['username']!=''){
				$username = $this->general_model->fetch_row_where('employees', 'username' , $_REQUEST['username'] );
				if(!empty($username)){
					$data['username']=$username->email;
				}
			}else{}
			echo json_encode($data); 
			
		}
		
		function delete($id=null){
			if($id){
				$this->general_model->raw_sql('UPDATE users SET is_deleted = !(is_deleted) WHERE id ='. $id);
				redirect('employee');
			}
			return false;
		}
		
	}
?>