<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Permission extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		
		function index($id = NULL){
			$data['role'] = $this->general_model->get_enum_where('role' , 'id' , 'name' , array('is_deleted' => 0));
			$data['permission'] = $this->general_model->get_enum_where('permission' , 'id' , 'role_name' , array('is_deleted' => 0));
			$data['permission_row'] = $this->general_model->fetch_row_by_key('permission' , 'id' ,$id);
			$query1="select * from information_schema.columns where table_schema = 'yellowpages3_index' order by table_name,ordinal_position ";
			$col_results = $this->general_model->find_by_sql($query1);
		
			
			
			
			$data['role'] = $this->general_model->get_enum_where('role' , 'id' , 'name' , array('is_deleted' => 0));
						$query = "SELECT permission.*, Count(role.id) AS total FROM permission LEFT JOIN role ON permission.role_id = role.id	WHERE permission.is_deleted = 0 GROUP  BY permission.id";
						
			if(!empty($_POST) && isset($_POST)){
				$role_id='';
			//	echo "<pre>";print_r($_REQUEST);exit;
				$role_id=$_REQUEST['role_id'];
				$features_list=$_REQUEST['features_list'];
				$module_list=$_REQUEST['module_list'];
				//echo $feature_list;exit;
				$Temp_module_list=explode(",",$module_list);
				$current_feature='';$current_module='';
			
					$Temp_features_list=explode(",",$features_list);
					
					for($j=0;$j<count($Temp_features_list);$j++)
					{
						$create_module_feature='';$permission_id='';
						$tempFeature=explode(".",$Temp_features_list[$j]);
						$current_feature=$tempFeature[1];
						$current_module=$tempFeature[0];
						$edit_module_feature='';
						$view_module_feature='';
						$delete_module_feature='';
						$freeze_module_feature='';
						if(isset($_REQUEST['create_'.$current_module.'_'.$current_feature]))
						{
							$create_module_feature=$_REQUEST['create_'.$current_module.'_'.$current_feature];
						}
						if(isset($_REQUEST['edit_'.$current_module.'_'.$current_feature]))
						{
							$edit_module_feature=$_REQUEST['edit_'.$current_module.'_'.$current_feature];
						}
						if(isset($_REQUEST['view_'.$current_module.'_'.$current_feature]))
						{
							$view_module_feature=$_REQUEST['view_'.$current_module.'_'.$current_feature];
						}
						if(isset($_REQUEST['delete_'.$current_module.'_'.$current_feature]))
						{
							$delete_module_feature=$_REQUEST['delete_'.$current_module.'_'.$current_feature];
						}
						if(isset($_REQUEST['freeze_'.$current_module.'_'.$current_feature]))
						{
							$freeze_module_feature=$_REQUEST['freeze_'.$current_module.'_'.$current_feature];
						}
						$existing_feature_checking =array();
						//echo $create_module_feature;exit;
						if($create_module_feature=='on'){$create_module_feature=1;}
						if($edit_module_feature=='on'){$edit_module_feature=1;}
						if($view_module_feature=='on'){$view_module_feature=1;}
						if($delete_module_feature=='on'){$delete_module_feature=1;}
						if($freeze_module_feature=='on'){$freeze_module_feature=1;}
						$sql='select * from permission where module_name='."'$current_module'".' and feature_name='."'$current_feature'".'  and role_id='.$role_id;
						//echo $sql;
						$existing_feature_checking = $this->general_model->find_by_sql($sql);
						//echo "<pre>";print_r($existing_feature_checking);exit;
						
						
						//echo count($existing_feature_checking);
						if(count($existing_feature_checking)>0)
						{
								$permission_id=$existing_feature_checking[0]->id;
								$data = array(				
								"create" =>$create_module_feature,
								"edit" =>$edit_module_feature,
								"view" =>$view_module_feature,
								"delete" =>$delete_module_feature,
								"freeze" =>$freeze_module_feature								
								);
								$this->general_model->update_row("permission", $data,$permission_id);
						}
						//exit;
						
						//echo $sql;exit;
					}
				//}
				//exit;
				header('Location: permission');exit;
			}
			
			
			
			$count_Category_completeness=1;
			$category_query="select * from permission where `module_name`='Category' and (`create`=0 or `edit`=0 or `view`=0 or `delete`=0 or `freeze`=0 )";
			$catgeory_completeness = $this->general_model->find_by_sql($category_query);
			if(count($catgeory_completeness)>0)
			{
				$count_Category_completeness=0;
			}
			$data['count_Category_completeness']=$count_Category_completeness;
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = 100;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			if(isset($_REQUEST['sort']) && $_REQUEST['sort']!='')
			{
				$sort=$_REQUEST['sort'];
				$sortTemp=explode("-",$sort);
				$sort_column=$sortTemp[0];
				$sort_order=$sortTemp[1];
				if($sort_column=='role_id')
				{
				$query.=" ORDER BY role.name"." ".$sort_order;	
				}
				else{
					$query.=" ORDER BY permission.".$sort_column." ".$sort_order;	
				}
			}
			
			$query.=" LIMIT ".($currentPage*$config['per_page']).",".$config['per_page'];	
			//echo $query;exit;
			//echo $query;exit;
			$data['results'] = $this->general_model->find_by_sql($query);
		
			$table_name1='';$table_name2='';
			//$data['results']=array_unique($data['results']);
			//echo "<pre>";print_r($data['results']);exit;
			$module_names = array();
			for($k=0;$k<count($data['results']);$k++)
			{
				 $module_names[] = $data['results'][$k]->module_name;
			}
			$unique_module_names= array_unique($module_names);
			$full_status=array();
			//	echo "<pre>";print_r($unique_module_names);exit;
			
			foreach($unique_module_names as $k=>$v)
			{
				$count_Category_completeness=1;
			$status_query="select * from permission where `module_name`='".$v."' and role_id=1 and (`create`=0 or `edit`=0 or `view`=0 or `delete`=0 or `freeze`=0 )";
		//	echo $status_query;exit;
			$status_completeness = $this->general_model->find_by_sql($status_query);
			if(count($status_completeness)>0)
			{
				
					$full_status[]=0;
				
			}else if(count($status_completeness)==0) {
					$full_status[]=1;
				
			}
			}
			$data['full_status'] = $full_status;
			
			$config['base_url'] = base_url().'permission/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();			
			$data['total'] = $this->general_model->record_count_where('permission', array('is_deleted' => 0));
			$data['pagermessage'] = '';
			if($data['links']!= '') {
					$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			
			$content = 	$this->load->view('permission/index',$data,true);
			$this->render($content);
			
		}	
		
		
		function create(){
			$data['permission'] = $this->general_model->get_enum('permission' , 'id' , 'name');
			
			if(isset($_POST) && !empty($_POST)){				
				$permissionactive = isset($_POST['permissionactive']) ? $_POST['permissionactive'] : 0;
				$data = array(				
				"name" => ucfirst($_POST['newpermission']),
				"report_id" => $_POST['report_id'],				
				"is_deleted" => 0,
				"status" => $permissionactive,				
				"created_by" =>0,
				"created_on" => date('Y-m-d H:i:s')				
				);
				
				$permission_id = $this->general_model->save('permission' , $data);
				redirect('permission');
			}
			$content = 	$this->load->view('permission/create',$data,true);
			$this->render($content);
		}
		
		
		function edit(){
			if($_POST){
				$permissionactive = isset($_POST['permissionactive']) ? $_POST['permissionactive'] : 0;
				$data = array(				
				"name" => ucfirst($_POST['newpermission']),
				"report_id" => $_POST['report_id'],	
				"status" => $permissionactive
				);
				$this->general_model->update_row("permission", $data, $_POST['permissionId']);
				echo 1;
			}
			exit;
		}
		
		function delete($id=null){
			if($id){
				$this->general_model->raw_sql('UPDATE permission SET is_deleted = !(is_deleted) WHERE id ='. $id);
				redirect('permission');
			}
			return false;
		}
		
		function change_status(){
			if(isset($_REQUEST) && !empty($_REQUEST)){
				$this->general_model->update_row("permission", array("status"=>$_REQUEST['status']) , $_REQUEST['id']);
				echo "Permission status has been changed successfully.";
			}
			return false;
		}
		
	}
?>