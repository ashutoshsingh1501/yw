<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class S extends CI_Controller {

    public $data;
    public $categories;

    public function __construct() {

        parent::__construct();

        $svalue = $this->input->get(); //print_r($svalue);exit;
        $this->categories = $this->general_model->get_enum('category', 'id', 'name');

        switch (key($svalue)) {

            case 'q':
                //echo "business listing by search \n\n for single word ".$svalue['q'] . " & location " . $svalue['location'] . "\n\n";

                if (!trim($svalue['q'])) {
                    redirect('welcome/');
                }

                $key = trim($svalue['q']);
                $b_qry = ''; //for matching key in business names
                $c_qry = ''; //for matching key in category names
                $t_qry = ''; //for matching key in category tags

                if (isset($svalue['location']) && trim($svalue['location'])) {

                    $this->data['term'] = " for " . $svalue['q'] . " in " . $svalue['location'];

                    //parse area and city
                    $location = explode(',', trim($svalue['location']));
                    if (count($location) == 1) {
                        //only city
                        $city_name = trim($location[0]);

                        //search for term in business name in that city
                        $b_qry = ' ( select * from business WHERE  business.title RLIKE "' . $key . '" AND (city_name rlike "' . $city_name . '" or area_name rlike "' . $city_name . '" ) '
                                . ' AND status=1 ORDER BY id DESC ) ';

                        //search for term in category name and business of category in that city 
                        $cat_result = $this->general_model->find_by_sql(' select id from category where category.name RLIKE "' . $key . '" ORDER BY id DESC ');
                        $cat_ids = $this->array_col($cat_result, 'id');
                        if (count($cat_ids)) {
                            $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                            for ($i = 1; $i < count($cat_result); $i++) {
                                $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                            }
                            $c_qry = ' ( SELECT * from business where (' . $cat_condition . ') AND ( city_name rlike "' . $city_name . '" OR area_name rlike "' . $city_name . '") AND status=1 ) ';
                        } else {
                            $c_qry = '';
                        }

                        //search for term in category tag and business of category in that city 
                        $cat_result = $this->general_model->find_by_sql('select id from category where  find_in_set("' . $key . '",category.tags) <> 0 ORDER BY id DESC');
                        $cat_ids = $this->array_col($cat_result, 'id');
                        if (count($cat_ids)) {
                            $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                            for ($i = 1; $i < count($cat_result); $i++) {
                                $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                            }
                            $t_qry = ' ( SELECT * from business where (' . $cat_condition . ') AND ( city_name rlike "' . $city_name . '" OR area_name rlike "' . $city_name . '") AND status=1 ) ';
                        } else {
                            $t_qry = '';
                        }
                    } else {
                        //city and area both
                        $city_name = trim($location[1]);
                        $area_name = trim($location[0]);

                        //search for term in business name in that city and area
                        $b_qry = ' ( select * from business WHERE  business.title RLIKE "' . $key . '" AND '
                                . ' ( city_name rlike "' . $city_name . '" OR city_name rlike "' . $area_name . '" OR area_name rlike "' . $city_name . '" OR area_name rlike "' . $area_name . '" ) '
                                . ' ORDER BY id DESC ) AND status=1 ';

                        //search for term in category name and business of category in that city and area
                        $cat_result = $this->general_model->find_by_sql(' select id from category where category.name RLIKE "' . $key . '" ORDER BY id DESC ');
                        $cat_ids = $this->array_col($cat_result, 'id');
                        if (count($cat_ids)) {
                            $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                            for ($i = 1; $i < count($cat_result); $i++) {
                                $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                            }
                            $c_qry = ' ( SELECT * from business where (' . $cat_condition . ') AND '
                                    . ' ( city_name rlike "' . $city_name . '" OR city_name rlike "' . $area_name . '" OR area_name rlike "' . $city_name . '" OR area_name rlike "' . $area_name . '" ) '
                                    . ' ) AND status=1 ';
                        } else {
                            $c_qry = '';
                        }

                        //search for term in category tag and business of category in that city and area
                        $cat_result = $this->general_model->find_by_sql('select id from category where  find_in_set("' . $key . '",category.tags) <> 0 ORDER BY id DESC');
                        $cat_ids = $this->array_col($cat_result, 'id');
                        if (count($cat_ids)) {
                            $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                            for ($i = 1; $i < count($cat_result); $i++) {
                                $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                            }
                            $t_qry = ' ( SELECT * from business where (' . $cat_condition . ') AND '
                                    . ' ( city_name rlike "' . $city_name . '" OR city_name rlike "' . $area_name . '" OR area_name rlike "' . $city_name . '" OR area_name rlike "' . $area_name . '" ) '
                                    . ' ) AND status=1 ';
                        } else {
                            $t_qry = '';
                        }
                    }
                } else {
                    //search for term only
                    $this->data['term'] = " for " . $svalue['q'];

                    //search for term in business name
                    $b_qry = ' ( select * from business WHERE  business.title RLIKE "' . $key . '" AND status=1 ORDER BY id DESC ) ';

                    //search for term in category name
                    $cat_result = $this->general_model->find_by_sql(' select id from category where category.name RLIKE "' . $key . '" ORDER BY id DESC ');
                    $cat_ids = $this->array_col($cat_result, 'id');
                    if (count($cat_ids)) {
                        $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                        for ($i = 1; $i < count($cat_result); $i++) {
                            $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                        }
                        $c_qry = ' ( SELECT * from business where status = 1 AND ' . $cat_condition . ' ) ';
                    } else {
                        $c_qry = '';
                    }

                    //search for term in category tag
                    $cat_result = $this->general_model->find_by_sql('select id from category where  find_in_set("' . $key . '",category.tags) <> 0 ORDER BY id DESC');
                    $cat_ids = $this->array_col($cat_result, 'id');
                    if (count($cat_ids)) {
                        $cat_condition = " find_in_set('" . $cat_ids[0] . "', categories_list) <> 0 ";
                        for ($i = 1; $i < count($cat_result); $i++) {
                            $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', categories_list) <> 0 ";
                        }
                        $t_qry = ' ( SELECT * from business where status = 1 AND ' . $cat_condition . ' ) ';
                    } else {
                        $t_qry = '';
                    }
                }

                $meta_query = 'select * from ( ';
                $meta_query .= $b_qry;

                if ($c_qry != '') {
                    $meta_query .= " UNION ";
                    $meta_query .= $c_qry;
                }

                if ($t_qry != '') {
                    $meta_query .= " UNION ";
                    $meta_query .= $t_qry;
                }

                $meta_query .= ' ) as a ';

//                if (!empty($businesses)) {
                //category suggest for search
                $distinct_cat_id_query = "SELECT DISTINCT categories_list from (" . $meta_query . ") as b limit 10 ";
                $distinct_cat_id_result = $this->general_model->find_by_sql($distinct_cat_id_query);
                $cat_ids_result = $this->array_col($distinct_cat_id_result, "categories_list");
                //echo "<pre>";print_r($cat_ids_result);exit;
                $cat_ids = array();
                $k = 0;
                for ($i = 0; $i < count($cat_ids_result); $i++) {
                    $t_arr = explode(',', $cat_ids_result[$i]);
                    for ($j = 0; $j < count($t_arr); $j++) {
                        $cat_ids[$k++] = $t_arr[$j];
                    }
                }
                $unique_cat_ids = array_unique($cat_ids);
                if (count($unique_cat_ids)) {
                    $cat_suggest_query = "select id, name from category where id in ( " . implode(',', $unique_cat_ids) . " )";
                    $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                    $cat_suggest = array();
                    foreach ($cat_suggest_result as $key => $r) {
                        array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name, 'link' => $this->prepareCategoryLink($r->id, $r->name)));
                    }
                    $this->data['cat_suggest'] = $cat_suggest;
                }


                //for filters    
//                $parent_cat_query = "select distinct parent_id from category where id in ( " . implode(',', $unique_cat_ids) . " )";
//                $parent_id_result = $this->general_model->find_by_sql($parent_cat_query);
//                $pat_ids_result = $this->array_col($parent_id_result, "parent_id");
//                $pat_ids = array();
//                $x = 0;
//                for ($i = 0; $i < count($pat_ids_result); $i++) {
//                    $x_arr = explode(',', $pat_ids_result[$i]);
//                    for ($j = 0; $j < count($x_arr); $j++) {
//                        $pat_ids[$x++] = $x_arr[$j];
//                    }
//                }
//                $parent_ids = array_unique($pat_ids);
//                $fil_suggest_query = "select * from filters where category_id in ( " . implode(',', $unique_cat_ids) . " ) OR category_id in (" . implode(',', $parent_ids) . ")";
//                $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
//                $this->data['filters'] = $fil_suggest_result;

                $f_query = "select * from business where is_featured=1 AND status=1 ORDER BY id DESC LIMIT 1";
                $featured_result = $this->general_model->find_by_sql($f_query);
                $featured_business = array();
                foreach ($featured_result as $key => $r) {
                    array_push($featured_business, array(
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode
                            )
                    );
                }
                $this->data['featured_business'] = $featured_business;
//                }
                //echo "<pre>";print_r($this->data);exit;
                break;

            case 'category':

                if (!trim($svalue['category'])) {
                    redirect('welcome/');
                }

                //to show in search result message
                $cname_qry = 'SELECT name FROM category WHERE id = ' . $svalue['category'];
                $cat_name = $this->general_model->fetch_row_by_sql($cname_qry);
                $this->data['term'] = " in " . $cat_name['name'] . " Category";

                //get category tree
                $cat_tree = $this->getCategoryTree($svalue['category']);

                if (count($cat_tree)) {
                    $cat_condition = " find_in_set('" . $cat_tree[0] . "', categories_list) <> 0 ";
                    for ($i = 1; $i < count($cat_tree); $i++) {
                        $cat_condition .= " OR find_in_set('" . $cat_tree[$i] . "', categories_list) <> 0 ";
                    }
                    if (isset($svalue['location']) && trim($svalue['location'])) {
                        // category search with location

                        $this->data['term'] = " in " . $cat_name['name'] . " Category in " . $svalue['location'];

                        // parse area and city
                        $location = explode(',', trim($svalue['location']));
                        if (count($location) == 1) {
                            //only city
                            $city_name = trim($location[0]);

                            $meta_query = ' SELECT DISTINCT * from business where ' . $cat_condition . ' '
                                    . ' AND ( city_name rlike "' . $city_name . '" OR area_name rlike "' . $city_name . '") AND status=1 '
                                    . 'ORDER BY id ';
                        } else {
                            //city and area both
                            $city_name = trim($location[1]);
                            $area_name = trim($location[0]);

                            $meta_query = ' SELECT DISTINCT * from business where ' . $cat_condition . ' '
                                    . ' AND ( city_name rlike "' . $city_name . '" OR city_name rlike "' . $area_name . '" OR area_name rlike "' . $city_name . '" OR area_name rlike "' . $area_name . '" ) '
                                    . ' AND status=1 ORDER BY id ';
                        }
                    } else {
                        // category search without location
                        $meta_query = ' SELECT DISTINCT * from business where (' . $cat_condition . ') AND status=1 ORDER BY id ';
                    }
                } else {
                    $meta_query = 'SELECT * from business where status=1 AND find_in_set("' . $svalue['category'] . '", categories_list) <> 0';
                }

                //Category suggest query to fetch, 
                //1. parent category, 
                //2. Sibling categories (but not root) & 
                //3. Children categories

                $cat_suggest_query = 'SELECT id,name from (
                                    SELECT id,name from category where id = ( select parent_id from category where id =' . $svalue['category'] . ' )
                                    UNION
                                    SELECT id,name from category where parent_id = ( select parent_id from category where id = ' . $svalue['category'] . ' ) AND parent_id != 0 AND id != ' . $svalue['category'] . ' 
                                    UNION
                                    SELECT id,name from category where parent_id = ' . $svalue['category'] . '
                                )as a ORDER BY id LIMIT 10 ';

                $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                $cat_suggest = array();
                foreach ($cat_suggest_result as $key => $r) {
                    array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name, 'link' => $this->prepareCategoryLink($r->id, $r->name)));
                }
                $this->data['cat_suggest'] = $cat_suggest;

                //featured business
                $f_query = 'select * from business where is_featured=1 ORDER BY id DESC LIMIT 1';
                $featured_result = $this->general_model->find_by_sql($f_query);
                $featured_business = array();
                foreach ($featured_result as $key => $r) {
                    array_push($featured_business, array(
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91" . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode
                            )
                    );
                }
                $this->data['featured_business'] = $featured_business;
                break;

            case 'featured':

                //to show in search result message
                $this->data['term'] = " as featured ";

                $meta_query = 'SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id DESC ';

                //suggested categories
                $distinct_cat_id_query = 'SELECT DISTINCT categories_list from (' . $meta_query . ') as b LIMIT 10 ';
                $distinct_cat_id_result = $this->general_model->find_by_sql($distinct_cat_id_query);
                $cat_ids_result = $this->array_col($distinct_cat_id_result, "categories_list");
                $cat_ids = array();
                $k = 0;
                for ($i = 0; $i < count($cat_ids_result); $i++) {
                    $t_arr = explode(',', $cat_ids_result[$i]);
                    for ($j = 0; $j < count($t_arr); $j++) {
                        $cat_ids[$k++] = $t_arr[$j];
                    }
                }
                $unique_cat_ids = array_unique($cat_ids);
                if (count($unique_cat_ids)) {
                    $cat_suggest_query = "select id, name from category where id in ( " . implode(',', $unique_cat_ids) . " )";
                    $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                    $cat_suggest = array();
                    foreach ($cat_suggest_result as $key => $r) {
                        array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name, 'link' => $this->prepareCategoryLink($r->id, $r->name)));
                    }
                    $this->data['cat_suggest'] = $cat_suggest;
                }

                //for filters
//                $parent_cat_query = "select distinct parent_id from category where id in ( " . implode(',', $unique_cat_ids) . " )";
//                $parent_id_result = $this->general_model->find_by_sql($parent_cat_query);
//                $pat_ids_result = $this->array_col($parent_id_result, "parent_id");
//                $pat_ids = array();
//                $x = 0;
//                for ($i = 0; $i < count($pat_ids_result); $i++) {
//                    $x_arr = explode(',', $pat_ids_result[$i]);
//                    for ($j = 0; $j < count($x_arr); $j++) {
//                        $pat_ids[$x++] = $x_arr[$j];
//                    }
//                }
//                $parent_ids = array_unique($pat_ids);
//                $fil_suggest_query = "select * from filters where category_id in ( " . implode(',', $unique_cat_ids) . " ) OR category_id in (" . implode(',', $parent_ids) . ")";
//                $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
//                $this->data['filters'] = $fil_suggest_result;

                $f_query = " SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id DESC LIMIT 1";
                $featured_result = $this->general_model->find_by_sql($f_query);
                $featured_business = array();
                foreach ($featured_result as $key => $r) {
                    array_push($featured_business, array(
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode
                            )
                    );
                }
                //print_r($featured_business);exit;
                $this->data['featured_business'] = $featured_business;
                break;

            default:redirect('welcome/');
        }

        //start of pagination code
        $currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        //echo $currentPage;exit;
        $limitPage = '';
        $limitRecords = '';
        $limitPage = $currentPage - 1;
        $perpage = '10';
        $limitRecords = $perpage;
        $limitPage = $limitPage * $perpage + 1;
        $this->data['businesses'] = $this->general_model->find_by_sql($meta_query);
        if (count($this->data['businesses'])) {
            $this->data['msg'] = count($this->data['businesses']) . " businesses found " . $this->data['term'];
        } else {
            $this->data['msg'] = "Sorry! no businesses found " . $this->data['term'];
        }
        $config['total_rows'] = count($this->data['businesses']);
        $config['per_page'] = $perpage;
        $config['use_page_numbers'] = TRUE;

//        $config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
//        $config['full_tag_close'] = '</ul></div><!--pagination-->';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '>';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="" class="active">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';


        $meta_query .= " LIMIT " . ($limitPage - 1) . "," . $limitRecords;
        //echo "<pre> Meta query with pagination : $meta_query"; exit;
        $search_result = $this->general_model->find_by_sql($meta_query);
        $count = count($search_result);
        $businesses = array();
        foreach ($search_result as $key => $r) {
            if (isset($this->session->userdata["id"])) {
                $uid = $this->session->userdata["id"];
                $bid = $r->id;
                $table = "visitor_business";
                $cond = array(
                    "visitor_id" => $uid,
                    "business_id" => $bid
                );
                $favourite_records = $this->general_model->record_count_where($table, $cond);
                if ($favourite_records == "0") {
                    $favclass = "addFav";
                } else {
                    $favclass = "addFav favActive";
                }
            } else {
                $favclass = "addFav";
            }
            array_push($businesses, array('image' => $r->image_path,
                'business_id' => $r->id,
                'title' => $r->title,
                'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                'page_url' => $r->page_url,
                'is_verified' => $r->is_verified,
                'rating' => $r->rating_name,
                'overall' => $r->roverall,
                'review' => $r->treview,
                'datetime' => $this->getbussinesstime($r->id),
                'categories' => $this->getbussinesscat($r->categories_list),
                'email' => $r->email,
                'website_url' => $r->website_url,
                'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                'area' => (isset($r->area_name)) ? $r->area_name : "",
                'city' => (isset($r->city_name)) ? $r->city_name : "",
                'pincode' => $r->pincode,
//                'location' => $this->getbussinesloc($r->id),
                'latitude' => $r->latitude,
                'favclass' => $favclass,
                'longitude' => $r->longitude,
                'image_note' => $r->image_note,
                'is_logo' => $this->checkThumbnail($r->image_note)
                    )
            );
        }
        $this->data['businesses'] = $businesses;

        if (count($_GET) > 0)
            $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['base_url'] = base_url() . 'search_result/index';
        $config['first_url'] = $config['base_url'] . '?' . http_build_query($_GET);
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $this->data['pagermessage'] = '';
        $this->data['total_cur_page'] = $this->pagination->cur_page * $config["per_page"];
        // $data['cur_page']=$this->pagination->cur_page;
        $this->data['per_page'] = $config["per_page"];
        if ($this->data['links'] != '') {
            $this->data['pagermessage'] = 'Showing ' . ((($this->pagination->cur_page - 1) * $config["per_page"]) + 1) . ' to ' . ($this->pagination->cur_page * $config["per_page"]) . ' of ' . $config["total_rows"];
        }
        //End of pagination code

        $reviews_qry = "SELECT visitor_detail.profile_pic, visitor_detail.name, business.title, "
                . "business.id, business.area_name, business.city_name, business_review.rating, business_review.comments, business_review.added_on "
                . "FROM `business_review` "
                . "LEFT JOIN business on business.id = business_review.business_id "
                . "LEFT JOIN visitor_detail on visitor_detail.id = business_review.user_id "
                //. "WHERE business.is_deleted = 0 and business.status = 1 "
                . "ORDER BY business_review.id desc Limit 4";


        $reviews_result = $this->general_model->find_by_sql($reviews_qry);
        $reviews = array();
        foreach ($reviews_result as $key => $review) {
            array_push($reviews, array(
                'image' => $review->profile_pic,
                'name' => $review->name,
                'rating' => $review->rating,
                'datetime' => $this->humanTiming(strtotime($review->added_on)),
                'business_id' => $review->id,
                'business' => $review->title,
                'link' => $this->prepareBusinessLink($review->id, $review->title, $review->area_name, $review->city_name),
                'comments' => $review->comments
                    )
            );
        }
        //print_r($reviews);exit; //DEBUG

        $this->data['reviews'] = $reviews;

        $paid_business_query = 'SELECT * FROM business WHERE status=1 AND is_deleted=0 AND is_paid=1 LIMIT 2';
        $paid_business_result = $this->general_model->find_by_sql($paid_business_query);
        $paid_business = array();
        foreach ($paid_business_result as $key => $r) {
            array_push($paid_business, array('image' => $r->image_path,
                'business_id' => $r->id,
                'title' => $r->title,
                'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                'page_url' => $r->page_url,
                'is_verified' => $r->is_verified,
                'rating' => $r->rating_name,
                'overall' => $r->roverall,
                'review' => $r->treview,
                'datetime' => $this->getbussinesstime($r->id),
                'categories' => $this->getbussinesscat($r->categories_list),
                'email' => $r->email,
                'website_url' => $r->website_url,
                'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                'area' => (isset($r->area_name)) ? $r->area_name : "",
                'city' => (isset($r->city_name)) ? $r->city_name : "",
                'pincode' => $r->pincode,
//                'location' => $this->getbussinesloc($r->id),
                'image_note' => $r->image_note,
                'is_logo' => $this->checkThumbnail($r->image_note)
                    )
            );
        }
        $this->data['paid_business'] = $paid_business;
    }

    function index() {
        $content = $this->load->view('search-result', $this->data, true);
        $this->render_inner($content);
    }

    function humanTiming($time) {
        $time = time() - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'min',
            1 => 'sec'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
        }
    }

    function getbussinesstime($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            //echo 'Not empty';
            //echo $business_id;
            //print_r($businesstime);
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                    //echo 'Case 1';
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 2';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } elseif (date('H:i:s') > $bt->eve_start_time && date('H:i:s') < $bt->eve_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    //echo 'Case 3';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 4';
                    $datetime = date('G:i:s'); //echo $datetime;echo '             ';echo strtotime($datetime);echo '         ';
                    //echo strtotime($bt->eve_start_time);echo '                         ';
                    //echo strtotime($bt->eve_end_time);
                    if (strtotime($datetime) > strtotime($bt->eve_start_time) && strtotime($datetime) < strtotime($bt->eve_end_time)) {
                        //echo 'in';
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                }
            }
        }
        return $result;
    }

    function getbussinesscat($categories_list) {
        //$categories = $this->general_model->get_enum('category', 'id', 'name');
        $businesscat = explode(',', $categories_list);
        //print_r($businesscat);
        $result = '';
        if (!empty($businesscat)) {
            foreach ($businesscat as $key => $val) {
                $result = $result . '<li>' . $this->categories[trim($val)] . '</li>';
            }
        }
        return $result;
    }

    function getbussinesloc($business_id) {
        $business = $this->general_model->get_enum('business', 'id', 'title');
        $businessloc = $this->general_model->find_by_sql('SELECT * FROM business WHERE title = "' . $business[$business_id] . '"');
        return count($businessloc);
    }

    function array_col($arr, $col) {
        $res = array();
        $k = 0;
        for ($i = 0; $i < count($arr); $i++) {
            $res[$k++] = $arr[$i]->$col;
        }
        return $res;
    }

    function refine() {
        // $ratings = $this->general_model->get_enum('business_rating', 'id', 'title');
        //$areas = $this->general_model->get_enum('area', 'id', 'name');
        // $cities = $this->general_model->get_enum('city', 'id', 'name');
        $svalue = $this->input->get(); //echo key($svalue);exit;
        //$svalue=Array ( 'q' => 'Banana Leaf', 'location' => 'Hyderabad', '81' => 'Afghani,Italian', '82' => 'Lunch' );
        switch (key($svalue)) {
            case 'q':
                $keys = array_keys($svalue);

                if (!trim($svalue['q']) || !trim($svalue['location'])) {
                    redirect('welcome/');
                }

                //search business by business name
                $keys = array_keys($svalue);
                $values = array_values($svalue);

                if (count($svalue) > 2) {
                    $b_qry = " SELECT z.* FROM ( "
                            . "SELECT id, business_id, GROUP_CONCAT(value) AS merged_value FROM business_filter_mapping GROUP BY business_id ) z "
                            . "WHERE ";
                    for ($i = 2; $i < count($keys); $i++) {
                        $fid = $keys[$i];
                        $atr_arr = explode(',', $values[$i]);

                        //for filter specific query
                        for ($j = 0; $j < count($atr_arr); $j++) {
                            $b_qry .= " ( find_in_set('" . trim(str_replace("_", " ", $atr_arr[$j])) . "',merged_value) <> 0 ) ";
                            if (($j != count($atr_arr) - 1 ) || ($i != count($values) - 1 )) {
                                $b_qry .= " AND "; //CHECK
                            }
                        }
                    }
                } else {
                    $b_qry = 'select * from business WHERE business.title RLIKE "' . $svalue['q'] . '" ORDER BY id DESC';
                }

                //search business by category name
                $cat_result = $this->general_model->find_by_sql('select id from category where category.name RLIKE "' . $svalue['q'] . '" ORDER BY id DESC');
                $cat_ids = $this->array_col($cat_result, 'id');
                if (count($cat_ids)) {
                    $cat_condition = " find_in_set('" . $cat_ids[0] . "', b.categories_list) <> 0 ";
                    for ($i = 1; $i < count($cat_result); $i++) {
                        $cat_condition .= " OR find_in_set('" . $cat_ids[$i] . "', b.categories_list) <> 0 ";
                    }
                    if (count($keys) > 2) {
                        $c_qry = '(SELECT b.* from business b join business_filter_mapping bm where ' . $cat_condition . ' AND (' . $fil_condition1 . ') AND (' . $fil_condition . ') )';
                    } else {
                        $c_qry = '(SELECT * from business b where ' . $cat_condition . ' )';
                    }
                } else {
                    $c_qry = '';
                }

                //search business by tag name
                $cat_result = $this->general_model->find_by_sql('select id from category where  find_in_set("' . $svalue['q'] . '",category.tags) <> 0 ORDER BY id DESC');
                $cat_ids = $this->array_col($cat_result, 'id');
                if (count($cat_ids)) {
                    $cat_condition = ' find_in_set("' . $cat_ids[0] . '", b.categories_list) <> 0 ';
                    for ($i = 1; $i < count($cat_result); $i++) {
                        $cat_condition .= ' OR find_in_set("' . $cat_ids[$i] . '", b.categories_list) <> 0 ';
                    }
                    if (count($key) > 2) {
                        $t_qry = '(SELECT b.* from business b join business_filter_mapping bm where ' . $cat_condition . ' AND (' . $fil_condition1 . ') AND (' . $fil_condition . ') )';
                    } else {
                        $t_qry = '(SELECT * from business b where ' . $cat_condition . ' )';
                    }
                } else {
                    $t_qry = '';
                }

                $meta_query = 'select * from ( ';
                $meta_query .= $b_qry;
                if ($c_qry != '') {
                    $meta_query .= " UNION ";
                    $meta_query .= $c_qry;
                }
                if ($t_qry != '') {
                    $meta_query .= " UNION ";
                    $meta_query .= $t_qry;
                }
                $meta_query .= ' ) as a ';

                $search_result = $this->general_model->find_by_sql($meta_query);
                $count = count($search_result);

                $msg = "Sorry! no results found for '" . $svalue['q'] . "' in " . $svalue['location'];
                if ($count) {
                    $msg = $count . " results found for '" . $svalue['q'] . "' in " . $svalue['location'];
                }
                $this->data['msg'] = $msg;

                $businesses = array();
                foreach ($search_result as $key => $r) {

                    if (isset($this->session->userdata["id"])) {
                        $uid = $this->session->userdata["id"];
                        $bid = $r->id;
                        $table = "visitor_business";
                        $cond = array(
                            "visitor_id" => $uid,
                            "business_id" => $bid
                        );
                        $favourite_records = $this->general_model->record_count_where($table, $cond);
                        if ($favourite_records == "0") {
                            $favclass = "addFav";
                        } else {
                            $favclass = "addFav favActive";
                        }
                    } else {
                        $favclass = "addFav";
                    }
                    $location = $this->getbussinesloc($r->id);

                    if ($location > 1) {
                        $locations = $location . " Locations";
                    } else {
                        $locations = $location . " Location";
                    }

                    if ($r->treview > 1) {

                        $review = $r->treview . " reviews";
                    } else {
                        $review = $r->treview . " review";
                    }




                    if ($r->pincode) {
                        $city_pincode = $r->city_name . " - " . $r->pincode;
                    } else {
                        $city_pincode = $r->city_name;
                    }
                    $websiteurl1 = "http://" . $r->website_url;
                    $datetime = $this->getbussinesstime($r->id);
                    if ($datetime) {
                        $datetime = "<strong>Open now</strong> - " . $datetime;
                    } else {
                        $datetime = "<strong> </strong>";
                    }
                    if ($r->is_verified == "1") {
                        $verified = 'ypApproved';
                    } else {
                        $verified = '';
                    }

                    $msg .= '<li><div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <img width="170" height="170" src="' . STYLEURL . 'logo/' . $r->image_note . '-0.jpg" alt="">
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <a href="' . base_url() . 'business_detail?id=' . $r->id . '" class="eachPopularTitle">
                                                    ' . $r->title . '
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <a target="_blank" href="' . $websiteurl1 . '"><span class="openNewWindow"></span></a>
                                                    <span class="' . $verified . '"></span>
                                                    <span class="certified aaP">' . $r->rating_name . '</span>
                                                </div>
                                            </div>
                                            <div class="eachPopularRateOpen">
                                                <div class="eachPopularRatingBlock">
                                                    <span class="rating r' . $r->roverall . '">' . $r->roverall . '</span>
                                                    <a href="#" class="ratingCount">' . $review . '</a>
                                                </div>
                                                <div class="openNow">' . $datetime . '</div>
                                            </div>
                                            <ul class="eachPopularTagsList">
                                            <li></li>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <a href="mailto:' . $r->email . '">' . $r->email . '</a>
                                                <a href="' . $websiteurl1 . '" target="_blank">website</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="' . $favclass . '" onclick="addtofavorite(' . $r->id . ')">Add Favorite</div>
                                        <a class="businessContact" href="tel:' . $r->phone . '">' . $r->phone . '</a>
                                        <address class="businessArea">
                                            <strong>' . $r->area_name . '</strong>
                                           ' . $city_pincode . '
                                        </address>
                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount">' . $locations . '</span>
                                            <a  href="//maps.google.com/maps?f=d&amp;daddr=17.444442,78.379597&amp;hl=en" target="_blank">Directions</a>
                                        </div>
                                    </div>
                                </div>
                            </li>';

                    array_push($businesses, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'rating' => $r->rating_name,
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesstime($r->id),
                        'categories' => $this->getbussinesscat($r->categories_list),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode,
                        'location' => $this->getbussinesloc($r->id),
                        'latitude' => $r->latitude,
                        'favclass' => $favclass,
                        'longitude' => $r->longitude,
                        'image_note' => $r->image_note,
                        'is_logo' => $r->is_logo
                            )
                    );
                }
                $msg = "<ul class='popularThisWeekList'>" . $msg . "</ul>"; // Content for Data

                echo $msg;
                $this->data['businesses'] = $businesses;
                if (!empty($businesses)) {
                    //category suggest for search
                    $distinct_cat_id_query = "SELECT DISTINCT categories_list from (" . $meta_query . ") as b";
                    $distinct_cat_id_result = $this->general_model->find_by_sql($distinct_cat_id_query);
                    $cat_ids_result = $this->array_col($distinct_cat_id_result, "categories_list");
                    $cat_ids = array();
                    $k = 0;
                    for ($i = 0; $i < count($cat_ids_result); $i++) {
                        $t_arr = explode(',', $cat_ids_result[$i]);
                        for ($j = 0; $j < count($t_arr); $j++) {
                            $cat_ids[$k++] = $t_arr[$j];
                        }
                    }
                    $unique_cat_ids = array_unique($cat_ids);
                    $cat_suggest_query = "select id, name from category where id in ( " . implode(',', $unique_cat_ids) . " )";
                    $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                    $cat_suggest = array();
                    foreach ($cat_suggest_result as $key => $r) {
                        array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name));
                    }
                    $this->data['cat_suggest'] = $cat_suggest;
                    $parent_cat_query = "select distinct parent_id from category where id in ( " . implode(',', $unique_cat_ids) . " )";
                    $parent_id_result = $this->general_model->find_by_sql($parent_cat_query);
                    $pat_ids_result = $this->array_col($parent_id_result, "parent_id");
                    $pat_ids = array();
                    $x = 0;
                    for ($i = 0; $i < count($pat_ids_result); $i++) {
                        $x_arr = explode(',', $pat_ids_result[$i]);
                        for ($j = 0; $j < count($x_arr); $j++) {
                            $pat_ids[$x++] = $x_arr[$j];
                        }
                    }
                    $parent_ids = array_unique($pat_ids);
                    $fil_suggest_query = "select * from filters where category_id in ( " . implode(',', $unique_cat_ids) . " ) OR category_id in (" . implode(',', $parent_ids) . ")";
                    $fil_suggest_result = $this->general_model->find_by_sql($fil_suggest_query);
                    $this->data['filters'] = $fil_suggest_result;

                    $f_query = "select * from business where is_featured=1 ORDER BY id DESC LIMIT 1";
//                if ($count) {
//                    $f_query = $meta_query . " WHERE is_featured=1 ORDER BY id DESC LIMIT 1";
//                }
                    $featured_result = $this->general_model->find_by_sql($f_query);
                    $featured_business = array();
                    foreach ($featured_result as $key => $r) {
                        array_push($featured_business, array('image' => $r->image_path,
                            'business_id' => $r->id,
                            'title' => $r->title,
                            'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                            'page_url' => $r->page_url,
                            'is_verified' => $r->is_verified,
                            'rating' => $r->rating_name,
                            'overall' => $r->roverall,
                            'review' => $r->treview,
                            'datetime' => $this->getbussinesstime($r->id),
                            'categories' => $this->getbussinesscat($r->categories_list),
                            'email' => $r->email,
                            'website_url' => $r->website_url,
                            'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                            'area' => (isset($r->area_name)) ? $r->area_name : "",
                            'city' => (isset($r->city_name)) ? $r->city_name : "",
                            'pincode' => $r->pincode,
                            'location' => $this->getbussinesloc($r->id),
                            'image_note' => $r->image_note
                                )
                        );
                    }
                    $this->data['featured_business'] = $featured_business;
                }
                //echo "<pre>";print_r($this->data);exit;
                break;
/*
 * Implemented in search_category controller
 * 
 * 
            case 'category':
                //search business by category name
                //we will have to traverse through category tree : TODO

                $keys = array_keys($svalue);
                //$key = $keys[2];
                for ($i = 1; $i < count($keys); $i++) {
                    $filter_id = $keys[$i];
                    $attributes = str_replace('_', ' ', $svalue[$filter_id]);
                    $attr_value = explode(',', $attributes);
                    $fil_condition = " find_in_set('" . $attr_value[0] . "', bm.value)";
                    for ($j = 1; $j < count($attr_value); $j++) {
                        $fil_condition .= " OR find_in_set('" . $attr_value[$j] . "',bm.value)";
                    }
                    $fil_condition1 = " find_in_set('" . $keys[1] . "', bm.filter_id)";
                    for ($k = 3; $k < count($keys); $j++) {
                        $fil_condition1 .= " OR find_in_set('" . $keys[$k] . "',bm.filter_id)";
                    }
                }
                $cname = $svalue['category'];
                $cname = str_replace('%20', ' ', $cname);
                $cat_result = $this->general_model->find_by_sql('select id from category where category.name RLIKE "' . $cname . '" ORDER BY id DESC');
                $cat_ids = $this->array_col($cat_result, 'id');
                if (count($cat_ids)) {
                    $cat_condition = ' find_in_set("' . $cat_ids[0] . '", categories_list) <> 0 ';
                    for ($i = 1; $i < count($cat_result); $i++) {
                        $cat_condition .= ' OR find_in_set("' . $cat_ids[$i] . '", categories_list) <> 0 ';
                    }
                    if (count($keys) >= 1) {
                        $c_qry = '(SELECT b.* from business b join business_filter_mapping bm ON b.id=bm.business_id where ' . $cat_condition . ' AND (' . $fil_condition1 . ') AND (' . $fil_condition . ') ORDER BY b.id DESC )';
                    } else {
                        $c_qry = '(SELECT * from business where ' . $cat_condition . ' )';
                    }
                } else {
                    $c_qry = '';
                }

                $msg = "Sorry! no results found for '" . $cname . "' category.";
                if ($c_qry != '') {
                    $meta_query = 'select * from ( ';
                    $meta_query .= $c_qry;
                    $meta_query .= ' ) as a ';
//echo $meta_query;exit;
                    $search_result = $this->general_model->find_by_sql($meta_query);
                    $count = count($search_result);
                    if ($count > 1) {
                        $msg = $count . " results found for '" . $cname . "' category.";
                    }
                    $businesses = array();
                    foreach ($search_result as $key => $r) {
                        if (isset($this->session->userdata["id"])) {
                            $uid = $this->session->userdata["id"];
                            $bid = $r->id;
                            $table = "visitor_business";
                            $cond = array(
                                "visitor_id" => $uid,
                                "business_id" => $bid
                            );
                            $favourite_records = $this->general_model->record_count_where($table, $cond);
                            if ($favourite_records == "0") {
                                $favclass = "addFav";
                            } else {
                                $favclass = "addFav favActive";
                            }
                        } else {
                            $favclass = "addFav";
                        }

                        array_push($businesses, array('image' => $r->image_path,
                            'business_id' => $r->id,
                            'title' => $r->title,
                            'page_url' => $r->page_url,
                            'is_verified' => $r->is_verified,
                            'rating' => $r->rating_name,
                            'overall' => $r->roverall,
                            'review' => $r->treview,
                            'datetime' => $this->getbussinesstime($r->id),
                            'categories' => $this->getbussinesscat($r->categories_list),
                            'email' => $r->email,
                            'website_url' => $r->website_url,
                            'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                            'area' => (isset($r->area_name)) ? $r->area_name : "",
                            'city' => (isset($r->city_name)) ? $r->city_name : "",
                            'pincode' => $r->pincode,
                            'location' => $this->getbussinesloc($r->id),
                            'latitude' => $r->latitude,
                            'favclass' => $favclass,
                            'longitude' => $r->longitude,
                            'image_note' => $r->image_note,
                            'is_logo' => $r->is_logo
                                )
                        );
                    }
                    $this->data['businesses'] = $businesses;
                }
                $this->data['msg'] = $msg;


                $cat_suggest_query = 'SELECT id,name from (
                                    SELECT id,name from category where id = (
                                        select parent_id from category where name ="' . $cname . '"
                                    )
                                    UNION
                                    SELECT id,name from category where parent_id = (
                                        select parent_id from category where id = (
                                            select id from category where name ="' . $cname . '"
                                        )
                                    ) AND id != ( select id from category where name ="' . $cname . '" )
                                    UNION
                                    SELECT id,NAME from category where parent_id = ( 
                                        select id from category where name ="' . $cname . '" 
                                    )
                                )as a ORDER BY id ';
                $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                $cat_suggest = array();
                foreach ($cat_suggest_result as $key => $r) {
                    array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name));
                }
                $this->data['cat_suggest'] = $cat_suggest;

                //featured business
                $f_query = 'select * from business where is_featured=1 ORDER BY id DESC LIMIT 1';
//                if ($count) {
//                    $f_query = $meta_query . ' WHERE is_featured=1 ORDER BY id DESC LIMIT 1';
//                }
                $featured_result = $this->general_model->find_by_sql($f_query);
                $featured_business = array();
                foreach ($featured_result as $key => $r) {
                    array_push($featured_business, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'rating' => $r->rating_name,
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesstime($r->id),
                        'categories' => $this->getbussinesscat($r->categories_list),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91 " . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode,
                        'location' => $this->getbussinesloc($r->id))
                    );
                }
                $this->data['featured_business'] = $featured_business;
                //echo "<pre>";print_r($this->data['businesses']);exit;
                break;

 * 
 * Implemented in search_category controller
 *  */
            case 'featured':
                $keys = array_keys($svalue);
                $values = array_values($svalue);
                if (count($svalue) > 1) {
                    $meta_query = " SELECT z.* FROM ( "
                            . "SELECT id, business_id, GROUP_CONCAT(value) AS merged_value FROM business_filter_mapping GROUP BY business_id ) z "
                            . "join business b on b.id=z.business_id "
                            . "WHERE b.is_featured=1 AND b.is_deleted = 0 AND b.status = 1 AND ";
                    for ($i = 1; $i < count($keys); $i++) {
                        $fid = $keys[$i];
                        $atr_arr = explode(',', $values[$i]);

                        //for filter specific query
                        for ($j = 0; $j < count($atr_arr); $j++) {
                            $meta_query .= " ( find_in_set('" . trim(str_replace("_", " ", $atr_arr[$j])) . "',merged_value) <> 0 ) ";
                            if (($j != count($atr_arr) - 1 ) || ($i != count($values) - 1 )) {
                                $meta_query .= " AND ";
                            }
                        }
                    }
                } else {
                    $meta_query = 'select * from business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id DESC';
                }
                $search_result = $this->general_model->find_by_sql($meta_query);
                $count = count($search_result);

                //strict checks for pagination
                // 1. queryString parameter page should exists
                // 2. queryString parameter page, value should be parseable to integer
                // 3. queryString parameter page, integer value should be greater than 0
                if (isset($svalue['page']) && is_int(intval($svalue['page'])) && intval($svalue['page']) > 0) {
                    $result_page_count = ceil($count / PERPAGE_SEARCH_LIST);
                    $this->data['result_page_count'] = $result_page_count;
//                  echo "page=".intval($svalue['page']);
                    $offset = (intval($svalue['page']) - 1) * PERPAGE_SEARCH_LIST;
                    $fbl_qry = $f_qry . " LIMIT " . PERPAGE_SEARCH_LIST . " OFFSET $offset";
//                  echo "qry $fbl_qry";
                    $search_result = $this->general_model->find_by_sql($fbl_qry);
                }
                //Pagination ENDS

                $msg = "Sorry!, no featured business found.";
                if ($count > 1) {
                    $msg = $count . " featured businesses found.";
                }
                $this->data['msg'] = $msg;

                $businesses = array();
                $msg = "";
                //$qry = "SELECT * from business ";
                $search_result = $this->general_model->find_by_sql($meta_query);
                foreach ($search_result as $key => $r) {
                    if (isset($this->session->userdata["id"])) {
                        $uid = $this->session->userdata["id"];
                        $bid = $r->id;
                        $table = "visitor_business";
                        $cond = array(
                            "visitor_id" => $uid,
                            "business_id" => $bid
                        );
                        $favourite_records = $this->general_model->record_count_where($table, $cond);
                        if ($favourite_records == "0") {
                            $favclass = "addFav";
                        } else {
                            $favclass = "addFav favActive";
                        }
                    } else {
                        $favclass = "addFav";
                    }
                    $location = $this->getbussinesloc($r->id);

                    if ($location > 1) {
                        $locations = $location . " Locations";
                    } else {
                        $locations = $location . " Location";
                    }

                    if ($r->treview > 1) {

                        $review = $r->treview . " reviews";
                    } else {
                        $review = $r->treview . " review";
                    }
                    if ($r->pincode) {
                        $city_pincode = $r->city_name . " - " . $r->pincode;
                    } else {
                        $city_pincode = $r->city_name;
                    }
                    $websiteurl1 = "http://" . $r->website_url;
                    $datetime = $this->getbussinesstime($r->id);
                    if ($datetime) {
                        $datetime = "<strong>Open now</strong> - " . $datetime;
                    } else {
                        $datetime = "<strong> </strong>";
                    }
                    if ($r->is_verified == "1") {
                        $verified = 'ypApproved';
                    } else {
                        $verified = '';
                    }

                    $msg .= '<li><div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <img width="170" height="170" src="' . $r->image_note . '" alt="">
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <a href="' . base_url() . 'business_detail?id=' . $r->id . '" class="eachPopularTitle">
                                                    ' . $r->title . '
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <a target="_blank" href="' . $websiteurl1 . '"><span class="openNewWindow"></span></a>
                                                    <span class="' . $verified . '"></span>
                                                    <span class="certified aaP">' . $r->rating_name . '</span>
                                                </div>
                                            </div>
                                            <div class="eachPopularRateOpen">
                                                <div class="eachPopularRatingBlock">
                                                    <span class="rating r' . $r->roverall . '">' . $r->roverall . '</span>
                                                    <a href="#" class="ratingCount">' . $review . '</a>
                                                </div>
                                                <div class="openNow">' . $datetime . '</div>
                                            </div>
                                            <ul class="eachPopularTagsList">
                                            <li></li>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <a href="mailto:' . $r->email . '">' . $r->email . '</a>
                                                <a href="' . $websiteurl1 . '" target="_blank">website</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="' . $favclass . '" onclick="addtofavorite(' . $r->id . ')">Add Favorite</div>
                                        <a class="businessContact" href="tel:' . $r->phone . '">' . $r->phone . '</a>
                                        <address class="businessArea">
                                            <strong>' . $r->area_name . '</strong>
                                           ' . $city_pincode . '
                                        </address>
                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount">' . $locations . '</span>
                                            <a  href="//maps.google.com/maps?f=d&amp;daddr=17.444442,78.379597&amp;hl=en" target="_blank">Directions</a>
                                        </div>
                                    </div>
                                </div>
                            </li>';
                }
                $msg = "<ul class='popularThisWeekList'>" . $msg . "</ul>"; // Content for Data
                echo $msg;

                //echo "<pre>";print_r($businesses);exit;
                $this->data['businesses'] = $businesses;

                //suggested categories
                $distinct_cat_id_query = 'SELECT DISTINCT categories_list from (' . $meta_query . ') as b';
                $distinct_cat_id_result = $this->general_model->find_by_sql($distinct_cat_id_query);
                $cat_ids_result = $this->array_col($distinct_cat_id_result, "categories_list");
                $cat_ids = array();
                $k = 0;
                for ($i = 0; $i < count($cat_ids_result); $i++) {
                    $t_arr = explode(',', $cat_ids_result[$i]);
                    for ($j = 0; $j < count($t_arr); $j++) {
                        $cat_ids[$k++] = $t_arr[$j];
                    }
                }
                $unique_cat_ids = array_unique($cat_ids);
                if (!empty($unique_cat_ids)) {
                    $cat_suggest_query = "select id, name from category where id in ( " . implode(',', $unique_cat_ids) . " )";
                    $cat_suggest_result = $this->general_model->find_by_sql($cat_suggest_query);
                    $cat_suggest = array();
                    foreach ($cat_suggest_result as $key => $r) {
                        array_push($cat_suggest, array('id' => $r->id, 'name' => $r->name));
                    }
                    $this->data['cat_suggest'] = $cat_suggest;
                }

                $f_query = " SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id DESC LIMIT 1";
                $featured_result = $this->general_model->find_by_sql($f_query);
                $featured_business = array();
                foreach ($featured_result as $key => $r) {
                    array_push($featured_business, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'rating' => $r->rating_name,
                        'overall' => $r->roverall,
                        'review' => $r->treview,
//                        'datetime' => $this->getbussinesstime($r->id),
//                        'categories' => $this->getbussinesscat($r->categories_list),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => ($r->phone == "") ? $r->landline : "+91" . $r->phone,
                        'area' => (isset($r->area_name)) ? $r->area_name : "",
                        'city' => (isset($r->city_name)) ? $r->city_name : "",
                        'pincode' => $r->pincode,
//                        'location' => $this->getbussinesloc($r->id),
                        'image_note' => $r->image_note
                            )
                    );
                }
                $this->data['featured_business'] = $featured_business;
                break;

            default:redirect('welcome/');
        }
    }

    function getCategoryTree($id) {

        $cat_level = $this->general_model->fetch_row_by_sql("select level from category where id = " . $id);
        $level = $cat_level['level'];
        $qry = "";
        $cat_arr = array();
        if ($level === "L1") {
            $qry = " select id from category where parent_id = " . $id
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ) "
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ))";
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        } elseif ($level === "L2") {
            $qry = " select id from category where parent_id = " . $id
                    . " UNION "
                    . " SELECT id from category where parent_id in "
                    . " (select id from category where parent_id = " . $id . " ) ";
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        } elseif ($level === "L3") {
            $qry = " select id from category where parent_id = " . $id;
            $cat_tree = $this->general_model->find_by_sql($qry);
            $cat_arr = $this->array_col($cat_tree, 'id');
        }
        array_unshift($cat_arr, $id);
        return $cat_arr;
    }

    function checkThumbnail($id) {

        $file = '/var/www/yellowpages.in/static/business-images/' . $id . '/190x125_' . $id . '-1.jpg';
        return file_exists($file);
    }

    function prepareBusinessLink($id, $name,$area,$city) {
        $name = $name."-".$area."-".$city;
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/b/' . strtolower($name) . '/' . $id;
    }

    function prepareCategoryLink($id, $name) {
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/hyderabad/' . strtolower($name) . '/' . $id;
    }

}

?>