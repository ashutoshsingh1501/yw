<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Setting extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		function index(){
			$logged_in = $this->session->userdata('logged_in');;
		    $data['user'] = $this->general_model->fetch_row_by_key('users' , 'id' ,$logged_in['id']);
			$data['role'] = $this->general_model->fetch_row_by_key('role', 'id', $data['user']->role_id);
			$data['roles'] = $this->general_model->get_enum('role', 'id', 'name');
						
			if(isset($_POST) && !empty($_POST)){
				$receive_notification = (isset($_POST['receive_notification'])) ? $_POST['receive_notification'] : 0;
				$data = array(				
					"first_name" => $_POST['username'],
					"email" => $_POST['useremail'],	
					"phone" => $_POST['userphone'],
					"receive_notification" => $receive_notification	
				);
				
				$user_id = $this->general_model->update_row('users' , $data,$logged_in['id']);
				if(isset($_FILES)&&!empty($_FILES)){
					$target_dir = "uploads/";
					$old_profile_image_url=$_REQUEST['old_image_url'];
					$oldimage_url='http://'.$_SERVER['HTTP_HOST'].$target_dir.$old_profile_image_url;
					$oldimage_url= base_url().$target_dir.$old_profile_image_url;
					//unlink($oldimage_url);
					$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
					move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);					
					$imagedata = array(	"image_url" =>  $target_file );
					$this->general_model->update_row('users' , $imagedata,$logged_in['id']);
				}
				
				if(isset($_REQUEST['password'])&&!empty($_REQUEST['password'])&&isset($_REQUEST['confirm_password'])&&!empty($_REQUEST['confirm_password'])){
					$passworddata = array(				
					"password" => md5($_REQUEST['password']));
					$this->general_model->update_row('users' , $passworddata,$logged_in['id']);
				}
				redirect('setting');
				
			}
			$content = 	$this->load->view('setting/index',$data,true);
			$this->render($content);
		}
	}
?>