<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			
		}
		
		public function wtopcities(){
			$results = $this->general_model->find_by_sql('SELECT COUNT( * ) AS total, city_id FROM  `business` where business.is_deleted = 0 and  `created_at` <=  "' . date('Y-m-d 00:00:00') . '"  and  `created_at` >=  "' .date('Y-m-d 00:00:00', strtotime('-7 days')) . '" GROUP BY city_id ORDER BY total DESC LIMIT 5');
			$cities = $this->general_model->get_enum('city' , 'id' , 'name');
			$wcities = array();
			foreach($results as $key => $r){
				$city_name = isset($cities[$r->city_id]) ? $cities[$r->city_id] : 'Other';
				array_push($wcities,array('name'=>$city_name , 'y'=>(int)$r->total));
			}
			
			echo json_encode($wcities);
			exit;
		}
		
		public function mtopcities(){
			$results = $this->general_model->find_by_sql('SELECT COUNT( * ) AS total, city_id FROM  `business` where business.is_deleted = 0 and  `created_at` <=  "' . date('Y-m-d 00:00:00') . '"  and  `created_at` >=  "' .date('Y-m-d 00:00:00', strtotime('-30 days')) . '" GROUP BY city_id ORDER BY total DESC LIMIT 5');
			
			$cities = $this->general_model->get_enum('city' , 'id' , 'name');
			$wcities = array();
			foreach($results as $key => $r){
				$city_name = isset($cities[$r->city_id]) ? $cities[$r->city_id] : 'Other';
				array_push($wcities,array('name'=>$city_name , 'y'=>(int)$r->total));
			}
			
			echo json_encode($wcities);
			exit;
		}
		
		
		public function wtopcategories(){
			$results = $this->general_model->find_by_sql('SELECT COUNT( * ) AS total, category_id FROM  `business_attribute` LEFT JOIN business ON business.id = business_attribute.business_id WHERE  business.is_deleted = 0 and  `created_at` <=  "' . date('Y-m-d 00:00:00') . '"   and   `created_at` >=  "' .date('Y-m-d 00:00:00', strtotime('-7 days')) . '" GROUP BY category_id ORDER BY total DESC LIMIT 5');
			$cities = $this->general_model->get_enum('category' , 'id' , 'name');
			$wcities = array();
			foreach($results as $key => $r){
				$city_name = isset($cities[$r->category_id]) ? $cities[$r->category_id] : 'Other';
				array_push($wcities,array('name'=>$city_name , 'y'=>(int)$r->total));
			}
			
			echo json_encode($wcities);
			exit;
		}
		
		public function mtopcategories(){
			$results = $this->general_model->find_by_sql('SELECT COUNT( * ) AS total, category_id FROM  `business_attribute` LEFT JOIN business ON business.id = business_attribute.business_id WHERE business.is_deleted = 0 and  `created_at` <=  "' . date('Y-m-d 00:00:00') . '"  and  `created_at` >=  "' .date('Y-m-d 00:00:00', strtotime('-30 days')) . '" GROUP BY category_id ORDER BY total DESC LIMIT 5');
			$cities = $this->general_model->get_enum('category' , 'id' , 'name');
			$wcities = array();
			foreach($results as $key => $r){
				$city_name = isset($cities[$r->category_id]) ? $cities[$r->category_id] : 'Other';
				array_push($wcities,array('name'=>$city_name , 'y'=>(int)$r->total));
			}
			
			echo json_encode($wcities);
			exit;
		}
		
		
		function index(){
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
			
			if($this->form_validation->run() == FALSE && !$this->session->userdata('logged_in')){
				$content = 	$this->load->view('user/login',$data=null,true);
				$this->render_login($content);
				
			}
			else {
				$session_data = $this->session->userdata('logged_in');
				$data['total'] = $this->general_model->record_count_where('business', array('is_deleted' => 0));
				$data['paid'] = $this->general_model->record_count_where('business' , array('is_deleted' => 0,'is_paid' => 1));
				$data['unpaid'] = $this->general_model->record_count_where('business' , array('is_deleted' => 0, 'is_paid' => 0));
				$data['review'] = $this->general_model->record_count_where('business' , array('is_deleted' => 0 ,'is_review' => 1));
				$content = $this->load->view('dashboard',$data,true);
				$this->render($content);
			}
		}
		
		
		function check_database($password) {
			$username = $this->input->post('username');
			$remember_me = $this->input->post('remember');
			$result = $this->users_model->login($username, $password);
			$usertype_list = $this->general_model->get_enum('user_role' , 'id' , 'role_name');
			if($result){
				$sess_array = array();
				foreach($result as $row){
					$sess_array = array(
					'id' => $row->id,
					'username' => $row->username,
					'user_type' => $usertype_list[$row->user_type],
					'displayname' => $row->first_name . '  ' . $row->last_name 
					);
					$this->session->set_userdata('logged_in', $sess_array);
				}
				if($remember_me == "on"){
					setcookie("remember_me",$username,time()+60*60*24*100,"/");
					}else{
					setcookie("remember_me",'gone',time()-60*60*24*100,"/");
				}
				return TRUE;
			}
			else {
				return false;
			}
		}
		
		public function logout(){
			$this->session->unset_userdata('logged_in');
			session_destroy();
			redirect('dashboard', 'refresh');
		}
		
	}
?>