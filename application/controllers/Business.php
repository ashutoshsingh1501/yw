<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Business extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		function index(){
			$data['states'] = $this->general_model->get_enum('state' , 'id' , 'name');
			$data['cities'] = $this->general_model->get_enum('city' , 'id' , 'name');
			$data['areas'] = $this->general_model->get_enum('area' , 'id' , 'name');
			
			$categories = array();
			$category = $this->general_model->get_enum_where('category','id','name', array('level'=>0));
			foreach($category as $key => $c){
				$subcategory = $this->general_model->get_enum_where('category','id','name', array('level'=>$key));
				foreach($subcategory as $key => $sc){
					$categories[$key] = $c . ' > ' . $sc;
				}
			}
			
			$data['categories'] = $categories;
			$data['status'] = $this->general_model->get_enum('business_status' , 'id' , 'name');
			
			$query = "SELECT * FROM   business WHERE is_deleted = 0";
			
			if(isset($_GET) && !empty($_GET)){
				$subquery = '   AND  1';
				if($_GET['state_id'] != 0 ){
					$data['cities'] = $this->general_model->get_enum_where('city' , 'id' , 'name' , array('state_id' => $_GET['state_id']));
					$subquery = $subquery . "   and business.state_id = " . $_GET['state_id'];
				}
				
				if($_GET['city_id'] != 0){
					$subquery = $subquery . "   and business.city_id = " . $_GET['city_id'];
				}
				
				if($_GET['category_id'] != 0){
					$subcategory = $this->general_model->get_enum_where('category' , 'id' , 'id' , array('parent_id' => $_GET['category_id']));
					$scategory = !empty($subcategory) ? implode(',' , $subcategory) : 0 ;
					$business = $this->general_model->get_enum_where('business_attribute' , 'id' , 'business_id' , 'category_id IN (' . $scategory . ')');
					$bbusiness = !empty($business) ? implode(',' , $business) : 0 ;
					$subquery = $subquery . "   and business.id IN ( " . $bbusiness .") ";
					//$subquery = $subquery . "   and business.city_id = " . $_GET['city_id'];
				}
				
				
				if($_GET['class_id'] == 1 ){
					$subquery = $subquery . "   and business.is_paid = 0";
				}
				
				
				if($_GET['class_id'] == 2 ){
					$subquery = $subquery . "   and business.is_paid = 1";
				}
				
				
				if($_GET['class_id'] == 3 ){
					$subquery = $subquery . "   and business.is_featured = 1";
				}
				
				if($_GET['class_id'] == 4 ){
					$subquery = $subquery . "   and business.is_featured = 1 and is_paid = 0";
				}
				
				
				if($_GET['class_id'] == 5 ){
					$subquery = $subquery . "   and business.is_featured = 1 and is_paid = 1";
				}
				
				if($_GET['rating_id'] == 1 ){
					$subquery = $subquery . "   and business.is_verified = 1";
				}
				
				if($_GET['rating_id'] == 3 ){
					$subquery = $subquery . "   and business.rating_id = 1";
				}
				if($_GET['rating_id'] == 4 ){
					$subquery = $subquery . "   and business.rating_id = 2";
				}
				if($_GET['rating_id'] == 5 ){
					$subquery = $subquery . "   and business.rating_id = 3";
				}
				if($_GET['rating_id'] == 6 ){
					$subquery = $subquery . "   and business.rating_id = 4";
				}
				
				
				if(!empty($_GET['business_name']) && isset($_GET['business_name'])){
					$subquery .= '   AND  business.title  LIKE  "%' . $_GET['business_name'] .'%"' ;
				}
				if(!empty($_GET['contact_person']) && isset($_GET['contact_person'])){
					$subquery .= '   AND  business.contact_person  LIKE  "%' . $_GET['contact_person'] .'%"' ;
				}
				if(!empty($_GET['phone']) && isset($_GET['phone'])){
					$subquery .= '   AND  business.phone  LIKE  "%' . $_GET['phone'] .'%"' ;
				}
				if(!empty($_GET['email']) && isset($_GET['email'])){
					$subquery .= '   AND  business.email  LIKE  "%' . $_GET['email'] .'%"' ;
				}
				
				if(!empty($_GET['area_name']) && isset($_GET['area_name'])){
					$subquery .= '   AND  name  LIKE  "%' . $_GET['area_name'] .'%"' ;
				}
				
				$query  = "SELECT business.* , area.name   FROM   business LEFT JOIN area ON area.id = business.area_id WHERE business.is_deleted = 0 " . $subquery;
				
			}
			
			
			//start of pagination code
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;	
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = PERPAGE;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			
			
			$query.=" LIMIT ".$currentPage.",".$config['per_page'];	
			//echo $query; exit;
			$data['results'] = $this->general_model->find_by_sql($query);
			
			$config['base_url'] = base_url().'business/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();
			$data['pagermessage'] = '';
			if($data['links']!= '') {
					$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			//End of pagination code
			
			
			$content = 	$this->load->view('business/index',$data,true);
			$this->render($content);
			
		}
		
		
		function profile_info($id=null){
			$data['states'] = $this->general_model->get_enum('state' , 'id' , 'name');
			$data['cities'] = $this->general_model->get_enum('city' , 'id' , 'name');
			$data['areas'] = $this->general_model->get_enum('area' , 'id' , 'name');
			$data['categories'] = $this->general_model->get_enum('category' , 'id' , 'name');
			$data['status'] = $this->general_model->get_enum('business_status' , 'id' , 'name');
			$data['results'] = $this->general_model->fetch_row_by_key('business' , 'id' , $id);
			$data['result'] = $this->general_model->fetch_row_by_key('business_profile' , 'business_id' , $id);
			$data['bimages'] = $this->general_model->fetch_row_by_key('business_image' , 'business_id' , $id);
			//print_r($data);exit;
			
			if(isset($_POST) && !empty($_POST)){
				//print_r($_POST); exit;
				$is_verified = isset($_POST['is_verified']) ? $_POST['is_verified'] : 0;
				$status = 4;//In Process
				
				$data = array(
				"created_by" => 0,
				"title" => ucfirst($_POST['business_name']),
				"status" => $status,
				"state_id" =>0,
				"area_id" => $_POST['business_area_id'],
				"city_id" => $_POST['business_city_id'],
				"contact_person" => $_POST['contact_person'],
				"phone" => $_POST['business_phone'],
				"email" => $_POST['business_email'],
				"website_url" => $_POST['website_url'],
				"created_at" => date('Y-m-d H:i:s'),
				"modified_at" => date('Y-m-d H:i:s'),
				"is_deleted" => 0,
				"is_verified" => $is_verified,
				
				);
				
				if($_POST['id']){
					$record_id = $_POST['id'];
					$this->general_model->update_row('business' , $data , $_POST['id']);
					}else{
					$record_id = $this->general_model->save('business' , $data);
				}
				
				$is_phone = (isset($_POST['is_phone_display'])) ? $_POST['is_phone_display'] : 0;
				$is_notification = (isset($_POST['is_phone_notification'])) ? $_POST['is_phone_notification'] : 0;
				$is_landline = (isset($_POST['is_landline_display'])) ? $_POST['is_landline_display'] : 0;
				//$verified = (isset($_POST['is_year_verfied'])) ? $_POST['is_year_verfied'] : 0;
				
				$logo_file_path = "";
				if( isset( $_FILES['businesslogo'] )  &&  !empty($_FILES['businesslogo']['name']) ){
					$logo_file_path =$this->upload_file($_FILES['businesslogo']);
				}
				
				$data_info = array(
				"business_id" => $record_id,
				"landline" => $_POST['primarylandline'],
				"fax_number" => $_POST['fax_number'],
				"tollfree_number" => $_POST['tollfree'],
				"is_phone_display" => $is_phone,
				"is_phone_notification" => $is_notification,
				"is_landline_display" => $is_landline,
				"establishment_year" => $_POST['establishedyear'],
				/*"is_year_verfied" => $verified,*/
				"video_url" => $_POST['videourl'],
				"logo_image" => $logo_file_path,
				"lead_page_url" => $_POST['businessleadpage'],
				"address" => $_POST['address'],
				"landmark" => $_POST['landmark'],
				"pincode" => $_POST['pincode'],
				"twitter_url" =>  $_POST['twitter_url'],
				"facebook_url" =>  $_POST['facebook_url'],
				"pinterest_url" =>  $_POST['pinterest_url'],
				"latitude" =>  $_POST['latitude'],
				"longitude" =>  $_POST['longitude'],
				);
				
				
				if($_POST['id']){
					$bp = $this->general_model->fetch_row_by_key('business_profile' , 'business_id' , $_POST['id']);
					if(!empty($bp)){
						$this->general_model->update_row('business_profile' , $data_info , $bp->id);
						}else{
						$this->general_model->save('business_profile' , $data_info);
					}
					}else{
					$this->general_model->save('business_profile' , $data_info);
				}
				
				
				redirect('business/business_info/'. $record_id);
				
			}
			//$data['result'] = $result;
			$content = 	$this->load->view('business/profile_info',$data,true);
			$this->render($content);
			
		}
		
		
		
		function business_info($id=null){
			$categories = array();
			$category = $this->general_model->get_enum_where('category','id','name', array('level'=>0));
			foreach($category as $key => $c){
				$subcategory = $this->general_model->get_enum_where('category','id','name', array('level'=>$key));
				foreach($subcategory as $key => $sc){
					$categories[$key] = $c . ' > ' . $sc;
				}
			}
			
			$data['categories'] = $categories;
			$data['id'] = $id;
			$data['time'] = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = '. $id);
			$data['results'] = $this->general_model->fetch_row_by_key('business' , 'id' , $id);
			$data['rating'] = $this->general_model->get_enum('business_rating' , 'id', 'name');
			$data['vatt'] = $this->general_model->get_enum_where('business_attribute' ,'id' ,'attribute_id' , array('business_id' =>  $id));
			
			$vat = !empty(implode(',' , $data['vatt']))? implode(',' , $data['vatt']) :0;
			
			$data['vattribute'] = $this->general_model->find_by_sql(' SELECT ca.* , ba.answer FROM category_attribute ca LEFT JOIN business_attribute ba ON ba.attribute_id = ca.id WHERE ca.id IN (' . $vat .')');
			$data['subcategory'] = $this->general_model->get_enum_where('business_attribute' , 'category_id' , 'category_id' , array('business_id '=>$id));
			
			$data['category'] = !empty(implode(',', $data['subcategory'])) ? $this->general_model->get_enum_where('category' , 'id' , 'parent_id', array('id IN ('. implode(',' , $data['subcategory']) .')')) : array();
			
			if(isset($_POST) && !empty($_POST)){
				//foreach($_POST['morning_start_time'] as $key => $time){
				for($key = 0; $key < 7; $key++){
					$data = array(
					"business_id" => $id,
					"mor_start_time" => (isset($_POST['morning_start_time'][$key])) ? $_POST['morning_start_time'][$key] : '',
					"eve_end_time" => (isset($_POST['evening_end_time'][$key])) ? $_POST['evening_end_time'][$key] : '',
					"mor_end_time" => (isset($_POST['morning_end_time'][$key])) ? $_POST['morning_end_time'][$key] : '',
					"eve_start_time" => (isset($_POST['evening_start_time'][$key])) ? $_POST['evening_start_time'][$key] : '',
					"mor_status" => (isset($_POST['morning_status'][$key])) ? $_POST['morning_status'][$key] : 0,
					"eve_status" => (isset($_POST['evening_status'][$key])) ? $_POST['evening_status'][$key] : 0,
					"day_id" => $key
					);
					$time = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE day_id = '. $key . '  and  business_id = '. $id);
					if(!empty($time)){
						$this->general_model->update_row('business_time' , $data, $time[0]->id);
					}
					else{
						$this->general_model->save('business_time' , $data);
					}
				}
				$evening_on = (isset($_POST['evening_on'])) ? $_POST['evening_on'] : 0;
				$data_info = array(
				"relevancy_score" => $_POST['relevancyscore'],
				"is_featured" => $_POST['featuredbusiness'],	
				"note" => $_POST['note'],
				"rating_id" => $_POST['yp-rating'],
				"evening_on" =>$evening_on
				);
				
				$this->general_model->update_row('business' , $data_info , $id);
				
				$subcategory = (isset($_POST['subcategory']) && !empty(implode(',' , $_POST['subcategory']))) ? implode(',' , $_POST['subcategory']) : 0;
				$attribute = $this->general_model->find_by_sql(' SELECT * FROM category_attribute WHERE category_id IN ('. $subcategory .')');
				
				foreach($attribute as $key => $a){
					$answer = isset($_POST[$key]) ? $_POST[$key] : 0;
					if($a->field_type == 'Checkbox'){  $answer = !empty($answer) ? implode(',' , $answer) : $answer; }
					if(is_array($answer)){
						$answer = implode(',' , $answer);
					}
					$data = array(
					'category_id' => $a->category_id,
					'attribute_id' => $a->id,
					'answer' => $answer,
					'business_id' => $id
					);
					$cattribute = $this->general_model->find_by_sql('SELECT * FROM business_attribute WHERE category_id = '. $a->category_id .' and attribute_id = '. $a->id . '  and business_id = '. $id );
					if(!empty($cattribute)){
						$this->general_model->update_row('business_attribute' , $data, $cattribute[0]->id);
					}
					else{
						$this->general_model->save('business_attribute' , $data);
					}
				}
				
				redirect('business/brands_services/'. $id);
			}
			
			$content = 	$this->load->view('business/business_info',$data,true);
			$this->render($content);
			
			
		}
		
		
		function brands_services($id=null){
			$data['id'] = $id;
			$data['offer'] = $this->general_model->find_by_sql('SELECT * FROM business_offer WHERE business_id = '. $id);
			$data['service'] = $this->general_model->get_enum_where('business_services' , 'id' ,'services', array('business_id' => $id));
			$data['brand'] = $this->general_model->get_enum_where('business_brands' , 'id' ,'brands', array('business_id' => $id));
			$data['amentie'] = $this->general_model->get_enum_where('business_amenties' , 'id' ,'amenties', array('business_id' => $id));
			$data['results'] = $this->general_model->fetch_row_by_key('business_profile' ,'business_id' , $id);
			//print_r($data['results']);exit;
			if(isset($_POST) && !empty($_POST)){
				$this->general_model->raw_sql('DELETE  FROM business_offer WHERE business_id = '. $id);
				foreach($_POST['deal_type'] as $key => $time){
					$logo_file_path = "";
					//print_r($_FILES);exit;
					if( isset( $_FILES['offer_image']) ){
						$logo_file_path = $this->upload_file_key($_FILES['offer_image'] , $key);
					}
					$data = array(
					"business_id" => $id,
					"deal_type" => $time,
					"title" => $_POST['offer_title'][$key],
					"image_path" => $logo_file_path,
					"offer_url" => $_POST['offer_url'][$key],
					"expire_on" => $_POST['offer_expire'][$key] ,
					"description" =>  $_POST['offer_description'][$key] ,
					);
					$this->general_model->create_row('business_offer' , $data);
				}
				$this->general_model->raw_sql('DELETE  FROM business_services WHERE business_id = '. $id);
				$services = !empty(explode(',' , $_POST['business_service'])) ? explode(',' , $_POST['business_service']) : array();
				foreach($services as $key => $service){
					$data = array(
					'business_id' => $id,
					'services' => $service
					);
					
					$this->general_model->save('business_services' , $data);
				}
				$this->general_model->raw_sql('DELETE  FROM business_brands WHERE business_id = '. $id);
				$brandes = !empty(explode(',' , $_POST['business_brand'])) ? explode(',' , $_POST['business_brand']) : array();
				foreach($brandes as $key => $brands){
					$data = array(
					'business_id' => $id,
					'brands' => $brands
					);
					
					$this->general_model->save('business_brands' , $data);
				}
				$this->general_model->raw_sql('DELETE  FROM business_amenties WHERE business_id = '. $id);
				$amentiest = !empty(explode(',' , $_POST['business_amenities'])) ? explode(',' , $_POST['business_amenities']) : array();
				foreach($amentiest as $key => $amenties){
					$data = array(
					'business_id' => $id,
					'amenties' => $amenties
					);
					
					$this->general_model->save('business_amenties' , $data);
				}
				
				$this->general_model->raw_sql('UPDATE business_profile SET expectation = "'. $_POST['business_expectation'] . '"  WHERE business_id ='. $id);
				
				redirect('business/payment_info/'. $id);
				
			}
			
			$content = 	$this->load->view('business/brands_services',$data,true);
			$this->render($content);
			
		}
		
		
		function payment_info($id=null){
			$data['id'] = $id;
			$data['payment_id'] = $this->general_model->get_enum_where('business_payment' , 'id' , 'payment_id', array('business_id' => $id));
			if(isset($_POST) && !empty($_POST)){
				$this->general_model->raw_sql('DELETE FROM business_payment WHERE business_id ='. $id);
				foreach($_POST['payment'] as $key => $p){
					$data = array('business_id' => $id , 'payment_id' => $p);
					$this->general_model->save('business_payment' , $data);
				}
				redirect('business');
			}
			
			$content = 	$this->load->view('business/payment_info',$data,true);
			$this->render($content);
			
		}
		
		
		
		function delete($id=null){
			if($_POST['id']){
				$this->general_model->raw_sql('UPDATE business SET is_deleted = !(is_deleted) WHERE id ='. $_POST['id']);
				//redirect('category');
				echo 1;
				exit;
			}
			
		}
		
		function upload_file($file){
			$target_dir = "uploads/";
			$target_file = $target_dir .uniqid(). ".".pathinfo( basename($file["name"]), PATHINFO_EXTENSION) ;
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			
			if(isset($_POST["submit"])) {
				$check = getimagesize($file["tmp_name"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";exit;
					$uploadOk = 1;
					} else {
					echo "File is not an image.";exit;
					$uploadOk = 0;
				}
			}
			
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";exit;
				$uploadOk = 0;
			}
			
			if ($file["size"] > 50000000) {
				echo "Sorry, your file is too large.";exit;
				$uploadOk = 0;
			}
			
			if(  	strtolower($imageFileType) != "jpg" && 
			strtolower($imageFileType) != "png" && 
			strtolower($imageFileType) != "jpeg" 
			) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";exit;
				$uploadOk = 0;
			}
			
			if ($uploadOk == 0) {
				echo "Sorry, your file was not uploaded."; exit;
				
				} else {
				if (move_uploaded_file($file["tmp_name"], $target_file)) {
					
					} else {
					echo "Sorry, there was an error uploading your file."; exit;
				}
			}
			return $target_file;
		}
		
		
		function upload_file_key($file,$key){
			$target_dir = "uploads/";
			$target_file = '';
			if(isset($file[$key]['name']) && !empty($file[$key]['name']) ){
				
				$target_file = $target_dir .uniqid(). ".".pathinfo( basename($file[$key]["name"]), PATHINFO_EXTENSION) ;
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				
				if(isset($_POST["submit"])) {
					$check = getimagesize($file[$key]["tmp_name"]);
					if($check !== false) {
						echo "File is an image - " . $check["mime"] . ".";exit;
						$uploadOk = 1;
						} else {
						echo "File is not an image.";exit;
						$uploadOk = 0;
					}
				}
				
				if (file_exists($target_file)) {
					echo "Sorry, file already exists.";exit;
					$uploadOk = 0;
				}
				
				if ($file["size"] > 50000000) {
					echo "Sorry, your file is too large.";exit;
					$uploadOk = 0;
				}
				
				if(  	strtolower($imageFileType) != "jpg" && 
				strtolower($imageFileType) != "png" && 
				strtolower($imageFileType) != "jpeg" 
				) {
					echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";exit;
					$uploadOk = 0;
				}
				
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded."; exit;
					
				} 
				else {
					if (move_uploaded_file($file[$key]["tmp_name"], $target_file)) {
						
						} else {
						echo "Sorry, there was an error uploading your file."; exit;
					}
				}
			}
			
			return $target_file;
		}
		
		function bulk_action(){
			$status='';			
			$status=$_REQUEST['status'];
			$params = array();
			parse_str($_REQUEST['datastring'], $params);
			if(!isset($params['addcheck']) && $status!=5){
				echo "Please select atleast one Business to change status";exit;	
			}
			if(!isset($params['addcheck']) && $status==5){
				echo "Please select atleast one Business to delete";exit;	
			}
			$id='';
			if($status==5){
				for($i=0;$i<count($params['addcheck']);$i++){
					
					$id=$params['addcheck'][$i];
					
					$this->general_model->raw_sql('UPDATE business SET is_deleted = !(is_deleted) WHERE id ='. $id);
					
				}
				echo "Selected Business(s) have been deleted successfully...";exit;
				
				}else{
				for($i=0;$i<count($params['addcheck']);$i++){
					$id=$params['addcheck'][$i];
					$data=array("status"=>$status);
					$this->general_model->update_row("business", $data, $id);
				}
				echo "Selected Business(s) status has been changed successfully...";exit;
				
			}
			
			
			return false;
		}
		
	}
?>