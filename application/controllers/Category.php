<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Category extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			$logged_in = $this->session->userdata('logged_in');
			
		}
		
		function check_category(){
			if(isset($_POST) && !empty($_POST)){
				$attribute = $this->general_model->fetch_row_by_key('category_attribute' , 'category_id' , $_POST['parent_id']);
				if(!empty($attribute)){
					echo 'Please Choose another Parent Category, Otherwise attribute of currently choosen category will be vanished ';
				}
			}
			exit;
		}
		
		
		function index(){
			$data['categories'] = $this->general_model->get_enum('category' , 'id' , 'name');
			
			$query = "SELECT c.*, Count(category.id) AS total FROM category c LEFT JOIN category ON c.id = category.parent_id WHERE c.is_deleted = 0 GROUP BY c.id ORDER BY c.id desc";
			
			
			if(isset($_GET) && !empty($_GET)){
				$subquery = '   AND  1';
				if($_GET['parent'] == 1  ){
					$subquery = "   and c.level in ( 0 )";
				}
				
				if($_GET['parent'] == 2){
					$subquery = "   and c.level in ( 1 )";
				}
				
				if($_GET['parent'] == 3){
					$subquery = "   and c.level in ( 2)";
				}
				
				if($_GET['parent'] == 4){
					$subquery = "   and c.level in ( 0,1 )";
				}
						
				if($_GET['parent'] == 5){
					$subquery = "   and c.level in ( 1,2 )";
				}
				
				if($_GET['parent'] == 6){
					$subquery = "   and c.level in ( 2 , 0 )";
				}
				
				if(!empty($_GET['category_name']) && isset($_GET['category_name'])){
					$subquery .= '   AND  c.name  LIKE  "%' . $_GET['category_name'] .'%"' ;
				}
				
				if(!empty($_GET['parent_name']) && isset($_GET['parent_name'])){
					$category = $this->general_model->get_enum_where('category' , 'id' ,'id' ,'name LIKE  "%'.$_GET['parent_name'].'%" and level != 2');
					$carray = (!empty(implode(',' , $category))) ? implode(',' , $category) : 0;
					$subquery .= '   AND  c.parent_id  IN   (' . $carray .')' ;
				}
				
				$query = "SELECT c.*, Count(category.id) AS total , category.name as parent_name FROM category c LEFT JOIN category ON c.id = category.parent_id WHERE c.is_deleted = 0  " . $subquery . "  GROUP BY c.id ORDER BY c.id desc ";
				
			//	$data['results'] = $this->general_model->find_by_sql($query);
			}
			
			
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
			$data['results'] = $this->general_model->find_by_sql($query);
			
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = PERPAGE;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			
			
			$query.=" LIMIT ".$currentPage.",".$config['per_page'];			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['base_url'] = base_url().'category/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();
			$data['pagermessage'] = '';
			if($data['links']!= '') {
				$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			
			$content = 	$this->load->view('category/index',$data,true);
			$this->render($content);
			
		}
		
		
		function create(){
			$data['categories'] = $this->general_model->get_enum_where('category','id','display_name', 'level != 2 and is_deleted = 0');
			if(isset($_POST) && !empty($_POST)){
				$parent_id = $_POST['parent_id'];
				$level = 0;
				$display_name = $_POST['name'];
				if($parent_id){
					$parentinfo =  $this->general_model->fetch_row_by_key('category' , 'id', $_POST['parent_id']) ;
					$level = $parentinfo->level + 1;
					$display_name = $level == 1 ? $parentinfo->name . ' > ' . $_POST['name'] : $data['categories'][$parentinfo->parent_id] . ' > '. $parentinfo->name .' > '. $_POST['name'];
				}
				
				$category_id = 0;
				
				foreach(explode(',' , $_POST['name']) as $key => $name){
					$data = array(				
					"name" => ucfirst($name),
					"parent_id" => $parent_id,
					"tags" => $_POST['tags'],
					"is_deleted" => 0,
					"level" => $level,
					"status" => 1,
					"created_by" =>0,
					"created_on" => date('Y-m-d H:i:s'),
					'display_name' => $display_name
					);
					$category_id = $this->general_model->save('category' , $data);
				}
				$this->general_model->raw_sql('DELETE  FROM category_attribute WHERE category_id = '. $category_id);
				if(isset($_FILES['excelfile'])&& !empty($_FILES['excelfile'])){
					foreach($_FILES['excelfile']['name'] as $key =>  $filename){
						if($filename != null){
							$filetemp = $_FILES['excelfile']['tmp_name'];
							$file = $filetemp[$key];
							$handle = fopen($file, "r");
							$c = 0;
							while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
								$data = array(				
								"field_type" => $filesop[0],
								"label" => $filesop[1],
								"value" => $filesop[2],
								"value_type" => $filesop[3],
								"category_id" => $category_id,
								);
								
								$this->general_model->save('category_attribute' , $data);
							}
						}
					}
				}
				redirect('category');
			}
			
			$content = 	$this->load->view('category/create',$data,true);
			$this->render($content);
			
		}
		
		
		function edit($id=null){
			/*$category = $this->general_model->get_enum_where('category','id','name', array('level'=>0,'is_deleted'=>0));
			foreach($category as $key => $c){
				$categories[$key] = $c ;
				$subcategory = $this->general_model->get_enum_where('category','id','name', array('level'=>$key));
				foreach($subcategory as $keys => $sc){
					$categories[$keys] = $c . ' > ' . $sc;
				}
			}*/
			$data['categories'] = $this->general_model->get_enum_where('category','id','display_name', 'level != 2 and is_deleted = 0');
			//$data['categories'] = $categories;
			$data['results'] = $this->general_model->fetch_row_by_key('category' , 'id', $id);
			$data['id'] = $id;
			
			if(isset($_POST) && !empty($_POST)){
				$parent_id = $_POST['parent_id'];
				$parentinfo = $this->general_model->fetch_row_by_key('category' , 'id', $_POST['parent_id']);
				$level = 0;
				$display_name = $_POST['name'];
				if($parent_id){
					$parentinfo =  $this->general_model->fetch_row_by_key('category' , 'id', $_POST['parent_id']) ;
					$level = $parentinfo->level + 1;
					$display_name = $level == 1 ? $parentinfo->name . ' > ' . $_POST['name'] : $data['categories'][$parentinfo->parent_id] . ' > '. $parentinfo->name .' > '. $_POST['name'];
				}
				$category_id = 0;
				foreach(explode(',' , $_POST['name']) as $key => $name){
					$data = array(				
					"name" => ucfirst($name),
					"parent_id" => $parent_id,
					"tags" => $_POST['tags'],
					"is_deleted" => 0,
					"level" => $level,
					"status" => 1,
					"created_by" =>0,
					"created_on" => date('Y-m-d H:i:s'),
					"display_name" => $display_name
					);
					$this->general_model->update_row('category' , $data , $id);
				}
				$this->general_model->raw_sql('DELETE  FROM category_attribute WHERE category_id = '. $id);
				if(isset($_FILES['excelfile'])&& !empty($_FILES['excelfile'])){
					foreach($_FILES['excelfile']['name'] as $key =>  $filename){
						if($filename != null){
							$filetemp = $_FILES['excelfile']['tmp_name'];
							$file = $filetemp[$key];
							$handle = fopen($file, "r");
							$c = 0;
							while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
								$data = array(				
								"field_type" => $filesop[0],
								"label" => $filesop[1],
								"value" => $filesop[2],
								"value_type" => $filesop[3],
								"category_id" => $id,
								);
							
								$this->general_model->save('category_attribute' , $data);
							}
						}
					}
				}
				redirect('category');
			}
			
			$content = 	$this->load->view('category/edit',$data,true);
			$this->render($content);
			
		}
		
		
		function delete(){
			if($_POST['id']){
				$this->general_model->raw_sql('UPDATE category SET is_deleted = !(is_deleted) WHERE id ='. $_POST['id']);
				echo 1;
			}
			exit;
		}
		
		
		function upload_file($file){
			$target_dir = "uploads/";
			$target_file = $target_dir .uniqid(). ".".pathinfo( basename($file["name"]), PATHINFO_EXTENSION) ;
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
			
			if(isset($_POST["submit"])) {
				$check = getimagesize($file["tmp_name"]);
				if($check !== false) {
					echo "File is an image - " . $check["mime"] . ".";exit;
					$uploadOk = 1;
					} else {
					echo "File is not an image.";exit;
					$uploadOk = 0;
				}
			}
			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";exit;
				$uploadOk = 0;
			}
			// Check file size
			if ($file["size"] > 50000000) {
				echo "Sorry, your file is too large.";exit;
				$uploadOk = 0;
			}
			// Allow certain file formats
			if(  	strtolower($imageFileType) != "jpg" && 
			strtolower($imageFileType) != "png" && 
			strtolower($imageFileType) != "jpeg" 
			) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";exit;
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				echo "Sorry, your file was not uploaded."; exit;
				// if everything is ok, try to upload file
				} else {
				if (move_uploaded_file($file["tmp_name"], $target_file)) {
					//echo "The file ". basename( $file["name"]). " has been uploaded.";
					} else {
					echo "Sorry, there was an error uploading your file."; exit;
				}
			}
			return $target_file;
		}
		
	}
?>