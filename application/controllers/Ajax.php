<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function get_subcategories() {
        if (isset($_POST) && !empty($_POST) && $_POST['category_id'] != 0) {
            $result = $this->general_model->get_enum_where('category', 'id', 'name', 'parent_id IN (' . $_POST['category_id'] . ')');
            echo json_encode($result);
        }
        exit;
    }

    function change_bstatus() {
        if (isset($_POST) && !empty($_POST)) {
            $this->general_model->raw_sql('UPDATE business SET status = ' . $_POST['status_id'] . ' WHERE id = ' . $_POST['business_id']);
        }
        exit;
    }

    function get_role($id) {
        $row = $this->general_model->fetch_row_by_key('role', 'id', $id);
        echo json_encode($row);
        exit;
    }

    function change_estatus() {
        if (isset($_POST) && !empty($_POST)) {
            $this->general_model->raw_sql('UPDATE users SET status = ' . $_POST['status_id'] . ' WHERE id = ' . $_POST['employee_id']);
        }
        exit;
    }

    function change_astatus() {
        if (isset($_POST) && !empty($_POST)) {
            $this->general_model->raw_sql('UPDATE area SET status = ' . $_POST['status_id'] . ' WHERE id = ' . $_POST['area_id']);
            //echo  0;
        }
        exit;
    }

    function add_attributes() {
        $attributes = '';
        if (isset($_POST) && !empty($_POST)) {
            $result = $this->general_model->find_by_sql('SELECT * FROM category_attribute WHERE category_id = ' . $_POST['subcategory_id']);
            foreach ($result as $key => $r) {

                if ($r->field_type == 'Radio') {
                    $attributes = $attributes . '<div class="col-sm-12" id="' . $r->category_id . '"><div class="form-group"><div class="col-sm-2"><h4>' . $r->label . ' </h4></div>';
                    foreach (explode(',', $r->value) as $keys => $v) {
                        $attributes = $attributes . '<div class="col-sm-4"><div class="radio radio-styled"><label><input type="radio" name="' . $key . '" value="' . $v . '" checked><span>' . $v . '</span></label></div></div>';
                    }
                    $attributes = $attributes . '</div></div>';
                }

                if ($r->field_type == 'Checkbox') {
                    $attributes = $attributes . '<div class="col-sm-12" id="' . $r->category_id . '"><div class="form-group"><div class="col-sm-2"><h4>' . $r->label . ' </h4></div>';
                    foreach (explode(',', $r->value) as $keys => $v) {
                        $attributes = $attributes . '<div class="col-sm-2"><div class="checkbox checkbox-styled"><label><input type="checkbox" value="' . $v . '" name="' . $key . '[]"><span>&nbsp;</span>' . $v . '</label></div></div>';
                    }
                    $attributes = $attributes . '</div></div>';
                }

                if ($r->field_type == 'Dropdown') {
                    $attributes = $attributes . '<div class="col-sm-12" id="' . $r->category_id . '"><div class="form-group"><div class="col-sm-2"><h4>' . $r->label . ' </h4></div><div class="col-sm-6"><div class="form-group"><select id="yp-rating" name="' . $r->category_id . '" class="form-control" data-live-search="true" >';
                    foreach (explode(',', $r->value) as $keys => $v) {
                        $attributes = $attributes . '	<option value="' . $v . '">' . $v . '</option>';
                    }
                    $attributes = $attributes . '</select></div></div></div></div>';
                }

                if ($r->field_type == 'Star') {
                    $attributes = $attributes . '<div class="col-sm-12" id="' . $r->category_id . '"><div class="form-group"><div class="col-sm-2"><h4>' . $r->label . ' </h4></div>';
                    //foreach(explode(',' , $r->value) as $keys => $v){
                    $attributes = $attributes . '	<div class="star-rating">
						<div class="rating-input">
						<input type="number" name="your_awesome_parameter" id="rating1" class="rating" data-clearable="remove"/>
						</div>
						</div>
						';
                    //}
                    $attributes = $attributes . '</div></div>';
                }
            }
        }
        echo $attributes;
        exit;
    }

    function remove_attributes() {
        if (isset($_POST) && !empty($_POST)) {

            //$result = subcategory_id
            echo 1;
        }
        exit;
    }

    function match_postalcode() {
        $flag = 0; //Match Not Found
        if (isset($_POST) && !empty($_POST)) {

            $result = $this->general_model->fetch_row_by_key('area', 'area_postalcode', $_POST['area_postalcode']);
            if ($result) {
                $flag = 1; // Match Found
            }
        }
        echo $flag;
        exit;
    }

    function get_cities() {
        $city = $this->general_model->get_enum_where('city', 'id', 'name', array('state_id' => $_POST['state_id']));
        if ($_POST['state_id'] == 0) {
            $city = $this->general_model->get_enum_where('city', 'id', 'name', 1);
        }
        echo json_encode($city);
        exit;
    }

    function get_areas() {
        $city = $this->general_model->get_enum_where('area', 'id', 'name', array('city_id' => $_POST['city_id']));
        if ($_POST['city_id'] == 0) {
            $city = $this->general_model->get_enum_where('city', 'id', 'name', 1);
        }
        echo json_encode($city);
        exit;
    }

    function get_topcategory() {
        $results = $this->general_model->find_by_sql('SELECT c.* FROM category c WHERE c.is_deleted = 0 and c.status = 1 GROUP BY c.id ORDER BY c.id desc LIMIT 5');
        $cities = $this->general_model->get_enum('category', 'id', 'name');
        $wcities = ''; //array();
        foreach ($results as $key => $r) {
            $wcities = $wcities . "'" . $r->name . "' ,";
        }
        $wcities = $wcities . "'" . 'Others' . "'";

        echo json_encode($wcities);
        exit;
        //$topcategory = $this->general_model->get_enum_where('category', 'id' , 'name' , array('status'=>1,'is_deleted'=>0));
    }

    function get_csearch() {
        $wcities = array();
        $result = $this->general_model->find_by_sql("SELECT * FROM city WHERE status = 1 and is_deleted = 0");
        $i = 0;
        foreach ($result as $key => $r) {
            $wcities[$i] = $r->name;
            $i = $i + 1;
            $areas = $this->general_model->find_by_sql("SELECT * FROM area WHERE status = 1 and is_deleted = 0 and city_id = " . $r->id . " AND name!='' ");
            foreach ($areas as $key => $a) {
                $wcities[$i] = $a->name . ',' . $r->name;
                $i = $i + 1;
            }
        }
        echo json_encode($wcities);
        exit;
    }

    function get_search() {
        $wcities = array();

        $results = $this->general_model->find_by_sql('SELECT c.* FROM category c WHERE c.is_deleted = 0 and c.status = 1 GROUP BY c.id ORDER BY c.id desc');
        //$cities = $this->general_model->get_enum('category' , 'id' , 'name');
        foreach ($results as $key => $r) {
            array_push($wcities, array('team' => $r->name));
            //$wcities[$key] = $r->name;
        }

        $results = $this->general_model->find_by_sql('SELECT * FROM business b WHERE b.is_deleted = 0  GROUP BY b.id ORDER BY b.id desc');
        //$cities = $this->general_model->get_enum('category' , 'id' , 'name');
        foreach ($results as $key => $r) {
            array_push($wcities, array('team' => $r->title));
            //$wcities[$key] = $r->name;
        }

        array_push($wcities, array('team' => 'Others'));

        echo json_encode($wcities);
        exit;
        //$topcategory = $this->general_model->get_enum_where('category', 'id' , 'name' , array('status'=>1,'is_deleted'=>0));
    }

    function get_searchdata() {
        $search = $_REQUEST['q'];
        if (!empty($search)) {
            $search_cat_query = " SELECT  CONCAT('search_cat_', id) id,name FROM `category` c where c.is_deleted = 0 and c.status = 1 and name rlike '" . $search . "' and id in (Select distinct category_id from business_category_mapping) order by c.id desc LIMIT 3";
//            $search_cat_query = " SELECT  CONCAT('search_cat_', id) c.id,c.name FROM `category` c join business b on b.categories_list = c.id where c.is_deleted = 0 and c.status = 1 and c.name rlike '".$search."' and find_in_set(c.id,b.categories_list) order by c.id desc LIMIT 3";
//            $search_bus_query = " SELECT  CONCAT('search_bus_', id) id,title FROM `business` b where b.is_deleted = 0 and b.status = 1 and title rlike '" . $search . "' order by b.id desc LIMIT 3";
            $search_bus_query = " SELECT  CONCAT('search_bus_', b.id) id, b.title, area.name as areaname, city.name as cityname FROM `business` b inner join area on area.id = b.area_id inner join city on city.id = b.city_id  where b.is_deleted = 0 and b.status = 1 and title rlike '" . $search . "' order by b.id desc LIMIT 3";
            
            $cat_results = $this->general_model->find_by_sql($search_cat_query);
            $bus_results = $this->general_model->find_by_sql($search_bus_query);

            $res = "";
            if (count($cat_results) || count($bus_results)) {
//                        $res .="<div class='searchResult'>";
                if (count($cat_results)) {
                    $res .="<div class='resultTypeHeading'>By Categories</div><ul class='searchResultList'>";
                    foreach ($cat_results as $key => $r) {
                        $res .= "<li id='" . $r->id . "' class='sresult' >" . $r->name . "</li>";
                    }
                    $res .="</ul>";
                }
                if (count($bus_results)) {
                    $res .="<div class='resultTypeHeading'>By Business</div><ul class='searchResultList'>";
                    foreach ($bus_results as $key => $r) {
//                        $res .= "<li id='" . $r->id . "' class='sresult' >" . $r->title . "</li>";
                        $res .= "<li id='" . $r->id . "' class='sresult' >" . $r->title . " <span class='areaName'>".$r->areaname.", ".$r->cityname."</span></li>";
                    }
                    $res .="</ul>";
                }
//                        $res .= "</div>";
            }
            echo $res;
        }
        exit;
    }

    function get_location() {
        $search = $_REQUEST['q'];
        if (!empty($search)) {
            $search_query = "SELECT concat(a.name,', ',c.name) as name FROM area a "
                    . "inner join city c on c.id = a.city_id "
                    . "where a.name rlike '" . $search . "'"
                    . " LIMIT 5 ";
            $results = $this->general_model->find_by_sql($search_query);
            $res = "";
            if (count($results)) {
                $res .= "<ul class='searchResultList'>";
                foreach ($results as $key => $r) {
                    $res .= "<li id='" . $r->name . "' class='lresult' >" . $r->name . "</li>";
                }
                $res .= "</ul>";
            }
            echo $res;
        }
        exit;
    }

    function get_business() {
        $wbusiness = array();
        $results = $this->general_model->find_by_sql('SELECT b.* FROM business b WHERE b.is_deleted = 0 GROUP BY b.id ORDER BY b.id desc');
        //$cities = $this->general_model->get_enum('category' , 'id' , 'name');
        foreach ($results as $key => $r) {
            array_push($wbusiness, array('b' => $r->title));
            //$wcities[$key] = $r->name;
        }
        echo json_decode($wbusiness);
        exit;
    }

    function get_category() {
        $wbusiness = array();
        $results = $this->general_model->find_by_sql('SELECT c.* FROM category c WHERE c.is_deleted = 0 and c.status = 1 GROUP BY c.id ORDER BY c.id desc');
        //$cities = $this->general_model->get_enum('category' , 'id' , 'name');
        foreach ($results as $key => $r) {
            array_push($wbusiness, array('c' => $r->title));
            //$wcities[$key] = $r->name;
        }
        echo json_decode($wbusiness);
        exit;
    }

    function get_area($id) {

        $city = $this->general_model->fetch_row_by_key('area', 'id', $id);
        echo json_encode($city);
        exit;
    }

    function get_city($id) {
        $city = $this->general_model->fetch_row_by_key('city', 'id', $id);
        echo json_encode($city);
        exit;
    }

    function get_categories() {
        $category_array = array();
        $level0 = $this->general_model->get_enum_where('category', 'id', 'name', array('level' => '0'));
        foreach ($level0 as $key => $l0) {
            $level0[$key] = $l0;
            $level1 = $this->general_model->get_enum_where('category', 'id', 'name', array('level' => '0'));
        }
    }

    function update_subcategories() {
        $city = $this->general_model->get_enum_where('category', 'id', 'name', array('parent_id' => $_POST['parent_id']));
        echo json_encode($city);
        exit;
    }

    function get_allcategories() {
        $search = $_REQUEST['q'];
        if (!empty($search)) {
            $search_query = 'SELECT id,name FROM category c '
                    . 'WHERE c.is_deleted = 0 and c.status = 1 '
                    . 'and c.name rlike "' . $search . '" ORDER BY c.id desc '
                    . 'LIMIT 5';
            
            $results = $this->general_model->find_by_sql($search_query);
            $res = "";
            if (count($results)) {
                foreach ($results as $key => $r) {
                    $res .= "<li data-id=" . $r->id . " class='cresult'>" . $r->name . "</li>";
                }
            }
            echo $res;
        }
        exit;
    }
    
        }
