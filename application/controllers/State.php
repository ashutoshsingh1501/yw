<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class State extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			$this->load->model('general_model','',TRUE);
		}	
		
		
		function index(){
			
			if($this->session->userdata('logged_in')){
				
				$data['state_list'] = $this->general_model->get_enum('state' , 'id' , 'state_name');
				$data['city_list'] = $this->general_model->get_enum('city' , 'id' , 'city_name');
				$data['area_list'] = $this->general_model->get_enum('area' , 'id' , 'area_name');
								
				$query = "SELECT area.*, Count(business.id) AS total FROM   area LEFT JOIN business ON area.id = business.business_area_id	WHERE area.area_isdeleted = 0			GROUP  BY area.id	ORDER  BY area.id";
				
				$data['areas'] = $this->general_model->find_by_sql($query);
				$data['total_area'] = $this->general_model->record_count_where('area', array('area_isdeleted' => 0));
				
				$content = 	$this->load->view('area/index',$data,true);
				$this->render($content);
				
			}
			else {
				redirect('');
			}
		}
		
		
		
		function get_state(){
			
			$city = $this->general_model->get_enum_where('state', 'id' , 'state_name' , array('state_status' => 1,'state_ideleted' => 0) );
			echo json_encode($city); 
			exit;
			
		}
		
	}
