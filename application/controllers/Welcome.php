<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public $categories;
    
    public function __construct() {
        parent::__construct();
        $this->categories = $this->general_model->get_enum('category', 'id', 'name');
    }

    function humanTiming($time) {

        $time = time() - $time; // to get the time since that moment
        $time = ($time < 1) ? 1 : $time;
        $tokens = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'min',
            1 => 'sec'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit)
                continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
        }
    }

    function index() {

        $categories = $this->general_model->find_by_sql('SELECT * FROM category WHERE status = 1 AND is_deleted = 0 AND is_primary = 1');
        $featured = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id DESC');
        $recently = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id LIMIT 2');
        $popular = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1 ORDER BY id LIMIT 4 OFFSET 2');
//        $featured_proc = $this->general_model->featured();
//        $categories_proc = $this->general_model->categories();
//        $popular_proc = $this->general_model->popular();
//        $recently_proc = $this->general_model->recently();
        
        $featured_categories = array();
        foreach ($categories as $key =>$r){
            array_push($featured_categories, 
                    array(
                        'id' => $r->id,
                        'name' => $r->name,
                        'image_path' => $r->image_path,
                        'link' => $this->prepareCategoryLink($r->id, $r->name)
                    )
                );
        }
        
        $featured_businesses = array();
        foreach ($featured as $key =>$r){
            array_push($featured_businesses, 
                    array(
                        'is_verified'=>$r->is_verified,
                        'image' => $r->image_path,
                        'id' => $r->id,
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'area' => $r->area_name,
                        'roverall' => $r->roverall,
                        'treview' => $r->treview,
                        'is_logo' => $this->checkThumbnail($r->image_note),
                        'image_note' => $r->image_note
                    )
                );
        }

        $view_recently = array();
        foreach ($recently as $key => $r) {
            if (isset($this->session->userdata["id"])) {
                $uid = $this->session->userdata["id"];
                $bid = $r->id;
                $table = "visitor_business";
                $cond = array(
                    "visitor_id" => $uid,
                    "business_id" => $bid
                );
                $favourite_records = $this->general_model->record_count_where($table, $cond);
                if ($favourite_records == "0") {
                    $favclass = "addFav";
                } else {
                    $favclass = "addFav favActive";
                }
            } else {
                $favclass = "addFav";
            }
            array_push($view_recently, 
                    array(
                        'image' => $r->image_path,
                        'business_id' => $r->id, 
                        'title' => $r->title, 
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'page_url' => $r->page_url, 
                        'rating' => $r->rating_name, 
                        'overall' => $r->roverall, 
                        'review' => $r->treview, 
                        'datetime' => $this->getbussinesstime($r->id), 
                        'categories' => $this->getbussinesscat($r->categories_list), 
                        'email' => $r->email, 
                        'website_url' => $r->website_url, 
                        'phone' => $r->phone,
                        'alternative_number' => $r->alternative_number,
                        'landline' => $r->landline,
                        'secondary_land_line' => $r->secondary_land_line,
                        'area' => $r->area_name, 
                        'city' => $r->city_name, 
                        'pincode' => $r->pincode, 
                        //'location' => $this->getbussinesloc($r->id), 
                        'latitude' => $r->latitude, 
                        'favclass' => $favclass, 
                        'longitude' => $r->longitude,
                        'is_logo' => $this->checkThumbnail($r->image_note),
                        'image_note'=>$r->image_note,
                        'is_verified'=>$r->is_verified
                    )
                );
        }
        //print_r($view_recently);exit;//DEBUG

        $view_popular = array();
        foreach ($popular as $key => $r) {
             if (isset($this->session->userdata["id"])) {
                $uid = $this->session->userdata["id"];
                $bid = $r->id;
                $table = "visitor_business";
                $cond = array(
                    "visitor_id" => $uid,
                    "business_id" => $bid
                );
                $favourite_records = $this->general_model->record_count_where($table, $cond);
                if ($favourite_records == "0") {
                    $popular_favclass = "opFavClick";
                } else {
                    $popular_favclass = "opFavClick activeFavIcon";
                }
            } else {
                $popular_favclass = "opFavClick";
            }
            array_push($view_popular,
                    array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'popular_favclass' => $popular_favclass, 
                        'title' => $r->title,
                        'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                        'is_verified' => $r->is_verified,
                        'page_url' => $r->page_url,
                        'rating' => $r->rating_name,
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesssch($r->id),
                        'categories' => $this->getbussinesscat($r->categories_list),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => $r->phone,
                        'alternative_number' => $r->alternative_number,
                        'landline' => $r->landline,
                        'secondary_land_line' => $r->secondary_land_line,
                        'area' => $r->area_name,
                        'city' => $r->city_name,
                        'pincode' => $r->pincode,
                        'is_logo' => $this->checkThumbnail($r->image_note),
                        'image_note' => $r->image_note
                        //'location' => $this->getbussinesloc($r->id)
                    )
                );
        }
        //print_r($view_popular);exit;//DEBUG

        $reviews_query = "SELECT visitor_detail.profile_pic, visitor_detail.name, business.title, "
                . "business.id, business.area_name, business.city_name, business_review.rating, business_review.comments, business_review.added_on "
                . "FROM `business_review` "
                . "LEFT JOIN business on business.id = business_review.business_id "
                . "LEFT JOIN visitor_detail on visitor_detail.id = business_review.user_id "
                //. "WHERE business.is_deleted = 0 and business.status = 1 "
                . "ORDER BY business_review.id desc Limit 4";
        
        
        $reviews = $this->general_model->find_by_sql($reviews_query);
        //print_r($reviews);exit; //DEBUG
        $view_reviews = array();
        foreach ($reviews as $key => $review) {
            array_push($view_reviews, 
                    array(
                        'image' => $review->profile_pic,
                        'name' => $review->name,
                        'rating' => $review->rating,
                        'datetime' => $this->humanTiming(strtotime($review->added_on)),
                        'business_id' => $review->id,
                        'business' => $review->title,
                        'link' => $this->prepareBusinessLink($review->id, $review->title, $review->area_name, $review->city_name),
                        'comments' => $review->comments
                    )
                );
        }

        $data = array(
            'categories' => $featured_categories,
            'featured' => $featured_businesses,
            'recently' => $view_recently,
            'popular' => $view_popular,
            'reviews' => $view_reviews
        );
        
        $content = $this->load->view('home', $data, true);
        $this->render_front($content); //footer
    }

    function getfavorite() {
        $q = intval($_GET['q']);
        if (isset($this->session->userdata["id"])) {
            $table = "visitor_business";
            $vid = $this->session->userdata("id");
            $cond = array(
                "visitor_id" => $vid,
                "business_id" => $q
            );

            $inputs11 = $this->general_model->record_count_where($table, $cond);
                 if ($inputs11 == "0") {
                $inputs11 = $this->general_model->save($table, $cond);
                  echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You added one business to favorites.</div></div>';
                
            } else {
                $inputs11 = $this->general_model->delete_favourite_row($table, $vid, $q);
                 echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You unfavorited one business from  favorites.</div></div>';
                
            }
        } else {
            echo '<div class="notification showNotification">'
                    . '<div class="notificationText">'
                        . 'If you are already registered memeber please '
                        . '<span class="actionsBlock">'
                        . '<a href="' . base_url() . 'my-account" '
                            . 'class="loginBtn notificationBtn">Login</a> or '
                            . '<a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a>'
                        . '</span>'
                    . '</div>'
                    . '<div class="closeNotification">Close</div>'
                . '</div>';exit;
        }
    }  

    function getfavorite_more() {
        $q = intval($_GET['q']);
        if (isset($this->session->userdata["id"])) {
            $table = "visitor_business";
            $vid = $this->session->userdata("id");
            $cond = array(
                "visitor_id" => $vid,
                "business_id" => $q
            );

            $inputs11 = $this->general_model->record_count_where($table, $cond);
            if ($inputs11 == "0") {
                $inputs11 = $this->general_model->save($table, $cond);
                  echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You added one business to favorites.</div></div>';
            } else {
                $inputs11 = $this->general_model->delete_favourite_row($table, $vid, $q);
                echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You unfavorited one business from  favorites.</div></div>';
            }
        } else {
             echo '<div class="notification showNotification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="' . base_url() . 'my-account" class="loginBtn notificationBtn">Login</a> or <a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>';
             exit;
        }
    }

    function getmyaccount_favorite() {

        $q = intval($_GET['q']);
        if (isset($this->session->userdata["id"])) {
            $table = "visitor_business";
            $vid = $this->session->userdata("id");
            $cond = array(
                "visitor_id" => $vid,
                "business_id" => $q
            );

            $fav_count = $this->general_model->record_count_where($table, $cond);
            if ($fav_count == "0") {
                $inputs16 = $this->general_model->save($table, $cond);

                //echo"<div id='light' style='display:block;position:fixed;top:250px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1>You Liked one business. <div onclick='f1()'>X</div><div>";
            
                 echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You Liked one business. </div></div>';
            } else if ($fav_count == "1") {

                if ($this->session->userdata["myaccount_fav_count"] == "1") {
                    $inputs11 = $this->general_model->delete_favourite_row($table, $vid, $q);
                    echo "<script>window.location.reload();</script>";
                   // echo"No Favorite Businesses<div id='light' style='display:block;position:fixed;top:250px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You Dis Liked one business.  <div onclick='close_favorite()'>X</div><div>";
                echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You Dis Liked one business.  </div></div>';
                    } else {
                    $inputs11 = $this->general_model->delete_favourite_row($table, $vid, $q);
                    echo "<script>window.location.reload();</script>";
                    //echo"<div id='light' style='display:block;position:fixed;top:250px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You Dis Liked one business.  <div onclick='close_favorite()'>X</div><div>";
              
                    }
            }
        } else {

             echo '<div class="notification showNotification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="' . base_url() . 'my-account" class="loginBtn notificationBtn">Login</a> or <a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>';
        }
    }

    function getrating() {
        if (isset($this->session->userdata["id"])) {
            $rating1 = $_POST['rating1'];
            $bid = $_POST['bid'];
            $rating_desc1 = $_POST['rating_desc1'];
            $vid = $this->session->userdata("id");
            $table = "business_review";
            $added_on = date('Y-m-d H:i:s');
            $data = array(
                "user_id" => $vid,
                "business_id" => $bid,
                "comments" => $rating_desc1,
                "rating" => $rating1,
                "added_on" => $added_on
            );
            $cond = array(
                "user_id" => $vid,
                "business_id" => $bid
            );

            $business_review_user_count = $this->general_model->record_count_where($table, $cond);
            if ($business_review_user_count == "0") {
                $store_review = $this->general_model->save($table, $data);
                $condition = array(
                    "business_id" => $bid
                );
                $business_review_count = $this->general_model->record_count_where($table, $condition);
                $review_data = array(
                    "treview" => $business_review_count
                );
                $business_table = "business";
                $update_business_treview = $this->general_model->update_row($business_table, $review_data, $bid);
                $total_reviews = $this->general_model->find_by_sql('SELECT * FROM `business_review` WHERE business_id="' . $bid . '"');

                $sum = 0;
                foreach ($total_reviews as $key => $review) {
                    $sum+=$review->rating;
                }
                $average_rating = $sum / $business_review_count;
                $average_absolute_rating = round($average_rating);
                $review_rating_data = array(
                    "roverall" => $average_absolute_rating
                );
                $business_table = "business";
                $update_business_treview = $this->general_model->update_row($business_table, $review_rating_data, $bid);
                //echo "<div id='light' style='display:block;position:fixed;top:1050px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> one review added <a href='" . base_url() . "business_detail?id=$bid'>Click here and Go For Posted</a><div onclick='f3()'>X</div><div>";
            
                 echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">one review added</div></div>';
                
                } else {
                //echo "<div id='light' style='display:block;position:fixed;top:1050px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1>  one review already added  <div onclick='f3()'>X</div><div>";
            echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">one review already added.</div></div>';
                }
        } else {
            //echo "<div id='light' style='display:block;position:fixed;top:1050px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> If you are already registered memeber please login. <div onclick='f3()'>X</div><div>";
         echo '<div class="notification showNotification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="' . base_url() . 'my-account" class="loginBtn notificationBtn">Login</a> or <a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>';
            
        }
    }

    function getvote() {
        $vote = $_POST['vote'];
        $i = $_POST['id'];
        $review_id = $_POST['review_id'];
        $vid = $this->session->userdata("id");
        $get_review = $this->general_model->find_by_sql('SELECT * FROM `business_review` WHERE id="' . $review_id . '"');
        if (isset($this->session->userdata["id"])) {

            $table = "review_map";
            $cond = array(
                "user_id" => $vid,
                "review_id" => $review_id
            );
            $business_review_user_map_count = $this->general_model->record_count_where($table, $cond);

            $get_review_map = $this->general_model->find_by_sql('SELECT * FROM `review_map` WHERE user_id="' . $vid . '" AND review_id="' . $review_id . '"');
            if ($business_review_user_map_count == "0") {

                $data = array("vote" => $vote,
                    "user_id" => $vid,
                    "review_id" => $review_id);
                $store_review_map = $this->general_model->save($table, $data);
                if ($vote == "1") {


                    foreach ($get_review as $key => $review) {

                        $upvote = $review->upvote;
                        $downvote = $review->downvote;
                    }
                    $upvote = $upvote + 1;
                    $review_upvote_data = array(
                        "upvote" => $upvote
                    );
                    $review_table = "business_review";
                    $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);
                   // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have liked one review <div onclick='f2($i)'>X</div><div>";
                 echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You have liked one review</div></div>';
                    
                    
                    } else
                if ($vote == "-1") {

                    foreach ($get_review as $key => $review) {

                        $downvote = $review->downvote;
                        $upvote = $review->upvote;
                    }
                    $downvote = $downvote + 1;
                    $review_downvote_data = array(
                        "downvote" => $downvote
                    );
                    $review_table = "business_review";
                    $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);
                   // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have disliked one review <div onclick='f2($i)'>X</div><div>";
             
                     echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have disliked one review  </div></div>';
                    
                    }
            } else {

                foreach ($get_review as $key => $review) {

                    $downvote = $review->downvote;
                    $upvote = $review->upvote;
                }
                foreach ($get_review_map as $key => $review_map) {

                    $dbvote = $review_map->vote;
                }

                if ($vote == $dbvote && $vote == "1") {
                    //echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already liked this review <div onclick='f2($i)'>X</div><div>";
               
                     echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have already liked this review </div></div>';
                    
                } else
                if ($vote == $dbvote && $vote == "-1") {
                   // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already disliked this review <div onclick='f2($i)'>X</div><div>";
                   echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have already disliked this review </div></div>';
                    
                    
                } else {
                    if ($vote == "1") {

                        foreach ($get_review_map as $key => $review_map) {

                            $dbvote = $review_map->vote;
                        }
                        if ($dbvote == "0") {

                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $upvote = $upvote + 1;
                            $review_upvote_data = array(
                                "upvote" => $upvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);


                            $one = "1";
                            $review_map_vote = array(
                                "vote" => $one
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                            //echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have liked one review <div onclick='f2($i)'>X</div><div>";
                        echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You have liked one review</div></div>';
                            } else {
                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $downvote = $downvote - 1;
                            $review_downvote_data = array(
                                "downvote" => $downvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);




                            $zero = "0";
                            $review_map_vote = array(
                                "vote" => $zero
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already disliked this review <div onclick='f2($i)'>X</div><div>";
                         echo '<div class="notification showNotification green"><div class="notificationText notificationMessage"> You have already disliked this review</div></div>';
                            }
                    } else
                    if ($vote == "-1") {
                        foreach ($get_review_map as $key => $review_map) {

                            $dbvote = $review_map->vote;
                        }
                        if ($dbvote == "0") {

                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $downvote = $downvote + 1;
                            $review_upvote_data = array(
                                "downvote" => $downvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);


                            $one = "-1";
                            $review_map_vote = array(
                                "vote" => $one
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have disliked one review <div onclick='f2($i)'>X</div><div>";
              echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage"> You have disliked one review</div></div>';
                            } else {
                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $upvote = $upvote - 1;
                            $review_downvote_data = array(
                                "upvote" => $upvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);




                            $zero = "0";
                            $review_map_vote = array(
                                "vote" => $zero
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already liked this review <div onclick='f2($i)'>X</div><div>";
                       echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage"> You have already liked this review</div></div>';
                            }
                    }
                }
            }
        } else {
            foreach ($get_review as $key => $review) {


                $downvote = $review->downvote;
                $upvote = $review->upvote;
            }
           // echo "<span class='upVoting' id='upclick$i'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$i'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$i' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> Please login then like or dislike <div onclick='f2($i)'>X</div><div>";
         echo '<div class="notification showNotification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="' . base_url() . 'my-account" class="loginBtn notificationBtn">Login</a> or <a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>';
            }
    }

    function getbussinesssch($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //$result = " " . date("g:i a", strtotime($bt->mor_start_time)) . " - " . date("g:i a", strtotime($bt->mor_end_time)) . " & " . date("g:i a", strtotime($bt->eve_start_time)) . " - " . date("g:i a", strtotime($bt->eve_end_time));
					$result = " " . date("g:i a", strtotime($bt->mor_start_time)) . " - " . date("g:i a", strtotime($bt->eve_end_time));
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    $result = " " . date("g:i a", strtotime($bt->mor_start_time)) . " - " . date("g:i a", strtotime($bt->mor_end_time));
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    $result = " " . date("g:i a", strtotime($bt->eve_start_time)) . " - " . date("g:i a", strtotime($bt->eve_end_time));
                }
            }
        }
        return $result;
    }

    function getbussinesstime($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            //echo 'Not empty';
            //echo $business_id;
            //print_r($businesstime);
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                    //echo 'Case 1';
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 2';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } elseif (date('H:i:s') > $bt->eve_start_time && date('H:i:s') < $bt->eve_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    //echo 'Case 3';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 4';
                    $datetime = date('G:i:s'); //echo $datetime;echo '             ';echo strtotime($datetime);echo '         ';
                    //echo strtotime($bt->eve_start_time);echo '                         ';
                    //echo strtotime($bt->eve_end_time);
                    if (strtotime($datetime) > strtotime($bt->eve_start_time) && strtotime($datetime) < strtotime($bt->eve_end_time)) {
                        //echo 'in';
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                }
            }
        }
        return $result;
    }

    function getbussinesscat($categories_list) {
        //$categories = $this->general_model->get_enum('category', 'id', 'name');
        $businesscat = explode(',', $categories_list);
        //print_r($businesscat);
        $result = '';
        if (!empty($businesscat)) {
            foreach ($businesscat as $key => $val) {
                $result = $result . '<li>' . $this->categories[trim($val)] . '</li>';
            }
        }
        return $result;
    }

    function getbussinesloc($business_id) {
        $business = $this->general_model->get_enum('business', 'id', 'title');
        $businessloc = $this->general_model->find_by_sql('SELECT * FROM business WHERE title = "' . $business[$business_id] . '"');
        return count($businessloc);
    }

    function getreviewvote() {
        $vote = $_GET['vote'];
        $review_id = $_GET['review_id'];
        $vid = $this->session->userdata("id");
        $get_review = $this->general_model->find_by_sql('SELECT * FROM `business_review` WHERE id="' . $review_id . '"');
        if (isset($this->session->userdata["id"])) {

            $table = "review_map";
            $cond = array(
                "user_id" => $vid,
                "review_id" => $review_id
            );
            $business_review_user_map_count = $this->general_model->record_count_where($table, $cond);

            $get_review_map = $this->general_model->find_by_sql('SELECT * FROM `review_map` WHERE user_id="' . $vid . '" AND review_id="' . $review_id . '"');
            if ($business_review_user_map_count == "0") {

                $data = array("vote" => $vote,
                    "user_id" => $vid,
                    "review_id" => $review_id);
                $store_review_map = $this->general_model->save($table, $data);
                if ($vote == "1") {


                    foreach ($get_review as $key => $review) {

                        $upvote = $review->upvote;
                        $downvote = $review->downvote;
                    }
                    $upvote = $upvote + 1;
                    $review_upvote_data = array(
                        "upvote" => $upvote
                    );
                    $review_table = "business_review";
                    $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);
                   // echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have liked one review <div onclick='f2($review_id)'>X</div><div>";
                echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You have liked one review</div></div>';
                    } else
                if ($vote == "-1") {

                    foreach ($get_review as $key => $review) {

                        $downvote = $review->downvote;
                        $upvote = $review->upvote;
                    }
                    $downvote = $downvote + 1;
                    $review_downvote_data = array(
                        "downvote" => $downvote
                    );
                    $review_table = "business_review";
                    $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);
                  //  echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have disliked one review <div onclick='f2($review_id)'>X</div><div>";
                  echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have disliked one review  </div></div>';
                    
                    }
            } else {

                foreach ($get_review as $key => $review) {

                    $downvote = $review->downvote;
                    $upvote = $review->upvote;
                }
                foreach ($get_review_map as $key => $review_map) {

                    $dbvote = $review_map->vote;
                }

                if ($vote == $dbvote && $vote == "1") {
                  //  echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already liked this review <div onclick='f2($review_id)'>X</div><div>";
                
                     echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have already liked this review </div></div>';
                } else
                if ($vote == $dbvote && $vote == "-1") {
                 //   echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already disliked this review <div onclick='f2($review_id)'>X</div><div>";
               echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage">You have already disliked this review </div></div>';
                    } else {
                    if ($vote == "1") {

                        foreach ($get_review_map as $key => $review_map) {

                            $dbvote = $review_map->vote;
                        }
                        if ($dbvote == "0") {

                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $upvote = $upvote + 1;
                            $review_upvote_data = array(
                                "upvote" => $upvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);


                            $one = "1";
                            $review_map_vote = array(
                                "vote" => $one
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have liked one review <div onclick='f2($review_id)'>X</div><div>";
                        echo '<div class="notification showNotification green"><div class="notificationText notificationMessage">You have liked one review</div></div>';
                            } else {
                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $downvote = $downvote - 1;
                            $review_downvote_data = array(
                                "downvote" => $downvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);




                            $zero = "0";
                            $review_map_vote = array(
                                "vote" => $zero
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                          //  echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already disliked this review <div onclick='f2($review_id)'>X</div><div>";
                        
                              echo '<div class="notification showNotification green"><div class="notificationText notificationMessage"> You have already disliked this review</div></div>';
                            }
                    } else
                    if ($vote == "-1") {
                        foreach ($get_review_map as $key => $review_map) {

                            $dbvote = $review_map->vote;
                        }
                        if ($dbvote == "0") {

                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $downvote = $downvote + 1;
                            $review_upvote_data = array(
                                "downvote" => $downvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_upvote_data, $review_id);


                            $one = "-1";
                            $review_map_vote = array(
                                "vote" => $one
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have disliked one review <div onclick='f2($review_id)'>X</div><div>";
                         echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage"> You have disliked one review</div></div>';
                            
                            } else {
                            foreach ($get_review as $key => $review) {


                                $downvote = $review->downvote;
                                $upvote = $review->upvote;
                            }
                            $upvote = $upvote - 1;
                            $review_downvote_data = array(
                                "upvote" => $upvote
                            );
                            $review_table = "business_review";
                            $update_business_treview = $this->general_model->update_row($review_table, $review_downvote_data, $review_id);




                            $zero = "0";
                            $review_map_vote = array(
                                "vote" => $zero
                            );
                            $review_map_table = "review_map";
                            $update_business_treview = $this->general_model->update_review_map($review_map_table, $review_map_vote, $vid, $review_id);
                           // echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> You have already liked this review <div onclick='f2($review_id)'>X</div><div>";
                        
                              echo '<div class="notification showNotification orange"><div class="notificationText notificationMessage"> You have already likedddd this review</div></div>';
                            }
                    }
                }
            }
        } else {
            foreach ($get_review as $key => $review) {


                $downvote = $review->downvote;
                $upvote = $review->upvote;
            }
           // echo "<span class='upVoting' id='upclick$review_id'></span><span class='reviewVotingNumb'><div>$upvote</div></span><span class='downVoting' id='downclick$review_id'></span><span class='reviewVotingNumb' id='down'><div></div></span><div id='light$review_id' style='display:block;position:fixed;top:1450px;left:25%;width:50%;height:180px;padding:16px;border: 16px solid orange;background-color: white;z-index:1002;overflow:auto;'><h1>Yellow Pages</h1> Please login then like or dislike <div onclick='f2($review_id)'>X</div><div>";
        
            echo '<div class="notification showNotification"><div class="notificationText">If you are already registered memeber please <span class="actionsBlock"><a href="' . base_url() . 'my-account" class="loginBtn notificationBtn">Login</a> or <a href="' . base_url() . 'register" class="regBtn notificationBtn">Register</a></span></div><div class="closeNotification">Close</div></div>'; 
            }
    }

    function checkThumbnail($id){
        
        $file = '/var/www/yellowpages.in/static/business-images/'.$id.'/190x125_'.$id.'-1.jpg';
        return file_exists($file);
    }
    
    function prepareBusinessLink($id, $name,$area,$city) {
        $name = $name."-".$area."-".$city;
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/b/' . strtolower($name) . '/' . $id;
    }
    
    function prepareCategoryLink($id,$name){
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '',$name);
        $name = preg_replace('/-+/', '-',$name);
        return '/hyderabad/'.strtolower($name).'/'.$id;
    }
    
}

?>