<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Role extends CI_Controller {
		
		public function __construct() {
			parent::__construct();
			if(!$this->session->userdata('logged_in')){
				redirect('');
			}
			
			$logged_in = $this->session->userdata('logged_in');
		}	
		
		
		function index($id = NULL){
			$data['role'] = $this->general_model->get_enum_where('role' , 'id' , 'name' , array('is_deleted' => 0));
			$data['role_row'] = $this->general_model->fetch_row_by_key('role' , 'id' ,$id);
			
			$query = "SELECT role.*, Count(users.id) AS total FROM role LEFT JOIN users ON role.id = users.role_id	WHERE role.is_deleted = 0 GROUP  BY role.id";
			
			if(!empty($_GET) && isset($_GET)){
				
				$subquery = '   AND  1';
				if(!empty($_GET['role_id']) && isset($_GET['role_id']) && ($_GET['role_id'] != 0)){
					$subquery .= '   AND  role.report_id = ' . $_GET['role_id'] ;
				}
				
				if(!empty($_GET['role_name']) && isset($_GET['role_name'])){
					$subquery .= '   AND  role.name  LIKE  "%' . $_GET['role_name'] .'%"' ;
				}
				
				$query = "SELECT role.*, Count(users.id) AS total 
				FROM role LEFT JOIN users ON role.id = users.role_id	
				WHERE role.is_deleted = 0 " . $subquery . "  GROUP  BY role.id	";
				
			}
			
			
			$currentPage = $this->uri->segment(3) ? $this->uri->segment(3) : 0;
			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['total_rows'] = count($data['results']);		   
			
			$config['per_page'] = PERPAGE;
			$config['use_page_numbers'] = TRUE;
			
			$config['full_tag_open'] = '<div class="col-sm-12" ><ul class="pagination pcolour" style="float:right">';
			$config['full_tag_close'] = '</ul></div><!--pagination-->';
			
			$config['first_link'] = 'First';
			$config['first_tag_open'] = '<li class="prev page">';
			$config['first_tag_close'] = '</li>';
			
			$config['last_link'] = 'Last';
			$config['last_tag_open'] = '<li class="next page">';
			$config['last_tag_close'] = '</li>';
			
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li class="next page">';
			$config['next_tag_close'] = '</li>';
			
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev page">';
			$config['prev_tag_close'] = '</li>';
			
			$config['cur_tag_open'] = '<li class="active"><a href="">';
			$config['cur_tag_close'] = '</a></li>';
			
			$config['num_tag_open'] = '<li class="page">';
			$config['num_tag_close'] = '</li>';
			if(isset($_REQUEST['sort']))
			{
				$sort=$_REQUEST['sort'];
				$sortTemp=explode("-",$sort);
				$sort_column=$sortTemp[0];
				$sort_order=$sortTemp[1];
				if($sort_column!='user')
				{
				$query.=" ORDER BY role.".$sort_column." ".$sort_order;	
				}else
				{
				$query.=" ORDER BY Count(users.id)"." ".$sort_order;	
				}
			}
			
			$query.=" LIMIT ".$currentPage.",".$config['per_page'];	
			//echo $query;exit;
			
			$data['results'] = $this->general_model->find_by_sql($query);
			$config['base_url'] = base_url().'role/index';
			$this->pagination->initialize($config);		
			$data["links"]    = $this->pagination->create_links();			
			$data['total'] = $this->general_model->record_count_where('role', array('is_deleted' => 0));
			$data['pagermessage'] = '';
			if($data['links']!= '') {
					$data['pagermessage'] = 'Showing '.((($this->pagination->cur_page-1)*$config["per_page"])+1).' to '.($this->pagination->cur_page*$config["per_page"]).' of '.$config["total_rows"];
				}
			
			$content = 	$this->load->view('role/index',$data,true);
			$this->render($content);
			
		}	
		
		
		function create(){
			$data['role'] = $this->general_model->get_enum('role' , 'id' , 'name');
			
			if(isset($_POST) && !empty($_POST)){				
				$roleactive = isset($_POST['roleactive']) ? $_POST['roleactive'] : 0;
				$data = array(				
				"name" => ucfirst($_POST['newrole']),
				"report_id" => $_POST['report_id'],				
				"is_deleted" => 0,
				"status" => $roleactive,				
				"created_by" =>0,
				"created_on" => date('Y-m-d H:i:s')				
				);
				
				$role_id = $this->general_model->save('role' , $data);
				redirect('role');
			}
			$content = 	$this->load->view('role/create',$data,true);
			$this->render($content);
		}
		
		
		function edit(){
			if($_POST){
				$roleactive = isset($_POST['roleactive']) ? $_POST['roleactive'] : 0;
				$data = array(				
				"name" => ucfirst($_POST['newrole']),
				"report_id" => $_POST['report_id'],	
				"status" => $roleactive
				);
				$this->general_model->update_row("role", $data, $_POST['roleId']);
				echo 1;
			}
			exit;
		}
		
		function delete($id=null){
			if($id){
				$this->general_model->raw_sql('UPDATE role SET is_deleted = !(is_deleted) WHERE id ='. $id);
				redirect('role');
			}
			return false;
		}
		
		function change_status(){
			if(isset($_REQUEST) && !empty($_REQUEST)){
				$this->general_model->update_row("role", array("status"=>$_REQUEST['status']) , $_REQUEST['id']);
				echo "Role status has been changed successfully.";
			}
			return false;
		}
		
	}
?>