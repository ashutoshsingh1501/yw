<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        
    }

    function send_mail($receiver_email, $subject, $message) {
        
        $this->load->library('email');
        $this->email->from(SENDER_MAIL, SENDER_NAME);
        $this->email->to($receiver_email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return TRUE;
        } else {
            //echo $this->email->print_debugger();//DEBUG
            return FALSE;
        }
    }

    function about_us() {
        $content = $this->load->view('static-pages/about-us', "", true);
        $this->render_user($content);
    }

    function contact_us() {
        $this->load->helper('form');
        $content = $this->load->view('static-pages/contact-us', "", true);
        $this->render_user($content);
    }

    function contact_us_mail() {

        $this->load->helper('inflector');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required', array('required' => 'Please provide a business name'));
        $this->form_validation->set_rules('user_contact', 'Phone No', 'trim|required|is_numeric|min_length[10]|max_length[10]', array('required' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'min_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'max_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix'));
        $this->form_validation->set_rules('user_mail', 'Email', 'trim|required|valid_email', array('required' => 'Please provide valid email id',
            'valid_email' => 'Please provide valid email id'));
        $this->form_validation->set_rules('user_message', 'Message', 'trim|required', array('required' => 'Please write your query.'));

        if ($this->form_validation->run() == FALSE) {
            $this->contact_us();
        } else {
            $user_name = $this->input->post('user_name');
            $user_contact = $this->input->post('user_contact');
            $user_mail = $this->input->post('user_mail');
            $user_message = $this->input->post('user_message');

            $subject = $user_name." - ".$user_contact." tried to contact you";
            $message = "<br> Hi,<br><br> $user_name tried to contact you using yellowpages.in. Below is user's contact information,<br><br> Name: $user_name<br> Contact: $user_contact<br>mail: $user_mail<br> What they said: $user_message<br><br>Thanks.";
            $ack_subject = "You tried to contact yellowpages.in";
            $ack_message = "<br> Hi $user_name,<br><br>You recently tried to contact yellowpages.in. We have noted your contact information and our team would get back to you shortly. Below is the contact information you shared,<br><br>Name: $user_name<br>Contact: $user_contact<br>Mail: $user_mail<br>What you said: $user_message<br><br>If you haven't contacted yellowpages.in or someone else used your email, please email to admin@yellowpages.in to report this issue.";

//            echo "SUCCESS: Contact Us<br/><br/>" . $team_mail . "<br/>" . $subject . "<br/>" . $message . "<br/>" . $ack_subject . "<br/>" . $ack_message;

            if ($this->send_mail(TEAM_MAIL, $subject, $message)) {
                if($this->send_mail($user_mail, $ack_subject, $ack_message)){
                    $this->session->set_flashdata("success", "Enquiry Email sent successfully.");
                    redirect('welcome');
                }else{
                   $this->session->set_flashdata("error", "Error in sending Acknowledgement.");
                   $this->contact_us(); 
                }
            } else {
                $this->session->set_flashdata("error", "Error in sending Enquiry Email.");
                $this->contact_us();
            }
        }
    }

    function feedback() {
        $this->load->helper('form');
        $content = $this->load->view('static-pages/feedback', "", true);
        $this->render_user($content);
    }

    function feedback_mail() {

        $this->load->helper('inflector');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Username', 'trim|required', array('required' => 'Please provide a business name'));
        $this->form_validation->set_rules('user_contact', 'Phone No', 'trim|required|is_numeric|min_length[10]|max_length[10]', array('required' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'min_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix',
            'max_length' => 'Please provide 10 digit valid phone number, without +91 or 0 prefix'));
        $this->form_validation->set_rules('user_mail', 'Email', 'trim|required|valid_email', array('required' => 'Please provide valid email id',
            'valid_email' => 'Please provide valid email id'));
        $this->form_validation->set_rules('user_message', 'Message', 'trim|required', array('required' => 'Please write your feedback.'));

        if ($this->form_validation->run() == FALSE) {
            $this->feedback();
        } else {
            $user_name = $this->input->post('user_name');
            $user_contact = $this->input->post('user_contact');
            $user_mail = $this->input->post('user_mail');
            $user_message = $this->input->post('user_message');
            $rating = $this->input->post('rating');
            $suggest = $this->input->post('suggest');
            $user_type = $this->input->post('iam');
            $talk_about = $this->input->post('iwant');

            $subject = "$user_type want to talk about $talk_about";
            $message = "<br><br>Hi,<br><br> $user_name, a $user_type shared feedback for yellowpages.in. Below is the user's information and feedback they shared,<br><br>Name: $user_name<br>Mobile: $user_contact<br>Email: $user_mail<br>What they said: $user_message<br><br>User's experience with yellowpages.in - $rating<br>User is $suggest in recommending yellowpages.in to their friends.<br><br>";
            $ack_subject = "Your feedback for yellowpages.in";
            $ack_message = "<br><br>Hi $user_name,<br>We have noted your feedback and we promise to take appropriate action on this. Below is your contact information and feedback,<br><br>Name: $user_name<br>Mobile: $user_contact<br>Email: $user_mail<br>What you said: $user_message<br><br>Your experience with yellowpages.in - $rating<br>You $suggest in recommending yellowpages.in to their friends<br><br>If you haven't shared feedback for yellowpages.in or someone else used your email, please email to admin@yellowpages.in to report this issue.";

            //echo "SUCCESS: Contact Us<br/><br/>" . TEAM_MAIL . "<br/>" . $subject . "<br/>" . $message . "<br/>" . $ack_subject . "<br/>" . $ack_message;exit;
            
            if ($this->send_mail(TEAM_MAIL, $subject, $message)) {
                if($this->send_mail($user_mail, $ack_subject, $ack_message)){
                    $this->session->set_flashdata("success", "Feedback mail sent successfully.");
                    redirect('welcome');
                }else{
                    $this->session->set_flashdata("error", "Error in sending Acknowledgement.");
                    $this->feedback();
                }
            } else {
                $this->session->set_flashdata("error", "Error in sending Feedback Email.");
                $this->feedback();
            }
        }
    }

    function faqs() {
        $content = $this->load->view('static-pages/faqs', "", true);
        $this->render_user($content);
    }

    function careers() {
        $content = $this->load->view('static-pages/careers', "", true);
        $this->render_user($content);
    }

    function terms_conditions() {
        $content = $this->load->view('static-pages/terms-conditions', "", true);
        $this->render_user($content);
    }

    function eua() {
        $content = $this->load->view('static-pages/end-user-agreement', "", true);
        $this->render_user($content);
    }

    function privacy_policy() {
        $content = $this->load->view('static-pages/privacy-policy', "", true);
        $this->render_user($content);
    }

}

?>