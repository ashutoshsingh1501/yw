<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Categories_model');
    }

    function index() {
        $svalue = $this->input->get();

        if (isset($svalue) && !empty($svalue)) {
            $categories = $this->Categories_model->get_categories_by_letter($svalue['s']);
            $data['active'] = $svalue['s'];
        } else {
            $categories = $this->Categories_model->get_categories();
            $data['active'] = 'all';
        }

//        echo "<pre>";
//        print_r($categories);
//        exit;

        $featured_categories = array();
		if(!empty($categories)){
        foreach ($categories as $key => $r) {
            array_push($featured_categories, array(
                'id' => $r->id,
                'name' => $r->name,
                'image_path' => $r->image_path,
                'link' => $this->prepareCategoryLink($r->id, $r->name)
                    )
            );
        }
		}
        
        $data['categories'] = $featured_categories;
        $content = $this->load->view('categories', $data, true);
        $this->render_user($content);
    }

    function prepareCategoryLink($id, $name) {
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/hyderabad/' . strtolower($name) . '/' . $id;
    }

}

?>