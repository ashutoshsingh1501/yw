<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->data['category'] = $this->general_model->find_by_sql('SELECT * FROM category WHERE level = 0 AND status = 1 AND is_deleted = 0 AND is_primary = 1');
        $this->data['fbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1');
        $this->data['cbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 order by count desc Limit 4');
        $this->data['areas'] = $this->general_model->get_enum('area', 'id', 'name');
        $this->data['cities'] = $this->general_model->get_enum('city', 'id', 'name');
        $this->data['ratings'] = $this->general_model->get_enum('business_rating', 'id', 'title');

        $this->data['lbusiness'] = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 order by id desc Limit 2');
        $this->data['recentrev'] = $this->general_model->find_by_sql('SELECT * FROM `business_review` LEFT JOIN business on business.id = business_review.business_id WHERE business.is_deleted = 0 and business.status = 1 ORDER BY business_review.id Limit 4');

        $this->data['nusers'] = $this->general_model->get_enum('users', 'id', 'display_name');
        $this->data['iusers'] = $this->general_model->get_enum('users', 'id', 'image_url');
        $this->ratings = $this->general_model->get_enum('business_rating', 'id', 'title');
        $this->areas = $this->general_model->get_enum('area', 'id', 'name');
        $this->cities = $this->general_model->get_enum('city', 'id', 'name');
    }

    function register() {
        $this->load->helper('form');
        $content = $this->load->view('register', $this->data, true);
        $this->render_user($content);
    }
    function forgot_password() {
        $this->load->helper('form');
        $content = $this->load->view('forgot-password', $this->data, true);
        $this->render_user($content);
    }

    function change_password() {
        $this->load->helper('form');
        $content = $this->load->view('change_password', $this->data, true);
        $this->render_user($content);
    }
    function get_myfilters() {
        print_r($_POST);
        print_r($_GET);

        /*
          echo $_GET['search']."<br>";
          echo $_GET['location']."<br>";

          echo $_GET['attr_value']."<br>";
          echo $_GET['filter_id']."<br>";

          echo $_GET['filter_term']."<br>";
          echo $_GET['vals']."<br>";
         * */
        $msg = "";
        $qry = "SELECT * from business LIMIT 10";
        $search_result = $this->general_model->find_by_sql($qry);
        foreach ($search_result as $key => $r) {
            if (isset($this->session->userdata["id"])) {
                $uid = $this->session->userdata["id"];
                $bid = $r->id;
                $table = "visitor_business";
                $cond = array(
                    "visitor_id" => $uid,
                    "business_id" => $bid
                );
                $favourite_records = $this->general_model->record_count_where($table, $cond);
                if ($favourite_records == "0") {
                    $favclass = "addFav";
                } else {
                    $favclass = "addFav favActive";
                }
            } else {
                $favclass = "addFav";
            }
            /*$location = $this->getbussinesloc($r->id);

            if ($location > 1) {
                $locations = $location . " Locations";
            } else {
                $locations = $location . " Location";
            }*/

            if ($r->treview > 1) {

                $review = $r->treview . " reviews";
            } else {
                $review = $r->treview . " review";
            }




            if ($r->pincode) {
                $city_pincode = $this->cities[$r->city_id] . " - " . $r->pincode;
            } else {
                $city_pincode = $this->cities[$r->city_id];
            }
            $websiteurl1 = "http://" . $r->website_url;
            $datetime = $this->getbussinesstime($r->id);
            if ($datetime) {
                $datetime = "<strong>Open now</strong> - " . $datetime;
            } else {
                $datetime = "<strong> </strong>";
            }
            if ($r->is_verified == "1") {
                $verified = 'ypApproved';
            } else {
                $verified = '';
            }

            $msg .= '<li><div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <img width="170" height="170" src="' . $r->image_path . '" alt="">
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <a href="' . base_url() . 'business_detail?id=' . $r->id . '" class="eachPopularTitle">
                                                    ' . $r->title . '
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <a target="_blank" href="' . $r->website_url . '"><span class="openNewWindow"></span></a>
                                                    <span class="' . $verified . '"></span>
                                                    <span class="certified aaP">' . $this->ratings[$r->rating_id] . '</span>
                                                </div>
                                            </div>
                                            <div class="eachPopularRateOpen">
                                                <div class="eachPopularRatingBlock">
                                                    <span class="rating r' . $r->roverall . '">' . $r->roverall . '</span>
                                                    <a href="#" class="ratingCount">' . $review . '</a>
                                                </div>
                                                <div class="openNow">' . $datetime . '</div>
                                            </div>
                                            <ul class="eachPopularTagsList">
                                            <li></li>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <a href="mailto:' . $r->email . '">' . $r->email . '</a>
                                                <a href="' . $r->website_url . '" target="_blank">website</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="' . $favclass . '" onclick="addtofavorite(' . $r->id . ')">Add Favorite</div>
                                        <a class="businessContact" href="tel:' . $r->phone . '">' . $r->phone . '</a>
                                        <address class="businessArea">
                                            <strong>' . $this->areas[$r->area_id] . '</strong>
                                           ' . $city_pincode . '
                                        </address>
                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount"></span>
                                            <a  href="//maps.google.com/maps?f=d&amp;daddr=17.444442,78.379597&amp;hl=en" target="_blank">Directions</a>
                                        </div>
                                    </div>
                                </div>
                            </li>';
        }
        $msg = "<ul class='popularThisWeekList'>" . $msg . "</ul>"; // Content for Data

        echo $msg;
    }

    function myreviews() {
        if (isset($this->session->userdata["active"])) {
            $data['businesses'] = $this->general_model->find_by_sql('SELECT * from business where business.id in ( select business_id from visitor_business where visitor_id = ' . $this->session->userdata["id"] . ' )');
            $data['breview'] = $this->general_model->find_by_sql('SELECT b.id, b.title, b.image_path, br.added_on, br.rating, br.comments, br.upvote FROM business_review br INNER JOIN business b on b.id = br.business_id where br.user_id = ' . $this->session->userdata["id"] . ' ORDER BY br.added_on DESC  LIMIT 4');
            $content = $this->load->view('account_visitor/my-reviews', $data, true);
        } else {
            $this->load->helper('form');
            $content = $this->load->view('flogin', $this->data, true);
        }
        $this->render_user($content);
    }

    function oldpasswordcheck() {
        $old_password = $_REQUEST['old_password'];
        $old_password = md5($old_password);
        $table = "visitor_account";
        $vid = $this->session->userdata("id");
        $cond = array(
            "password" => $old_password,
            "id" => $vid
        );

        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {
            echo "true";
        } else {
            echo "false";
        }
    }
    function check_otp() {
        $otp = $_REQUEST['otp'];

          if($this->session->userdata('otp')==$otp)
          {
            $this->session->unset_userdata('otp');
            $email_for_change_pass = $this->session->userdata('email_for_change_pass');
            $this->session->set_userdata('change_pass', $email_for_change_pass);

            redirect('change-password');
        }
        
         
         
         
    }
    function check_for_otp() {
        $otp = $_REQUEST['otp'];

          if($this->session->userdata('otp')==$otp)
          {
         echo "true";
          }
          else 
          {
         echo "false";

          }
        
         
         
         
     }
    function emailcheck() {
        $username = $_REQUEST['username'];

        $table = "visitor_account";

        $cond = array(
            "username" => $username
        );
        $this->session->set_userdata($cond);
        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {
            echo "true";
        } else {
            echo "false";
        }
    }
    function forgotemailcheck() {
        $username = $_REQUEST['forgot_email'];

        $table = "visitor_account";

        $cond = array(
            "username" => $username
        );
        $this->session->set_userdata($cond);
        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {
            echo "true";
        } else {
            echo "false";
        }
    }
    function forgot_pass() {

        $forgot_pass = $_REQUEST['forgot_email'];





        $get_user_data = $this->general_model->find_by_sql('SELECT * FROM `visitor_detail` WHERE email="' . $forgot_pass . '"');
        foreach ($get_user_data as $key => $sms) {

            $user_mobile = $sms->phone;
           
        }
        $apiKey = urlencode('JyiGQa9SZ+4-ppyR9ZsbpIfdzT8bc4GeNoP9DjxSrA');

        // Message details
        $sender = urlencode('TXTLCL');
       $otp_code = rand("000000","999999");
        $otp = "otp : ".$otp_code;
        $this->session->set_userdata('otp', $otp_code);
        $this->session->set_userdata('email_for_change_pass', $forgot_pass);

        $msg = "$otp";
        $message = rawurlencode($msg);

        // Prepare data for POST request
        $data = 'apiKey=' . $apiKey . '&numbers=' . $user_mobile . "&sender=" . $sender . "&message=" . $message;

        // Send the GET request with cURL
        $ch = curl_init('http://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if($response)
        {
           redirect('my-account'); 
        }
        else
        {
            echo "false";
        }
        curl_close($ch);
        
        
        
     
    }
    function phonecheck() {
        $phone = $_REQUEST['phone'];

        $table = "business";

        $cond = array(
            "phone" => $phone
        );
        $this->session->set_userdata($cond);
        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {

            echo "false";
            }
            else {

            echo "true";
        }
    }
    function businessemailcheck() {
        $email = $_REQUEST['email'];

        $table = "business";

        $cond = array(
            "email" => $email
        );
        $this->session->set_userdata($cond);
        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {

            echo "false";
            }
            else {

            echo "true";
        }
    }
    function registeremailcheck() {
        $username = $_REQUEST['email'];
        $table = "visitor_account";

        $cond = array(
            "username" => $username
        );
        $this->session->set_userdata($cond);
        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {
            echo "false";
        } else {
            echo "true";
        }
    }
    function logincheck() {
        $password = $_REQUEST['pass'];
        $password = md5($password);
        $email = $this->session->userdata("username");
        $table = "visitor_account";

        $cond = array(
            "password" => $password,
            "username" => $email
        );

        $check_user_datacount = $this->general_model->record_count_where($table, $cond);
        if ($check_user_datacount == "1") {
            echo "true";
        } else {
            echo "false";
        }
    }
    function index() {
        if (isset($this->session->userdata["active"])) {

            $qry = "SELECT * from business where business.id in ( select business_id from visitor_business where visitor_id = " . $this->session->userdata["id"] . " )";
            $search_result = $this->general_model->find_by_sql($qry);
            //print_r($search_result);
            $this->data['search_result'] = $this->general_model->find_by_sql($qry);
            $businesses = array();
            foreach ($search_result as $key => $r) {
                array_push($businesses, array('image' => $r->image_path,
                    'business_id' => $r->id,
                    'title' => $r->title,
                    'page_url' => $r->page_url,
                    'is_verified' => $r->is_verified,
                    'rating' => $this->ratings[$r->rating_id],
                    'overall' => $r->roverall,
                    'review' => $r->treview,
                    'datetime' => $this->getbussinesstime($r->id),
                    'categories' => $this->getbussinesscat($r->categories_list),
                    'email' => $r->email,
                    'website_url' => $r->website_url,
                    'phone' => $r->phone,
                    'area' => (isset($this->areas[$r->area_id])) ? $this->areas[$r->area_id] : "",
                    'area_name' => $r->area_name,
                    'city' => (isset($this->cities[$r->city_id])) ? $this->cities[$r->city_id] : "",
                    'pincode' => $r->pincode
//                    'location' => $this->getbussinesloc($r->id)
                    )
                );
            }
            $this->data['businesses'] = $businesses;
            $this->data['breview'] = $this->general_model->find_by_sql('SELECT b.id, b.title, b.image_path, br.added_on, br.rating, br.comments, br.upvote FROM business_review br INNER JOIN business b on b.id = br.business_id where br.user_id = ' . $this->session->userdata["id"] . ' ORDER BY br.added_on DESC  LIMIT 4');
            $content = $this->load->view('account_visitor/my-favorites', $this->data, true);
        } else {
            $this->load->helper('form');
            $content = $this->load->view('flogin', $this->data, true);
        }
        $this->render_user($content);
    }

    function getbussinesstime($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            //echo 'Not empty';
            //echo $business_id;
            //print_r($businesstime);
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                    //echo 'Case 1';
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 2';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } elseif (date('H:i:s') > $bt->eve_start_time && date('H:i:s') < $bt->eve_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    //echo 'Case 3';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 4';
                    $datetime = date('G:i:s'); //echo $datetime;echo '             ';echo strtotime($datetime);echo '         ';
                    //echo strtotime($bt->eve_start_time);echo '                         ';
                    //echo strtotime($bt->eve_end_time);
                    if (strtotime($datetime) > strtotime($bt->eve_start_time) && strtotime($datetime) < strtotime($bt->eve_end_time)) {
                        //echo 'in';
                        $result = "until " . date("g:i a", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                }
            }
        }
        return $result;
    }

    function getbussinesscat($categories_list) {
        $categories = $this->general_model->get_enum('category', 'id', 'name');
        $businesscat = explode(',', $categories_list);
        //print_r($businesscat);
        $result = '';
        if (!empty($businesscat)) {
            foreach ($businesscat as $key => $val) {
                $result = $result . '<li>' . $categories[trim($val)] . '</li>';
            }
        }
        //$results = implode(',', $result);
        return $result;
    }

    function getbussinesloc($business_id) {
        $business = $this->general_model->get_enum('business', 'id', 'title');
        $businessloc = $this->general_model->find_by_sql('SELECT * FROM business WHERE title = "' . $business[$business_id] . '"');
        return count($businessloc);
    }
    public function rate_favorite_order($rate) {

        $this->session->set_userdata('rate_favorite_order', $rate);
        redirect('my-favorites');
    }
    public function favorite_date() {
        $date_by_favorite = "yes";
        $this->session->set_userdata('date_favorite', $date_by_favorite);
        redirect('my-favorites');
    }
    public function getdata_list() {

        if ($_POST['page']) {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = PERPAGE_MYFAVORITES;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
           $order="";
            $order1="";
            $ratethis="";
            if($this->session->userdata("rate_favorite_order"))
            {
            $ratethis=$this->session->userdata["rate_favorite_order"];
            }
            if($ratethis=="1")
            {
                $order = "ORDER BY roverall DESC";
            }
            else
            if($ratethis=="2")
            {
                $order = "ORDER BY roverall ASC";
            }
            else
            if($ratethis=="3")
            {
                $order1 = "ORDER BY added_on DESC";
                
            }
            else
            if($ratethis=="4")
            {
                $order1 = "ORDER BY added_on ASC";
            }
            $qry = "SELECT * from business where business.id in ( select business_id from visitor_business where visitor_id = " . $this->session->userdata["id"] . " $order1) $order LIMIT $start, $per_page";
            $search_result = $this->general_model->find_by_sql($qry);
            $qry2 = "SELECT COUNT(*) AS count from business where business.id in ( select business_id from visitor_business where visitor_id = " . $this->session->userdata["id"] . ")";
            $total_favorite_count = $this->general_model->find_by_sql($qry2);


            foreach ($total_favorite_count as $key => $br) {
                $count = $br->count;
            }
            //print_r($search_result);
            $businesses = array();


            $msg = "";
            if($count>0)
            {
                echo "<script> $('html, body').animate({scrollTop: $('body').offset().top}, 300); </script>";


                foreach ($search_result as $key => $r) {

//                    $location = $this->getbussinesloc($r->id);
//
//                    if ($location > 1) {
//                        $locations = $location . " Locations";
//                    } else {
//                        $locations = " ";
//                    }

                    /* if ($r->treview > 1) {
                      $review = $r->treview . " reviews";
                      } else {
                      $review = $r->treview . " review";
                      } */
                    if ($r->pincode) {
                        $city_pincode = $this->cities[$r->city_id] . " - " . $r->pincode;
                    } else {
                        $city_pincode = $this->cities[$r->city_id];
                    }
                    $websiteurl1 = "http://" . $r->website_url;
                    $datetime = $this->getbussinesstime($r->id);
                    if ($datetime) {
                        $datetime = "<strong>Open now</strong> - " . $datetime;
                    } else {
                        $datetime = "<strong> </strong>";
                    }
                    if ($r->website_url != '') {
                        $website = "Website";
                    } else {
                        $website = "";
                    }


                    if (isset($this->session->userdata["id"])) {
                        $uid = $this->session->userdata["id"];
                        $bid = $r->id;
                        $table = "visitor_business";
                        $cond = array(
                            "visitor_id" => $uid,
                            "business_id" => $bid
                        );
                        $favourite_records = $this->general_model->record_count_where($table, $cond);
                        if ($favourite_records == "0") {
                            $favclass = "addFav";
                        } else {
                            $favclass = "addFav favActive";
                        }
                    } else {
                        $favclass = "addFav";
                    }
                    $area_qry = "SELECT name from area where id in ( select area_id from business where id = " . $r->id . ")";
                    $areas = $this->general_model->fetch_row_by_sql($area_qry);
                    $area = $areas['name'];
                    $area = str_replace(' ', '+', $area);
                    $reviews = $this->general_model->fetch_row_by_sql("SELECT rating,count(*) AS rcount FROM business_review where business_id in (select id from business where id = " . $r->id . ")");
                    $breview = $reviews['rcount'];
                    $rating = $reviews['rating'];
                    /* if ($breview > 1) {
                      $review = $breview . " reviews";
                      } else {
                      $review = $breview . " review";
                      } */
                    if ($breview > 1) {
                        $review = $breview . " reviews";
                    } else if($breview < 1){
                        $review = " ";
                    } else {
                        $review = $breview . " review";
                    }
                    if ($r->phone) {
                        $r->phone = "+91 " . $r->phone;
                    } else if ($r->alternative_number) {
                        $r->phone = "+91 " . $r->alternative_number;
                    } else if ($r->landline) {
                        $r->phone = $r->landline;
                    } else {
                        $r->phone = $r->secondary_land_line;
                    }

                    if ($r->area_name == "") {
                        $area_name = $r->area_name;
                    } else {
                        $area_name = "<strong> " . $r->area_name . " </strong>";
                    }

                    $msg .= '<li id="myfav_bus' . $r->id . '"><div class="eachPopular">
                                    <div class="eachPopularLeft">
                                        <div class="eachPopularImage">
                                            <img width="170" height="170" src="' . $r->image_path . '" alt="">
                                        </div>
                                        <div class="eachPopularTitleBlock">
                                            <div class="popularTitleTextBlock">
                                                <a href="' . base_url() . 'business_detail?id=' . $r->id . '" class="eachPopularTitle">
                                                    ' . $r->title . '
                                                </a>
                                                <div class="eachPopularOtherActions">
                                                    <a target="_blank" href="' . $r->website_url . '"><span class="openNewWindow"></span></a>
                                                    <span class="ypApprovedItem"></span>
                                                    <span class="certified aaP">' . $this->ratings[$r->rating_id] . '</span>
                                                        
                                                </div>
                                            </div>
                                            <div class="eachPopularRateOpen">
                                                 <div class="eachPopularRatingBlock">
                                                    <span class="rating r' . str_replace(".", "-", $r->roverall) . '">' . $r->roverall . '</span>
                                                    <!--<span class="rating r' . str_replace(".", "-", $rating) . '">' . $rating . '</span>-->

                                            
                                                    <a href="#" class="ratingCount">' . $review . '</a>
                                                </div>
                                                <div class="openNow">' . $datetime . '</div>
                                            </div>
                                            <ul class="eachPopularTagsList">
                                            <li> '.$this->getbussinesscat($r->categories_list) .' </li>
                                            </ul>
                                            <div class="eachPopularLink">
                                                <a href="mailto:' . $r->email . '">' . $r->email . '</a>
                                                <a href="' . $r->website_url . '" target="_blank">' . $website . '</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="eachPopularRight">
                                        <div class="' . $favclass . '" onclick="getmy_favorite'.$r->id.'('.$r->id.')">Add Favorite</div>

                                       <a class="businessContact" href="tel:' . $r->phone . '">' . $r->phone . '</a>
                                        <address class="businessArea">
                                           ' . $area_name . '
                                           ' . $city_pincode . '
                                        </address>
                                        <div class="directionsLocationsBlock">
                                            <span class="locationsCount"></span>
                                            <a  href="http://maps.google.com/maps?q=' . $area . '+ON&loc:43.25911+-79.879494&z=15&output=embed" target="_blank">Directions</a>
                                        </div>
                                    </div>
                                </div>
                            </li>';
                }
                $msg = "<div class='data'><ul class='popularThisWeekList'>" . $msg . "</ul></div><div id='myfav_bus'></div>"; // Content for Data

            }
            else {
                $msg = "<div class='data'>Your favorite businesses would be shown here, for quick access.<br> As of now, you have not Favorited any business. <a href='". base_url()."'>Keep Favoriting</a></div>";
            }
                /* --------------------------------------------- */

              $this->session->set_userdata('myaccount_fav_count', $count);
     
            if($count>5)
            {
                $no_of_paginations = ceil($count / $per_page);

                /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
                if ($cur_page >= 7) {
                    $start_loop = $cur_page - 3;
                    if ($no_of_paginations > $cur_page + 3)
                        $end_loop = $cur_page + 3;
                    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                        $start_loop = $no_of_paginations - 6;
                        $end_loop = $no_of_paginations;
                    } else {
                        $end_loop = $no_of_paginations;
                    }
                } else {
                    $start_loop = 1;
                    if ($no_of_paginations > 7)
                        $end_loop = 7;
                    else
                        $end_loop = $no_of_paginations;
                }
                /* ----------------------------------------------------------------------------------------------------------- */
                $msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
                if ($first_btn && $cur_page > 1) {
                    $msg .= "<li p='1' class='active cl'>First</li>";
                } else if ($first_btn) {
                    $msg .= "<li p='1' class='inactive cl'>First</li>";
                }

// FOR ENABLING THE PREVIOUS BUTTON
                if ($previous_btn && $cur_page > 1) {
                    $pre = $cur_page - 1;
                    $msg .= "<li p='$pre' class='active cl'>Previous</li>";
                } else if ($previous_btn) {
                    $msg .= "<li class='inactive cl'>Previous</li>";
                }
                for ($i = $start_loop; $i <= $end_loop; $i++) {

                    if ($cur_page == $i)
                        $msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active cl'>{$i}</li>";
                    else
                        $msg .= "<li p='$i' class='active cl'>{$i}</li>";
                }

// TO ENABLE THE NEXT BUTTON
                if ($next_btn && $cur_page < $no_of_paginations) {
                    $nex = $cur_page + 1;
                    $msg .= "<li p='$nex' class='active cl'>Next</li>";
                } else if ($next_btn) {
                    $msg .= "<li class='inactive cl'>Next</li>";
                }

// TO ENABLE THE END BUTTON
                if ($last_btn && $cur_page < $no_of_paginations) {
                    $msg .= "<li p='$no_of_paginations' class='active cl'>Last</li>";
                } else if ($last_btn) {
                    $msg .= "<li p='$no_of_paginations' class='inactive cl'>Last</li>";
                }
                $goto = "<input type='text' class='goto cl' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
                $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
                $msg = $msg . "</ul>" . $goto . $total_string . "</div>";
            }
            $this->session->unset_userdata('rate_favorite_order');
            $this->session->unset_userdata('date_favorite');
            echo $msg;
            
    }
    }
    public function rating_order($rate) {

        $this->session->set_userdata('rate_order', $rate);
        redirect('my-reviews');
    }
      public function business_rating_order($rate,$bid) {

            $this->session->set_userdata('business_rate_order', $rate);
            redirect('business_detail?id='.$bid.'#reviews'); 
    }

    public function getreviews_list() {

        if ($_POST['page']) {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = PERPAGE_MYREVIEWS;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            $order="";
            $ratethis="";
            if($this->session->userdata("rate_order"))
            {
            $ratethis=$this->session->userdata["rate_order"];
            }
            if($ratethis=="1")
            {
                $order = "ORDER BY br.rating DESC";
            }
            else
            if($ratethis=="2")
            {
                $order = "ORDER BY br.rating ASC";
            }
            else
            if($ratethis=="3")
            {
                $order = "ORDER BY br.upvote DESC";
            }
            else
            if($ratethis=="4")
            {
                $order = "ORDER BY br.upvote ASC";
            }
            else
            if($ratethis=="5")
            {
                $order = "ORDER BY br.added_on DESC";
            }
            else
            if($ratethis=="6")
            {
                $order = "ORDER BY br.added_on ASC";
            }




            $qry = "SELECT b.id, b.title, b.image_path, br.added_on, br.rating, br.comments, br.upvote FROM business_review br INNER JOIN business b on b.id = br.business_id where br.user_id = " . $this->session->userdata["id"] . " $order LIMIT $start, $per_page";
            $search_result = $this->general_model->find_by_sql($qry);
            $qry2 = "SELECT COUNT(*) AS count FROM business_review br INNER JOIN business b on b.id = br.business_id where br.user_id = " . $this->session->userdata["id"] . " ORDER BY br.added_on DESC,br.rating ASC";
            $total_favorite_count = $this->general_model->find_by_sql($qry2);


            foreach ($total_favorite_count as $key => $br) {
                $count = $br->count;
            }
            //print_r($search_result);
            $businesses = array();


            $msg = "";
            if ($count > 0) {
                echo "<script> $('html, body').animate({scrollTop: $('body').offset().top}, 300); </script>";


                foreach ($search_result as $key => $r) {



                    $msg .= '<li>
							<div class="eachBusinessReview">
								<div class="photoVotesBlock">
									<div class="reviewerPhoto">
										<img src="' . $r->image_path . '" alt="">
									</div>
								</div>

								<div class="reviewTextBlock">
									<a href="' . base_url() . 'business_detail?id=' . $r->id . '" class="reviewTitle">' . $r->title . '</a>
									<div class="ratingBlock">
										<span class="rating r' . str_replace(".", "-", $r->rating) . '">$r->rating</span>
										<span class="ratingTimeDate">' . date('Y-m-d H:i', strtotime($r->added_on)) . '</span>
									</div>
									<p>' . $r->comments . '</p>
								</div>
								<div class="reviewVoting">
									<span class="upVoting">Upvote</span>
									<span class="reviewVotingNumb">' . $r->upvote . '</span>
									<span class="downVoting">Downvote</span>
								</div>
							</div>
						</li>';
                }
                $msg = "<div class='data'><ul class='reviewsList'>" . $msg . "</ul></div>"; // Content for Data


                /* --------------------------------------------- */


                $no_of_paginations = ceil($count / $per_page);

                /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
                if ($cur_page >= 7) {
                    $start_loop = $cur_page - 3;
                    if ($no_of_paginations > $cur_page + 3)
                        $end_loop = $cur_page + 3;
                    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                        $start_loop = $no_of_paginations - 6;
                        $end_loop = $no_of_paginations;
                    } else {
                        $end_loop = $no_of_paginations;
                    }
                } else {
                    $start_loop = 1;
                    if ($no_of_paginations > 7)
                        $end_loop = 7;
                    else
                        $end_loop = $no_of_paginations;
                }
                /* ----------------------------------------------------------------------------------------------------------- */
                $msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
                if ($first_btn && $cur_page > 1) {
                    $msg .= "<li p='1' class='active cl'>First</li>";
                } else if ($first_btn) {
                    $msg .= "<li p='1' class='inactive cl'>First</li>";
                }

// FOR ENABLING THE PREVIOUS BUTTON
                if ($previous_btn && $cur_page > 1) {
                    $pre = $cur_page - 1;
                    $msg .= "<li p='$pre' class='active cl'>Previous</li>";
                } else if ($previous_btn) {
                    $msg .= "<li class='inactive cl'>Previous</li>";
                }
                for ($i = $start_loop; $i <= $end_loop; $i++) {

                    if ($cur_page == $i)
                        $msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active cl'>{$i}</li>";
                    else
                        $msg .= "<li p='$i' class='active cl'>{$i}</li>";
                }

// TO ENABLE THE NEXT BUTTON
                if ($next_btn && $cur_page < $no_of_paginations) {
                    $nex = $cur_page + 1;
                    $msg .= "<li p='$nex' class='active cl'>Next</li>";
                } else if ($next_btn) {
                    $msg .= "<li class='inactive cl'>Next</li>";
                }

// TO ENABLE THE END BUTTON
                if ($last_btn && $cur_page < $no_of_paginations) {
                    $msg .= "<li p='$no_of_paginations' class='active cl'>Last</li>";
                } else if ($last_btn) {
                    $msg .= "<li p='$no_of_paginations' class='inactive cl'>Last</li>";
                }
                $goto = "<input type='text' class='goto cl' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
                $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
                $msg = $msg . "</ul>" . $goto . $total_string . "</div>";
            } else {
                $msg = "<div class='data'>No Reviews Posted For Any Business</div>";
            }
            echo $msg;
        }
    }
}
