<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search_map extends CI_Controller {

    public $data;

    public function __construct() {

        parent::__construct();

        $ratings = $this->general_model->get_enum('business_rating', 'id', 'title');
        $areas = $this->general_model->get_enum('area', 'id', 'name');
        $cities = $this->general_model->get_enum('city', 'id', 'name');
        $svalue = $this->input->get();

        switch (key($svalue)) {
            case 'search':
                //echo "business listing by search \n\n for single word ".$svalue['search'] . " & location " . $svalue['location'] . "\n\n";
                //search business by business name
                $b_qry = '(select * from business WHERE  business.title RLIKE "' . $svalue['search'] . '" ORDER BY id DESC)';

                //search business by category name
                $c_qry = '(SELECT * from business where id in 
                            (select business_id from business_attribute where category_id in
                                (select id from category where category.name RLIKE "' . $svalue['search'] . '" ORDER BY id DESC)
                            )
                        )';

                //search business by tag name
                $t_qry = '(SELECT * from business where id in 
                            (select business_id from business_attribute where category_id in
                                (select id from category where category.tags RLIKE "' . $svalue['search'] . '" ORDER BY id DESC)
                            )
                        )';

                $meta_query = 'select * from ( ';
                $meta_query .= $b_qry;
                $meta_query .= " UNION ";
                $meta_query .= $c_qry;
                $meta_query .= " UNION ";
                $meta_query .= $t_qry;
                $meta_query .= ' ) as a';

                $search_result = $this->general_model->find_by_sql($meta_query);
                $count = count($search_result);
                $msg="Sorry! no results found for '".$svalue['search']."' in ".$svalue['location'];
                if($count){
                    $msg=$count." results found for '".$svalue['search']."' in ".$svalue['location'];
                }
                $businesses = array();
                foreach ($search_result as $key => $r) {
                    array_push($businesses, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'is_paid' => $r->is_paid,
                        'rating' => $ratings[$r->rating_id],
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesstime($r->id),
                        'categories' => $this->getbussinesscat($r->id),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => $r->phone,
                        'area' => (isset($areas[$r->area_id])) ? $areas[$r->area_id] : "",
                        'city' => (isset($cities[$r->city_id])) ? $cities[$r->city_id] : "",
                        'pincode' => $r->pincode,
                        'location' => $this->getbussinesloc($r->id),
                        'latitude' => $r->latitude,
                        'longitude' => $r->longitude
                        )
                    );
                }
                $this->data['businesses'] = $businesses;
                $this->data['msg'] = $msg;
                break;

            case 'category':
                //search business by category name
                $c_qry = '(SELECT * from business where id in 
                            (select business_id from business_attribute where category_id in
                                (select id from category where category.name RLIKE "' . $svalue['category'] . '" ORDER BY id DESC)
                            )
                        )';

                //search business by tag name
                $t_qry = '(SELECT * from business where id in 
                            (select business_id from business_attribute where category_id in
                                (select id from category where category.tags RLIKE "' . $svalue['category'] . '" ORDER BY id DESC)
                            )
                        )';

                $meta_query = 'select * from ( ';
                $meta_query .= $c_qry;
                $meta_query .= " UNION ";
                $meta_query .= $t_qry;
                $meta_query .= ' ) as a';

                $search_result = $this->general_model->find_by_sql($meta_query);
                $count = count($search_result);
                $msg="Sorry! no results found for '".$svalue['category']."' category.";
                if($count){
                    $msg=$count." results found for '".$svalue['category']."' category.";
                }
                $businesses = array();
                foreach ($search_result as $key => $r) {
                    array_push($businesses, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'is_paid' => $r->is_paid,
                        'rating' => $ratings[$r->rating_id],
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesstime($r->id),
                        'categories' => $this->getbussinesscat($r->id),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => $r->phone,
                        'area' => (isset($areas[$r->area_id])) ? $areas[$r->area_id] : "",
                        'city' => (isset($cities[$r->city_id])) ? $cities[$r->city_id] : "",
                        'pincode' => $r->pincode,
                        'location' => $this->getbussinesloc($r->id),
                        'latitude' => $r->latitude,
                        'longitude' => $r->longitude
                        )
                    );
                }
                $this->data['businesses'] = $businesses;
                $this->data['msg'] = $msg;
                break;

            case 'featured':
                $f_qry = "SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND is_featured = 1";
                $search_result = $this->general_model->find_by_sql($f_qry);
                $count = count($search_result);
                $msg="Sorry!, no featured business found.";
                if($count){
                    $msg=$count." featured businesses found.";
                }
                $businesses = array();
                foreach ($search_result as $key => $r) {
                    array_push($businesses, array('image' => $r->image_path,
                        'business_id' => $r->id,
                        'title' => $r->title,
                        'page_url' => $r->page_url,
                        'is_verified' => $r->is_verified,
                        'is_paid' => $r->is_paid,
                        'rating' => $ratings[$r->rating_id],
                        'overall' => $r->roverall,
                        'review' => $r->treview,
                        'datetime' => $this->getbussinesstime($r->id),
                        'categories' => $this->getbussinesscat($r->id),
                        'email' => $r->email,
                        'website_url' => $r->website_url,
                        'phone' => $r->phone,
                        'area' => (isset($areas[$r->area_id])) ? $areas[$r->area_id] : "",
                        'city' => (isset($cities[$r->city_id])) ? $cities[$r->city_id] : "",
                        'pincode' => $r->pincode,
                        'location' => $this->getbussinesloc($r->id),
                        'latitude' => $r->latitude,
                        'longitude' => $r->longitude
                        )
                    );
                }
                $this->data['businesses'] = $businesses;
                $this->data['msg'] = $msg;
                break;
        }
    }

    function index() {
        $content = $this->load->view('search-map', $this->data, true);
        $this->render_inner($content);
    }

    function getbussinesstime($business_id) {
        $day_id = 0;
        $day_of_week = date("l");
        switch ($day_of_week) {
            case 'Monday' : $day_id = 0;
                break;
            case 'Tuesday' : $day_id = 1;
                break;
            case 'Wednesday' : $day_id = 2;
                break;
            case 'Thursday' : $day_id = 3;
                break;
            case 'Friday' : $day_id = 4;
                break;
            case 'Saturday' : $day_id = 5;
                break;
            case 'Sunday' : $day_id = 6;
                break;
            default : $day_id = 7;
        }

        $businesstime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $business_id . ' and  day_id = ' . $day_id);
        $result = "";
        if (!empty($businesstime)) {
            //echo 'Not empty';
            //echo $business_id;
            //print_r($businesstime);
            foreach ($businesstime as $key => $bt) {
                if ($bt->mor_status && $bt->eve_status) {
                    $result = "";
                    //echo 'Case 1';
                } elseif (!$bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 2';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } elseif (date('H:i:s') > $bt->eve_start_time && date('H:i:s') < $bt->eve_end_time) {
                        $result = "until " . date("g:i p", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                } elseif (!$bt->mor_status && $bt->eve_status) {
                    //echo 'Case 3';
                    if (date('H:i:s') > $bt->mor_start_time && date('H:i:s') < $bt->mor_end_time) {
                        $result = "until " . date("g:i a", strtotime($bt->mor_end_time));
                    } else {
                        $result = '';
                    }
                } elseif ($bt->mor_status && !$bt->eve_status) {
                    //echo 'Case 4';
                    $datetime = date('G:i:s'); //echo $datetime;echo '             ';echo strtotime($datetime);echo '         ';
                    //echo strtotime($bt->eve_start_time);echo '                         ';
                    //echo strtotime($bt->eve_end_time);
                    if (strtotime($datetime) > strtotime($bt->eve_start_time) && strtotime($datetime) < strtotime($bt->eve_end_time)) {
                        //echo 'in';
                        $result = "until " . date("g:i p", strtotime($bt->eve_end_time));
                    } else {
                        $result = '';
                    }
                }
            }
        }
        return $result;
    }

    function getbussinesscat($business_id) {
        $categories = $this->general_model->get_enum('category', 'id', 'name');
        $businesscat = $this->general_model->find_by_sql('SELECT * FROM business_attribute WHERE business_id = ' . $business_id . '  LIMIT 3');
        $result = '';
        if (!empty($businesscat)) {
            foreach ($businesscat as $key => $cat) {
                $result = $result . '<li>' . $categories[$cat->category_id] . '</li>';
            }
        }
        return $result;
    }

    function getbussinesloc($business_id) {
        $business = $this->general_model->get_enum('business', 'id', 'title');
        $businessloc = $this->general_model->find_by_sql('SELECT * FROM business WHERE title = "' . $business[$business_id] . '"');
        return count($businessloc);
    }

}

?>