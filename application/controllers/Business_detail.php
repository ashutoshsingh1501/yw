<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Business_detail extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index($b_name, $b_id) {

        if (preg_match("/[A-Z]/", $b_name) === 1) {
            echo "<pre>404";
        } else {
            $data['bbid'] = $b_id;
            $business_detail_rs = $this->general_model->find_by_sql('SELECT * FROM business WHERE id = ' . $data['bbid']);
            if (empty($business_detail_rs)) {
                echo "<pre>404";
            } else {

                $b_name_db = $business_detail_rs[0]->title;
                $b_name_db = str_replace(' ', '-', $b_name_db); // Replaces all spaces with hyphens.
                $b_name_db = preg_replace('/[^A-Za-z0-9\-]/', '', $b_name_db); // Removes special chars. /[&\/\\#,+()$~%.'":*?<>{}]/g
                $b_name_db = preg_replace('/-+/', '-', $b_name_db); // Replaces multiple hyphens with single one.
                $b_name_db = strtolower($b_name_db);
                
                if (strpos($b_name,$b_name_db) === false) {
                    echo "<pre>404";
                } else {
                    $this->load->helper('form');
                    $this->load->library('user_agent');
                    $data["mobile"] = $this->agent->is_mobile();
                    $business_detail_arr = array();
                    foreach ($business_detail_rs as $key => $r) {
                        array_push($business_detail_arr, array(
                            'image_path' => $r->image_path,
                            'is_logo' => $r->is_logo,
                            'image_note' => $r->image_note,
                            'is_featured' => $this->checkFeaturedImage($r->image_note),
                            'carousel_count' => $this->carouselThumbnailCount($r->image_note),
                            'id' => $r->id,
                            'title' => $r->title,
                            'categories_list' => $r->categories_list,
                            'is_verified' => $r->is_verified,
                            'rating_name' => $r->rating_name,
                            'roverall' => $r->roverall,
                            'treview' => $r->treview,
                            'address' => $r->address,
                            'area_name' => (isset($r->area_name)) ? ucfirst($r->area_name) : "",
                            'city_name' => (isset($r->city_name)) ? ucfirst($r->city_name) : "",
                            'pincode' => $r->pincode,
                            'phone' => $r->phone,
                            'alternative_number' => $r->alternative_number,
                            'landline' => $r->landline,
                            'secondary_land_line' => $r->secondary_land_line,
                            'email' => $r->email,
                            'website_url' => $r->website_url,
                            'latitude' => $r->latitude,
                            'longitude' => $r->longitude,
                            'description' => $r->description,
                            'est_year' => $r->est_year,
                            'landmark' => $r->landmark,
                            'facebook_url' => $r->facebook_url,
                            'twitter_url' => $r->twitter_url,
                            'video_url' => $r->video_url,
                            'google_url' => $r->google_url,
                            'intragram_url' => $r->intragram_url,
                            'pinterest_url' => $r->pinterest_url,
                            'services' => $r->services,
                            'brands' => $r->brands,
                            'amenities' => $r->amenities,
                            'payment_method' => $r->payment_method
                                )
                        );
                    }
                    $data['bbusiness'] = $business_detail_arr;
                    //        echo "<pre>";
                    //        print_r($data['bbusiness']);
                    //        exit;

                    if (isset($this->session->userdata["id"])) {
                        $data['front_user_logged'] = "yes";
                        $uid = $this->session->userdata["id"];
                        $bid = $data['bbusiness'][0]['id'];
                        $table = "visitor_business";
                        $cond = array(
                            "visitor_id" => $uid,
                            "business_id" => $bid
                        );
                        $favourite_records = $this->general_model->record_count_where($table, $cond);
                        if ($favourite_records == "0") {
                            $data['favclass'] = "favoriteBtn tooltip";
                        } else {
                            $data['favclass'] = "favoriteBtn tooltip active";
                        }
                    } else {
                        $data['favclass'] = "favoriteBtn tooltip";
                        $data['front_user_logged'] = "no";
                    }
                    //print_r($bbusiness);exit;
                    //Similiar businesses
                    //        $cbusiness = array();
                    //			$bcategory = $this->general_model->get_enum_where('business_attribute', 'category_id' , 'category_id' , array('business_id'=>$data['bbid']));
                    //			if(!empty($bcategory)){
                    //				$c = $this->general_model->get_enum_where('business_attribute' , 'business_id' ,  'business_id' , 'category_id in ('. implode(',' , $bcategory) .')');
                    //				if(!empty($c)){
                    //					$cbusiness = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND id IN ('. implode(',' , $c) . ')  and id != '.$data['bbid'].' order by count desc Limit 6');
                    //				}
                    //			}


                    $c = $data['bbusiness'][0]['categories_list'];
                    $c_ids = explode(',', $c);
                    $c_id = end($c_ids);
                    $cbusiness = $this->general_model->find_by_sql('SELECT * FROM business WHERE status = 1 AND is_deleted = 0 AND categories_list rlike "' . $c_id . '"  and id != ' . $data['bbid'] . ' order by pincode desc Limit 12');
                    $similar_businesses = array();
                    foreach ($cbusiness as $key => $r) {
                        array_push($similar_businesses, array(
                            'is_verified' => $r->is_verified,
                            'image' => $r->image_path,
                            'id' => $r->id,
                            'link' => $this->prepareBusinessLink($r->id, $r->title, $r->area_name, $r->city_name),
                            'title' => $r->title,
                            'area' => $r->area_name,
                            'roverall' => $r->roverall,
                            'treview' => $r->treview,
                            'is_logo' => $this->checkThumbnail($r->image_note),
                            'image_note' => $r->image_note
                                )
                        );
                    }
                    $data['bcategory'] = $similar_businesses;


                    $btime = $this->general_model->find_by_sql('SELECT * FROM business_time WHERE business_id = ' . $data['bbid']);
                    if (!empty($btime)) {
                        $data['btime'] = $this->getBusinessTime($btime);
                    }
                    $data['boffer'] = $this->general_model->find_by_sql('SELECT * FROM business_offer WHERE expire_on > "' . date('Y-m-d H:i:s') . '" and  business_id = ' . $data['bbid']);
                    $data['beoffer'] = $this->general_model->find_by_sql('SELECT * FROM business_offer WHERE expire_on < "' . date('Y-m-d H:i:s') . '" and  business_id = ' . $data['bbid']);
                    //			print_r($data['boffer']);exit;

                    $data['bservice'] = $data['bbusiness'][0]['services'];
                    $data['bbrand'] = $data['bbusiness'][0]['brands'];
                    $data['bamenties'] = $data['bbusiness'][0]['amenities'];
                    $data['bpayment'] = $data['bbusiness'][0]['payment_method'];

                    $data['breview'] = $this->general_model->find_by_sql('SELECT * FROM `business_review` WHERE business_id = ' . $data['bbid'] . '  LIMIT 3');
                    $data['wreview'] = $this->general_model->find_by_sql('SELECT * FROM `business_review` WHERE business_id = ' . $data['bbid']);
                    //        $data['total_reviews_count'] = $this->general_model->find_by_sql('SELECT COUNT(*) AS count FROM `business_review` WHERE business_id = ' . $data['bbid']);
                    $upvote_count = $this->general_model->find_by_sql('SELECT SUM(upvote) AS upvote_count FROM `business_review` WHERE business_id = ' . $data['bbid']);
                    $downvote_count = $this->general_model->find_by_sql('SELECT SUM(downvote) AS downvote_count FROM `business_review` WHERE business_id = ' . $data['bbid']);
                    foreach ($upvote_count as $key => $br) {
                        $upvote_sum = $br->upvote_count;
                    }
                    if ($upvote_sum == "1") {

                        $data['total_upvotes'] = $upvote_sum;

                        $data['upvotetitle'] = "Upvote";
                        foreach ($downvote_count as $key => $br) {
                            $downvote_sum = $br->downvote_count;
                        }
                        $total_count = $upvote_sum + $downvote_sum;
                        $data['avarage_count'] = $upvote_sum / $total_count * 100;
                    } else
                    if ($upvote_sum > "1") {
                        foreach ($upvote_count as $key => $br) {
                            $upvote_sum = $br->upvote_count;
                            $data['total_upvotes'] = $upvote_sum;
                        }
                        $data['upvotetitle'] = "Upvotes";
                        foreach ($downvote_count as $key => $br) {
                            $downvote_sum = $br->downvote_count;
                        }
                        $total_count = $upvote_sum + $downvote_sum;
                        $data['avarage_count'] = $upvote_sum / $total_count * 100;
                    } else {
                        $data['total_upvotes'] = "0";
                    }

                    $data['treview'] = $this->general_model->record_count_where('business_review', array('business_id' => $data['bbid']));
                    $data['rating1'] = $this->general_model->record_count_where('business_review', ' rating > 0 and rating <= 1 and business_id = ' . $data['bbid']);
                    $data['rating2'] = $this->general_model->record_count_where('business_review', ' rating > 1 and rating <= 2 and  business_id = ' . $data['bbid']);
                    $data['rating3'] = $this->general_model->record_count_where('business_review', ' rating > 2 and rating <= 3 and  business_id = ' . $data['bbid']);
                    $data['rating4'] = $this->general_model->record_count_where('business_review', ' rating >  3 and rating <= 4 and  business_id = ' . $data['bbid']);
                    $data['rating5'] = $this->general_model->record_count_where('business_review', ' rating > 4 and rating <= 5 and  business_id = ' . $data['bbid']);

                    $data['nusers'] = $this->general_model->get_enum('visitor_detail', 'id', 'name');
                    $data['iusers'] = $this->general_model->get_enum('visitor_detail', 'id', 'profile_pic');

                    //$data['bareas'] = $this->general_model->fetch_row_by_sql("select * from business b join area a on b.area_id=a.id where b.id=" . $data['bbid']);

                    $content = $this->load->view('business-detail', $data, true);
                    $this->render_user($content);
                }
            }
        }
    }

    function getBusinessTime($btime) {

        $b_time_arr = array();
        for ($i = 0; $i < 7; $i++) {
            $b_time_arr[$i]['day'] = $this->getDayOfWeek($i);
            $b_time_arr[$i]['time'] = 'closed'; //'Holiday';
            $b_time_arr[$i]['status'] = ''; //'closed';
        }

        for ($i = 0; $i < count($btime); $i++) {
            if (strcmp($this->getDayOfWeek($btime[$i]->day_id), date('l', time())) == 0) {
                $b_time_arr[$i]['day'] = "Today";
                $b_time_arr[$i]['status'] = $this->businessStatus($btime[$i]->day_id, $btime[$i]->mor_start_time, $btime[$i]->eve_end_time);
            } elseif (strcmp($this->getDayOfWeek($btime[$i]->day_id), date('l', strtotime("+1 day"))) == 0) {
                $b_time_arr[$i]['day'] = "Tomorrow";
            }
            if (!$btime[$i]->mor_status || !$btime[$i]->eve_status) {
                // change here for closed
                $b_time_arr[$i]['time'] = date('h:i a', strtotime($btime[$i]->mor_start_time)) . " - " . date('h:i a', strtotime($btime[$i]->eve_end_time));
                //$b_time_arr[$i]['status'] = '';
            }
        }
        return $this->rotate_days($b_time_arr);
    }

    function rotate_days($b_time_arr) {
        $day_arr = array();
        $today = intval(date('w', time()));

        $j = 0;
        $i = $today;
        do {
            $day_arr[$j++] = $b_time_arr[$i];
            $i = ($i + 1) % 7;
        } while ($i != $today);

        return $day_arr;
    }

    function getDayOfWeek($n) {
        switch ($n) {
            case 0:return "Sunday";
            case 1:return "Monday";
            case 2:return "Tuesday";
            case 3:return "Wednesday";
            case 4:return "Thursday";
            case 5:return "Friday";
            case 6:return "Saturday";
        }
    }

    function businessStatus($n, $start, $end) {
        if (strcmp($this->getDayOfWeek($n), date('l', time())) == 0) {
            $s = strtotime(date("Y-m-d", time()) . $start);
            $c = strtotime(date("Y-m-d", time()) . $end);
            if (time() > $s && time() < $c) {
                return "open";
            } else {
                return "closed";
            }
        } else {
            return "open";
        }
    }

    function send_mail($receiver_email, $subject, $message) {

        $this->load->library('email');
        $this->email->from(SENDER_MAIL, SENDER_NAME);
        $this->email->to($receiver_email);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
            return TRUE;
        } else {
            //echo $this->email->print_debugger();//DEBUG
            return FALSE;
        }
    }

    function suggestEdit() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', array('required', 'min_length[3]'));
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]+$/]');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('message', 'Message', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

        /*  if ($this->form_validation->run() == FALSE) {
          $this->index();
          } else { */

        $name = $this->input->post('name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $message = $this->input->post('message');

        $subject = "$name - $mobile want to suggest yellowpages.in";
        $mail = "<br><br>Hi,<br><br>$name, suggested an edit. Below is the user's information for further communications,<br><br>Name: $name<br>Mobile: $mobile<br>Email: $email<br>What they said: $message.";
        $ack_subject = "Your email regarding suggestion to yellowpages.in";
        $ack_message = "<br><br>Hi $name,<br><br>We have noted your suggestion and our team would get back to you shortly. Below is the informaiton you shared,<br><br>Name: $name<br>Mobile: $mobile<br>Email: $email<br>What they said: $message.<br><br>If you sent this by mistake or someone else used your email, please email to admin@yellowpages.in to report this issue.";

        //echo "SUCCESS: Suggest Edit<br/><br/>" . TEAM_MAIL . "<br/>" . $subject . "<br/>" . $mail . "<br/>" . $ack_subject . "<br/>" . $ack_message;exit;

        if ($this->send_mail(TEAM_MAIL, $subject, $mail)) {
            //redirect('welcome');
            if ($this->send_mail($email, $ack_subject, $ack_message)) {
                $this->session->set_flashdata("success", "Advertisement request mail sent successfully.");
                redirect('welcome');
            } else {
                $this->session->set_flashdata("error", "Error in sending Acknowledgement.");
                echo "Error in sending Acknowledgement.";
                exit;
                $this->index();
            }
        } else {
            $this->session->set_flashdata("error", "Error in sending advertise request.");
            echo "Error in sending mail";
            exit;
            $this->index();
        }
        //}
    }

    function report_business() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'Name', array('required', 'min_length[3]'));
        $this->form_validation->set_rules('user_contact', 'Mobile', 'required|regex_match[/^[0-9]+$/]');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('user_message', 'Message', 'required');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

        /*  if ($this->form_validation->run() == FALSE) {
          $this->index();
          } else { */

        $phoneIncorrect = $this->input->post('phoneIncorrect');
        $emailIncorrect = $this->input->post('emailIncorrect');
        $addressIncorrect = $this->input->post('addressIncorrect');
        $businessClosed = $this->input->post('businessClosed');
        $others = $this->input->post('others');
        $name = $this->input->post('user_name');
        $mobile = $this->input->post('user_contact');
        $email = $this->input->post('user_email');
        $message = $this->input->post('user_message');

        $subject = "$name - $mobile want to Report about a business on yellowpages.in";
        $mail = "<br><br>Hi,<br><br>$name, wants to report about a business on yellowpages.in. Below is the user's information for further communications,<br><br>Name: $name<br>Mobile: $mobile<br>Email: $email<br>What they said: $message.";
        $ack_subject = "Your email regarding advertisement with yellowpages.in";
        $ack_message = "<br><br>Hi $name,<br><br>We have noted your report for business and our team would get back to you shortly. Below is the informaiton you shared,<br><br>Name: $name<br>Mobile: $mobile<br>Email: $email<br>What they said: $message.<br><br>If you sent this by mistake or someone else used your email, please email to admin@yellowpages.in to report this issue.";

        //echo "SUCCESS: Report Business<br/><br/>" . TEAM_MAIL . "<br/>" . $subject . "<br/>" . $mail . "<br/>" . $ack_subject . "<br/>" . $ack_message;exit;

        if ($this->send_mail(TEAM_MAIL, $subject, $mail)) {
            //redirect('welcome');
            if ($this->send_mail($email, $ack_subject, $ack_message)) {
                $this->session->set_flashdata("success", "Advertisement request mail sent successfully.");
                redirect('welcome');
            } else {
                $this->session->set_flashdata("error", "Error in sending Acknowledgement.");
                echo "Error in sending Acknowledgement.";
                exit;
                $this->index();
            }
        } else {
            $this->session->set_flashdata("error", "Error in sending advertise request.");
            echo "Error in sending mail";
            exit;
            $this->index();
        }
        //}
    }

    function mailCoupon() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

        $user_email = $this->input->post('user_email');
        $offer_id = $this->input->post('offer_id');
        $offer_sql = "select * from business_offer where id=$offer_id";
        $offers = $this->general_model->fetch_row_by_sql($offer_sql);
        $offer_url = $offers['offer_url'];
        $subject = "Coupon code from yellowpages.in";
        $mail = "<br><br>Hi,<br><br>Please find the requested Coupon code: $offer_url"; //echo $mail;exit;

        if ($this->send_mail($user_email, $subject, $mail)) {
            $this->session->set_flashdata("success", "Coupon code sent successfully.");
            redirect('welcome');
        } else {
            $this->session->set_flashdata("error", "Error in sending advertise request.");
            echo "Error in sending mail";
            exit;
            $this->index();
        }
    }

    public function getreviews_list($bid) {

        if ($_POST['page']) {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = PERPAGE_MYREVIEWS;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            if (isset($this->session->userdata["business_rate_order"])) {
                $ratethis = $this->session->userdata["business_rate_order"];
            } else {
                $ratethis = "";
            }
            $qry = "SELECT * FROM `business_review` WHERE business_id = " . $bid . " ORDER BY rating='$ratethis' DESC LIMIT $start, $per_page";
            $search_result = $this->general_model->find_by_sql($qry);
            $qry2 = "SELECT COUNT(*) AS count FROM `business_review` WHERE business_id = " . $bid;
            $total_favorite_count = $this->general_model->find_by_sql($qry2);

            $nusers = $this->general_model->get_enum('visitor_detail', 'id', 'name');
            $iusers = $this->general_model->get_enum('visitor_detail', 'id', 'profile_pic');
            foreach ($total_favorite_count as $key => $br) {
                $count = $br->count;
            }
            //print_r($search_result);
            $businesses = array();


            $msg = "";
            if ($count > 0) {

                foreach ($search_result as $key => $r) {

                    if (isset($iusers[$r->user_id])) {
                        $photo = STYLEURL . "front/images/" . $iusers[$r->user_id];
                    } else {
                        $photo = STYLEURL . 'front/images/nophoto.jpg';
                    }

                    $msg .= ' <li>
                                    <div class="eachBusinessReview">
                                        <div class="photoVotesBlock">
                                            <div class="reviewerPhoto">
                                               <img src="' . $photo . '" alt="">
                                                                        
                                            </div>
                                            <div class="reviewVoting" id="review_responce' . $r->id . '">
                                                <span class="upVoting" onclick="myvote(' . $r->id . ',1)"></span>
                                                <span class="reviewVotingNumb">
                                                    <div>' . $r->upvote . '</div></span>
                                                <span class="downVoting" onclick="myvote(' . $r->id . ',-1)"></span>
                                                <span class="reviewVotingNumb" id="down">
                                                    <div></div></span>

                                        </div>
                                        </div>

                                        <div class="reviewTextBlock">
                                            <a href="#" class="reviewTitle">' . $nusers[$r->user_id] . '</a>
                                            <div class="ratingBlock">
                                                <span class="rating r' . str_replace(".", "-", $r->rating) . '">$r->rating</span>
                                                <span class="ratingTimeDate">' . date('Y-m-d H:i', strtotime($r->added_on)) . '</span>
                                            </div>
                                            <p>' . $r->comments . '</p>
                                        </div>
                                        </div>
                                </li>';
                }
                $msg = "<div class='data'><ul class='reviewsList'>" . $msg . "</ul></div>"; // Content for Data


                /* --------------------------------------------- */


                $no_of_paginations = ceil($count / $per_page);

                /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
                if ($cur_page >= 7) {
                    $start_loop = $cur_page - 3;
                    if ($no_of_paginations > $cur_page + 3)
                        $end_loop = $cur_page + 3;
                    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                        $start_loop = $no_of_paginations - 6;
                        $end_loop = $no_of_paginations;
                    } else {
                        $end_loop = $no_of_paginations;
                    }
                } else {
                    $start_loop = 1;
                    if ($no_of_paginations > 7)
                        $end_loop = 7;
                    else
                        $end_loop = $no_of_paginations;
                }
                /* ----------------------------------------------------------------------------------------------------------- */
                $msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
                if ($first_btn && $cur_page > 1) {
                    $msg .= "<li p='1' class='active cl'><a href='#container_business_reviews'>First</a></li>";
                } else if ($first_btn) {
                    $msg .= "<li p='1' class='inactive cl'><a href='#container_business_reviews'>First</a></li>";
                }

// FOR ENABLING THE PREVIOUS BUTTON
                if ($previous_btn && $cur_page > 1) {
                    $pre = $cur_page - 1;
                    $msg .= "<li p='$pre' class='active cl'><a href='#container_business_reviews'>Previous</a></li>";
                } else if ($previous_btn) {
                    $msg .= "<li class='inactive cl'><a href='#container_business_reviews'>Previous</a></li>";
                }
                for ($i = $start_loop; $i <= $end_loop; $i++) {

                    if ($cur_page == $i)
                        $msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active cl'><a href='#container_business_reviews'>{$i}</a></li>";
                    else
                        $msg .= "<li p='$i' class='active cl'><a href='#container_business_reviews'>{$i}</a></li>";
                }

// TO ENABLE THE NEXT BUTTON
                if ($next_btn && $cur_page < $no_of_paginations) {
                    $nex = $cur_page + 1;
                    $msg .= "<li p='$nex' class='active cl'><a href='#container_business_reviews'>Next</a></li>";
                } else if ($next_btn) {
                    $msg .= "<li class='inactive cl'><a href='#container_business_reviews'>Next</a></li>";
                }

// TO ENABLE THE END BUTTON
                if ($last_btn && $cur_page < $no_of_paginations) {
                    $msg .= "<li p='$no_of_paginations' class='active cl'><a href='#container_business_reviews'>Last</a></li>";
                } else if ($last_btn) {
                    $msg .= "<li p='$no_of_paginations' class='inactive cl'><a href='#container_business_reviews'>Last</a></li>";
                }
                $goto = "<input type='text' class='goto cl' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
                $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
                $msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
            } else {
                $msg = "<div class='data'>No Reviews Posted For Any Business</div>";
            }

            echo $msg;
        }
    }

    public function getoffers_list($bid) {

        if ($_POST['page']) {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = PERPAGE_MYREVIEWS;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            $boffer = $this->general_model->find_by_sql('SELECT * FROM business_offer WHERE expire_on > "' . date('Y-m-d H:i:s') . '" and  business_id = ' . $_GET['id'] . ' LIMIT $start, $per_page');

            $qry = "SELECT * FROM `business_review` WHERE business_id = " . $bid . " LIMIT $start, $per_page";
            $search_result = $this->general_model->find_by_sql($qry);
            $qry2 = "SELECT COUNT(*) AS count FROM `business_review` WHERE business_id = " . $bid;
            $total_favorite_count = $this->general_model->find_by_sql($qry2);

            $nusers = $this->general_model->get_enum('visitor_detail', 'id', 'name');
            $iusers = $this->general_model->get_enum('visitor_detail', 'id', 'profile_pic');
            foreach ($total_favorite_count as $key => $br) {
                $count = $br->count;
            }
            //print_r($search_result);
            $businesses = array();


            $msg = "";
            if ($count > 0) {
                $m = 100;

                foreach ($search_result as $key => $r) {

                    if (isset($iusers[$r->user_id])) {
                        $photo = STYLEURL . "front/images/" . $iusers[$r->user_id];
                    } else {
                        $photo = STYLEURL . 'front/images/nophoto.jpg';
                    }

                    $msg .= ' <li>
                                    <div class="eachBusinessReview">
                                        <div class="photoVotesBlock">
                                            <div class="reviewerPhoto">
                                               <img src="' . $photo . '" alt="">
                                                <input type="hidden" id="review_id' . $m . '" value="' . $r->id . '">   
                                                <input type="hidden" id="upvote_id' . $m . '" value="1">   
                                                <input type="hidden" id="downvote_id' . $m . '" value="-1"> 
                                                <input type="hidden" id="id$m" value="' . $m . '">                             
                                            </div>
                                            <div class="reviewVoting" id="review_responce' . $m . '">
                                                <span class="upVoting" id="upclick' . $m . '" onclick="myFunction(' . $r->id . ',1,' . $m . ')"></span>
                                                <span class="reviewVotingNumb">
                                                    <div>' . $r->upvote . '</div></span>
                                                <span class="downVoting" id="downclick' . $m . '" onclick="myFunction(' . $r->id . ',-1,' . $m . ')"></span>
                                                <span class="reviewVotingNumb" id="down">
                                                    <div></div></span>

                                        </div>
                                        </div>

                                        <div class="reviewTextBlock">
                                            <a href="#" class="reviewTitle">' . $nusers[$r->user_id] . '</a>
                                            <div class="ratingBlock">
                                                <span class="rating r' . str_replace(".", "-", $r->rating) . '">$r->rating</span>
                                                <span class="ratingTimeDate">' . date('Y-m-d H:i', strtotime($r->added_on)) . '</span>
                                            </div>
                                            <p>' . $r->comments . '</p>
                                        </div>
                                        </div>
                                </li>';
                    $m++;
                }
                $msg = "<div class='data'><ul class='reviewsList'>" . $msg . "</ul></div>"; // Content for Data


                /* --------------------------------------------- */


                $no_of_paginations = ceil($count / $per_page);

                /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
                if ($cur_page >= 7) {
                    $start_loop = $cur_page - 3;
                    if ($no_of_paginations > $cur_page + 3)
                        $end_loop = $cur_page + 3;
                    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                        $start_loop = $no_of_paginations - 6;
                        $end_loop = $no_of_paginations;
                    } else {
                        $end_loop = $no_of_paginations;
                    }
                } else {
                    $start_loop = 1;
                    if ($no_of_paginations > 7)
                        $end_loop = 7;
                    else
                        $end_loop = $no_of_paginations;
                }
                /* ----------------------------------------------------------------------------------------------------------- */
                $msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
                if ($first_btn && $cur_page > 1) {
                    $msg .= "<li p='1' class='active cl'><a href='#container_business_reviews'>First</a></li>";
                } else if ($first_btn) {
                    $msg .= "<li p='1' class='inactive cl'><a href='#container_business_reviews'>First</a></li>";
                }

// FOR ENABLING THE PREVIOUS BUTTON
                if ($previous_btn && $cur_page > 1) {
                    $pre = $cur_page - 1;
                    $msg .= "<li p='$pre' class='active cl'><a href='#container_business_reviews'>Previous</a></li>";
                } else if ($previous_btn) {
                    $msg .= "<li class='inactive cl'><a href='#container_business_reviews'>Previous</a></li>";
                }
                for ($i = $start_loop; $i <= $end_loop; $i++) {

                    if ($cur_page == $i)
                        $msg .= "<li p='$i' style='color:#fff;background-color:#006699;' class='active cl'><a href='#container_business_reviews'>{$i}</a></li>";
                    else
                        $msg .= "<li p='$i' class='active cl'><a href='#container_business_reviews'>{$i}</a></li>";
                }

// TO ENABLE THE NEXT BUTTON
                if ($next_btn && $cur_page < $no_of_paginations) {
                    $nex = $cur_page + 1;
                    $msg .= "<li p='$nex' class='active cl'><a href='#container_business_reviews'>Next</a></li>";
                } else if ($next_btn) {
                    $msg .= "<li class='inactive cl'><a href='#container_business_reviews'>Next</a></li>";
                }

// TO ENABLE THE END BUTTON
                if ($last_btn && $cur_page < $no_of_paginations) {
                    $msg .= "<li p='$no_of_paginations' class='active cl'><a href='#container_business_reviews'>Last</a></li>";
                } else if ($last_btn) {
                    $msg .= "<li p='$no_of_paginations' class='inactive cl'><a href='#container_business_reviews'>Last</a></li>";
                }
                $goto = "<input type='text' class='goto cl' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
                $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
                $msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
            } else {
                $msg = "<div class='data'>No Reviews Posted For Any Business</div>";
            }

            echo $msg;
        }
    }

    function sendSms() {
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_mobile', 'Mobile', 'required|regex_match[/^[0-9]+$/]');
        $this->form_validation->set_error_delimiters('<div class="error" style="color:red;">', '</div>');

        $user_mobile = $this->input->post('user_mobile');
        $business_id = $this->input->post('business_id');
        $sms_sql = "select * from business where id=$business_id";
        $bdata = $this->general_model->fetch_row_by_sql($sms_sql);
        $bname = $bdata['title'];
        $baddress = $bdata['address'];
        $bphone = $bdata['phone'];

        /* $username = 'youremail@address.com';
          $hash = 'Your API hash'; */
        $apiKey = urlencode('JyiGQa9SZ+4-ppyR9ZsbpIfdzT8bc4GeNoP9DjxSrA');

        // Message details
        $sender = urlencode('TXTLCL');
        $msg = "$bname,\n $baddress,\n $bphone";
        $message = rawurlencode($msg);

        // Prepare data for POST request
        $data = 'apiKey=' . $apiKey . '&numbers=' . $user_mobile . "&sender=" . $sender . "&message=" . $message;

        // Send the GET request with cURL
        $ch = curl_init('http://api.textlocal.in/send/?' . $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        // Process your response here
        //echo $response;
        redirect(base_url() . 'business_detail?id=' . $business_id);
    }

    function checkThumbnail($id) {
        $file = '/var/www/yellowpages.in/static/business-images/' . $id . '/190x125_' . $id . '-1.jpg';
        return file_exists($file);
    }

    function checkFeaturedImage($id) {
        $file = '/var/www/yellowpages.in/static/business-images/' . $id . '/1170x480_' . $id . '-1.jpg';
        return file_exists($file);
    }

    function carouselThumbnailCount($id) {
        $count = 0;
        $dir = '/var/www/yellowpages.in/static/business-images/' . $id . '/';
        if (file_exists($dir)) {
            $files = scandir($dir);
            $thumbnail_pattern = '190x125_' . $id . '-';
            for ($i = 2; $i < count($files); $i++) {
                if (strstr($files[$i], $thumbnail_pattern)) {
                    $count++;
                    //preg_match('/190x125_.*-(.*)\.jpg/', $files[$i], $matches);
                    //print_r($matches);
                }
            }
        }
        return $count;
    }

    function businessByNameID($name, $id) {
        $this->index($name, $id);
    }

    function businessByCityNameID($cityname, $name, $id) {
        $this->index($id);
    }

    function businessByCityAreaNameID($cityname, $areaname, $name, $id) {
        $this->index($id);
    }

    function prepareBusinessLink($id, $name,$area,$city) {
        $name = $name."-".$area."-".$city;
        $name = str_replace(' ', '-', $name);
        $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);
        $name = preg_replace('/-+/', '-', $name);
        return '/b/' . strtolower($name) . '/' . $id;
    }

}

?>