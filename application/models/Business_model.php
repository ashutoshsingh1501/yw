<?php

class Business_model extends CI_Model {

    function insert_business($data) {
        $q = $this->db->insert('add_business', $data);
        if ($q) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    function update_business($table, $column, $value, $condition_key, $condition_value) {
        $data = array(
            $column => $value
        );
        $this->db->where($condition_key, $condition_value);
        if( $this->db->update($table, $data)){
            return true;
        }
        return false;
    }

}
