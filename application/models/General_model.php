<?php

class General_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function save($table, $data) {
        if ($this->db->insert($table, $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    function get_enum_value($table, $field) {
        $type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        /* foreach($enum as $value){
          $data[$value] = $value;
          } */
        return $enum;
    }

    function record_count($table) {
        return $this->db->count_all($table);
    }

    function record_count_where($table, $condition) {
        $this->db->where($condition);
        return $this->db->count_all_results($table);
    }

    function fetch_row($table, $id) {
        $query = $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    function fetch_all_row_where($table, $limit, $start, $condition) {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->where($condition);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function fetch_all_where($table, $condition) {
        $this->db->order_by("full_name");
        $query = $this->db->where($condition);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $result[$row->id] = $row->full_name;
            }
            return $result;
        }
        return false;
    }

    function fetch_all_row($table, $limit, $start) {
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    function delete_row($table, $id) {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        }
        return false;
    }

    function delete_favourite_row($table, $visitor_id, $business_id) {
        $this->db->where('visitor_id', $visitor_id);
        $this->db->where('business_id', $business_id);
        if ($this->db->delete($table)) {
            return true;
        }
        return false;
    }

    function delete_row_where($table, $id, $field) {
        $this->db->where($field, $id);
        if ($this->db->delete($table)) {
            return true;
        }
        return false;
    }

    function get_enum($table, $column_key, $column_value) {
        $result = array();
        $query = $this->db->get($table);
        foreach ($query->result() as $row) {
            $result[$row->$column_key] = $row->$column_value;
        }
        return $result;
    }

    function get_enum_where($table, $column_key, $column_value, $condition) {
        $result = array();
        $query = $this->db->where($condition);
        $query = $this->db->get($table);
        foreach ($query->result() as $row) {
            $result[$row->$column_key] = $row->$column_value;
        }
        return $result;
    }

    function update_row($table, $data, $id) {
        $this->db->where('id', $id);
        if ($this->db->update($table, $data)) {
            return true;
        }
        return false;
    }

    function create_row($table, $data) {
        if ($this->db->insert($table, $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    function find_by_sql($sql) {
        $query = $this->db->query($sql);

        return $query->result();
    }

    function raw_sql($sql) {
        $query = $this->db->query($sql);

        return true;
    }

    function raw_insert_sql($sql) {
        $query = $this->db->query($sql);
        return $this->db->insert_id();
        //return true;
    }

    function fetch_row_by_key($table, $keyname, $key) {
        $query = $this->db->where($keyname, $key);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    function fetch_all($table) {

        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {

            return $query->result();
        }
        return false;
    }

    function login($username, $password) {
        $this->db->select('id, username, password');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function fetch_row_by_sql($sql) {
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    function update_query($table, $data, $vid, $review_id, $vote) {
        $this->db->where('user_id', $vid);
        $this->db->where('review_id', $review_id);
        $this->db->where('vote', $vote);
        if ($this->db->update($table, $data)) {
            return true;
        }
        return false;
    }

    function update_review_map($table, $data, $vid, $id) {
        $this->db->where('review_id', $id);
        $this->db->where('user_id', $vid);
        if ($this->db->update($table, $data)) {
            return true;
        }
        return false;
    }
    
    public function featured() {
        $qry_res = $this->db->query("CALL sp_featured()");
        $res = $qry_res->result_array();

        $qry_res->next_result();
        $qry_res->free_result();

        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
    }
    
    public function categories() {
        $qry_res = $this->db->query("CALL sp_categories()");
        $res = $qry_res->result_array();
        $qry_res->next_result();
        $qry_res->free_result();
        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
    }
    
    public function popular() {
        $qry_res = $this->db->query("CALL sp_popular()");
        $res = $qry_res->result();
        $qry_res->next_result();
        $qry_res->free_result();
        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
    }
    
    public function recently() {
        $qry_res = $this->db->query("CALL sp_recently()");
        $res = $qry_res->result();
        $qry_res->next_result();
        $qry_res->free_result();
        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
    }
    
    public function reviews() {
        $qry_res = $this->db->query("CALL sp_reviews()");
        $res = $qry_res->result();
        $qry_res->next_result();
        $qry_res->free_result();
        if (count($res) > 0) {
              return $res;
        } else {
              return 0;
        }
    }
}
?>