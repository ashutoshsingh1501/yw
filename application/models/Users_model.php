<?php
class Users_model extends CI_Model{
	
	function __construct() {
		parent::__construct();
	}
	
	function login($username, $password){
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('username', $username);
		$this -> db -> where('password', MD5($password));
		$this -> db -> limit(1);
		
		$query = $this -> db -> get();
		
		//print_r($result);
		//exit;
		
		if($query -> num_rows() == 1){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	
	function subscribers_login($username, $password){
		$this -> db -> select('id, email','first_name','last_name');
		$this -> db -> from('subscribers');
		$this -> db -> where('email', $username);
		$this -> db -> where('password', MD5($password));
		$this -> db -> limit(1);
		
		$query = $this -> db -> get();
		
		//print_r($result);
		//exit;
		
		if($query -> num_rows() == 1){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	
	function subscribers_details($id){
		
		$this -> db -> from('subscribers');
		$this -> db -> where('id', $id);
		
		$this -> db -> limit(1);
		
		$query = $this -> db -> get();
		
		//print_r($result);
		//exit;
		
		if($query -> num_rows() == 1){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	
	
	function fetch_row_by_key($table,$keyname,$key){
		$query = $this->db->where($keyname,$key);
		$query = $this->db->get($table);
		if($query->num_rows() > 0){
			return $query->row();
		}
		return false;
	}
	
	function get_enum( $table, $column_key, $column_value ){
		$result = array();
		$query = $this->db->get($table);
		foreach ($query->result() as $row){
			$result[$row->$column_key] = $row->$column_value;
		}
		return $result;
	}
	
	function fetch_all_where($table , $condition){
	
		$query = $this->db->where( $condition );
		$query = $this->db->get($table);
		if($query->num_rows() > 0){
			
			return $query->result();
		}
		return false;
	}

}
?>