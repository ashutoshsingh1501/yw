<?php

class Categories_model extends CI_Model {

    function get_categories() {

        $this->db->select('*');
//        $this->db->where('parent_id', 0);
//        $this->db->like('name', 'f', 'after');
        $this->db->order_by("name", "asc");
        $query = $this->db->get('category');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    
    function get_categories_by_letter($v){
        
        $this->db->select('*');
//        $this->db->where('parent_id', 0);
        $this->db->like('name', $v, 'after');
        $this->db->order_by("name", "asc");
        $query = $this->db->get('category');

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

}
