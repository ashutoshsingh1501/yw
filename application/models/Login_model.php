<?php

class Login_model extends CI_Model {

    public function can_log_in() {

        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $query = $this->db->get('visitor_account');
        $active = "";

        foreach ($query->result() as $ro) {
            $active = $ro->active;
        }

        if ($query->num_rows() == 1 && $active == '1') {
            $msg = true;
            return $msg;
        } else if ($query->num_rows() == 1 && $active !== '1') {
            $msg = "blocked";
            return $msg;
        } else {
            $msg = false;
            return $msg;
        }
    }

    public function check_exists() {
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('old_password')));
        $query = $this->db->get('visitor_account');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function check_username() {
        $this->db->where('username', $this->input->post('email'));
        $query = $this->db->get('visitor_account');
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function add_account($data){
        $q = $this->db->insert('visitor_account', $data);
        if ($q) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    
    public function add_account_detail($data){
        $q = $this->db->insert('visitor_detail', $data);
        if ($q) {
            return TRUE;
        } else {
            return false;
        }
    }

    public function change_password() {

        $this->db->select('*');
        $this->db->where('username', $this->session->userdata('username'));
//        $this->db->where('type', $this->session->userdata('type'));
        $this->db->where('password', md5($this->input->post('old_password')));
        $query = $this->db->get('visitor_account');
//        print_r($query->result());exit;

        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($row->username == $this->session->userdata('username')) {
                $data = array(
                    'password' => md5($this->input->post('password'))
                );

                $this->db->where('username', $this->session->userdata('username'));
                $this->db->where('password', md5($this->input->post('old_password')));
                $query = $this->db->update('visitor_account', $data);

                return $query;
            }
        }
    }
      public function forgot_to_change_password() {

        $this->db->select('*');
        $this->db->where('username', $this->session->userdata('change_pass'));
//        $this->db->where('type', $this->session->userdata('type'));
        
        $query = $this->db->get('visitor_account');
//        print_r($query->result());exit;

        if ($query->num_rows() > 0) {  
            $row = $query->row();
            if ($row->username == $this->session->userdata('change_pass')) {
                $data = array(
                    'password' => md5($this->input->post('password'))
                );

                $this->db->where('username', $this->session->userdata('change_pass'));
                $query = $this->db->update('visitor_account', $data);

                return $query;
            }
        }
    }

    function get_login_detail($user, $pass) {
        
        $this->db->select('*');
        $array = array('username' => $user, 'password' => $pass);
        $this->db->where($array);
        $query = $this->db->get('visitor_account');
        return $query->result();
    }

}

?>