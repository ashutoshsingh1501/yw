<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;
	protected $layout = 'layout/fixed'; //UpdatedBy Harshi For Layout :: render
        protected $layout_login = 'layout/fixed_login'; //UpdatedBy Harshi For Layout :: render_login
	protected $layout_business = 'layout/fixed_business'; //UpdatedBy Harshi For Layout :: render_business
        
        protected $layout_front = 'layout/fixed_website'; //UpdatedBy Harshi For Layout :: render_front [homepage]
	protected $layout_inner = 'layout/fixed_inner'; //UpdatedBy Harshi For Layout :: render_inner [search,map]
	protected $layout_user = 'layout/fixed_user'; //UpdatedBy Harshi For Layout :: render_user [static pages]
	

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
	}

	public static function &get_instance()
	{
		return self::$instance;
	}
	
  
	protected function render($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout,$view_data);
	}
	
	protected function render_front($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout_front,$view_data);
	}
	
	protected function render_inner($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout_inner,$view_data);
	}
	
	protected function render_user($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout_user,$view_data);
	}
	
	protected function render_business($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout_business,$view_data);
	}
	protected function render_login($content) {
		$view_data = array(
			'content' => $content
		);
		$this->load->view($this->layout_login,$view_data);
	}

}
